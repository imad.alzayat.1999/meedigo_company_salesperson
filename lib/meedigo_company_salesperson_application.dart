import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:meedigo_company_sales_person/core/di/dependency_injection.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_strings.dart';
import 'package:meedigo_company_sales_person/core/theming/app_theme.dart';
import 'package:meedigo_company_sales_person/core/utils/global_variables.dart';
import 'package:meedigo_company_sales_person/features/no_internet/presentation/bloc/internet_bloc.dart';

class MeedigoCompanySalesPersonApplication extends StatelessWidget {
  const MeedigoCompanySalesPersonApplication({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => getIt<InternetBloc>(),
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        title: AppStrings.applicationName,
        navigatorKey: GlobalVariable.navState,
        theme: AppTheme.mainApplicationTheme(context),
      ),
    );
  }
}
