import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_sizes.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_sizes.dart';
import '../../../../core/helpers/app_assets.dart';
import '../../../../core/theming/app_colors.dart';
import '../../../../core/theming/app_fonts.dart';
import '../../../../core/theming/app_styles.dart';
import 'cart_card_widget.dart';

class WarehouseExpansionTileWidget extends StatefulWidget {
  @override
  State<WarehouseExpansionTileWidget> createState() =>
      _WarehouseExpansionTileWidgetState();
}

class _WarehouseExpansionTileWidgetState
    extends State<WarehouseExpansionTileWidget> {
  bool _isExpanded = false;
  bool _isInitExpanded = true;

  @override
  Widget build(BuildContext context) {
    return ExpansionTile(
      onExpansionChanged: (value) {
        setState(() {
          _isExpanded = value;
        });
      },
      initiallyExpanded: _isInitExpanded,
      tilePadding: EdgeInsets.zero,
      trailing: ExpansionTileTrailing(
        isExpanded: _isExpanded,
        warehouseName: "Company name",
      ),
      title: ExpansionTileTitle(
        warehouseName: "Company name",
      ),
      children: [
        ListView.separated(
          shrinkWrap: true,
          physics: const NeverScrollableScrollPhysics(),
          itemBuilder: (context, index) => Padding(
            padding: EdgeInsets.symmetric(
              vertical: getHeight(15),
            ),
            child: CartCardWidget(),
          ),
          separatorBuilder: (context, index) => Divider(),
          itemCount: 10,
        ),
      ],
    );
  }
}

class ExpansionTileTrailing extends StatelessWidget {
  final bool isExpanded;
  final String warehouseName;
  const ExpansionTileTrailing({
    super.key,
    required this.isExpanded,
    required this.warehouseName,
  });

  @override
  Widget build(BuildContext context) {
    return FittedBox(
      fit: BoxFit.fill,
      child: Row(
        children: [
          PopupMenuButton(
            itemBuilder: (BuildContext context) => [
              PopupMenuItem(
                onTap: () {},
                child: Row(
                  children: [
                    SvgPicture.asset(AppIcons.noteIcon),
                    horizontalSpacing(5),
                    Text(
                      "Order note",
                      style: AppStyles.medium(
                        fontSize: 12,
                        context: context,
                      ),
                    ),
                  ],
                ),
              ),
            ],
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(
                getRadius(8),
              ),
            ),
            child: SvgPicture.asset(
              AppIcons.optionsIcon,
              color: AppColors.kPrimaryText,
            ),
          ),
          horizontalSpacing(12),
          SizedBox(
            width: getRadius(15),
            height: getRadius(15),
            child: SvgPicture.asset(
              isExpanded ? AppIcons.arrowUpIcon : AppIcons.arrowDownIcon,
              color: AppColors.kPrimaryText,
            ),
          ),
        ],
      ),
    );
  }
}

class ExpansionTileTitle extends StatelessWidget {
  final String warehouseName;
  const ExpansionTileTitle({
    super.key,
    required this.warehouseName,
  });

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        SizedBox(
          height: getRadius(22),
          width: getRadius(22),
          child: SvgPicture.asset(
            AppIcons.medicineIcon,
            color: AppColors.kPrimaryText,
          ),
        ),
        horizontalSpacing(12),
        Text(
          warehouseName,
          style: AppStyles.bold(
            fontSize: 16,
            context: context,
          ),
        ),
      ],
    );
  }
}
