import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_extensions.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_strings.dart';
import '../../../../core/helpers/app_sizes.dart';
import '../../../../core/helpers/app_assets.dart';
import '../../../../core/theming/app_colors.dart';
import '../../../../core/theming/app_styles.dart';
import 'out_of_stock_warning_widget.dart';

class CartCardWidget extends StatefulWidget {
  @override
  State<CartCardWidget> createState() => _CartCardWidgetState();
}

class _CartCardWidgetState extends State<CartCardWidget> {
  num _offerWhenEnabled = 0;
  num _offerValue = 0;
  String _offerType = "";

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    // _init();
  }

  // /// init
  // _init() {
  //   context.read<CartCubit>().getIndex(
  //         groupedByProducts: widget.groupedByProducts,
  //         productName: widget.cart.productName,
  //       );
  //   _assignValues();
  //   context.read<CartCubit>().calculateTotalForCartItem(
  //         cart: widget.cart,
  //         offerType: _offerType,
  //         offerValue: _offerValue,
  //         offerWhenEnabled: _offerWhenEnabled,
  //       );
  // }

  // /// assign values
  // _assignValues() {
  //   for (int i = 0; i < widget.cart.offers.length; i++) {
  //     if (AppFunctions.checkIfOfferIsEnabled(
  //       index: i,
  //       quantity: widget.cart.quantity,
  //       offers: widget.cart.offers,
  //     )) {
  //       _mapValuesToOfferValues(
  //         offer: widget.cart.offers[i],
  //       );
  //     }
  //   }
  // }

  // /// map values to offer values
  // _mapValuesToOfferValues({required Offer offer}) {
  //   _offerType = offer.type;
  //   _offerValue = offer.value;
  //   _offerWhenEnabled = offer.whenEnabled;
  // }

  /// increase the counter
  _increaseCounter() {
    // AppFunctions.vibrate();
    // if (widget.cart.maxQuantity == 0) {
    //   setState(() {
    //     widget.cart.quantity++;
    //   });
    //   _updateProduct();
    // } else {
    //   if (widget.cart.quantity >= widget.cart.maxQuantity) {
    //     AppFunctions.showWarningMessage(
    //       context: context,
    //       message: AppStrings.maxQuantityWarningString,
    //     );
    //   } else if (widget.cart.quantity >= widget.cart.productQuantity) {
    //     AppFunctions.showWarningMessage(
    //       context: context,
    //       message: AppStrings.warningQuantityRequiredString,
    //     );
    //   } else {
    //     setState(() {
    //       widget.cart.quantity++;
    //     });
    //     _updateProduct();
    //   }
    // }
  }

  /// decrease the counter
  _decreaseCounter() {
    // AppFunctions.vibrate();
    // if (widget.cart.quantity <= widget.cart.minQuantity) {
    //   context.read<CartCubit>().showConfirmDialogFunction(
    //         context: context,
    //         cart: widget.cart,
    //         groupedByProducts: widget.groupedByProducts,
    //       );
    // } else {
    //   setState(() {
    //     widget.cart.quantity--;
    //   });
    //   _updateProduct();
    // }
  }

  _updateProduct() {
    // context.read<CartCubit>().updateProductFromTheCartCard(
    //       cart: widget.cart,
    //       index: widget.index,
    //       offerWhenEnabled: _offerWhenEnabled,
    //       offerValue: _offerValue,
    //       offerType: _offerType,
    //       groupedByProducts: widget.groupedByProducts,
    //     );
  }

  Widget _buildSelectedOfferWidget() {
    return Padding(
      padding: EdgeInsets.only(top: getHeight(6)),
      child: ListView.separated(
        shrinkWrap: true,
        itemBuilder: (context, index) {
          return Text(
            context.translate(
              AppStrings.fixedOfferString,
              args: {
                "required_quantity": "10",
                "offer_quantity": "5",
              },
            ),
            style: AppStyles.semiBold(
              fontSize: 12,
              textColor: AppColors.kDarkPrimary,
              context: context,
              height: 1.5,
            ),
          );
        },
        separatorBuilder: (context, index) => SizedBox.shrink(),
        itemCount: 10,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        OutOfStockWarningWidget(),
        Container(
          child: Row(
            children: [
              Expanded(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    HeaderWidget(name: "Product name"),
                    _buildSelectedOfferWidget(),
                    verticalSpacing(6),
                    ContentWidget(
                      result: "",
                    ),
                    verticalSpacing(6),
                    ContentWidget(
                      result: context.translate(
                        AppStrings.totalPriceOfCartProductString,
                        args: {
                          "total_price_for_cart_product": "10",
                        },
                      ),
                    ),
                  ],
                ),
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  Row(
                    children: [
                      Text(
                        "Ration",
                        style: AppStyles.bold(
                          fontSize: 14,
                          textColor: AppColors.kPrimary,
                          context: context,
                          height: 1.5,
                        ),
                      ),
                      horizontalSpacing(10),
                      IconButton(
                        style: ButtonStyle(
                          padding: MaterialStateProperty.all(
                            EdgeInsets.zero,
                          ),
                        ),
                        onPressed: () {},
                        icon: SvgPicture.asset(
                          AppIcons.trashIcon,
                          color: AppColors.kPrimaryText,
                        ),
                      ),
                    ],
                  ),
                  verticalSpacing(30),
                  // CounterWidget(
                  //   callbackMinus: (timer) => _decreaseCounter(),
                  //   callbackPlus: (timer) => _increaseCounter(),
                  //   onPlusFunc: () => _increaseCounter(),
                  //   onMinusFunc: () => _decreaseCounter(),
                  //   number: widget.cart.quantity,
                  //   isFromCart: true,
                  //   isMinQuantity:
                  //       widget.cart.quantity <= widget.cart.minQuantity,
                  // ),
                ],
              )
            ],
          ),
        ),
      ],
    );
  }
}

class HeaderWidget extends StatelessWidget {
  final String name;
  const HeaderWidget({super.key, required this.name});

  @override
  Widget build(BuildContext context) {
    return Text(
      name,
      overflow: TextOverflow.ellipsis,
      style: AppStyles.bold(
        fontSize: 14,
        textColor: AppColors.kSecondaryText,
        context: context,
        height: 1.5,
      ),
    );
  }
}

class ContentWidget extends StatelessWidget {
  final String result;
  const ContentWidget({super.key, required this.result});

  @override
  Widget build(BuildContext context) {
    return Text(
      result,
      textDirection: TextDirection.ltr,
      style: AppStyles.semiBold(
        fontSize: 10,
        textColor: AppColors.kSecondaryText,
        context: context,
        height: 1.5,
      ),
    );
  }
}
