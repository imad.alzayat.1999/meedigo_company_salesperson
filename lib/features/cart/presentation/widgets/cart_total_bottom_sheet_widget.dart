import 'package:flutter/material.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_extensions.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_strings.dart';
import '../../../../core/helpers/app_sizes.dart';
import '../../../../core/theming/app_colors.dart';
import '../../../../core/widgets/bottom_sheet_button_confirm_widget.dart';

class CartTotalBottomSheetWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: getHeight(80),
      padding: EdgeInsets.symmetric(
        horizontal: getWidth(16),
      ),
      decoration: BoxDecoration(
        color: AppColors.kWhite,
      ),
      child: Center(
        child: BottomSheetButtonConfirmWidget(
          btnTitle: context.translate(
            AppStrings.checkoutBtnString,
          ),
          onPressFunction: () {},
        ),
      ),
    );
  }
}
