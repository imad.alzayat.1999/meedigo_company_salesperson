import 'package:flutter/material.dart';
import 'package:meedigo_company_sales_person/core/widgets/app_padding_widget.dart';
import '../../../../core/helpers/app_sizes.dart';
import '../../../../core/widgets/button_widget.dart';
import '../../../../core/widgets/text_field_widget.dart';

class OrderNoteDialogWidget extends StatefulWidget {
  final String companyName;
  const OrderNoteDialogWidget({
    Key? key,
    required this.companyName,
  }) : super(key: key);

  @override
  State<OrderNoteDialogWidget> createState() => _OrderNoteDialogWidgetState();
}

class _OrderNoteDialogWidgetState extends State<OrderNoteDialogWidget> {
  var _orderNoteController = TextEditingController();

  // /// function to change order note
  _changeOrderNoteValue(String value) {
    // AppPreferences.setOrderNote(
    //   companyName: widget.companyName,
    //   note: value,
    // );
  }
  //
  // @override
  // void initState() {
  //   // TODO: implement initState
  //   super.initState();
  //   _init();
  // }
  //
  // _init() {
  //   _orderNoteController.text = AppPreferences.getOrderNote(
  //     widget.companyName,
  //   );
  // }

  /// save order note
  _saveOrderNote() {
    // AppFunctions.unFocus();
    // context.pop();
  }

  @override
  Widget build(BuildContext context) {
    return Dialog(
      child: SizedBox(
        height: getHeight(200),
        child: AppPaddingWidget(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              TextFieldWidget(
                labelText: "Order note",
                textEditingController: _orderNoteController,
                onChange: (value) => _changeOrderNoteValue(value),
              ),
              verticalSpacing(25),
              ButtonWidget(
                onPressFunction: () => _saveOrderNote(),
                btnText: "Add note",
              ),
            ],
          ),
        ),
      ),
    );
  }
}
