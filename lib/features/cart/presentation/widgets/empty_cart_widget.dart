// import 'package:flutter/material.dart';
// import 'package:flutter_svg/flutter_svg.dart';
// import 'package:meedigo_salesperson/core/helpers/extensions.dart';
// import 'package:meedigo_salesperson/core/helpers/app_sizes.dart';
//
// import '../../../../core/helpers/app_assets.dart';
// import '../../../../core/helpers/app_strings.dart';
// import '../../../../core/theming/app_colors.dart';
// import '../../../../core/theming/app_fonts.dart';
// import '../../../../core/theming/app_styles.dart';
//
// class EmptyCartWidget extends StatelessWidget {
//   const EmptyCartWidget({Key? key}) : super(key: key);
//
//   @override
//   Widget build(BuildContext context) {
//     return Center(
//       child: Column(
//         mainAxisAlignment: MainAxisAlignment.center,
//         children: [
//           SvgPicture.asset(
//             AppIcons.emptyCartIcon,
//             width: getHeight(200),
//             height: getHeight(200),
//           ),
//           verticalSpace(20),
//           Text(
//             context.translate(AppStrings.noCartProductsString),
//             style: AppStyles.medium(
//               fontSize: AppFontSizes.f16,
//               textColor: AppColors.kTextColor,
//               context: context,
//             ),
//           ),
//         ],
//       ),
//     );
//   }
// }
