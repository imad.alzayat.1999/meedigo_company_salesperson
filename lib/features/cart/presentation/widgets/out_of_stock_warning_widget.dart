import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_extensions.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_strings.dart';
import '../../../../core/helpers/app_assets.dart';
import '../../../../core/helpers/app_sizes.dart';
import '../../../../core/theming/app_colors.dart';
import '../../../../core/theming/app_styles.dart';

class OutOfStockWarningWidget extends StatelessWidget {
  const OutOfStockWarningWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: getHeight(40),
      decoration: BoxDecoration(
        color: AppColors.kOutOfStock.withOpacity(.35),
        borderRadius: BorderRadius.only(
          topRight: Radius.circular(
            getRadius(15),
          ),
          topLeft: Radius.circular(
            getRadius(15),
          ),
        ),
      ),
      child: Row(
        children: [
          SizedBox(
            height: getHeight(30),
            width: getHeight(30),
            child: SvgPicture.asset(
              AppIcons.warningIcon,
              color: AppColors.kOutOfStock,
            ),
          ),
          horizontalSpacing(4),
          Expanded(
            child: Text(
              context.translate(
                AppStrings.outOfStockMessageString,
              ),
              style: AppStyles.semiBold(
                fontSize: 14,
                context: context,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
