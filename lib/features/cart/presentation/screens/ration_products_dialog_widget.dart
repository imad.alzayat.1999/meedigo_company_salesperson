import 'package:flutter/material.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_extensions.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_sizes.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_strings.dart';
import '../../../../core/theming/app_colors.dart';
import '../../../../core/theming/app_styles.dart';
import '../../../../core/widgets/button_widget.dart';

class RationProductsDialogWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Dialog(
      insetPadding: EdgeInsets.symmetric(
        horizontal: getWidth(25),
        vertical: getHeight(10),
      ),
      child: SizedBox(
        height: getHeight(800),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              width: MediaQuery.of(context).size.width,
              padding: EdgeInsets.all(
                getRadius(14),
              ),
              decoration: BoxDecoration(
                color: AppColors.kPrimary,
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(
                    getRadius(10),
                  ),
                  topRight: Radius.circular(
                    getRadius(10),
                  ),
                ),
              ),
              child: Text(
                context.translate(
                  AppStrings.rationProductDialogTitleString,
                ),
                style: AppStyles.bold(
                  fontSize: 20,
                  context: context,
                  textColor: AppColors.kWhite,
                ),
              ),
            ),
            verticalSpacing(20),
            Expanded(
              child: ListView(
                children: [
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: getWidth(20)),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          context.translate(
                            AppStrings.dearSalespersonSectionTitleString,
                          ),
                          style: AppStyles.bold(fontSize: 18, context: context),
                        ),
                        verticalSpacing(15),
                        Padding(
                          padding:
                              EdgeInsets.symmetric(horizontal: getWidth(5)),
                          child: RichText(
                            text: TextSpan(
                              text: context.translate(
                                AppStrings.rationMessageString,
                              ),
                              style: AppStyles.medium(
                                fontSize: 16,
                                context: context,
                                height: 1.5,
                              ),
                              children: <TextSpan>[
                                TextSpan(
                                  text: context.translate(
                                    AppStrings.rationLimitString,
                                    args: {
                                      "ration_percent": 30,
                                    },
                                  ),
                                  style: AppStyles.bold(
                                    fontSize: 16,
                                    context: context,
                                    textColor: AppColors.kPrimary,
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                        verticalSpacing(20),
                        GroupedByRationProductsSection(),
                        verticalSpacing(40),
                        Text(
                          context.translate(
                            AppStrings.reviewOrderWarningString,
                          ),
                          style: AppStyles.medium(
                            fontSize: 14,
                            context: context,
                            height: 1.5,
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            Padding(
              padding: EdgeInsets.symmetric(
                horizontal: getWidth(28),
                vertical: getHeight(10),
              ),
              child: ButtonWidget(
                onPressFunction: () {},
                btnText: context.translate(
                  AppStrings.okBtnString,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class GroupedByRationProductsSection extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          context.translate(
            AppStrings.overflowWarehousesSectionTitleString,
          ),
          style: AppStyles.bold(
            fontSize: 18,
            context: context,
          ),
        ),
        verticalSpacing(15),
        Padding(
          padding: EdgeInsets.symmetric(horizontal: getWidth(5)),
          child: Text(
            "Company",
            style: AppStyles.medium(
              fontSize: 16,
              context: context,
              textColor: AppColors.kPrimary,
            ),
          ),
        ),
        verticalSpacing(20),
        Text(
          context.translate(
            AppStrings.rationProductsSectionTitleString,
          ),
          style: AppStyles.bold(
            fontSize: 18,
            context: context,
          ),
        ),
        verticalSpacing(16),
        ListOfRationProducts(),
      ],
    );
  }
}

class ListOfRationProducts extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ListView.separated(
      shrinkWrap: true,
      physics: const NeverScrollableScrollPhysics(),
      padding: EdgeInsets.symmetric(horizontal: getWidth(5)),
      itemBuilder: (context, index) => Row(
        children: [
          Text(
            "${index + 1} - ",
            style: AppStyles.semiBold(
              fontSize: 14,
              context: context,
            ),
          ),
          Text(
            "Product name",
            style: AppStyles.medium(
              fontSize: 14,
              context: context,
            ),
          ),
        ],
      ),
      separatorBuilder: (context, index) => Divider(),
      itemCount: 10,
    );
  }
}
