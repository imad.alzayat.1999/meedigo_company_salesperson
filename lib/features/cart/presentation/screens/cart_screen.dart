import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_extensions.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_strings.dart';
import 'package:meedigo_company_sales_person/core/widgets/app_padding_widget.dart';
import '../../../../core/widgets/appbar_widget.dart';
import '../widgets/cart_total_bottom_sheet_widget.dart';
import '../widgets/empty_cart_widget.dart';
import '../widgets/warehouse_expansion_tile_widget.dart';

class CartScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: appbarWidget(
        title: context.translate(
          AppStrings.cartTitleString,
        ),
        context: context,
        isCartPage: true,
      ),
      body: PopScope(
        onPopInvoked: (bool) {},
        child: Scaffold(
          body: AppPaddingWidget(
            top: 15,
            child: ListView(shrinkWrap: true, children: [
              WarehouseExpansionTileWidget(),
              WarehouseExpansionTileWidget()
            ]),
          ),
          bottomSheet: CartTotalBottomSheetWidget(),
        ),
      ),
    );
  }
}
