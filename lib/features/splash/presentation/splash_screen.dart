import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:package_info_plus/package_info_plus.dart';
import '../../../core/di/dependency_injection.dart';
import '../../../core/helpers/app_assets.dart';
import '../../../core/helpers/app_functions.dart';
import '../../../core/helpers/app_preferences.dart';
import '../../../core/theming/app_colors.dart';
import '../../versioning/presentation/bloc/versioning_bloc.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({super.key});

  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  PackageInfo _packageInfo = getIt<PackageInfo>();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _init();
    _checkIfCleared();
  }

  _checkIfCleared() {
    bool isCleared = AppPreferences.getLastCleared();
    if (isCleared == false) {
      AppFunctions.clearAllStoredDataFromTheApp();
      AppPreferences.setLastCleared();
    }
  }

  _init() {
    _getAppVersion();
  }

  _getAppVersion() async {
    final info = await PackageInfo.fromPlatform();
    setState(() {
      _packageInfo = info;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.kPrimary,
      body: BlocConsumer<VersioningBloc, VersioningState>(
        listener: (context, state) {
          state.whenOrNull(
            checkUpdateDone: (version) =>
                context.read<VersioningBloc>().checkAndRouteToNextPage(
              context: context,
              versioningData: version.versioningData!,
              appVersion: _packageInfo.version,
              products: [],
            ),
            checkUpdateFailed: (failure) =>
                context.read<VersioningBloc>().handleFailedState(
              failure: failure,
              context: context,
              products: [],
            ),
          );
        },
        builder: (context, state) {
          return state.maybeWhen(
            orElse: () => Center(
              child: Image.asset(
                AppImages.meedigoSplashGifImage,
              ),
            ),
          );
        },
      ),
    );
  }
}
