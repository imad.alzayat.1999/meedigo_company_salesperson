import 'package:flutter/material.dart';
import '../core/theming/app_styles.dart';

class PageNotFoundScreen extends StatelessWidget {
  const PageNotFoundScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Text(
          "Page Not found",
          style: AppStyles.bold(
            fontSize: 20,
            context: context,
          ),
        ),
      ),
    );
  }
}
