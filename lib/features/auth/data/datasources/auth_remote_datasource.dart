import 'package:dio/dio.dart';
import 'package:meedigo_company_sales_person/core/networking/api_error_handler.dart';
import 'package:meedigo_company_sales_person/core/networking/api_requests.dart';
import 'package:meedigo_company_sales_person/core/networking/api_services.dart';
import 'package:meedigo_company_sales_person/features/auth/data/models/check_code_response_model.dart';
import 'package:meedigo_company_sales_person/features/auth/data/models/generate_code_response_model.dart';
import 'package:meedigo_company_sales_person/features/auth/data/models/login_response_model.dart';

abstract class AuthRemoteDataSource {
  Future<GenerateCodeResponseModel> generateCodeForSms({
    required GenerateCodeForSmsAndWhatsappRequestBody
        generateCodeForSmsAndWhatsappRequestBody,
  });
  Future<GenerateCodeResponseModel> generateCodeForWhatsapp({
    required GenerateCodeForSmsAndWhatsappRequestBody
        generateCodeForSmsAndWhatsappRequestBody,
  });
  Future<CheckCodeResponseModel> checkCodeForSms({
    required CheckCodeForSmsAndWhatsappRequestBody
        checkCodeForSmsAndWhatsappRequestBody,
  });
  Future<CheckCodeResponseModel> checkCodeForWhatsapp({
    required CheckCodeForSmsAndWhatsappRequestBody
        checkCodeForSmsAndWhatsappRequestBody,
  });
  Future<LoginResponseModel> login({
    required LoginRequestBody loginRequestBody,
  });
}

class AuthRemoteDataSourceImpl implements AuthRemoteDataSource {
  final ApiService apiService;

  AuthRemoteDataSourceImpl({required this.apiService});

  @override
  Future<CheckCodeResponseModel> checkCodeForSms({
    required CheckCodeForSmsAndWhatsappRequestBody
        checkCodeForSmsAndWhatsappRequestBody,
  }) async {
    try {
      final response = await apiService.checkCodeForSms(
        checkCodeForSmsAndWhatsappRequestBody:
            checkCodeForSmsAndWhatsappRequestBody,
        appVersion: 'appVersion',
        platform: 'platform',
      );
      return response;
    } on DioException catch (e) {
      throw handleError(e);
    }
  }

  @override
  Future<CheckCodeResponseModel> checkCodeForWhatsapp({
    required CheckCodeForSmsAndWhatsappRequestBody
        checkCodeForSmsAndWhatsappRequestBody,
  }) async {
    try {
      final response = await apiService.checkCodeForWhatsapp(
        checkCodeForSmsAndWhatsappRequestBody:
            checkCodeForSmsAndWhatsappRequestBody,
        appVersion: 'appVersion',
        platform: 'platform',
      );
      return response;
    } on DioException catch (e) {
      throw handleError(e);
    }
  }

  @override
  Future<GenerateCodeResponseModel> generateCodeForSms({
    required GenerateCodeForSmsAndWhatsappRequestBody
        generateCodeForSmsAndWhatsappRequestBody,
  }) async {
    try {
      final response = await apiService.generateSmsOtpCode(
        generateCodeForSmsAndWhatsappRequestBody:
            generateCodeForSmsAndWhatsappRequestBody,
        appVersion: 'appVersion',
        platform: 'platform',
      );
      return response;
    } on DioException catch (e) {
      throw handleError(e);
    }
  }

  @override
  Future<GenerateCodeResponseModel> generateCodeForWhatsapp({
    required GenerateCodeForSmsAndWhatsappRequestBody
        generateCodeForSmsAndWhatsappRequestBody,
  }) async {
    try {
      final response = await apiService.generateWhatsappOtpCode(
        generateCodeForSmsAndWhatsappRequestBody:
            generateCodeForSmsAndWhatsappRequestBody,
        appVersion: 'appVersion',
        platform: 'platform',
      );
      return response;
    } on DioException catch (e) {
      throw handleError(e);
    }
  }

  @override
  Future<LoginResponseModel> login({
    required LoginRequestBody loginRequestBody,
  }) async {
    try {
      final response = await apiService.login(
        loginRequestBody: loginRequestBody,
        appVersion: 'appVersion',
        platform: 'platform',
      );
      return response;
    } on DioException catch (e) {
      throw handleError(e);
    }
  }
}
