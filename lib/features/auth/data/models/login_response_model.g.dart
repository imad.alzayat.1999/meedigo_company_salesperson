// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'login_response_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

LoginResponseModel _$LoginResponseModelFromJson(Map<String, dynamic> json) =>
    LoginResponseModel(
      loginDataModel: json['data'] == null
          ? null
          : LoginDataModel.fromJson(json['data'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$LoginResponseModelToJson(LoginResponseModel instance) =>
    <String, dynamic>{
      'data': instance.loginDataModel,
    };

LoginDataModel _$LoginDataModelFromJson(Map<String, dynamic> json) =>
    LoginDataModel(
      salesPersonId: (json['salesperson_id'] as num?)?.toInt(),
      token: json['token'] as String?,
      type: json['type'] as String?,
      companyId: (json['company_id'] as num?)?.toInt(),
      companyName: json['company_name'] as String?,
      allowOrder: (json['allow_order'] as num?)?.toInt(),
      active: (json['active'] as num?)?.toInt(),
      representatives: (json['representatives'] as List<dynamic>?)
          ?.map((e) => e as String)
          .toList(),
    );

Map<String, dynamic> _$LoginDataModelToJson(LoginDataModel instance) =>
    <String, dynamic>{
      'salesperson_id': instance.salesPersonId,
      'token': instance.token,
      'type': instance.type,
      'company_id': instance.companyId,
      'company_name': instance.companyName,
      'allow_order': instance.allowOrder,
      'active': instance.active,
      'representatives': instance.representatives,
    };
