import 'package:json_annotation/json_annotation.dart';

part 'login_response_model.g.dart';

@JsonSerializable()
class LoginResponseModel {
  @JsonKey(name: "data")
  final LoginDataModel? loginDataModel;

  LoginResponseModel({required this.loginDataModel});

  factory LoginResponseModel.fromJson(Map<String, dynamic> json) =>
      _$LoginResponseModelFromJson(json);

  Map<String, dynamic> toJson() => _$LoginResponseModelToJson(this);
}

@JsonSerializable()
class LoginDataModel {
  @JsonKey(name: "salesperson_id")
  final int? salesPersonId;

  @JsonKey(name: "token")
  final String? token;

  @JsonKey(name: "type")
  final String? type;

  @JsonKey(name: "company_id")
  final int? companyId;

  @JsonKey(name: "company_name")
  final String? companyName;

  @JsonKey(name:"allow_order")
  final int? allowOrder;

  @JsonKey(name:"active")
  final int? active;

  @JsonKey(name:"representatives")
  final List<String>? representatives;

  LoginDataModel({
    required this.salesPersonId,
    required this.token,
    required this.type,
    required this.companyId,
    required this.companyName,
    required this.allowOrder,
    required this.active,
    required this.representatives,
  });

  factory LoginDataModel.fromJson(Map<String, dynamic> json) =>
      _$LoginDataModelFromJson(json);

  Map<String, dynamic> toJson() => _$LoginDataModelToJson(this);
}