import 'package:json_annotation/json_annotation.dart';

part 'check_code_response_model.g.dart';

@JsonSerializable()
class CheckCodeResponseModel {
  @JsonKey(name: "data")
  final CheckCodeDataModel? checkCodeDataModel;

  CheckCodeResponseModel({required this.checkCodeDataModel});

  factory CheckCodeResponseModel.fromJson(Map<String, dynamic> json) =>
      _$CheckCodeResponseModelFromJson(json);

  Map<String, dynamic> toJson() => _$CheckCodeResponseModelToJson(this);
}

@JsonSerializable()
class CheckCodeDataModel {
  @JsonKey(name: "verifications_check")
  final bool? verificationsCheck;

  @JsonKey(name: "verifications_way")
  final String? verificationsWay;

  CheckCodeDataModel({
    required this.verificationsCheck,
    required this.verificationsWay,
  });

  factory CheckCodeDataModel.fromJson(Map<String, dynamic> json) =>
      _$CheckCodeDataModelFromJson(json);

  Map<String, dynamic> toJson() => _$CheckCodeDataModelToJson(this);
}