// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'check_code_response_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CheckCodeResponseModel _$CheckCodeResponseModelFromJson(
        Map<String, dynamic> json) =>
    CheckCodeResponseModel(
      checkCodeDataModel: json['data'] == null
          ? null
          : CheckCodeDataModel.fromJson(json['data'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$CheckCodeResponseModelToJson(
        CheckCodeResponseModel instance) =>
    <String, dynamic>{
      'data': instance.checkCodeDataModel,
    };

CheckCodeDataModel _$CheckCodeDataModelFromJson(Map<String, dynamic> json) =>
    CheckCodeDataModel(
      verificationsCheck: json['verifications_check'] as bool?,
      verificationsWay: json['verifications_way'] as String?,
    );

Map<String, dynamic> _$CheckCodeDataModelToJson(CheckCodeDataModel instance) =>
    <String, dynamic>{
      'verifications_check': instance.verificationsCheck,
      'verifications_way': instance.verificationsWay,
    };
