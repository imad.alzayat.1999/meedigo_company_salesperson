import 'package:json_annotation/json_annotation.dart';

part 'generate_code_response_model.g.dart';

@JsonSerializable()
class GenerateCodeResponseModel {
  @JsonKey(name: "message")
  final String? message;

  GenerateCodeResponseModel({
    required this.message,
  });

  factory GenerateCodeResponseModel.fromJson(Map<String, dynamic> json) =>
      _$GenerateCodeResponseModelFromJson(json);

  Map<String, dynamic> toJson() => _$GenerateCodeResponseModelToJson(this);
}
