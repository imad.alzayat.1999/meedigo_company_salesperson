// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'generate_code_response_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

GenerateCodeResponseModel _$GenerateCodeResponseModelFromJson(
        Map<String, dynamic> json) =>
    GenerateCodeResponseModel(
      message: json['message'] as String?,
    );

Map<String, dynamic> _$GenerateCodeResponseModelToJson(
        GenerateCodeResponseModel instance) =>
    <String, dynamic>{
      'message': instance.message,
    };
