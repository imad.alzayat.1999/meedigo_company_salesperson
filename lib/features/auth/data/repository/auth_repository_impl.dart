import 'package:dartz/dartz.dart';
import 'package:meedigo_company_sales_person/core/mappers/check_code_mapper.dart';
import 'package:meedigo_company_sales_person/core/mappers/generate_code_mapper.dart';
import 'package:meedigo_company_sales_person/core/mappers/login_mapper.dart';
import 'package:meedigo_company_sales_person/core/networking/api_error_model.dart';
import 'package:meedigo_company_sales_person/core/networking/api_failure.dart';
import 'package:meedigo_company_sales_person/core/networking/api_requests.dart';
import 'package:meedigo_company_sales_person/features/auth/data/datasources/auth_remote_datasource.dart';
import 'package:meedigo_company_sales_person/features/auth/domain/entities/check_code.dart';
import 'package:meedigo_company_sales_person/features/auth/domain/entities/generate_code.dart';
import 'package:meedigo_company_sales_person/features/auth/domain/entities/login.dart';
import 'package:meedigo_company_sales_person/features/auth/domain/repository/auth_repository.dart';

class AuthRepositoryImpl implements AuthRepository {
  final AuthRemoteDataSource authRemoteDataSource;

  AuthRepositoryImpl({required this.authRemoteDataSource});

  @override
  Future<Either<Failure, CheckCodeResponse>> checkCodeForSms({
    required CheckCodeForSmsAndWhatsappRequestBody
        checkCodeForSmsAndWhatsappRequestBody,
  }) async {
    try {
      final result = await authRemoteDataSource.checkCodeForSms(
        checkCodeForSmsAndWhatsappRequestBody:
            checkCodeForSmsAndWhatsappRequestBody,
      );
      return Right(result.toDomain());
    } on ApiErrorModel catch (e) {
      return Left(
        ServerFailure(
          errorMessage: e.message,
          statusCode: e.code,
        ),
      );
    }
  }

  @override
  Future<Either<Failure, CheckCodeResponse>> checkCodeForWhatsapp({
    required CheckCodeForSmsAndWhatsappRequestBody
        checkCodeForSmsAndWhatsappRequestBody,
  }) async {
    try {
      final result = await authRemoteDataSource.checkCodeForWhatsapp(
        checkCodeForSmsAndWhatsappRequestBody:
            checkCodeForSmsAndWhatsappRequestBody,
      );
      return Right(result.toDomain());
    } on ApiErrorModel catch (e) {
      return Left(
        ServerFailure(
          errorMessage: e.message,
          statusCode: e.code,
        ),
      );
    }
  }

  @override
  Future<Either<Failure, GenerateCodeResponse>> generateCodeForSms({
    required GenerateCodeForSmsAndWhatsappRequestBody
        generateCodeForSmsAndWhatsappRequestBody,
  }) async {
    try {
      final result = await authRemoteDataSource.generateCodeForSms(
        generateCodeForSmsAndWhatsappRequestBody:
            generateCodeForSmsAndWhatsappRequestBody,
      );
      return Right(result.toDomain());
    } on ApiErrorModel catch (e) {
      return Left(
        ServerFailure(
          errorMessage: e.message,
          statusCode: e.code,
        ),
      );
    }
  }

  @override
  Future<Either<Failure, GenerateCodeResponse>> generateCodeForWhatsapp({
    required GenerateCodeForSmsAndWhatsappRequestBody
        generateCodeForSmsAndWhatsappRequestBody,
  }) async {
    try {
      final result = await authRemoteDataSource.generateCodeForWhatsapp(
        generateCodeForSmsAndWhatsappRequestBody:
            generateCodeForSmsAndWhatsappRequestBody,
      );
      return Right(result.toDomain());
    } on ApiErrorModel catch (e) {
      return Left(
        ServerFailure(
          errorMessage: e.message,
          statusCode: e.code,
        ),
      );
    }
  }

  @override
  Future<Either<Failure, LoginResponse>> login({
    required LoginRequestBody loginRequestBody,
  }) async {
    try {
      final result = await authRemoteDataSource.login(
        loginRequestBody: loginRequestBody,
      );
      return Right(result.toDomain());
    } on ApiErrorModel catch (e) {
      return Left(
        ServerFailure(
          errorMessage: e.message,
          statusCode: e.code,
        ),
      );
    }
  }
}
