import 'package:flutter/material.dart';
import 'package:meedigo_company_sales_person/features/auth/domain/entities/generate_code.dart';
import 'package:meedigo_company_sales_person/features/auth/presentation/widgets/verify_otp_widgets/verify_otp_screen_count_down_widget.dart';
import 'package:meedigo_company_sales_person/features/auth/presentation/widgets/verify_otp_widgets/verify_otp_screen_resend_code_btn_widget.dart';

class VerifyOtpScreenResendCodeWidget extends StatelessWidget {
  final int seconds;
  final Object? Function(GenerateCodeResponse)? resendSuccess;

  const VerifyOtpScreenResendCodeWidget({
    super.key,
    required this.seconds,
    required this.resendSuccess,
  });

  @override
  Widget build(BuildContext context) {
    if (seconds == 0) {
      return VerifyOtpScreenResendCodeBtnWidget(
        resendSuccess: resendSuccess,
      );
    } else {
      return VerifyOtpScreenCountDownWidget(
        seconds: seconds,
      );
    }
  }
}
