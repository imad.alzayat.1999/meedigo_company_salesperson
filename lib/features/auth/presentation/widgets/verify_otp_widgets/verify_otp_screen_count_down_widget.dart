import 'package:flutter/material.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_extensions.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_strings.dart';
import 'package:meedigo_company_sales_person/core/theming/app_colors.dart';
import 'package:meedigo_company_sales_person/core/theming/app_styles.dart';

class VerifyOtpScreenCountDownWidget extends StatelessWidget {
  final int seconds;

  const VerifyOtpScreenCountDownWidget({
    super.key,
    required this.seconds,
  });

  @override
  Widget build(BuildContext context) {
    return Text(
      context.translate(
        AppStrings.resendCodeString,
        args: {
          "seconds": seconds,
        },
      ),
      style: AppStyles.bold(
        fontSize: 16,
        context: context,
        textColor: AppColors.kSecondaryText,
      ),
    );
  }
}
