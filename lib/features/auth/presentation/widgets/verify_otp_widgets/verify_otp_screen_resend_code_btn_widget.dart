import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_extensions.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_strings.dart';
import 'package:meedigo_company_sales_person/core/helpers/enums.dart';
import 'package:meedigo_company_sales_person/core/theming/app_colors.dart';
import 'package:meedigo_company_sales_person/core/theming/app_styles.dart';
import 'package:meedigo_company_sales_person/core/widgets/loading_indicator_widget.dart';
import 'package:meedigo_company_sales_person/features/auth/domain/entities/generate_code.dart';
import 'package:meedigo_company_sales_person/features/auth/presentation/bloc/check_code/check_code_bloc.dart';
import 'package:meedigo_company_sales_person/features/auth/presentation/bloc/generate_code/generate_code_bloc.dart';

class VerifyOtpScreenResendCodeBtnWidget extends StatelessWidget {
  final Object? Function(GenerateCodeResponse)? resendSuccess;
  const VerifyOtpScreenResendCodeBtnWidget({
    super.key,
    required this.resendSuccess,
  });

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<GenerateCodeBloc, GenerateCodeState>(
      listener: (context, state) {
        state.whenOrNull(
          generateCodeForSmsSuccess: resendSuccess,
          generateCodeForWhatsappSuccess: resendSuccess,
          generateCodeForWhatsappFailed: (failure) =>
              context.read<GenerateCodeBloc>()
                ..showSmsOrWhatsappCodeGenerationFailedMessage(
                  context: context,
                  failure: failure,
                ),
          generateCodeForSmsFailed: (failure) =>
              context.read<GenerateCodeBloc>()
                ..showSmsOrWhatsappCodeGenerationFailedMessage(
                  context: context,
                  failure: failure,
                ),
        );
      },
      builder: (context, state) {
        return state.maybeWhen(
          generateCodeForSmsLoading: () => LoadingIndicatorWidget(
            loadingSize: LoadingSizes.button,
          ),
          generateCodeForWhatsappLoading: () => LoadingIndicatorWidget(
            loadingSize: LoadingSizes.button,
          ),
          orElse: () => Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                context.translate(
                  AppStrings.verifyOtpResendCodeQuestionString,
                ),
                style: AppStyles.medium(
                  fontSize: 16,
                  textColor: AppColors.kSecondaryText,
                  context: context,
                ),
              ),
              TextButton(
                onPressed: () {
                  if (context.read<CheckCodeBloc>().isWhatsapp) {
                    context.read<GenerateCodeBloc>()
                      ..add(
                        GenerateCodeEvent.generateCodeForWhatsapp(),
                      );
                  } else {
                    context.read<GenerateCodeBloc>()
                      ..add(
                        GenerateCodeEvent.generateCodeForSms(),
                      );
                  }
                },
                style: ButtonStyle(
                  padding: MaterialStateProperty.all<EdgeInsets>(
                    EdgeInsets.zero,
                  ),
                ),
                child: Text(
                  context.translate(
                    AppStrings.verifyOtpResendCodeBtnString,
                  ),
                  style: AppStyles.semiBold(
                    fontSize: 18,
                    textColor: AppColors.kSecondaryText,
                    context: context,
                  ),
                ),
              ),
            ],
          ),
        );
      },
    );
  }
}
