import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_extensions.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_strings.dart';
import 'package:meedigo_company_sales_person/core/theming/app_theme.dart';
import 'package:meedigo_company_sales_person/features/auth/presentation/bloc/check_code/check_code_bloc.dart';
import 'package:pinput/pinput.dart';
import '../../../../../core/helpers/app_sizes.dart';
import '../../../../../core/theming/app_colors.dart';
import '../../../../../core/theming/app_styles.dart';

class VerifyOtpScreenPinPutWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Align(
          alignment: AlignmentDirectional.centerStart,
          child: Text(
            context.translate(
              AppStrings.otpCodeSectionTitleString,
            ),
            style: AppStyles.semiBold(
              fontSize: 18,
              textColor: AppColors.kSecondaryText,
              context: context,
            ),
          ),
        ),
        verticalSpacing(17),
        Directionality(
          textDirection: TextDirection.ltr,
          child: Pinput(
            controller: context.read<CheckCodeBloc>().otpController,
            androidSmsAutofillMethod:
                AndroidSmsAutofillMethod.smsUserConsentApi,
            separatorBuilder: (index) => horizontalSpacing(20),
            length: 5,
            errorTextStyle: AppStyles.medium(
              fontSize: 12,
              textColor: AppColors.kError,
              height: 1.5,
              context: context,
            ),
            defaultPinTheme: AppTheme.getDefaultPinTheme(context),
            focusedPinTheme: AppTheme.getFocusedStateOfPinTheme(context),
            submittedPinTheme: AppTheme.getSubmittedStateOfPinTheme(context),
            errorPinTheme: AppTheme.getErrorStateOfPinTheme(context),
            validator: (value) =>
                context.read<CheckCodeBloc>().validateOtpInputField(
                      context: context,
                      input: value,
                    ),
            pinputAutovalidateMode: PinputAutovalidateMode.disabled,
            showCursor: true,
            onCompleted: (value) => context.read<CheckCodeBloc>()
              ..onCompleteOtpInputField(
                context: context,
              ),
          ),
        ),
      ],
    );
  }
}
