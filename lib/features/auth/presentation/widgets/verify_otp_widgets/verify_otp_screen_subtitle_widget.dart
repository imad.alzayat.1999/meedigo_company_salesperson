import 'package:flutter/material.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_extensions.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_strings.dart';
import '../../../../../core/theming/app_colors.dart';
import '../../../../../core/theming/app_styles.dart';

class VerifyOtpScreenSubtitleWidget extends StatelessWidget {
  const VerifyOtpScreenSubtitleWidget();

  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: AlignmentDirectional.centerStart,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            context.translate(
              AppStrings.verifyOtpContentString,
            ),
            style: AppStyles.medium(
              fontSize: 16,
              textColor: AppColors.kSecondaryText,
              context: context,
            ),
          ),
          Text(
            "0 (956) 759-576",
            textDirection: TextDirection.ltr,
            style: AppStyles.bold(
              fontSize: 16,
              textColor: AppColors.kSecondaryText,
              context: context,
            ),
          ),
        ],
      ),
    );
  }
}
