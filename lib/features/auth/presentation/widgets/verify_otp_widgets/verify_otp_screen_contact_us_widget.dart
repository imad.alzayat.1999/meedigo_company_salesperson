import 'package:flutter/material.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_extensions.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_strings.dart';
import '../../../../../core/theming/app_colors.dart';
import '../../../../../core/widgets/button_widget.dart';

class VerifyOtpScreenContactUsWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ButtonWidget(
      onPressFunction: () {},
      btnText: context.translate(
        AppStrings.contactWithSupportBtnString,
      ),
      btnColor: AppColors.kWhite,
      btnTextColor: AppColors.kPrimary,
      isOtpProblem: true,
    );
  }
}

///BlocProvider(
//       create: (context) => getIt<ProfileBloc>()..add(GetContactInfoEvent()),
//       child: BlocBuilder<ProfileBloc, ProfileState>(
//         builder: (context, state) {
//           switch (state.contactInfoRequestStates) {
//             case RequestStates.loading:
//               return ShimmerEffectWidget(
//                 width: context.width(),
//                 height: getHeight(120),
//               );
//             case RequestStates.success:
//               return ButtonWidget(
//                 onPressFunction: () =>
//                     context.read<AuthBloc>().redirectToWhatsappContact(
//                           state.contactUs!.whatsappNumber,
//                         ),
//                 btnText: "Otp missing, please contact us",
//                 btnColor: AppColors.kWhite,
//                 btnTextColor: AppColors.kPrimary,
//                 isOtpProblem: true,
//               );
//             case RequestStates.error:
//               return FailureWidget(
//                 onPressFunction: () => context.read<ProfileBloc>()
//                   ..add(
//                     GetContactInfoEvent(),
//                   ),
//                 failure: state.contactInfoFailure!,
//               );
//           }
//         },
//       ),
//     );
