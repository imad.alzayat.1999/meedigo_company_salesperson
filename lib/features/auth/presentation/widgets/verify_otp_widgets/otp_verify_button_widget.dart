import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_extensions.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_sizes.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_strings.dart';
import 'package:meedigo_company_sales_person/core/helpers/enums.dart';
import 'package:meedigo_company_sales_person/core/widgets/loading_indicator_widget.dart';
import 'package:meedigo_company_sales_person/features/auth/domain/entities/generate_code.dart';
import 'package:meedigo_company_sales_person/features/auth/presentation/bloc/check_code/check_code_bloc.dart';
import 'package:meedigo_company_sales_person/features/auth/presentation/bloc/generate_code/generate_code_bloc.dart';
import 'package:meedigo_company_sales_person/features/auth/presentation/widgets/verify_otp_widgets/verify_otp_screen_contact_us_widget.dart';
import '../../../../../core/theming/app_colors.dart';
import '../../../../../core/theming/app_styles.dart';
import '../../../../../core/widgets/button_widget.dart';

class OtpVerifyButtonWidget extends StatelessWidget {
  final Object? Function(GenerateCodeResponse)? sendWhatsappDone;

  const OtpVerifyButtonWidget({
    super.key,
    required this.sendWhatsappDone,
  });

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        ButtonWidget(
          btnHeight: 50,
          btnText: context.translate(
            AppStrings.verifyBtnString,
          ),
          onPressFunction: () => context.read<CheckCodeBloc>()..checkCode(),
        ),
        verticalSpacing(60),
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            BlocConsumer<GenerateCodeBloc, GenerateCodeState>(
              listener: (context, state) {
                state.whenOrNull(
                  generateCodeForWhatsappFailed: (failure) =>
                      context.read<GenerateCodeBloc>()
                        ..showSmsOrWhatsappCodeGenerationFailedMessage(
                          context: context,
                          failure: failure,
                        ),
                  generateCodeForWhatsappSuccess: sendWhatsappDone,
                );
              },
              builder: (context, state) {
                return state.maybeWhen(
                  generateCodeForWhatsappLoading: () => LoadingIndicatorWidget(
                    loadingSize: LoadingSizes.button,
                  ),
                  orElse: () => ButtonWidget(
                    onPressFunction: context
                            .read<GenerateCodeBloc>()
                            .isSendWhatsappBtnClicked
                        ? null
                        : () => context.read<GenerateCodeBloc>()
                          ..add(
                            GenerateCodeEvent.generateCodeForWhatsapp(),
                          ),
                    btnText: context.translate(
                      AppStrings.sendWhatsappMessageBtnString,
                    ),
                    btnColor: AppColors.kWhite,
                    btnTextColor: AppColors.kPrimary,
                    btnHeight: 50,
                    isAuth: true,
                    isWhatsapp: true,
                  ),
                );
              },
            ),
            Padding(
              padding: EdgeInsets.symmetric(
                vertical: getHeight(10),
              ),
              child: Align(
                alignment: Alignment.center,
                child: Text(
                  context.translate(
                    AppStrings.orString,
                  ),
                  style: AppStyles.bold(
                    fontSize: 16,
                    context: context,
                  ),
                ),
              ),
            ),
            VerifyOtpScreenContactUsWidget(),
          ],
        ),
      ],
    );
  }
}
