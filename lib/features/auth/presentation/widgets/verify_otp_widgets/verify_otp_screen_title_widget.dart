import 'package:flutter/material.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_extensions.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_strings.dart';
import '../../../../../core/theming/app_styles.dart';

class VerifyOtpScreenTitleWidget extends StatelessWidget {
  const VerifyOtpScreenTitleWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: AlignmentDirectional.centerStart,
      child: Text(
        context.translate(
          AppStrings.verifyOtpTitleString,
        ),
        style: AppStyles.bold(
          fontSize: 20,
          context: context,
        ),
      ),
    );
  }
}
