import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

import '../../../../../core/helpers/app_sizes.dart';
import '../../../../../core/helpers/app_assets.dart';

class SendSmsScreenLogoWidget extends StatelessWidget {
  const SendSmsScreenLogoWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: getRadius(250),
      height: getRadius(250),
      child: SvgPicture.asset(
        AppImages.sendSmsImage,
      ),
    );
  }
}
