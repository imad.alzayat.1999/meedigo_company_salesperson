import 'package:flutter/material.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_extensions.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_strings.dart';
import '../../../../../core/theming/app_styles.dart';

class SendSmsScreenTitleWidget extends StatelessWidget {
  const SendSmsScreenTitleWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Text(
      context.translate(
        AppStrings.sendSmsTitleString,
      ),
      textAlign: TextAlign.center,
      style: AppStyles.bold(
        fontSize: 20,
        context: context,
      ),
    );
  }
}
