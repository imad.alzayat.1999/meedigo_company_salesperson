import 'package:flutter/material.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_extensions.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_strings.dart';
import '../../../../../core/theming/app_colors.dart';
import '../../../../../core/theming/app_styles.dart';

class SendSmsScreenSubtitleWidget extends StatelessWidget {
  const SendSmsScreenSubtitleWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Text(
      context.translate(
        AppStrings.sendSmsContentString,
      ),
      textAlign: TextAlign.center,
      style: AppStyles.medium(
        fontSize: 16,
        textColor: AppColors.kSecondaryText,
        height: 1.5,
        context: context,
      ),
    );
  }
}
