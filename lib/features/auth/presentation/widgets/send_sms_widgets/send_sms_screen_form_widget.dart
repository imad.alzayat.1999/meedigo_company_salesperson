import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_extensions.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_sizes.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_strings.dart';
import 'package:meedigo_company_sales_person/core/helpers/enums.dart';
import 'package:meedigo_company_sales_person/core/widgets/button_widget.dart';
import 'package:meedigo_company_sales_person/core/widgets/loading_indicator_widget.dart';
import 'package:meedigo_company_sales_person/core/widgets/text_field_widget.dart';
import 'package:meedigo_company_sales_person/features/auth/presentation/bloc/generate_code/generate_code_bloc.dart';

class SendSmsScreenFormWidget extends StatelessWidget {
  const SendSmsScreenFormWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<GenerateCodeBloc, GenerateCodeState>(
      listener: (context, state) {
        state.whenOrNull(
          generateCodeForSmsSuccess: (response) =>
              context.read<GenerateCodeBloc>()
                ..routeToOtpVerificationScreen(
                  context: context,
                ),
          generateCodeForSmsFailed: (failure) =>
              context.read<GenerateCodeBloc>()
                ..showSmsOrWhatsappCodeGenerationFailedMessage(
                  context: context,
                  failure: failure,
                ),
        );
      },
      builder: (context, state) {
        return Form(
          key: context.read<GenerateCodeBloc>().globalKey,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              TextFieldWidget(
                labelText: context.translate(
                  AppStrings.phoneNumberString,
                ),
                onValidate: (value) =>
                    context.read<GenerateCodeBloc>().validatePhoneNumberInput(
                          input: value,
                          context: context,
                        ),
                textInputType: TextInputType.phone,
                textEditingController:
                    context.read<GenerateCodeBloc>().phoneController,
              ),
              verticalSpacing(20),
              state.maybeWhen(
                generateCodeForSmsLoading: () => LoadingIndicatorWidget(
                  loadingSize: LoadingSizes.auth,
                ),
                orElse: () => ButtonWidget(
                  isAuth: true,
                  onPressFunction: () => context.read<GenerateCodeBloc>()
                    ..validateAndGenerateSmsCode(),
                  btnText: context.translate(
                    AppStrings.sendSmsMessageBtnString,
                  ),
                ),
              ),
            ],
          ),
        );
      },
    );
  }
}
