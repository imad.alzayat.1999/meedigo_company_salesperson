// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'check_code_bloc.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

/// @nodoc
mixin _$CheckCodeEvent {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() checkCodeForSms,
    required TResult Function() checkCodeForWhatsapp,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? checkCodeForSms,
    TResult? Function()? checkCodeForWhatsapp,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? checkCodeForSms,
    TResult Function()? checkCodeForWhatsapp,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_CheckCodeForSms value) checkCodeForSms,
    required TResult Function(_CheckCodeForWhatsapp value) checkCodeForWhatsapp,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_CheckCodeForSms value)? checkCodeForSms,
    TResult? Function(_CheckCodeForWhatsapp value)? checkCodeForWhatsapp,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_CheckCodeForSms value)? checkCodeForSms,
    TResult Function(_CheckCodeForWhatsapp value)? checkCodeForWhatsapp,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $CheckCodeEventCopyWith<$Res> {
  factory $CheckCodeEventCopyWith(
          CheckCodeEvent value, $Res Function(CheckCodeEvent) then) =
      _$CheckCodeEventCopyWithImpl<$Res, CheckCodeEvent>;
}

/// @nodoc
class _$CheckCodeEventCopyWithImpl<$Res, $Val extends CheckCodeEvent>
    implements $CheckCodeEventCopyWith<$Res> {
  _$CheckCodeEventCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$CheckCodeForSmsImplCopyWith<$Res> {
  factory _$$CheckCodeForSmsImplCopyWith(_$CheckCodeForSmsImpl value,
          $Res Function(_$CheckCodeForSmsImpl) then) =
      __$$CheckCodeForSmsImplCopyWithImpl<$Res>;
}

/// @nodoc
class __$$CheckCodeForSmsImplCopyWithImpl<$Res>
    extends _$CheckCodeEventCopyWithImpl<$Res, _$CheckCodeForSmsImpl>
    implements _$$CheckCodeForSmsImplCopyWith<$Res> {
  __$$CheckCodeForSmsImplCopyWithImpl(
      _$CheckCodeForSmsImpl _value, $Res Function(_$CheckCodeForSmsImpl) _then)
      : super(_value, _then);
}

/// @nodoc

class _$CheckCodeForSmsImpl implements _CheckCodeForSms {
  const _$CheckCodeForSmsImpl();

  @override
  String toString() {
    return 'CheckCodeEvent.checkCodeForSms()';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$CheckCodeForSmsImpl);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() checkCodeForSms,
    required TResult Function() checkCodeForWhatsapp,
  }) {
    return checkCodeForSms();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? checkCodeForSms,
    TResult? Function()? checkCodeForWhatsapp,
  }) {
    return checkCodeForSms?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? checkCodeForSms,
    TResult Function()? checkCodeForWhatsapp,
    required TResult orElse(),
  }) {
    if (checkCodeForSms != null) {
      return checkCodeForSms();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_CheckCodeForSms value) checkCodeForSms,
    required TResult Function(_CheckCodeForWhatsapp value) checkCodeForWhatsapp,
  }) {
    return checkCodeForSms(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_CheckCodeForSms value)? checkCodeForSms,
    TResult? Function(_CheckCodeForWhatsapp value)? checkCodeForWhatsapp,
  }) {
    return checkCodeForSms?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_CheckCodeForSms value)? checkCodeForSms,
    TResult Function(_CheckCodeForWhatsapp value)? checkCodeForWhatsapp,
    required TResult orElse(),
  }) {
    if (checkCodeForSms != null) {
      return checkCodeForSms(this);
    }
    return orElse();
  }
}

abstract class _CheckCodeForSms implements CheckCodeEvent {
  const factory _CheckCodeForSms() = _$CheckCodeForSmsImpl;
}

/// @nodoc
abstract class _$$CheckCodeForWhatsappImplCopyWith<$Res> {
  factory _$$CheckCodeForWhatsappImplCopyWith(_$CheckCodeForWhatsappImpl value,
          $Res Function(_$CheckCodeForWhatsappImpl) then) =
      __$$CheckCodeForWhatsappImplCopyWithImpl<$Res>;
}

/// @nodoc
class __$$CheckCodeForWhatsappImplCopyWithImpl<$Res>
    extends _$CheckCodeEventCopyWithImpl<$Res, _$CheckCodeForWhatsappImpl>
    implements _$$CheckCodeForWhatsappImplCopyWith<$Res> {
  __$$CheckCodeForWhatsappImplCopyWithImpl(_$CheckCodeForWhatsappImpl _value,
      $Res Function(_$CheckCodeForWhatsappImpl) _then)
      : super(_value, _then);
}

/// @nodoc

class _$CheckCodeForWhatsappImpl implements _CheckCodeForWhatsapp {
  const _$CheckCodeForWhatsappImpl();

  @override
  String toString() {
    return 'CheckCodeEvent.checkCodeForWhatsapp()';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$CheckCodeForWhatsappImpl);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() checkCodeForSms,
    required TResult Function() checkCodeForWhatsapp,
  }) {
    return checkCodeForWhatsapp();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? checkCodeForSms,
    TResult? Function()? checkCodeForWhatsapp,
  }) {
    return checkCodeForWhatsapp?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? checkCodeForSms,
    TResult Function()? checkCodeForWhatsapp,
    required TResult orElse(),
  }) {
    if (checkCodeForWhatsapp != null) {
      return checkCodeForWhatsapp();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_CheckCodeForSms value) checkCodeForSms,
    required TResult Function(_CheckCodeForWhatsapp value) checkCodeForWhatsapp,
  }) {
    return checkCodeForWhatsapp(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_CheckCodeForSms value)? checkCodeForSms,
    TResult? Function(_CheckCodeForWhatsapp value)? checkCodeForWhatsapp,
  }) {
    return checkCodeForWhatsapp?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_CheckCodeForSms value)? checkCodeForSms,
    TResult Function(_CheckCodeForWhatsapp value)? checkCodeForWhatsapp,
    required TResult orElse(),
  }) {
    if (checkCodeForWhatsapp != null) {
      return checkCodeForWhatsapp(this);
    }
    return orElse();
  }
}

abstract class _CheckCodeForWhatsapp implements CheckCodeEvent {
  const factory _CheckCodeForWhatsapp() = _$CheckCodeForWhatsappImpl;
}

/// @nodoc
mixin _$CheckCodeState {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() checkCodeForSmsLoading,
    required TResult Function(CheckCodeResponse checkCodeResponse)
        checkCodeForSmsSuccess,
    required TResult Function(Failure failure) checkCodeForSmsFailed,
    required TResult Function() checkCodeForWhatsappLoading,
    required TResult Function(CheckCodeResponse checkCodeResponse)
        checkCodeForWhatsappSuccess,
    required TResult Function(Failure failure) checkCodeForWhatsappFailed,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? checkCodeForSmsLoading,
    TResult? Function(CheckCodeResponse checkCodeResponse)?
        checkCodeForSmsSuccess,
    TResult? Function(Failure failure)? checkCodeForSmsFailed,
    TResult? Function()? checkCodeForWhatsappLoading,
    TResult? Function(CheckCodeResponse checkCodeResponse)?
        checkCodeForWhatsappSuccess,
    TResult? Function(Failure failure)? checkCodeForWhatsappFailed,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? checkCodeForSmsLoading,
    TResult Function(CheckCodeResponse checkCodeResponse)?
        checkCodeForSmsSuccess,
    TResult Function(Failure failure)? checkCodeForSmsFailed,
    TResult Function()? checkCodeForWhatsappLoading,
    TResult Function(CheckCodeResponse checkCodeResponse)?
        checkCodeForWhatsappSuccess,
    TResult Function(Failure failure)? checkCodeForWhatsappFailed,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_CheckCodeForSmsLoading value)
        checkCodeForSmsLoading,
    required TResult Function(_CheckCodeForSmsSuccess value)
        checkCodeForSmsSuccess,
    required TResult Function(_CheckCodeForSmsFailed value)
        checkCodeForSmsFailed,
    required TResult Function(_CheckCodeForWhatsappLoading value)
        checkCodeForWhatsappLoading,
    required TResult Function(_CheckCodeForWhatsappSuccess value)
        checkCodeForWhatsappSuccess,
    required TResult Function(_CheckCodeForWhatsappFailed value)
        checkCodeForWhatsappFailed,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_CheckCodeForSmsLoading value)? checkCodeForSmsLoading,
    TResult? Function(_CheckCodeForSmsSuccess value)? checkCodeForSmsSuccess,
    TResult? Function(_CheckCodeForSmsFailed value)? checkCodeForSmsFailed,
    TResult? Function(_CheckCodeForWhatsappLoading value)?
        checkCodeForWhatsappLoading,
    TResult? Function(_CheckCodeForWhatsappSuccess value)?
        checkCodeForWhatsappSuccess,
    TResult? Function(_CheckCodeForWhatsappFailed value)?
        checkCodeForWhatsappFailed,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_CheckCodeForSmsLoading value)? checkCodeForSmsLoading,
    TResult Function(_CheckCodeForSmsSuccess value)? checkCodeForSmsSuccess,
    TResult Function(_CheckCodeForSmsFailed value)? checkCodeForSmsFailed,
    TResult Function(_CheckCodeForWhatsappLoading value)?
        checkCodeForWhatsappLoading,
    TResult Function(_CheckCodeForWhatsappSuccess value)?
        checkCodeForWhatsappSuccess,
    TResult Function(_CheckCodeForWhatsappFailed value)?
        checkCodeForWhatsappFailed,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $CheckCodeStateCopyWith<$Res> {
  factory $CheckCodeStateCopyWith(
          CheckCodeState value, $Res Function(CheckCodeState) then) =
      _$CheckCodeStateCopyWithImpl<$Res, CheckCodeState>;
}

/// @nodoc
class _$CheckCodeStateCopyWithImpl<$Res, $Val extends CheckCodeState>
    implements $CheckCodeStateCopyWith<$Res> {
  _$CheckCodeStateCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$InitialImplCopyWith<$Res> {
  factory _$$InitialImplCopyWith(
          _$InitialImpl value, $Res Function(_$InitialImpl) then) =
      __$$InitialImplCopyWithImpl<$Res>;
}

/// @nodoc
class __$$InitialImplCopyWithImpl<$Res>
    extends _$CheckCodeStateCopyWithImpl<$Res, _$InitialImpl>
    implements _$$InitialImplCopyWith<$Res> {
  __$$InitialImplCopyWithImpl(
      _$InitialImpl _value, $Res Function(_$InitialImpl) _then)
      : super(_value, _then);
}

/// @nodoc

class _$InitialImpl implements _Initial {
  const _$InitialImpl();

  @override
  String toString() {
    return 'CheckCodeState.initial()';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$InitialImpl);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() checkCodeForSmsLoading,
    required TResult Function(CheckCodeResponse checkCodeResponse)
        checkCodeForSmsSuccess,
    required TResult Function(Failure failure) checkCodeForSmsFailed,
    required TResult Function() checkCodeForWhatsappLoading,
    required TResult Function(CheckCodeResponse checkCodeResponse)
        checkCodeForWhatsappSuccess,
    required TResult Function(Failure failure) checkCodeForWhatsappFailed,
  }) {
    return initial();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? checkCodeForSmsLoading,
    TResult? Function(CheckCodeResponse checkCodeResponse)?
        checkCodeForSmsSuccess,
    TResult? Function(Failure failure)? checkCodeForSmsFailed,
    TResult? Function()? checkCodeForWhatsappLoading,
    TResult? Function(CheckCodeResponse checkCodeResponse)?
        checkCodeForWhatsappSuccess,
    TResult? Function(Failure failure)? checkCodeForWhatsappFailed,
  }) {
    return initial?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? checkCodeForSmsLoading,
    TResult Function(CheckCodeResponse checkCodeResponse)?
        checkCodeForSmsSuccess,
    TResult Function(Failure failure)? checkCodeForSmsFailed,
    TResult Function()? checkCodeForWhatsappLoading,
    TResult Function(CheckCodeResponse checkCodeResponse)?
        checkCodeForWhatsappSuccess,
    TResult Function(Failure failure)? checkCodeForWhatsappFailed,
    required TResult orElse(),
  }) {
    if (initial != null) {
      return initial();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_CheckCodeForSmsLoading value)
        checkCodeForSmsLoading,
    required TResult Function(_CheckCodeForSmsSuccess value)
        checkCodeForSmsSuccess,
    required TResult Function(_CheckCodeForSmsFailed value)
        checkCodeForSmsFailed,
    required TResult Function(_CheckCodeForWhatsappLoading value)
        checkCodeForWhatsappLoading,
    required TResult Function(_CheckCodeForWhatsappSuccess value)
        checkCodeForWhatsappSuccess,
    required TResult Function(_CheckCodeForWhatsappFailed value)
        checkCodeForWhatsappFailed,
  }) {
    return initial(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_CheckCodeForSmsLoading value)? checkCodeForSmsLoading,
    TResult? Function(_CheckCodeForSmsSuccess value)? checkCodeForSmsSuccess,
    TResult? Function(_CheckCodeForSmsFailed value)? checkCodeForSmsFailed,
    TResult? Function(_CheckCodeForWhatsappLoading value)?
        checkCodeForWhatsappLoading,
    TResult? Function(_CheckCodeForWhatsappSuccess value)?
        checkCodeForWhatsappSuccess,
    TResult? Function(_CheckCodeForWhatsappFailed value)?
        checkCodeForWhatsappFailed,
  }) {
    return initial?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_CheckCodeForSmsLoading value)? checkCodeForSmsLoading,
    TResult Function(_CheckCodeForSmsSuccess value)? checkCodeForSmsSuccess,
    TResult Function(_CheckCodeForSmsFailed value)? checkCodeForSmsFailed,
    TResult Function(_CheckCodeForWhatsappLoading value)?
        checkCodeForWhatsappLoading,
    TResult Function(_CheckCodeForWhatsappSuccess value)?
        checkCodeForWhatsappSuccess,
    TResult Function(_CheckCodeForWhatsappFailed value)?
        checkCodeForWhatsappFailed,
    required TResult orElse(),
  }) {
    if (initial != null) {
      return initial(this);
    }
    return orElse();
  }
}

abstract class _Initial implements CheckCodeState {
  const factory _Initial() = _$InitialImpl;
}

/// @nodoc
abstract class _$$CheckCodeForSmsLoadingImplCopyWith<$Res> {
  factory _$$CheckCodeForSmsLoadingImplCopyWith(
          _$CheckCodeForSmsLoadingImpl value,
          $Res Function(_$CheckCodeForSmsLoadingImpl) then) =
      __$$CheckCodeForSmsLoadingImplCopyWithImpl<$Res>;
}

/// @nodoc
class __$$CheckCodeForSmsLoadingImplCopyWithImpl<$Res>
    extends _$CheckCodeStateCopyWithImpl<$Res, _$CheckCodeForSmsLoadingImpl>
    implements _$$CheckCodeForSmsLoadingImplCopyWith<$Res> {
  __$$CheckCodeForSmsLoadingImplCopyWithImpl(
      _$CheckCodeForSmsLoadingImpl _value,
      $Res Function(_$CheckCodeForSmsLoadingImpl) _then)
      : super(_value, _then);
}

/// @nodoc

class _$CheckCodeForSmsLoadingImpl implements _CheckCodeForSmsLoading {
  const _$CheckCodeForSmsLoadingImpl();

  @override
  String toString() {
    return 'CheckCodeState.checkCodeForSmsLoading()';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$CheckCodeForSmsLoadingImpl);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() checkCodeForSmsLoading,
    required TResult Function(CheckCodeResponse checkCodeResponse)
        checkCodeForSmsSuccess,
    required TResult Function(Failure failure) checkCodeForSmsFailed,
    required TResult Function() checkCodeForWhatsappLoading,
    required TResult Function(CheckCodeResponse checkCodeResponse)
        checkCodeForWhatsappSuccess,
    required TResult Function(Failure failure) checkCodeForWhatsappFailed,
  }) {
    return checkCodeForSmsLoading();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? checkCodeForSmsLoading,
    TResult? Function(CheckCodeResponse checkCodeResponse)?
        checkCodeForSmsSuccess,
    TResult? Function(Failure failure)? checkCodeForSmsFailed,
    TResult? Function()? checkCodeForWhatsappLoading,
    TResult? Function(CheckCodeResponse checkCodeResponse)?
        checkCodeForWhatsappSuccess,
    TResult? Function(Failure failure)? checkCodeForWhatsappFailed,
  }) {
    return checkCodeForSmsLoading?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? checkCodeForSmsLoading,
    TResult Function(CheckCodeResponse checkCodeResponse)?
        checkCodeForSmsSuccess,
    TResult Function(Failure failure)? checkCodeForSmsFailed,
    TResult Function()? checkCodeForWhatsappLoading,
    TResult Function(CheckCodeResponse checkCodeResponse)?
        checkCodeForWhatsappSuccess,
    TResult Function(Failure failure)? checkCodeForWhatsappFailed,
    required TResult orElse(),
  }) {
    if (checkCodeForSmsLoading != null) {
      return checkCodeForSmsLoading();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_CheckCodeForSmsLoading value)
        checkCodeForSmsLoading,
    required TResult Function(_CheckCodeForSmsSuccess value)
        checkCodeForSmsSuccess,
    required TResult Function(_CheckCodeForSmsFailed value)
        checkCodeForSmsFailed,
    required TResult Function(_CheckCodeForWhatsappLoading value)
        checkCodeForWhatsappLoading,
    required TResult Function(_CheckCodeForWhatsappSuccess value)
        checkCodeForWhatsappSuccess,
    required TResult Function(_CheckCodeForWhatsappFailed value)
        checkCodeForWhatsappFailed,
  }) {
    return checkCodeForSmsLoading(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_CheckCodeForSmsLoading value)? checkCodeForSmsLoading,
    TResult? Function(_CheckCodeForSmsSuccess value)? checkCodeForSmsSuccess,
    TResult? Function(_CheckCodeForSmsFailed value)? checkCodeForSmsFailed,
    TResult? Function(_CheckCodeForWhatsappLoading value)?
        checkCodeForWhatsappLoading,
    TResult? Function(_CheckCodeForWhatsappSuccess value)?
        checkCodeForWhatsappSuccess,
    TResult? Function(_CheckCodeForWhatsappFailed value)?
        checkCodeForWhatsappFailed,
  }) {
    return checkCodeForSmsLoading?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_CheckCodeForSmsLoading value)? checkCodeForSmsLoading,
    TResult Function(_CheckCodeForSmsSuccess value)? checkCodeForSmsSuccess,
    TResult Function(_CheckCodeForSmsFailed value)? checkCodeForSmsFailed,
    TResult Function(_CheckCodeForWhatsappLoading value)?
        checkCodeForWhatsappLoading,
    TResult Function(_CheckCodeForWhatsappSuccess value)?
        checkCodeForWhatsappSuccess,
    TResult Function(_CheckCodeForWhatsappFailed value)?
        checkCodeForWhatsappFailed,
    required TResult orElse(),
  }) {
    if (checkCodeForSmsLoading != null) {
      return checkCodeForSmsLoading(this);
    }
    return orElse();
  }
}

abstract class _CheckCodeForSmsLoading implements CheckCodeState {
  const factory _CheckCodeForSmsLoading() = _$CheckCodeForSmsLoadingImpl;
}

/// @nodoc
abstract class _$$CheckCodeForSmsSuccessImplCopyWith<$Res> {
  factory _$$CheckCodeForSmsSuccessImplCopyWith(
          _$CheckCodeForSmsSuccessImpl value,
          $Res Function(_$CheckCodeForSmsSuccessImpl) then) =
      __$$CheckCodeForSmsSuccessImplCopyWithImpl<$Res>;
  @useResult
  $Res call({CheckCodeResponse checkCodeResponse});
}

/// @nodoc
class __$$CheckCodeForSmsSuccessImplCopyWithImpl<$Res>
    extends _$CheckCodeStateCopyWithImpl<$Res, _$CheckCodeForSmsSuccessImpl>
    implements _$$CheckCodeForSmsSuccessImplCopyWith<$Res> {
  __$$CheckCodeForSmsSuccessImplCopyWithImpl(
      _$CheckCodeForSmsSuccessImpl _value,
      $Res Function(_$CheckCodeForSmsSuccessImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? checkCodeResponse = null,
  }) {
    return _then(_$CheckCodeForSmsSuccessImpl(
      null == checkCodeResponse
          ? _value.checkCodeResponse
          : checkCodeResponse // ignore: cast_nullable_to_non_nullable
              as CheckCodeResponse,
    ));
  }
}

/// @nodoc

class _$CheckCodeForSmsSuccessImpl implements _CheckCodeForSmsSuccess {
  const _$CheckCodeForSmsSuccessImpl(this.checkCodeResponse);

  @override
  final CheckCodeResponse checkCodeResponse;

  @override
  String toString() {
    return 'CheckCodeState.checkCodeForSmsSuccess(checkCodeResponse: $checkCodeResponse)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$CheckCodeForSmsSuccessImpl &&
            (identical(other.checkCodeResponse, checkCodeResponse) ||
                other.checkCodeResponse == checkCodeResponse));
  }

  @override
  int get hashCode => Object.hash(runtimeType, checkCodeResponse);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$CheckCodeForSmsSuccessImplCopyWith<_$CheckCodeForSmsSuccessImpl>
      get copyWith => __$$CheckCodeForSmsSuccessImplCopyWithImpl<
          _$CheckCodeForSmsSuccessImpl>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() checkCodeForSmsLoading,
    required TResult Function(CheckCodeResponse checkCodeResponse)
        checkCodeForSmsSuccess,
    required TResult Function(Failure failure) checkCodeForSmsFailed,
    required TResult Function() checkCodeForWhatsappLoading,
    required TResult Function(CheckCodeResponse checkCodeResponse)
        checkCodeForWhatsappSuccess,
    required TResult Function(Failure failure) checkCodeForWhatsappFailed,
  }) {
    return checkCodeForSmsSuccess(checkCodeResponse);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? checkCodeForSmsLoading,
    TResult? Function(CheckCodeResponse checkCodeResponse)?
        checkCodeForSmsSuccess,
    TResult? Function(Failure failure)? checkCodeForSmsFailed,
    TResult? Function()? checkCodeForWhatsappLoading,
    TResult? Function(CheckCodeResponse checkCodeResponse)?
        checkCodeForWhatsappSuccess,
    TResult? Function(Failure failure)? checkCodeForWhatsappFailed,
  }) {
    return checkCodeForSmsSuccess?.call(checkCodeResponse);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? checkCodeForSmsLoading,
    TResult Function(CheckCodeResponse checkCodeResponse)?
        checkCodeForSmsSuccess,
    TResult Function(Failure failure)? checkCodeForSmsFailed,
    TResult Function()? checkCodeForWhatsappLoading,
    TResult Function(CheckCodeResponse checkCodeResponse)?
        checkCodeForWhatsappSuccess,
    TResult Function(Failure failure)? checkCodeForWhatsappFailed,
    required TResult orElse(),
  }) {
    if (checkCodeForSmsSuccess != null) {
      return checkCodeForSmsSuccess(checkCodeResponse);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_CheckCodeForSmsLoading value)
        checkCodeForSmsLoading,
    required TResult Function(_CheckCodeForSmsSuccess value)
        checkCodeForSmsSuccess,
    required TResult Function(_CheckCodeForSmsFailed value)
        checkCodeForSmsFailed,
    required TResult Function(_CheckCodeForWhatsappLoading value)
        checkCodeForWhatsappLoading,
    required TResult Function(_CheckCodeForWhatsappSuccess value)
        checkCodeForWhatsappSuccess,
    required TResult Function(_CheckCodeForWhatsappFailed value)
        checkCodeForWhatsappFailed,
  }) {
    return checkCodeForSmsSuccess(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_CheckCodeForSmsLoading value)? checkCodeForSmsLoading,
    TResult? Function(_CheckCodeForSmsSuccess value)? checkCodeForSmsSuccess,
    TResult? Function(_CheckCodeForSmsFailed value)? checkCodeForSmsFailed,
    TResult? Function(_CheckCodeForWhatsappLoading value)?
        checkCodeForWhatsappLoading,
    TResult? Function(_CheckCodeForWhatsappSuccess value)?
        checkCodeForWhatsappSuccess,
    TResult? Function(_CheckCodeForWhatsappFailed value)?
        checkCodeForWhatsappFailed,
  }) {
    return checkCodeForSmsSuccess?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_CheckCodeForSmsLoading value)? checkCodeForSmsLoading,
    TResult Function(_CheckCodeForSmsSuccess value)? checkCodeForSmsSuccess,
    TResult Function(_CheckCodeForSmsFailed value)? checkCodeForSmsFailed,
    TResult Function(_CheckCodeForWhatsappLoading value)?
        checkCodeForWhatsappLoading,
    TResult Function(_CheckCodeForWhatsappSuccess value)?
        checkCodeForWhatsappSuccess,
    TResult Function(_CheckCodeForWhatsappFailed value)?
        checkCodeForWhatsappFailed,
    required TResult orElse(),
  }) {
    if (checkCodeForSmsSuccess != null) {
      return checkCodeForSmsSuccess(this);
    }
    return orElse();
  }
}

abstract class _CheckCodeForSmsSuccess implements CheckCodeState {
  const factory _CheckCodeForSmsSuccess(
      final CheckCodeResponse checkCodeResponse) = _$CheckCodeForSmsSuccessImpl;

  CheckCodeResponse get checkCodeResponse;
  @JsonKey(ignore: true)
  _$$CheckCodeForSmsSuccessImplCopyWith<_$CheckCodeForSmsSuccessImpl>
      get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$CheckCodeForSmsFailedImplCopyWith<$Res> {
  factory _$$CheckCodeForSmsFailedImplCopyWith(
          _$CheckCodeForSmsFailedImpl value,
          $Res Function(_$CheckCodeForSmsFailedImpl) then) =
      __$$CheckCodeForSmsFailedImplCopyWithImpl<$Res>;
  @useResult
  $Res call({Failure failure});
}

/// @nodoc
class __$$CheckCodeForSmsFailedImplCopyWithImpl<$Res>
    extends _$CheckCodeStateCopyWithImpl<$Res, _$CheckCodeForSmsFailedImpl>
    implements _$$CheckCodeForSmsFailedImplCopyWith<$Res> {
  __$$CheckCodeForSmsFailedImplCopyWithImpl(_$CheckCodeForSmsFailedImpl _value,
      $Res Function(_$CheckCodeForSmsFailedImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? failure = null,
  }) {
    return _then(_$CheckCodeForSmsFailedImpl(
      null == failure
          ? _value.failure
          : failure // ignore: cast_nullable_to_non_nullable
              as Failure,
    ));
  }
}

/// @nodoc

class _$CheckCodeForSmsFailedImpl implements _CheckCodeForSmsFailed {
  const _$CheckCodeForSmsFailedImpl(this.failure);

  @override
  final Failure failure;

  @override
  String toString() {
    return 'CheckCodeState.checkCodeForSmsFailed(failure: $failure)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$CheckCodeForSmsFailedImpl &&
            (identical(other.failure, failure) || other.failure == failure));
  }

  @override
  int get hashCode => Object.hash(runtimeType, failure);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$CheckCodeForSmsFailedImplCopyWith<_$CheckCodeForSmsFailedImpl>
      get copyWith => __$$CheckCodeForSmsFailedImplCopyWithImpl<
          _$CheckCodeForSmsFailedImpl>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() checkCodeForSmsLoading,
    required TResult Function(CheckCodeResponse checkCodeResponse)
        checkCodeForSmsSuccess,
    required TResult Function(Failure failure) checkCodeForSmsFailed,
    required TResult Function() checkCodeForWhatsappLoading,
    required TResult Function(CheckCodeResponse checkCodeResponse)
        checkCodeForWhatsappSuccess,
    required TResult Function(Failure failure) checkCodeForWhatsappFailed,
  }) {
    return checkCodeForSmsFailed(failure);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? checkCodeForSmsLoading,
    TResult? Function(CheckCodeResponse checkCodeResponse)?
        checkCodeForSmsSuccess,
    TResult? Function(Failure failure)? checkCodeForSmsFailed,
    TResult? Function()? checkCodeForWhatsappLoading,
    TResult? Function(CheckCodeResponse checkCodeResponse)?
        checkCodeForWhatsappSuccess,
    TResult? Function(Failure failure)? checkCodeForWhatsappFailed,
  }) {
    return checkCodeForSmsFailed?.call(failure);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? checkCodeForSmsLoading,
    TResult Function(CheckCodeResponse checkCodeResponse)?
        checkCodeForSmsSuccess,
    TResult Function(Failure failure)? checkCodeForSmsFailed,
    TResult Function()? checkCodeForWhatsappLoading,
    TResult Function(CheckCodeResponse checkCodeResponse)?
        checkCodeForWhatsappSuccess,
    TResult Function(Failure failure)? checkCodeForWhatsappFailed,
    required TResult orElse(),
  }) {
    if (checkCodeForSmsFailed != null) {
      return checkCodeForSmsFailed(failure);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_CheckCodeForSmsLoading value)
        checkCodeForSmsLoading,
    required TResult Function(_CheckCodeForSmsSuccess value)
        checkCodeForSmsSuccess,
    required TResult Function(_CheckCodeForSmsFailed value)
        checkCodeForSmsFailed,
    required TResult Function(_CheckCodeForWhatsappLoading value)
        checkCodeForWhatsappLoading,
    required TResult Function(_CheckCodeForWhatsappSuccess value)
        checkCodeForWhatsappSuccess,
    required TResult Function(_CheckCodeForWhatsappFailed value)
        checkCodeForWhatsappFailed,
  }) {
    return checkCodeForSmsFailed(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_CheckCodeForSmsLoading value)? checkCodeForSmsLoading,
    TResult? Function(_CheckCodeForSmsSuccess value)? checkCodeForSmsSuccess,
    TResult? Function(_CheckCodeForSmsFailed value)? checkCodeForSmsFailed,
    TResult? Function(_CheckCodeForWhatsappLoading value)?
        checkCodeForWhatsappLoading,
    TResult? Function(_CheckCodeForWhatsappSuccess value)?
        checkCodeForWhatsappSuccess,
    TResult? Function(_CheckCodeForWhatsappFailed value)?
        checkCodeForWhatsappFailed,
  }) {
    return checkCodeForSmsFailed?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_CheckCodeForSmsLoading value)? checkCodeForSmsLoading,
    TResult Function(_CheckCodeForSmsSuccess value)? checkCodeForSmsSuccess,
    TResult Function(_CheckCodeForSmsFailed value)? checkCodeForSmsFailed,
    TResult Function(_CheckCodeForWhatsappLoading value)?
        checkCodeForWhatsappLoading,
    TResult Function(_CheckCodeForWhatsappSuccess value)?
        checkCodeForWhatsappSuccess,
    TResult Function(_CheckCodeForWhatsappFailed value)?
        checkCodeForWhatsappFailed,
    required TResult orElse(),
  }) {
    if (checkCodeForSmsFailed != null) {
      return checkCodeForSmsFailed(this);
    }
    return orElse();
  }
}

abstract class _CheckCodeForSmsFailed implements CheckCodeState {
  const factory _CheckCodeForSmsFailed(final Failure failure) =
      _$CheckCodeForSmsFailedImpl;

  Failure get failure;
  @JsonKey(ignore: true)
  _$$CheckCodeForSmsFailedImplCopyWith<_$CheckCodeForSmsFailedImpl>
      get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$CheckCodeForWhatsappLoadingImplCopyWith<$Res> {
  factory _$$CheckCodeForWhatsappLoadingImplCopyWith(
          _$CheckCodeForWhatsappLoadingImpl value,
          $Res Function(_$CheckCodeForWhatsappLoadingImpl) then) =
      __$$CheckCodeForWhatsappLoadingImplCopyWithImpl<$Res>;
}

/// @nodoc
class __$$CheckCodeForWhatsappLoadingImplCopyWithImpl<$Res>
    extends _$CheckCodeStateCopyWithImpl<$Res,
        _$CheckCodeForWhatsappLoadingImpl>
    implements _$$CheckCodeForWhatsappLoadingImplCopyWith<$Res> {
  __$$CheckCodeForWhatsappLoadingImplCopyWithImpl(
      _$CheckCodeForWhatsappLoadingImpl _value,
      $Res Function(_$CheckCodeForWhatsappLoadingImpl) _then)
      : super(_value, _then);
}

/// @nodoc

class _$CheckCodeForWhatsappLoadingImpl
    implements _CheckCodeForWhatsappLoading {
  const _$CheckCodeForWhatsappLoadingImpl();

  @override
  String toString() {
    return 'CheckCodeState.checkCodeForWhatsappLoading()';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$CheckCodeForWhatsappLoadingImpl);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() checkCodeForSmsLoading,
    required TResult Function(CheckCodeResponse checkCodeResponse)
        checkCodeForSmsSuccess,
    required TResult Function(Failure failure) checkCodeForSmsFailed,
    required TResult Function() checkCodeForWhatsappLoading,
    required TResult Function(CheckCodeResponse checkCodeResponse)
        checkCodeForWhatsappSuccess,
    required TResult Function(Failure failure) checkCodeForWhatsappFailed,
  }) {
    return checkCodeForWhatsappLoading();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? checkCodeForSmsLoading,
    TResult? Function(CheckCodeResponse checkCodeResponse)?
        checkCodeForSmsSuccess,
    TResult? Function(Failure failure)? checkCodeForSmsFailed,
    TResult? Function()? checkCodeForWhatsappLoading,
    TResult? Function(CheckCodeResponse checkCodeResponse)?
        checkCodeForWhatsappSuccess,
    TResult? Function(Failure failure)? checkCodeForWhatsappFailed,
  }) {
    return checkCodeForWhatsappLoading?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? checkCodeForSmsLoading,
    TResult Function(CheckCodeResponse checkCodeResponse)?
        checkCodeForSmsSuccess,
    TResult Function(Failure failure)? checkCodeForSmsFailed,
    TResult Function()? checkCodeForWhatsappLoading,
    TResult Function(CheckCodeResponse checkCodeResponse)?
        checkCodeForWhatsappSuccess,
    TResult Function(Failure failure)? checkCodeForWhatsappFailed,
    required TResult orElse(),
  }) {
    if (checkCodeForWhatsappLoading != null) {
      return checkCodeForWhatsappLoading();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_CheckCodeForSmsLoading value)
        checkCodeForSmsLoading,
    required TResult Function(_CheckCodeForSmsSuccess value)
        checkCodeForSmsSuccess,
    required TResult Function(_CheckCodeForSmsFailed value)
        checkCodeForSmsFailed,
    required TResult Function(_CheckCodeForWhatsappLoading value)
        checkCodeForWhatsappLoading,
    required TResult Function(_CheckCodeForWhatsappSuccess value)
        checkCodeForWhatsappSuccess,
    required TResult Function(_CheckCodeForWhatsappFailed value)
        checkCodeForWhatsappFailed,
  }) {
    return checkCodeForWhatsappLoading(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_CheckCodeForSmsLoading value)? checkCodeForSmsLoading,
    TResult? Function(_CheckCodeForSmsSuccess value)? checkCodeForSmsSuccess,
    TResult? Function(_CheckCodeForSmsFailed value)? checkCodeForSmsFailed,
    TResult? Function(_CheckCodeForWhatsappLoading value)?
        checkCodeForWhatsappLoading,
    TResult? Function(_CheckCodeForWhatsappSuccess value)?
        checkCodeForWhatsappSuccess,
    TResult? Function(_CheckCodeForWhatsappFailed value)?
        checkCodeForWhatsappFailed,
  }) {
    return checkCodeForWhatsappLoading?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_CheckCodeForSmsLoading value)? checkCodeForSmsLoading,
    TResult Function(_CheckCodeForSmsSuccess value)? checkCodeForSmsSuccess,
    TResult Function(_CheckCodeForSmsFailed value)? checkCodeForSmsFailed,
    TResult Function(_CheckCodeForWhatsappLoading value)?
        checkCodeForWhatsappLoading,
    TResult Function(_CheckCodeForWhatsappSuccess value)?
        checkCodeForWhatsappSuccess,
    TResult Function(_CheckCodeForWhatsappFailed value)?
        checkCodeForWhatsappFailed,
    required TResult orElse(),
  }) {
    if (checkCodeForWhatsappLoading != null) {
      return checkCodeForWhatsappLoading(this);
    }
    return orElse();
  }
}

abstract class _CheckCodeForWhatsappLoading implements CheckCodeState {
  const factory _CheckCodeForWhatsappLoading() =
      _$CheckCodeForWhatsappLoadingImpl;
}

/// @nodoc
abstract class _$$CheckCodeForWhatsappSuccessImplCopyWith<$Res> {
  factory _$$CheckCodeForWhatsappSuccessImplCopyWith(
          _$CheckCodeForWhatsappSuccessImpl value,
          $Res Function(_$CheckCodeForWhatsappSuccessImpl) then) =
      __$$CheckCodeForWhatsappSuccessImplCopyWithImpl<$Res>;
  @useResult
  $Res call({CheckCodeResponse checkCodeResponse});
}

/// @nodoc
class __$$CheckCodeForWhatsappSuccessImplCopyWithImpl<$Res>
    extends _$CheckCodeStateCopyWithImpl<$Res,
        _$CheckCodeForWhatsappSuccessImpl>
    implements _$$CheckCodeForWhatsappSuccessImplCopyWith<$Res> {
  __$$CheckCodeForWhatsappSuccessImplCopyWithImpl(
      _$CheckCodeForWhatsappSuccessImpl _value,
      $Res Function(_$CheckCodeForWhatsappSuccessImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? checkCodeResponse = null,
  }) {
    return _then(_$CheckCodeForWhatsappSuccessImpl(
      null == checkCodeResponse
          ? _value.checkCodeResponse
          : checkCodeResponse // ignore: cast_nullable_to_non_nullable
              as CheckCodeResponse,
    ));
  }
}

/// @nodoc

class _$CheckCodeForWhatsappSuccessImpl
    implements _CheckCodeForWhatsappSuccess {
  const _$CheckCodeForWhatsappSuccessImpl(this.checkCodeResponse);

  @override
  final CheckCodeResponse checkCodeResponse;

  @override
  String toString() {
    return 'CheckCodeState.checkCodeForWhatsappSuccess(checkCodeResponse: $checkCodeResponse)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$CheckCodeForWhatsappSuccessImpl &&
            (identical(other.checkCodeResponse, checkCodeResponse) ||
                other.checkCodeResponse == checkCodeResponse));
  }

  @override
  int get hashCode => Object.hash(runtimeType, checkCodeResponse);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$CheckCodeForWhatsappSuccessImplCopyWith<_$CheckCodeForWhatsappSuccessImpl>
      get copyWith => __$$CheckCodeForWhatsappSuccessImplCopyWithImpl<
          _$CheckCodeForWhatsappSuccessImpl>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() checkCodeForSmsLoading,
    required TResult Function(CheckCodeResponse checkCodeResponse)
        checkCodeForSmsSuccess,
    required TResult Function(Failure failure) checkCodeForSmsFailed,
    required TResult Function() checkCodeForWhatsappLoading,
    required TResult Function(CheckCodeResponse checkCodeResponse)
        checkCodeForWhatsappSuccess,
    required TResult Function(Failure failure) checkCodeForWhatsappFailed,
  }) {
    return checkCodeForWhatsappSuccess(checkCodeResponse);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? checkCodeForSmsLoading,
    TResult? Function(CheckCodeResponse checkCodeResponse)?
        checkCodeForSmsSuccess,
    TResult? Function(Failure failure)? checkCodeForSmsFailed,
    TResult? Function()? checkCodeForWhatsappLoading,
    TResult? Function(CheckCodeResponse checkCodeResponse)?
        checkCodeForWhatsappSuccess,
    TResult? Function(Failure failure)? checkCodeForWhatsappFailed,
  }) {
    return checkCodeForWhatsappSuccess?.call(checkCodeResponse);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? checkCodeForSmsLoading,
    TResult Function(CheckCodeResponse checkCodeResponse)?
        checkCodeForSmsSuccess,
    TResult Function(Failure failure)? checkCodeForSmsFailed,
    TResult Function()? checkCodeForWhatsappLoading,
    TResult Function(CheckCodeResponse checkCodeResponse)?
        checkCodeForWhatsappSuccess,
    TResult Function(Failure failure)? checkCodeForWhatsappFailed,
    required TResult orElse(),
  }) {
    if (checkCodeForWhatsappSuccess != null) {
      return checkCodeForWhatsappSuccess(checkCodeResponse);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_CheckCodeForSmsLoading value)
        checkCodeForSmsLoading,
    required TResult Function(_CheckCodeForSmsSuccess value)
        checkCodeForSmsSuccess,
    required TResult Function(_CheckCodeForSmsFailed value)
        checkCodeForSmsFailed,
    required TResult Function(_CheckCodeForWhatsappLoading value)
        checkCodeForWhatsappLoading,
    required TResult Function(_CheckCodeForWhatsappSuccess value)
        checkCodeForWhatsappSuccess,
    required TResult Function(_CheckCodeForWhatsappFailed value)
        checkCodeForWhatsappFailed,
  }) {
    return checkCodeForWhatsappSuccess(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_CheckCodeForSmsLoading value)? checkCodeForSmsLoading,
    TResult? Function(_CheckCodeForSmsSuccess value)? checkCodeForSmsSuccess,
    TResult? Function(_CheckCodeForSmsFailed value)? checkCodeForSmsFailed,
    TResult? Function(_CheckCodeForWhatsappLoading value)?
        checkCodeForWhatsappLoading,
    TResult? Function(_CheckCodeForWhatsappSuccess value)?
        checkCodeForWhatsappSuccess,
    TResult? Function(_CheckCodeForWhatsappFailed value)?
        checkCodeForWhatsappFailed,
  }) {
    return checkCodeForWhatsappSuccess?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_CheckCodeForSmsLoading value)? checkCodeForSmsLoading,
    TResult Function(_CheckCodeForSmsSuccess value)? checkCodeForSmsSuccess,
    TResult Function(_CheckCodeForSmsFailed value)? checkCodeForSmsFailed,
    TResult Function(_CheckCodeForWhatsappLoading value)?
        checkCodeForWhatsappLoading,
    TResult Function(_CheckCodeForWhatsappSuccess value)?
        checkCodeForWhatsappSuccess,
    TResult Function(_CheckCodeForWhatsappFailed value)?
        checkCodeForWhatsappFailed,
    required TResult orElse(),
  }) {
    if (checkCodeForWhatsappSuccess != null) {
      return checkCodeForWhatsappSuccess(this);
    }
    return orElse();
  }
}

abstract class _CheckCodeForWhatsappSuccess implements CheckCodeState {
  const factory _CheckCodeForWhatsappSuccess(
          final CheckCodeResponse checkCodeResponse) =
      _$CheckCodeForWhatsappSuccessImpl;

  CheckCodeResponse get checkCodeResponse;
  @JsonKey(ignore: true)
  _$$CheckCodeForWhatsappSuccessImplCopyWith<_$CheckCodeForWhatsappSuccessImpl>
      get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$CheckCodeForWhatsappFailedImplCopyWith<$Res> {
  factory _$$CheckCodeForWhatsappFailedImplCopyWith(
          _$CheckCodeForWhatsappFailedImpl value,
          $Res Function(_$CheckCodeForWhatsappFailedImpl) then) =
      __$$CheckCodeForWhatsappFailedImplCopyWithImpl<$Res>;
  @useResult
  $Res call({Failure failure});
}

/// @nodoc
class __$$CheckCodeForWhatsappFailedImplCopyWithImpl<$Res>
    extends _$CheckCodeStateCopyWithImpl<$Res, _$CheckCodeForWhatsappFailedImpl>
    implements _$$CheckCodeForWhatsappFailedImplCopyWith<$Res> {
  __$$CheckCodeForWhatsappFailedImplCopyWithImpl(
      _$CheckCodeForWhatsappFailedImpl _value,
      $Res Function(_$CheckCodeForWhatsappFailedImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? failure = null,
  }) {
    return _then(_$CheckCodeForWhatsappFailedImpl(
      null == failure
          ? _value.failure
          : failure // ignore: cast_nullable_to_non_nullable
              as Failure,
    ));
  }
}

/// @nodoc

class _$CheckCodeForWhatsappFailedImpl implements _CheckCodeForWhatsappFailed {
  const _$CheckCodeForWhatsappFailedImpl(this.failure);

  @override
  final Failure failure;

  @override
  String toString() {
    return 'CheckCodeState.checkCodeForWhatsappFailed(failure: $failure)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$CheckCodeForWhatsappFailedImpl &&
            (identical(other.failure, failure) || other.failure == failure));
  }

  @override
  int get hashCode => Object.hash(runtimeType, failure);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$CheckCodeForWhatsappFailedImplCopyWith<_$CheckCodeForWhatsappFailedImpl>
      get copyWith => __$$CheckCodeForWhatsappFailedImplCopyWithImpl<
          _$CheckCodeForWhatsappFailedImpl>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() checkCodeForSmsLoading,
    required TResult Function(CheckCodeResponse checkCodeResponse)
        checkCodeForSmsSuccess,
    required TResult Function(Failure failure) checkCodeForSmsFailed,
    required TResult Function() checkCodeForWhatsappLoading,
    required TResult Function(CheckCodeResponse checkCodeResponse)
        checkCodeForWhatsappSuccess,
    required TResult Function(Failure failure) checkCodeForWhatsappFailed,
  }) {
    return checkCodeForWhatsappFailed(failure);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? checkCodeForSmsLoading,
    TResult? Function(CheckCodeResponse checkCodeResponse)?
        checkCodeForSmsSuccess,
    TResult? Function(Failure failure)? checkCodeForSmsFailed,
    TResult? Function()? checkCodeForWhatsappLoading,
    TResult? Function(CheckCodeResponse checkCodeResponse)?
        checkCodeForWhatsappSuccess,
    TResult? Function(Failure failure)? checkCodeForWhatsappFailed,
  }) {
    return checkCodeForWhatsappFailed?.call(failure);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? checkCodeForSmsLoading,
    TResult Function(CheckCodeResponse checkCodeResponse)?
        checkCodeForSmsSuccess,
    TResult Function(Failure failure)? checkCodeForSmsFailed,
    TResult Function()? checkCodeForWhatsappLoading,
    TResult Function(CheckCodeResponse checkCodeResponse)?
        checkCodeForWhatsappSuccess,
    TResult Function(Failure failure)? checkCodeForWhatsappFailed,
    required TResult orElse(),
  }) {
    if (checkCodeForWhatsappFailed != null) {
      return checkCodeForWhatsappFailed(failure);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_CheckCodeForSmsLoading value)
        checkCodeForSmsLoading,
    required TResult Function(_CheckCodeForSmsSuccess value)
        checkCodeForSmsSuccess,
    required TResult Function(_CheckCodeForSmsFailed value)
        checkCodeForSmsFailed,
    required TResult Function(_CheckCodeForWhatsappLoading value)
        checkCodeForWhatsappLoading,
    required TResult Function(_CheckCodeForWhatsappSuccess value)
        checkCodeForWhatsappSuccess,
    required TResult Function(_CheckCodeForWhatsappFailed value)
        checkCodeForWhatsappFailed,
  }) {
    return checkCodeForWhatsappFailed(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_CheckCodeForSmsLoading value)? checkCodeForSmsLoading,
    TResult? Function(_CheckCodeForSmsSuccess value)? checkCodeForSmsSuccess,
    TResult? Function(_CheckCodeForSmsFailed value)? checkCodeForSmsFailed,
    TResult? Function(_CheckCodeForWhatsappLoading value)?
        checkCodeForWhatsappLoading,
    TResult? Function(_CheckCodeForWhatsappSuccess value)?
        checkCodeForWhatsappSuccess,
    TResult? Function(_CheckCodeForWhatsappFailed value)?
        checkCodeForWhatsappFailed,
  }) {
    return checkCodeForWhatsappFailed?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_CheckCodeForSmsLoading value)? checkCodeForSmsLoading,
    TResult Function(_CheckCodeForSmsSuccess value)? checkCodeForSmsSuccess,
    TResult Function(_CheckCodeForSmsFailed value)? checkCodeForSmsFailed,
    TResult Function(_CheckCodeForWhatsappLoading value)?
        checkCodeForWhatsappLoading,
    TResult Function(_CheckCodeForWhatsappSuccess value)?
        checkCodeForWhatsappSuccess,
    TResult Function(_CheckCodeForWhatsappFailed value)?
        checkCodeForWhatsappFailed,
    required TResult orElse(),
  }) {
    if (checkCodeForWhatsappFailed != null) {
      return checkCodeForWhatsappFailed(this);
    }
    return orElse();
  }
}

abstract class _CheckCodeForWhatsappFailed implements CheckCodeState {
  const factory _CheckCodeForWhatsappFailed(final Failure failure) =
      _$CheckCodeForWhatsappFailedImpl;

  Failure get failure;
  @JsonKey(ignore: true)
  _$$CheckCodeForWhatsappFailedImplCopyWith<_$CheckCodeForWhatsappFailedImpl>
      get copyWith => throw _privateConstructorUsedError;
}
