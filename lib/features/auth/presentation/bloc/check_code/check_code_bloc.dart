import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_functions.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_validators.dart';
import 'package:meedigo_company_sales_person/core/networking/api_failure.dart';
import 'package:meedigo_company_sales_person/core/networking/api_requests.dart';
import 'package:meedigo_company_sales_person/features/auth/domain/entities/check_code.dart';
import 'package:meedigo_company_sales_person/features/auth/presentation/bloc/login/login_bloc.dart';

import '../../../domain/usecases/check_code_for_sms_usecase.dart';
import '../../../domain/usecases/check_code_for_whatsapp_usecase.dart';

part 'check_code_event.dart';
part 'check_code_state.dart';
part 'check_code_bloc.freezed.dart';

class CheckCodeBloc extends Bloc<CheckCodeEvent, CheckCodeState> {
  final CheckCodeForSmsUseCase checkCodeForSmsUseCase;
  final CheckCodeForWhatsappUseCase checkCodeForWhatsappUseCase;

  final TextEditingController phoneController = TextEditingController();
  final TextEditingController otpController = TextEditingController();

  bool isWhatsapp = false;

  CheckCodeBloc({
    required this.checkCodeForSmsUseCase,
    required this.checkCodeForWhatsappUseCase,
  }) : super(const CheckCodeState.initial()) {
    on<CheckCodeEvent>(
      (event, emit) async {
        await event.when(
          checkCodeForSms: () async {
            emit(CheckCodeState.checkCodeForSmsLoading());
            final failureOrCheckCodeForSms = await checkCodeForSmsUseCase(
              CheckCodeForSmsAndWhatsappRequestBody(
                phoneNumber: phoneController.text,
                code: otpController.text,
              ),
            );
            failureOrCheckCodeForSms.fold(
              (l) => emit(
                CheckCodeState.checkCodeForSmsFailed(l),
              ),
              (r) => emit(
                CheckCodeState.checkCodeForSmsSuccess(r),
              ),
            );
          },
          checkCodeForWhatsapp: () async {
            emit(CheckCodeState.checkCodeForWhatsappLoading());
            final failureOrCheckCodeForWhatsapp =
                await checkCodeForWhatsappUseCase(
              CheckCodeForSmsAndWhatsappRequestBody(
                phoneNumber: phoneController.text,
                code: otpController.text,
              ),
            );
            failureOrCheckCodeForWhatsapp.fold(
              (l) => emit(
                CheckCodeState.checkCodeForWhatsappFailed(l),
              ),
              (r) => emit(
                CheckCodeState.checkCodeForWhatsappSuccess(r),
              ),
            );
          },
        );
      },
    );
  }

  /// function to validate otp input field
  validateOtpInputField({
    required BuildContext context,
    required String? input,
  }) {
    return AppValidators.validateOtpCodeField(
      input: input,
      context: context,
    );
  }

  /// handle otp input field when completed
  /// if auth provider is whatsapp => call check otp code for whatsapp
  /// else call check otp code for sms
  onCompleteOtpInputField({required BuildContext context}) {
    if (otpController.text.length == 5) {
      checkCode();
    }
  }

  /// function to call code check api
  checkCode() {
    if (isWhatsapp) {
      add(CheckCodeEvent.checkCodeForWhatsapp());
    } else {
      add(CheckCodeEvent.checkCodeForSms());
    }
  }

  /// show message that the
  /// sms or whatsapp code check process has failed
  showSmsOrWhatsappCodeCheckFailedMessage({
    required BuildContext context,
    required Failure failure,
  }) {
    AppFunctions.showFailedMessage(
      context: context,
      failure: failure,
    );
  }

  makeLogin(BuildContext context) async {
    LoginRequestBody loginRequestBody = LoginRequestBody(
      phoneNumber: phoneController.text,
      code: otpController.text,
      firebaseToken: '',
    );
    context.read<LoginBloc>()
      ..add(
        LoginEvent.login(
          loginRequestBody,
        ),
      );
  }
}
