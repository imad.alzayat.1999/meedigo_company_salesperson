part of 'check_code_bloc.dart';

@freezed
class CheckCodeState with _$CheckCodeState {
  const factory CheckCodeState.initial() = _Initial;

  /// check code states for sms
  const factory CheckCodeState.checkCodeForSmsLoading() =
      _CheckCodeForSmsLoading;
  const factory CheckCodeState.checkCodeForSmsSuccess(
    CheckCodeResponse checkCodeResponse,
  ) = _CheckCodeForSmsSuccess;
  const factory CheckCodeState.checkCodeForSmsFailed(
    Failure failure,
  ) = _CheckCodeForSmsFailed;

  /// check code states for whatsapp
  const factory CheckCodeState.checkCodeForWhatsappLoading() =
      _CheckCodeForWhatsappLoading;
  const factory CheckCodeState.checkCodeForWhatsappSuccess(
    CheckCodeResponse checkCodeResponse,
  ) = _CheckCodeForWhatsappSuccess;
  const factory CheckCodeState.checkCodeForWhatsappFailed(
    Failure failure,
  ) = _CheckCodeForWhatsappFailed;
}
