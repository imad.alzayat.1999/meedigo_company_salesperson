part of 'check_code_bloc.dart';

@freezed
class CheckCodeEvent with _$CheckCodeEvent {
  const factory CheckCodeEvent.checkCodeForSms() = _CheckCodeForSms;
  const factory CheckCodeEvent.checkCodeForWhatsapp() = _CheckCodeForWhatsapp;
}
