part of 'generate_code_bloc.dart';

@freezed
class GenerateCodeEvent with _$GenerateCodeEvent {
  const factory GenerateCodeEvent.generateCodeForSms() = _GenerateCodeForSms;
  const factory GenerateCodeEvent.generateCodeForWhatsapp() =
      _GenerateCodeForWhatsapp;
}
