part of 'generate_code_bloc.dart';

@freezed
class GenerateCodeState with _$GenerateCodeState {
  const factory GenerateCodeState.initial() = _Initial;

  /// generate code for sms states
  const factory GenerateCodeState.generateCodeForSmsLoading() =
      _GenerateCodeForSmsLoading;
  const factory GenerateCodeState.generateCodeForSmsSuccess(
    GenerateCodeResponse generateCodeResponse,
  ) = _GenerateCodeForSmsSuccess;
  const factory GenerateCodeState.generateCodeForSmsFailed(
    Failure failure,
  ) = _GenerateCodeForSmsFailed;

  /// generate code for whatsapp states
  const factory GenerateCodeState.generateCodeForWhatsappLoading() =
      _GenerateCodeForWhatsappLoading;
  const factory GenerateCodeState.generateCodeForWhatsappSuccess(
    GenerateCodeResponse generateCodeResponse,
  ) = _GenerateCodeForWhatsappSuccess;
  const factory GenerateCodeState.generateCodeForWhatsappFailed(
    Failure failure,
  ) = _GenerateCodeForWhatsappFailed;
}
