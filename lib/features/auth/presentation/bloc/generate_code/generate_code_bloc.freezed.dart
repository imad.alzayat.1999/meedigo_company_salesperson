// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'generate_code_bloc.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

/// @nodoc
mixin _$GenerateCodeEvent {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() generateCodeForSms,
    required TResult Function() generateCodeForWhatsapp,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? generateCodeForSms,
    TResult? Function()? generateCodeForWhatsapp,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? generateCodeForSms,
    TResult Function()? generateCodeForWhatsapp,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_GenerateCodeForSms value) generateCodeForSms,
    required TResult Function(_GenerateCodeForWhatsapp value)
        generateCodeForWhatsapp,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_GenerateCodeForSms value)? generateCodeForSms,
    TResult? Function(_GenerateCodeForWhatsapp value)? generateCodeForWhatsapp,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_GenerateCodeForSms value)? generateCodeForSms,
    TResult Function(_GenerateCodeForWhatsapp value)? generateCodeForWhatsapp,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $GenerateCodeEventCopyWith<$Res> {
  factory $GenerateCodeEventCopyWith(
          GenerateCodeEvent value, $Res Function(GenerateCodeEvent) then) =
      _$GenerateCodeEventCopyWithImpl<$Res, GenerateCodeEvent>;
}

/// @nodoc
class _$GenerateCodeEventCopyWithImpl<$Res, $Val extends GenerateCodeEvent>
    implements $GenerateCodeEventCopyWith<$Res> {
  _$GenerateCodeEventCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$GenerateCodeForSmsImplCopyWith<$Res> {
  factory _$$GenerateCodeForSmsImplCopyWith(_$GenerateCodeForSmsImpl value,
          $Res Function(_$GenerateCodeForSmsImpl) then) =
      __$$GenerateCodeForSmsImplCopyWithImpl<$Res>;
}

/// @nodoc
class __$$GenerateCodeForSmsImplCopyWithImpl<$Res>
    extends _$GenerateCodeEventCopyWithImpl<$Res, _$GenerateCodeForSmsImpl>
    implements _$$GenerateCodeForSmsImplCopyWith<$Res> {
  __$$GenerateCodeForSmsImplCopyWithImpl(_$GenerateCodeForSmsImpl _value,
      $Res Function(_$GenerateCodeForSmsImpl) _then)
      : super(_value, _then);
}

/// @nodoc

class _$GenerateCodeForSmsImpl implements _GenerateCodeForSms {
  const _$GenerateCodeForSmsImpl();

  @override
  String toString() {
    return 'GenerateCodeEvent.generateCodeForSms()';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$GenerateCodeForSmsImpl);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() generateCodeForSms,
    required TResult Function() generateCodeForWhatsapp,
  }) {
    return generateCodeForSms();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? generateCodeForSms,
    TResult? Function()? generateCodeForWhatsapp,
  }) {
    return generateCodeForSms?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? generateCodeForSms,
    TResult Function()? generateCodeForWhatsapp,
    required TResult orElse(),
  }) {
    if (generateCodeForSms != null) {
      return generateCodeForSms();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_GenerateCodeForSms value) generateCodeForSms,
    required TResult Function(_GenerateCodeForWhatsapp value)
        generateCodeForWhatsapp,
  }) {
    return generateCodeForSms(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_GenerateCodeForSms value)? generateCodeForSms,
    TResult? Function(_GenerateCodeForWhatsapp value)? generateCodeForWhatsapp,
  }) {
    return generateCodeForSms?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_GenerateCodeForSms value)? generateCodeForSms,
    TResult Function(_GenerateCodeForWhatsapp value)? generateCodeForWhatsapp,
    required TResult orElse(),
  }) {
    if (generateCodeForSms != null) {
      return generateCodeForSms(this);
    }
    return orElse();
  }
}

abstract class _GenerateCodeForSms implements GenerateCodeEvent {
  const factory _GenerateCodeForSms() = _$GenerateCodeForSmsImpl;
}

/// @nodoc
abstract class _$$GenerateCodeForWhatsappImplCopyWith<$Res> {
  factory _$$GenerateCodeForWhatsappImplCopyWith(
          _$GenerateCodeForWhatsappImpl value,
          $Res Function(_$GenerateCodeForWhatsappImpl) then) =
      __$$GenerateCodeForWhatsappImplCopyWithImpl<$Res>;
}

/// @nodoc
class __$$GenerateCodeForWhatsappImplCopyWithImpl<$Res>
    extends _$GenerateCodeEventCopyWithImpl<$Res, _$GenerateCodeForWhatsappImpl>
    implements _$$GenerateCodeForWhatsappImplCopyWith<$Res> {
  __$$GenerateCodeForWhatsappImplCopyWithImpl(
      _$GenerateCodeForWhatsappImpl _value,
      $Res Function(_$GenerateCodeForWhatsappImpl) _then)
      : super(_value, _then);
}

/// @nodoc

class _$GenerateCodeForWhatsappImpl implements _GenerateCodeForWhatsapp {
  const _$GenerateCodeForWhatsappImpl();

  @override
  String toString() {
    return 'GenerateCodeEvent.generateCodeForWhatsapp()';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$GenerateCodeForWhatsappImpl);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() generateCodeForSms,
    required TResult Function() generateCodeForWhatsapp,
  }) {
    return generateCodeForWhatsapp();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? generateCodeForSms,
    TResult? Function()? generateCodeForWhatsapp,
  }) {
    return generateCodeForWhatsapp?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? generateCodeForSms,
    TResult Function()? generateCodeForWhatsapp,
    required TResult orElse(),
  }) {
    if (generateCodeForWhatsapp != null) {
      return generateCodeForWhatsapp();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_GenerateCodeForSms value) generateCodeForSms,
    required TResult Function(_GenerateCodeForWhatsapp value)
        generateCodeForWhatsapp,
  }) {
    return generateCodeForWhatsapp(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_GenerateCodeForSms value)? generateCodeForSms,
    TResult? Function(_GenerateCodeForWhatsapp value)? generateCodeForWhatsapp,
  }) {
    return generateCodeForWhatsapp?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_GenerateCodeForSms value)? generateCodeForSms,
    TResult Function(_GenerateCodeForWhatsapp value)? generateCodeForWhatsapp,
    required TResult orElse(),
  }) {
    if (generateCodeForWhatsapp != null) {
      return generateCodeForWhatsapp(this);
    }
    return orElse();
  }
}

abstract class _GenerateCodeForWhatsapp implements GenerateCodeEvent {
  const factory _GenerateCodeForWhatsapp() = _$GenerateCodeForWhatsappImpl;
}

/// @nodoc
mixin _$GenerateCodeState {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() generateCodeForSmsLoading,
    required TResult Function(GenerateCodeResponse generateCodeResponse)
        generateCodeForSmsSuccess,
    required TResult Function(Failure failure) generateCodeForSmsFailed,
    required TResult Function() generateCodeForWhatsappLoading,
    required TResult Function(GenerateCodeResponse generateCodeResponse)
        generateCodeForWhatsappSuccess,
    required TResult Function(Failure failure) generateCodeForWhatsappFailed,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? generateCodeForSmsLoading,
    TResult? Function(GenerateCodeResponse generateCodeResponse)?
        generateCodeForSmsSuccess,
    TResult? Function(Failure failure)? generateCodeForSmsFailed,
    TResult? Function()? generateCodeForWhatsappLoading,
    TResult? Function(GenerateCodeResponse generateCodeResponse)?
        generateCodeForWhatsappSuccess,
    TResult? Function(Failure failure)? generateCodeForWhatsappFailed,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? generateCodeForSmsLoading,
    TResult Function(GenerateCodeResponse generateCodeResponse)?
        generateCodeForSmsSuccess,
    TResult Function(Failure failure)? generateCodeForSmsFailed,
    TResult Function()? generateCodeForWhatsappLoading,
    TResult Function(GenerateCodeResponse generateCodeResponse)?
        generateCodeForWhatsappSuccess,
    TResult Function(Failure failure)? generateCodeForWhatsappFailed,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_GenerateCodeForSmsLoading value)
        generateCodeForSmsLoading,
    required TResult Function(_GenerateCodeForSmsSuccess value)
        generateCodeForSmsSuccess,
    required TResult Function(_GenerateCodeForSmsFailed value)
        generateCodeForSmsFailed,
    required TResult Function(_GenerateCodeForWhatsappLoading value)
        generateCodeForWhatsappLoading,
    required TResult Function(_GenerateCodeForWhatsappSuccess value)
        generateCodeForWhatsappSuccess,
    required TResult Function(_GenerateCodeForWhatsappFailed value)
        generateCodeForWhatsappFailed,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_GenerateCodeForSmsLoading value)?
        generateCodeForSmsLoading,
    TResult? Function(_GenerateCodeForSmsSuccess value)?
        generateCodeForSmsSuccess,
    TResult? Function(_GenerateCodeForSmsFailed value)?
        generateCodeForSmsFailed,
    TResult? Function(_GenerateCodeForWhatsappLoading value)?
        generateCodeForWhatsappLoading,
    TResult? Function(_GenerateCodeForWhatsappSuccess value)?
        generateCodeForWhatsappSuccess,
    TResult? Function(_GenerateCodeForWhatsappFailed value)?
        generateCodeForWhatsappFailed,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_GenerateCodeForSmsLoading value)?
        generateCodeForSmsLoading,
    TResult Function(_GenerateCodeForSmsSuccess value)?
        generateCodeForSmsSuccess,
    TResult Function(_GenerateCodeForSmsFailed value)? generateCodeForSmsFailed,
    TResult Function(_GenerateCodeForWhatsappLoading value)?
        generateCodeForWhatsappLoading,
    TResult Function(_GenerateCodeForWhatsappSuccess value)?
        generateCodeForWhatsappSuccess,
    TResult Function(_GenerateCodeForWhatsappFailed value)?
        generateCodeForWhatsappFailed,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $GenerateCodeStateCopyWith<$Res> {
  factory $GenerateCodeStateCopyWith(
          GenerateCodeState value, $Res Function(GenerateCodeState) then) =
      _$GenerateCodeStateCopyWithImpl<$Res, GenerateCodeState>;
}

/// @nodoc
class _$GenerateCodeStateCopyWithImpl<$Res, $Val extends GenerateCodeState>
    implements $GenerateCodeStateCopyWith<$Res> {
  _$GenerateCodeStateCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$InitialImplCopyWith<$Res> {
  factory _$$InitialImplCopyWith(
          _$InitialImpl value, $Res Function(_$InitialImpl) then) =
      __$$InitialImplCopyWithImpl<$Res>;
}

/// @nodoc
class __$$InitialImplCopyWithImpl<$Res>
    extends _$GenerateCodeStateCopyWithImpl<$Res, _$InitialImpl>
    implements _$$InitialImplCopyWith<$Res> {
  __$$InitialImplCopyWithImpl(
      _$InitialImpl _value, $Res Function(_$InitialImpl) _then)
      : super(_value, _then);
}

/// @nodoc

class _$InitialImpl implements _Initial {
  const _$InitialImpl();

  @override
  String toString() {
    return 'GenerateCodeState.initial()';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$InitialImpl);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() generateCodeForSmsLoading,
    required TResult Function(GenerateCodeResponse generateCodeResponse)
        generateCodeForSmsSuccess,
    required TResult Function(Failure failure) generateCodeForSmsFailed,
    required TResult Function() generateCodeForWhatsappLoading,
    required TResult Function(GenerateCodeResponse generateCodeResponse)
        generateCodeForWhatsappSuccess,
    required TResult Function(Failure failure) generateCodeForWhatsappFailed,
  }) {
    return initial();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? generateCodeForSmsLoading,
    TResult? Function(GenerateCodeResponse generateCodeResponse)?
        generateCodeForSmsSuccess,
    TResult? Function(Failure failure)? generateCodeForSmsFailed,
    TResult? Function()? generateCodeForWhatsappLoading,
    TResult? Function(GenerateCodeResponse generateCodeResponse)?
        generateCodeForWhatsappSuccess,
    TResult? Function(Failure failure)? generateCodeForWhatsappFailed,
  }) {
    return initial?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? generateCodeForSmsLoading,
    TResult Function(GenerateCodeResponse generateCodeResponse)?
        generateCodeForSmsSuccess,
    TResult Function(Failure failure)? generateCodeForSmsFailed,
    TResult Function()? generateCodeForWhatsappLoading,
    TResult Function(GenerateCodeResponse generateCodeResponse)?
        generateCodeForWhatsappSuccess,
    TResult Function(Failure failure)? generateCodeForWhatsappFailed,
    required TResult orElse(),
  }) {
    if (initial != null) {
      return initial();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_GenerateCodeForSmsLoading value)
        generateCodeForSmsLoading,
    required TResult Function(_GenerateCodeForSmsSuccess value)
        generateCodeForSmsSuccess,
    required TResult Function(_GenerateCodeForSmsFailed value)
        generateCodeForSmsFailed,
    required TResult Function(_GenerateCodeForWhatsappLoading value)
        generateCodeForWhatsappLoading,
    required TResult Function(_GenerateCodeForWhatsappSuccess value)
        generateCodeForWhatsappSuccess,
    required TResult Function(_GenerateCodeForWhatsappFailed value)
        generateCodeForWhatsappFailed,
  }) {
    return initial(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_GenerateCodeForSmsLoading value)?
        generateCodeForSmsLoading,
    TResult? Function(_GenerateCodeForSmsSuccess value)?
        generateCodeForSmsSuccess,
    TResult? Function(_GenerateCodeForSmsFailed value)?
        generateCodeForSmsFailed,
    TResult? Function(_GenerateCodeForWhatsappLoading value)?
        generateCodeForWhatsappLoading,
    TResult? Function(_GenerateCodeForWhatsappSuccess value)?
        generateCodeForWhatsappSuccess,
    TResult? Function(_GenerateCodeForWhatsappFailed value)?
        generateCodeForWhatsappFailed,
  }) {
    return initial?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_GenerateCodeForSmsLoading value)?
        generateCodeForSmsLoading,
    TResult Function(_GenerateCodeForSmsSuccess value)?
        generateCodeForSmsSuccess,
    TResult Function(_GenerateCodeForSmsFailed value)? generateCodeForSmsFailed,
    TResult Function(_GenerateCodeForWhatsappLoading value)?
        generateCodeForWhatsappLoading,
    TResult Function(_GenerateCodeForWhatsappSuccess value)?
        generateCodeForWhatsappSuccess,
    TResult Function(_GenerateCodeForWhatsappFailed value)?
        generateCodeForWhatsappFailed,
    required TResult orElse(),
  }) {
    if (initial != null) {
      return initial(this);
    }
    return orElse();
  }
}

abstract class _Initial implements GenerateCodeState {
  const factory _Initial() = _$InitialImpl;
}

/// @nodoc
abstract class _$$GenerateCodeForSmsLoadingImplCopyWith<$Res> {
  factory _$$GenerateCodeForSmsLoadingImplCopyWith(
          _$GenerateCodeForSmsLoadingImpl value,
          $Res Function(_$GenerateCodeForSmsLoadingImpl) then) =
      __$$GenerateCodeForSmsLoadingImplCopyWithImpl<$Res>;
}

/// @nodoc
class __$$GenerateCodeForSmsLoadingImplCopyWithImpl<$Res>
    extends _$GenerateCodeStateCopyWithImpl<$Res,
        _$GenerateCodeForSmsLoadingImpl>
    implements _$$GenerateCodeForSmsLoadingImplCopyWith<$Res> {
  __$$GenerateCodeForSmsLoadingImplCopyWithImpl(
      _$GenerateCodeForSmsLoadingImpl _value,
      $Res Function(_$GenerateCodeForSmsLoadingImpl) _then)
      : super(_value, _then);
}

/// @nodoc

class _$GenerateCodeForSmsLoadingImpl implements _GenerateCodeForSmsLoading {
  const _$GenerateCodeForSmsLoadingImpl();

  @override
  String toString() {
    return 'GenerateCodeState.generateCodeForSmsLoading()';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$GenerateCodeForSmsLoadingImpl);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() generateCodeForSmsLoading,
    required TResult Function(GenerateCodeResponse generateCodeResponse)
        generateCodeForSmsSuccess,
    required TResult Function(Failure failure) generateCodeForSmsFailed,
    required TResult Function() generateCodeForWhatsappLoading,
    required TResult Function(GenerateCodeResponse generateCodeResponse)
        generateCodeForWhatsappSuccess,
    required TResult Function(Failure failure) generateCodeForWhatsappFailed,
  }) {
    return generateCodeForSmsLoading();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? generateCodeForSmsLoading,
    TResult? Function(GenerateCodeResponse generateCodeResponse)?
        generateCodeForSmsSuccess,
    TResult? Function(Failure failure)? generateCodeForSmsFailed,
    TResult? Function()? generateCodeForWhatsappLoading,
    TResult? Function(GenerateCodeResponse generateCodeResponse)?
        generateCodeForWhatsappSuccess,
    TResult? Function(Failure failure)? generateCodeForWhatsappFailed,
  }) {
    return generateCodeForSmsLoading?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? generateCodeForSmsLoading,
    TResult Function(GenerateCodeResponse generateCodeResponse)?
        generateCodeForSmsSuccess,
    TResult Function(Failure failure)? generateCodeForSmsFailed,
    TResult Function()? generateCodeForWhatsappLoading,
    TResult Function(GenerateCodeResponse generateCodeResponse)?
        generateCodeForWhatsappSuccess,
    TResult Function(Failure failure)? generateCodeForWhatsappFailed,
    required TResult orElse(),
  }) {
    if (generateCodeForSmsLoading != null) {
      return generateCodeForSmsLoading();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_GenerateCodeForSmsLoading value)
        generateCodeForSmsLoading,
    required TResult Function(_GenerateCodeForSmsSuccess value)
        generateCodeForSmsSuccess,
    required TResult Function(_GenerateCodeForSmsFailed value)
        generateCodeForSmsFailed,
    required TResult Function(_GenerateCodeForWhatsappLoading value)
        generateCodeForWhatsappLoading,
    required TResult Function(_GenerateCodeForWhatsappSuccess value)
        generateCodeForWhatsappSuccess,
    required TResult Function(_GenerateCodeForWhatsappFailed value)
        generateCodeForWhatsappFailed,
  }) {
    return generateCodeForSmsLoading(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_GenerateCodeForSmsLoading value)?
        generateCodeForSmsLoading,
    TResult? Function(_GenerateCodeForSmsSuccess value)?
        generateCodeForSmsSuccess,
    TResult? Function(_GenerateCodeForSmsFailed value)?
        generateCodeForSmsFailed,
    TResult? Function(_GenerateCodeForWhatsappLoading value)?
        generateCodeForWhatsappLoading,
    TResult? Function(_GenerateCodeForWhatsappSuccess value)?
        generateCodeForWhatsappSuccess,
    TResult? Function(_GenerateCodeForWhatsappFailed value)?
        generateCodeForWhatsappFailed,
  }) {
    return generateCodeForSmsLoading?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_GenerateCodeForSmsLoading value)?
        generateCodeForSmsLoading,
    TResult Function(_GenerateCodeForSmsSuccess value)?
        generateCodeForSmsSuccess,
    TResult Function(_GenerateCodeForSmsFailed value)? generateCodeForSmsFailed,
    TResult Function(_GenerateCodeForWhatsappLoading value)?
        generateCodeForWhatsappLoading,
    TResult Function(_GenerateCodeForWhatsappSuccess value)?
        generateCodeForWhatsappSuccess,
    TResult Function(_GenerateCodeForWhatsappFailed value)?
        generateCodeForWhatsappFailed,
    required TResult orElse(),
  }) {
    if (generateCodeForSmsLoading != null) {
      return generateCodeForSmsLoading(this);
    }
    return orElse();
  }
}

abstract class _GenerateCodeForSmsLoading implements GenerateCodeState {
  const factory _GenerateCodeForSmsLoading() = _$GenerateCodeForSmsLoadingImpl;
}

/// @nodoc
abstract class _$$GenerateCodeForSmsSuccessImplCopyWith<$Res> {
  factory _$$GenerateCodeForSmsSuccessImplCopyWith(
          _$GenerateCodeForSmsSuccessImpl value,
          $Res Function(_$GenerateCodeForSmsSuccessImpl) then) =
      __$$GenerateCodeForSmsSuccessImplCopyWithImpl<$Res>;
  @useResult
  $Res call({GenerateCodeResponse generateCodeResponse});
}

/// @nodoc
class __$$GenerateCodeForSmsSuccessImplCopyWithImpl<$Res>
    extends _$GenerateCodeStateCopyWithImpl<$Res,
        _$GenerateCodeForSmsSuccessImpl>
    implements _$$GenerateCodeForSmsSuccessImplCopyWith<$Res> {
  __$$GenerateCodeForSmsSuccessImplCopyWithImpl(
      _$GenerateCodeForSmsSuccessImpl _value,
      $Res Function(_$GenerateCodeForSmsSuccessImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? generateCodeResponse = null,
  }) {
    return _then(_$GenerateCodeForSmsSuccessImpl(
      null == generateCodeResponse
          ? _value.generateCodeResponse
          : generateCodeResponse // ignore: cast_nullable_to_non_nullable
              as GenerateCodeResponse,
    ));
  }
}

/// @nodoc

class _$GenerateCodeForSmsSuccessImpl implements _GenerateCodeForSmsSuccess {
  const _$GenerateCodeForSmsSuccessImpl(this.generateCodeResponse);

  @override
  final GenerateCodeResponse generateCodeResponse;

  @override
  String toString() {
    return 'GenerateCodeState.generateCodeForSmsSuccess(generateCodeResponse: $generateCodeResponse)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$GenerateCodeForSmsSuccessImpl &&
            (identical(other.generateCodeResponse, generateCodeResponse) ||
                other.generateCodeResponse == generateCodeResponse));
  }

  @override
  int get hashCode => Object.hash(runtimeType, generateCodeResponse);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$GenerateCodeForSmsSuccessImplCopyWith<_$GenerateCodeForSmsSuccessImpl>
      get copyWith => __$$GenerateCodeForSmsSuccessImplCopyWithImpl<
          _$GenerateCodeForSmsSuccessImpl>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() generateCodeForSmsLoading,
    required TResult Function(GenerateCodeResponse generateCodeResponse)
        generateCodeForSmsSuccess,
    required TResult Function(Failure failure) generateCodeForSmsFailed,
    required TResult Function() generateCodeForWhatsappLoading,
    required TResult Function(GenerateCodeResponse generateCodeResponse)
        generateCodeForWhatsappSuccess,
    required TResult Function(Failure failure) generateCodeForWhatsappFailed,
  }) {
    return generateCodeForSmsSuccess(generateCodeResponse);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? generateCodeForSmsLoading,
    TResult? Function(GenerateCodeResponse generateCodeResponse)?
        generateCodeForSmsSuccess,
    TResult? Function(Failure failure)? generateCodeForSmsFailed,
    TResult? Function()? generateCodeForWhatsappLoading,
    TResult? Function(GenerateCodeResponse generateCodeResponse)?
        generateCodeForWhatsappSuccess,
    TResult? Function(Failure failure)? generateCodeForWhatsappFailed,
  }) {
    return generateCodeForSmsSuccess?.call(generateCodeResponse);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? generateCodeForSmsLoading,
    TResult Function(GenerateCodeResponse generateCodeResponse)?
        generateCodeForSmsSuccess,
    TResult Function(Failure failure)? generateCodeForSmsFailed,
    TResult Function()? generateCodeForWhatsappLoading,
    TResult Function(GenerateCodeResponse generateCodeResponse)?
        generateCodeForWhatsappSuccess,
    TResult Function(Failure failure)? generateCodeForWhatsappFailed,
    required TResult orElse(),
  }) {
    if (generateCodeForSmsSuccess != null) {
      return generateCodeForSmsSuccess(generateCodeResponse);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_GenerateCodeForSmsLoading value)
        generateCodeForSmsLoading,
    required TResult Function(_GenerateCodeForSmsSuccess value)
        generateCodeForSmsSuccess,
    required TResult Function(_GenerateCodeForSmsFailed value)
        generateCodeForSmsFailed,
    required TResult Function(_GenerateCodeForWhatsappLoading value)
        generateCodeForWhatsappLoading,
    required TResult Function(_GenerateCodeForWhatsappSuccess value)
        generateCodeForWhatsappSuccess,
    required TResult Function(_GenerateCodeForWhatsappFailed value)
        generateCodeForWhatsappFailed,
  }) {
    return generateCodeForSmsSuccess(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_GenerateCodeForSmsLoading value)?
        generateCodeForSmsLoading,
    TResult? Function(_GenerateCodeForSmsSuccess value)?
        generateCodeForSmsSuccess,
    TResult? Function(_GenerateCodeForSmsFailed value)?
        generateCodeForSmsFailed,
    TResult? Function(_GenerateCodeForWhatsappLoading value)?
        generateCodeForWhatsappLoading,
    TResult? Function(_GenerateCodeForWhatsappSuccess value)?
        generateCodeForWhatsappSuccess,
    TResult? Function(_GenerateCodeForWhatsappFailed value)?
        generateCodeForWhatsappFailed,
  }) {
    return generateCodeForSmsSuccess?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_GenerateCodeForSmsLoading value)?
        generateCodeForSmsLoading,
    TResult Function(_GenerateCodeForSmsSuccess value)?
        generateCodeForSmsSuccess,
    TResult Function(_GenerateCodeForSmsFailed value)? generateCodeForSmsFailed,
    TResult Function(_GenerateCodeForWhatsappLoading value)?
        generateCodeForWhatsappLoading,
    TResult Function(_GenerateCodeForWhatsappSuccess value)?
        generateCodeForWhatsappSuccess,
    TResult Function(_GenerateCodeForWhatsappFailed value)?
        generateCodeForWhatsappFailed,
    required TResult orElse(),
  }) {
    if (generateCodeForSmsSuccess != null) {
      return generateCodeForSmsSuccess(this);
    }
    return orElse();
  }
}

abstract class _GenerateCodeForSmsSuccess implements GenerateCodeState {
  const factory _GenerateCodeForSmsSuccess(
          final GenerateCodeResponse generateCodeResponse) =
      _$GenerateCodeForSmsSuccessImpl;

  GenerateCodeResponse get generateCodeResponse;
  @JsonKey(ignore: true)
  _$$GenerateCodeForSmsSuccessImplCopyWith<_$GenerateCodeForSmsSuccessImpl>
      get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$GenerateCodeForSmsFailedImplCopyWith<$Res> {
  factory _$$GenerateCodeForSmsFailedImplCopyWith(
          _$GenerateCodeForSmsFailedImpl value,
          $Res Function(_$GenerateCodeForSmsFailedImpl) then) =
      __$$GenerateCodeForSmsFailedImplCopyWithImpl<$Res>;
  @useResult
  $Res call({Failure failure});
}

/// @nodoc
class __$$GenerateCodeForSmsFailedImplCopyWithImpl<$Res>
    extends _$GenerateCodeStateCopyWithImpl<$Res,
        _$GenerateCodeForSmsFailedImpl>
    implements _$$GenerateCodeForSmsFailedImplCopyWith<$Res> {
  __$$GenerateCodeForSmsFailedImplCopyWithImpl(
      _$GenerateCodeForSmsFailedImpl _value,
      $Res Function(_$GenerateCodeForSmsFailedImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? failure = null,
  }) {
    return _then(_$GenerateCodeForSmsFailedImpl(
      null == failure
          ? _value.failure
          : failure // ignore: cast_nullable_to_non_nullable
              as Failure,
    ));
  }
}

/// @nodoc

class _$GenerateCodeForSmsFailedImpl implements _GenerateCodeForSmsFailed {
  const _$GenerateCodeForSmsFailedImpl(this.failure);

  @override
  final Failure failure;

  @override
  String toString() {
    return 'GenerateCodeState.generateCodeForSmsFailed(failure: $failure)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$GenerateCodeForSmsFailedImpl &&
            (identical(other.failure, failure) || other.failure == failure));
  }

  @override
  int get hashCode => Object.hash(runtimeType, failure);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$GenerateCodeForSmsFailedImplCopyWith<_$GenerateCodeForSmsFailedImpl>
      get copyWith => __$$GenerateCodeForSmsFailedImplCopyWithImpl<
          _$GenerateCodeForSmsFailedImpl>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() generateCodeForSmsLoading,
    required TResult Function(GenerateCodeResponse generateCodeResponse)
        generateCodeForSmsSuccess,
    required TResult Function(Failure failure) generateCodeForSmsFailed,
    required TResult Function() generateCodeForWhatsappLoading,
    required TResult Function(GenerateCodeResponse generateCodeResponse)
        generateCodeForWhatsappSuccess,
    required TResult Function(Failure failure) generateCodeForWhatsappFailed,
  }) {
    return generateCodeForSmsFailed(failure);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? generateCodeForSmsLoading,
    TResult? Function(GenerateCodeResponse generateCodeResponse)?
        generateCodeForSmsSuccess,
    TResult? Function(Failure failure)? generateCodeForSmsFailed,
    TResult? Function()? generateCodeForWhatsappLoading,
    TResult? Function(GenerateCodeResponse generateCodeResponse)?
        generateCodeForWhatsappSuccess,
    TResult? Function(Failure failure)? generateCodeForWhatsappFailed,
  }) {
    return generateCodeForSmsFailed?.call(failure);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? generateCodeForSmsLoading,
    TResult Function(GenerateCodeResponse generateCodeResponse)?
        generateCodeForSmsSuccess,
    TResult Function(Failure failure)? generateCodeForSmsFailed,
    TResult Function()? generateCodeForWhatsappLoading,
    TResult Function(GenerateCodeResponse generateCodeResponse)?
        generateCodeForWhatsappSuccess,
    TResult Function(Failure failure)? generateCodeForWhatsappFailed,
    required TResult orElse(),
  }) {
    if (generateCodeForSmsFailed != null) {
      return generateCodeForSmsFailed(failure);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_GenerateCodeForSmsLoading value)
        generateCodeForSmsLoading,
    required TResult Function(_GenerateCodeForSmsSuccess value)
        generateCodeForSmsSuccess,
    required TResult Function(_GenerateCodeForSmsFailed value)
        generateCodeForSmsFailed,
    required TResult Function(_GenerateCodeForWhatsappLoading value)
        generateCodeForWhatsappLoading,
    required TResult Function(_GenerateCodeForWhatsappSuccess value)
        generateCodeForWhatsappSuccess,
    required TResult Function(_GenerateCodeForWhatsappFailed value)
        generateCodeForWhatsappFailed,
  }) {
    return generateCodeForSmsFailed(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_GenerateCodeForSmsLoading value)?
        generateCodeForSmsLoading,
    TResult? Function(_GenerateCodeForSmsSuccess value)?
        generateCodeForSmsSuccess,
    TResult? Function(_GenerateCodeForSmsFailed value)?
        generateCodeForSmsFailed,
    TResult? Function(_GenerateCodeForWhatsappLoading value)?
        generateCodeForWhatsappLoading,
    TResult? Function(_GenerateCodeForWhatsappSuccess value)?
        generateCodeForWhatsappSuccess,
    TResult? Function(_GenerateCodeForWhatsappFailed value)?
        generateCodeForWhatsappFailed,
  }) {
    return generateCodeForSmsFailed?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_GenerateCodeForSmsLoading value)?
        generateCodeForSmsLoading,
    TResult Function(_GenerateCodeForSmsSuccess value)?
        generateCodeForSmsSuccess,
    TResult Function(_GenerateCodeForSmsFailed value)? generateCodeForSmsFailed,
    TResult Function(_GenerateCodeForWhatsappLoading value)?
        generateCodeForWhatsappLoading,
    TResult Function(_GenerateCodeForWhatsappSuccess value)?
        generateCodeForWhatsappSuccess,
    TResult Function(_GenerateCodeForWhatsappFailed value)?
        generateCodeForWhatsappFailed,
    required TResult orElse(),
  }) {
    if (generateCodeForSmsFailed != null) {
      return generateCodeForSmsFailed(this);
    }
    return orElse();
  }
}

abstract class _GenerateCodeForSmsFailed implements GenerateCodeState {
  const factory _GenerateCodeForSmsFailed(final Failure failure) =
      _$GenerateCodeForSmsFailedImpl;

  Failure get failure;
  @JsonKey(ignore: true)
  _$$GenerateCodeForSmsFailedImplCopyWith<_$GenerateCodeForSmsFailedImpl>
      get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$GenerateCodeForWhatsappLoadingImplCopyWith<$Res> {
  factory _$$GenerateCodeForWhatsappLoadingImplCopyWith(
          _$GenerateCodeForWhatsappLoadingImpl value,
          $Res Function(_$GenerateCodeForWhatsappLoadingImpl) then) =
      __$$GenerateCodeForWhatsappLoadingImplCopyWithImpl<$Res>;
}

/// @nodoc
class __$$GenerateCodeForWhatsappLoadingImplCopyWithImpl<$Res>
    extends _$GenerateCodeStateCopyWithImpl<$Res,
        _$GenerateCodeForWhatsappLoadingImpl>
    implements _$$GenerateCodeForWhatsappLoadingImplCopyWith<$Res> {
  __$$GenerateCodeForWhatsappLoadingImplCopyWithImpl(
      _$GenerateCodeForWhatsappLoadingImpl _value,
      $Res Function(_$GenerateCodeForWhatsappLoadingImpl) _then)
      : super(_value, _then);
}

/// @nodoc

class _$GenerateCodeForWhatsappLoadingImpl
    implements _GenerateCodeForWhatsappLoading {
  const _$GenerateCodeForWhatsappLoadingImpl();

  @override
  String toString() {
    return 'GenerateCodeState.generateCodeForWhatsappLoading()';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$GenerateCodeForWhatsappLoadingImpl);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() generateCodeForSmsLoading,
    required TResult Function(GenerateCodeResponse generateCodeResponse)
        generateCodeForSmsSuccess,
    required TResult Function(Failure failure) generateCodeForSmsFailed,
    required TResult Function() generateCodeForWhatsappLoading,
    required TResult Function(GenerateCodeResponse generateCodeResponse)
        generateCodeForWhatsappSuccess,
    required TResult Function(Failure failure) generateCodeForWhatsappFailed,
  }) {
    return generateCodeForWhatsappLoading();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? generateCodeForSmsLoading,
    TResult? Function(GenerateCodeResponse generateCodeResponse)?
        generateCodeForSmsSuccess,
    TResult? Function(Failure failure)? generateCodeForSmsFailed,
    TResult? Function()? generateCodeForWhatsappLoading,
    TResult? Function(GenerateCodeResponse generateCodeResponse)?
        generateCodeForWhatsappSuccess,
    TResult? Function(Failure failure)? generateCodeForWhatsappFailed,
  }) {
    return generateCodeForWhatsappLoading?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? generateCodeForSmsLoading,
    TResult Function(GenerateCodeResponse generateCodeResponse)?
        generateCodeForSmsSuccess,
    TResult Function(Failure failure)? generateCodeForSmsFailed,
    TResult Function()? generateCodeForWhatsappLoading,
    TResult Function(GenerateCodeResponse generateCodeResponse)?
        generateCodeForWhatsappSuccess,
    TResult Function(Failure failure)? generateCodeForWhatsappFailed,
    required TResult orElse(),
  }) {
    if (generateCodeForWhatsappLoading != null) {
      return generateCodeForWhatsappLoading();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_GenerateCodeForSmsLoading value)
        generateCodeForSmsLoading,
    required TResult Function(_GenerateCodeForSmsSuccess value)
        generateCodeForSmsSuccess,
    required TResult Function(_GenerateCodeForSmsFailed value)
        generateCodeForSmsFailed,
    required TResult Function(_GenerateCodeForWhatsappLoading value)
        generateCodeForWhatsappLoading,
    required TResult Function(_GenerateCodeForWhatsappSuccess value)
        generateCodeForWhatsappSuccess,
    required TResult Function(_GenerateCodeForWhatsappFailed value)
        generateCodeForWhatsappFailed,
  }) {
    return generateCodeForWhatsappLoading(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_GenerateCodeForSmsLoading value)?
        generateCodeForSmsLoading,
    TResult? Function(_GenerateCodeForSmsSuccess value)?
        generateCodeForSmsSuccess,
    TResult? Function(_GenerateCodeForSmsFailed value)?
        generateCodeForSmsFailed,
    TResult? Function(_GenerateCodeForWhatsappLoading value)?
        generateCodeForWhatsappLoading,
    TResult? Function(_GenerateCodeForWhatsappSuccess value)?
        generateCodeForWhatsappSuccess,
    TResult? Function(_GenerateCodeForWhatsappFailed value)?
        generateCodeForWhatsappFailed,
  }) {
    return generateCodeForWhatsappLoading?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_GenerateCodeForSmsLoading value)?
        generateCodeForSmsLoading,
    TResult Function(_GenerateCodeForSmsSuccess value)?
        generateCodeForSmsSuccess,
    TResult Function(_GenerateCodeForSmsFailed value)? generateCodeForSmsFailed,
    TResult Function(_GenerateCodeForWhatsappLoading value)?
        generateCodeForWhatsappLoading,
    TResult Function(_GenerateCodeForWhatsappSuccess value)?
        generateCodeForWhatsappSuccess,
    TResult Function(_GenerateCodeForWhatsappFailed value)?
        generateCodeForWhatsappFailed,
    required TResult orElse(),
  }) {
    if (generateCodeForWhatsappLoading != null) {
      return generateCodeForWhatsappLoading(this);
    }
    return orElse();
  }
}

abstract class _GenerateCodeForWhatsappLoading implements GenerateCodeState {
  const factory _GenerateCodeForWhatsappLoading() =
      _$GenerateCodeForWhatsappLoadingImpl;
}

/// @nodoc
abstract class _$$GenerateCodeForWhatsappSuccessImplCopyWith<$Res> {
  factory _$$GenerateCodeForWhatsappSuccessImplCopyWith(
          _$GenerateCodeForWhatsappSuccessImpl value,
          $Res Function(_$GenerateCodeForWhatsappSuccessImpl) then) =
      __$$GenerateCodeForWhatsappSuccessImplCopyWithImpl<$Res>;
  @useResult
  $Res call({GenerateCodeResponse generateCodeResponse});
}

/// @nodoc
class __$$GenerateCodeForWhatsappSuccessImplCopyWithImpl<$Res>
    extends _$GenerateCodeStateCopyWithImpl<$Res,
        _$GenerateCodeForWhatsappSuccessImpl>
    implements _$$GenerateCodeForWhatsappSuccessImplCopyWith<$Res> {
  __$$GenerateCodeForWhatsappSuccessImplCopyWithImpl(
      _$GenerateCodeForWhatsappSuccessImpl _value,
      $Res Function(_$GenerateCodeForWhatsappSuccessImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? generateCodeResponse = null,
  }) {
    return _then(_$GenerateCodeForWhatsappSuccessImpl(
      null == generateCodeResponse
          ? _value.generateCodeResponse
          : generateCodeResponse // ignore: cast_nullable_to_non_nullable
              as GenerateCodeResponse,
    ));
  }
}

/// @nodoc

class _$GenerateCodeForWhatsappSuccessImpl
    implements _GenerateCodeForWhatsappSuccess {
  const _$GenerateCodeForWhatsappSuccessImpl(this.generateCodeResponse);

  @override
  final GenerateCodeResponse generateCodeResponse;

  @override
  String toString() {
    return 'GenerateCodeState.generateCodeForWhatsappSuccess(generateCodeResponse: $generateCodeResponse)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$GenerateCodeForWhatsappSuccessImpl &&
            (identical(other.generateCodeResponse, generateCodeResponse) ||
                other.generateCodeResponse == generateCodeResponse));
  }

  @override
  int get hashCode => Object.hash(runtimeType, generateCodeResponse);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$GenerateCodeForWhatsappSuccessImplCopyWith<
          _$GenerateCodeForWhatsappSuccessImpl>
      get copyWith => __$$GenerateCodeForWhatsappSuccessImplCopyWithImpl<
          _$GenerateCodeForWhatsappSuccessImpl>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() generateCodeForSmsLoading,
    required TResult Function(GenerateCodeResponse generateCodeResponse)
        generateCodeForSmsSuccess,
    required TResult Function(Failure failure) generateCodeForSmsFailed,
    required TResult Function() generateCodeForWhatsappLoading,
    required TResult Function(GenerateCodeResponse generateCodeResponse)
        generateCodeForWhatsappSuccess,
    required TResult Function(Failure failure) generateCodeForWhatsappFailed,
  }) {
    return generateCodeForWhatsappSuccess(generateCodeResponse);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? generateCodeForSmsLoading,
    TResult? Function(GenerateCodeResponse generateCodeResponse)?
        generateCodeForSmsSuccess,
    TResult? Function(Failure failure)? generateCodeForSmsFailed,
    TResult? Function()? generateCodeForWhatsappLoading,
    TResult? Function(GenerateCodeResponse generateCodeResponse)?
        generateCodeForWhatsappSuccess,
    TResult? Function(Failure failure)? generateCodeForWhatsappFailed,
  }) {
    return generateCodeForWhatsappSuccess?.call(generateCodeResponse);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? generateCodeForSmsLoading,
    TResult Function(GenerateCodeResponse generateCodeResponse)?
        generateCodeForSmsSuccess,
    TResult Function(Failure failure)? generateCodeForSmsFailed,
    TResult Function()? generateCodeForWhatsappLoading,
    TResult Function(GenerateCodeResponse generateCodeResponse)?
        generateCodeForWhatsappSuccess,
    TResult Function(Failure failure)? generateCodeForWhatsappFailed,
    required TResult orElse(),
  }) {
    if (generateCodeForWhatsappSuccess != null) {
      return generateCodeForWhatsappSuccess(generateCodeResponse);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_GenerateCodeForSmsLoading value)
        generateCodeForSmsLoading,
    required TResult Function(_GenerateCodeForSmsSuccess value)
        generateCodeForSmsSuccess,
    required TResult Function(_GenerateCodeForSmsFailed value)
        generateCodeForSmsFailed,
    required TResult Function(_GenerateCodeForWhatsappLoading value)
        generateCodeForWhatsappLoading,
    required TResult Function(_GenerateCodeForWhatsappSuccess value)
        generateCodeForWhatsappSuccess,
    required TResult Function(_GenerateCodeForWhatsappFailed value)
        generateCodeForWhatsappFailed,
  }) {
    return generateCodeForWhatsappSuccess(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_GenerateCodeForSmsLoading value)?
        generateCodeForSmsLoading,
    TResult? Function(_GenerateCodeForSmsSuccess value)?
        generateCodeForSmsSuccess,
    TResult? Function(_GenerateCodeForSmsFailed value)?
        generateCodeForSmsFailed,
    TResult? Function(_GenerateCodeForWhatsappLoading value)?
        generateCodeForWhatsappLoading,
    TResult? Function(_GenerateCodeForWhatsappSuccess value)?
        generateCodeForWhatsappSuccess,
    TResult? Function(_GenerateCodeForWhatsappFailed value)?
        generateCodeForWhatsappFailed,
  }) {
    return generateCodeForWhatsappSuccess?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_GenerateCodeForSmsLoading value)?
        generateCodeForSmsLoading,
    TResult Function(_GenerateCodeForSmsSuccess value)?
        generateCodeForSmsSuccess,
    TResult Function(_GenerateCodeForSmsFailed value)? generateCodeForSmsFailed,
    TResult Function(_GenerateCodeForWhatsappLoading value)?
        generateCodeForWhatsappLoading,
    TResult Function(_GenerateCodeForWhatsappSuccess value)?
        generateCodeForWhatsappSuccess,
    TResult Function(_GenerateCodeForWhatsappFailed value)?
        generateCodeForWhatsappFailed,
    required TResult orElse(),
  }) {
    if (generateCodeForWhatsappSuccess != null) {
      return generateCodeForWhatsappSuccess(this);
    }
    return orElse();
  }
}

abstract class _GenerateCodeForWhatsappSuccess implements GenerateCodeState {
  const factory _GenerateCodeForWhatsappSuccess(
          final GenerateCodeResponse generateCodeResponse) =
      _$GenerateCodeForWhatsappSuccessImpl;

  GenerateCodeResponse get generateCodeResponse;
  @JsonKey(ignore: true)
  _$$GenerateCodeForWhatsappSuccessImplCopyWith<
          _$GenerateCodeForWhatsappSuccessImpl>
      get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$GenerateCodeForWhatsappFailedImplCopyWith<$Res> {
  factory _$$GenerateCodeForWhatsappFailedImplCopyWith(
          _$GenerateCodeForWhatsappFailedImpl value,
          $Res Function(_$GenerateCodeForWhatsappFailedImpl) then) =
      __$$GenerateCodeForWhatsappFailedImplCopyWithImpl<$Res>;
  @useResult
  $Res call({Failure failure});
}

/// @nodoc
class __$$GenerateCodeForWhatsappFailedImplCopyWithImpl<$Res>
    extends _$GenerateCodeStateCopyWithImpl<$Res,
        _$GenerateCodeForWhatsappFailedImpl>
    implements _$$GenerateCodeForWhatsappFailedImplCopyWith<$Res> {
  __$$GenerateCodeForWhatsappFailedImplCopyWithImpl(
      _$GenerateCodeForWhatsappFailedImpl _value,
      $Res Function(_$GenerateCodeForWhatsappFailedImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? failure = null,
  }) {
    return _then(_$GenerateCodeForWhatsappFailedImpl(
      null == failure
          ? _value.failure
          : failure // ignore: cast_nullable_to_non_nullable
              as Failure,
    ));
  }
}

/// @nodoc

class _$GenerateCodeForWhatsappFailedImpl
    implements _GenerateCodeForWhatsappFailed {
  const _$GenerateCodeForWhatsappFailedImpl(this.failure);

  @override
  final Failure failure;

  @override
  String toString() {
    return 'GenerateCodeState.generateCodeForWhatsappFailed(failure: $failure)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$GenerateCodeForWhatsappFailedImpl &&
            (identical(other.failure, failure) || other.failure == failure));
  }

  @override
  int get hashCode => Object.hash(runtimeType, failure);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$GenerateCodeForWhatsappFailedImplCopyWith<
          _$GenerateCodeForWhatsappFailedImpl>
      get copyWith => __$$GenerateCodeForWhatsappFailedImplCopyWithImpl<
          _$GenerateCodeForWhatsappFailedImpl>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() generateCodeForSmsLoading,
    required TResult Function(GenerateCodeResponse generateCodeResponse)
        generateCodeForSmsSuccess,
    required TResult Function(Failure failure) generateCodeForSmsFailed,
    required TResult Function() generateCodeForWhatsappLoading,
    required TResult Function(GenerateCodeResponse generateCodeResponse)
        generateCodeForWhatsappSuccess,
    required TResult Function(Failure failure) generateCodeForWhatsappFailed,
  }) {
    return generateCodeForWhatsappFailed(failure);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? generateCodeForSmsLoading,
    TResult? Function(GenerateCodeResponse generateCodeResponse)?
        generateCodeForSmsSuccess,
    TResult? Function(Failure failure)? generateCodeForSmsFailed,
    TResult? Function()? generateCodeForWhatsappLoading,
    TResult? Function(GenerateCodeResponse generateCodeResponse)?
        generateCodeForWhatsappSuccess,
    TResult? Function(Failure failure)? generateCodeForWhatsappFailed,
  }) {
    return generateCodeForWhatsappFailed?.call(failure);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? generateCodeForSmsLoading,
    TResult Function(GenerateCodeResponse generateCodeResponse)?
        generateCodeForSmsSuccess,
    TResult Function(Failure failure)? generateCodeForSmsFailed,
    TResult Function()? generateCodeForWhatsappLoading,
    TResult Function(GenerateCodeResponse generateCodeResponse)?
        generateCodeForWhatsappSuccess,
    TResult Function(Failure failure)? generateCodeForWhatsappFailed,
    required TResult orElse(),
  }) {
    if (generateCodeForWhatsappFailed != null) {
      return generateCodeForWhatsappFailed(failure);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_GenerateCodeForSmsLoading value)
        generateCodeForSmsLoading,
    required TResult Function(_GenerateCodeForSmsSuccess value)
        generateCodeForSmsSuccess,
    required TResult Function(_GenerateCodeForSmsFailed value)
        generateCodeForSmsFailed,
    required TResult Function(_GenerateCodeForWhatsappLoading value)
        generateCodeForWhatsappLoading,
    required TResult Function(_GenerateCodeForWhatsappSuccess value)
        generateCodeForWhatsappSuccess,
    required TResult Function(_GenerateCodeForWhatsappFailed value)
        generateCodeForWhatsappFailed,
  }) {
    return generateCodeForWhatsappFailed(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_GenerateCodeForSmsLoading value)?
        generateCodeForSmsLoading,
    TResult? Function(_GenerateCodeForSmsSuccess value)?
        generateCodeForSmsSuccess,
    TResult? Function(_GenerateCodeForSmsFailed value)?
        generateCodeForSmsFailed,
    TResult? Function(_GenerateCodeForWhatsappLoading value)?
        generateCodeForWhatsappLoading,
    TResult? Function(_GenerateCodeForWhatsappSuccess value)?
        generateCodeForWhatsappSuccess,
    TResult? Function(_GenerateCodeForWhatsappFailed value)?
        generateCodeForWhatsappFailed,
  }) {
    return generateCodeForWhatsappFailed?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_GenerateCodeForSmsLoading value)?
        generateCodeForSmsLoading,
    TResult Function(_GenerateCodeForSmsSuccess value)?
        generateCodeForSmsSuccess,
    TResult Function(_GenerateCodeForSmsFailed value)? generateCodeForSmsFailed,
    TResult Function(_GenerateCodeForWhatsappLoading value)?
        generateCodeForWhatsappLoading,
    TResult Function(_GenerateCodeForWhatsappSuccess value)?
        generateCodeForWhatsappSuccess,
    TResult Function(_GenerateCodeForWhatsappFailed value)?
        generateCodeForWhatsappFailed,
    required TResult orElse(),
  }) {
    if (generateCodeForWhatsappFailed != null) {
      return generateCodeForWhatsappFailed(this);
    }
    return orElse();
  }
}

abstract class _GenerateCodeForWhatsappFailed implements GenerateCodeState {
  const factory _GenerateCodeForWhatsappFailed(final Failure failure) =
      _$GenerateCodeForWhatsappFailedImpl;

  Failure get failure;
  @JsonKey(ignore: true)
  _$$GenerateCodeForWhatsappFailedImplCopyWith<
          _$GenerateCodeForWhatsappFailedImpl>
      get copyWith => throw _privateConstructorUsedError;
}
