import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_extensions.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_functions.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_validators.dart';
import 'package:meedigo_company_sales_person/core/networking/api_failure.dart';
import 'package:meedigo_company_sales_person/core/networking/api_requests.dart';
import 'package:meedigo_company_sales_person/core/routing/app_routes.dart';
import 'package:meedigo_company_sales_person/features/auth/domain/entities/generate_code.dart';

import '../../../domain/usecases/generate_code_for_sms_usecase.dart';
import '../../../domain/usecases/generate_code_for_whatsapp_usecase.dart';

part 'generate_code_event.dart';
part 'generate_code_state.dart';
part 'generate_code_bloc.freezed.dart';

class GenerateCodeBloc extends Bloc<GenerateCodeEvent, GenerateCodeState> {
  final GenerateCodeForSmsUseCase generateCodeForSmsUseCase;
  final GenerateCodeForWhatsappUseCase generateCodeForWhatsappUseCase;

  final TextEditingController phoneController = TextEditingController();
  final GlobalKey<FormState> globalKey = GlobalKey<FormState>();

  bool isSendWhatsappBtnClicked = false;

  GenerateCodeBloc({
    required this.generateCodeForSmsUseCase,
    required this.generateCodeForWhatsappUseCase,
  }) : super(const GenerateCodeState.initial()) {
    on<GenerateCodeEvent>(
      (event, emit) async {
        await event.when(
          generateCodeForSms: () async {
            emit(GenerateCodeState.generateCodeForSmsLoading());
            final failureOrGenerateSmsCode = await generateCodeForSmsUseCase(
              GenerateCodeForSmsAndWhatsappRequestBody(
                phoneNumber: phoneController.text,
              ),
            );
            failureOrGenerateSmsCode.fold(
              (l) => emit(
                GenerateCodeState.generateCodeForSmsFailed(l),
              ),
              (r) => emit(
                GenerateCodeState.generateCodeForSmsSuccess(r),
              ),
            );
          },
          generateCodeForWhatsapp: () async {
            emit(GenerateCodeState.generateCodeForWhatsappLoading());
            final failureOrGenerateWhatsappCode =
                await generateCodeForWhatsappUseCase(
              GenerateCodeForSmsAndWhatsappRequestBody(
                phoneNumber: phoneController.text,
              ),
            );
            failureOrGenerateWhatsappCode.fold(
              (l) => emit(
                GenerateCodeState.generateCodeForWhatsappFailed(l),
              ),
              (r) => emit(
                GenerateCodeState.generateCodeForWhatsappSuccess(r),
              ),
            );
          },
        );
      },
    );
  }

  /// function to validate phone number input field
  String? validatePhoneNumberInput({
    required String? input,
    required BuildContext context,
  }) {
    return AppValidators.validatePhoneNumberField(
      input: input,
      context: context,
    );
  }

  /// function to validate send sms form
  /// if validation is done then proceed to generate sms code process
  /// else show validation error messages
  validateAndGenerateSmsCode() {
    if (globalKey.currentState!.validate()) {
      globalKey.currentState!.save();
      add(GenerateCodeEvent.generateCodeForSms());
    }
  }

  /// if sms code generation process is done
  /// , then route to otp verification screen
  routeToOtpVerificationScreen({
    required BuildContext context,
  }) {
    context.pushNamed(AppRoutes.verifyOtpRoute);
  }

  /// else show message that the
  /// sms or whatsapp code generation process has failed
  showSmsOrWhatsappCodeGenerationFailedMessage({
    required BuildContext context,
    required Failure failure,
  }) {
    AppFunctions.showFailedMessage(
      context: context,
      failure: failure,
    );
  }
}
