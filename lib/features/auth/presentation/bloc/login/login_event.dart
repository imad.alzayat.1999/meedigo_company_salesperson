part of 'login_bloc.dart';

@freezed
class LoginEvent with _$LoginEvent {
  const factory LoginEvent.login(
    LoginRequestBody loginRequestBody,
  ) = _Login;
}
