import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_extensions.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_functions.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_preferences.dart';
import 'package:meedigo_company_sales_person/core/networking/api_failure.dart';
import 'package:meedigo_company_sales_person/core/networking/api_requests.dart';
import 'package:meedigo_company_sales_person/core/routing/app_routes.dart';
import 'package:meedigo_company_sales_person/features/auth/domain/entities/login.dart';

import '../../../domain/usecases/login_usecase.dart';

part 'login_event.dart';
part 'login_state.dart';
part 'login_bloc.freezed.dart';

class LoginBloc extends Bloc<LoginEvent, LoginState> {
  final LoginUseCase loginUseCase;
  LoginBloc({
    required this.loginUseCase,
  }) : super(const LoginState.initial()) {
    on<LoginEvent>(
      (event, emit) async {
        await event.when(
          login: (loginRequestBody) async {
            emit(LoginState.loginLoading());
            final failureOrLogin = await loginUseCase(
              loginRequestBody,
            );
            failureOrLogin.fold(
              (l) => emit(
                LoginState.loginFailed(l),
              ),
              (r) => emit(
                LoginState.loginSuccess(r),
              ),
            );
          },
        );
      },
    );
  }

  /// save logged in data to local storage
  saveAuthenticatedData(LoginData login) {
    if (login.type == "Admin") {
      login = LoginData(
        salesPersonId: login.salesPersonId,
        token: login.token,
        type: login.type,
        companyId: 0,
        companyName: "",
        allowOrder: login.allowOrder,
        active: login.active,
        representatives: login.representatives,
      );
      AppPreferences.saveLoginData(login);
    } else {
      AppPreferences.saveLoginData(login);
    }
  }

  /// if login process done , then save login data and go to the app
  goToTheApp({
    required LoginData login,
    required BuildContext context,
  }) {
    saveAuthenticatedData(login);
    context.pushNamedAndRemoveUntil(
      AppRoutes.checkInRoute,
    );
  }

  /// else show message that the
  /// lgoin process has failed
  showLoginFailedMessage({
    required BuildContext context,
    required Failure failure,
  }) {
    AppFunctions.showFailedMessage(
      context: context,
      failure: failure,
    );
  }
}
