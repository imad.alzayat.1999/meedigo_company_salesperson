part of 'login_bloc.dart';

@freezed
class LoginState with _$LoginState {
  const factory LoginState.initial() = _Initial;

  /// login states
  const factory LoginState.loginLoading() = _LoginLoading;
  const factory LoginState.loginSuccess(
    LoginResponse loginResponse,
  ) = _LoginSuccess;
  const factory LoginState.loginFailed(
    Failure failure,
  ) = _LoginFailed;
}
