import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_consts.dart';
import 'package:meedigo_company_sales_person/core/helpers/enums.dart';
import 'package:meedigo_company_sales_person/core/networking/api_requests.dart';
import 'package:meedigo_company_sales_person/core/widgets/app_padding_widget.dart';
import 'package:meedigo_company_sales_person/core/widgets/loading_indicator_widget.dart';
import 'package:meedigo_company_sales_person/features/auth/presentation/bloc/check_code/check_code_bloc.dart';
import 'package:meedigo_company_sales_person/features/auth/presentation/bloc/generate_code/generate_code_bloc.dart';
import 'package:meedigo_company_sales_person/features/auth/presentation/bloc/login/login_bloc.dart';
import '../../../../core/helpers/app_sizes.dart';
import '../widgets/verify_otp_widgets/otp_verify_button_widget.dart';
import '../widgets/verify_otp_widgets/verify_otp_screen_pinput_widget.dart';
import '../widgets/verify_otp_widgets/verify_otp_screen_resend_code_widget.dart';
import '../widgets/verify_otp_widgets/verify_otp_screen_subtitle_widget.dart';
import '../widgets/verify_otp_widgets/verify_otp_screen_title_widget.dart';

class VerifyOtpScreen extends StatefulWidget {
  @override
  State<VerifyOtpScreen> createState() => _VerifyOtpScreenState();
}

class _VerifyOtpScreenState extends State<VerifyOtpScreen> {
  int _seconds = 0;
  int _baseCountDownTime = 60;
  int _resendTimes = 0;
  Timer? _timer;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _init();
  }

  _init() {
    _seconds = _baseCountDownTime;
    _startCountdown();
  }

  void _startCountdown() {
    _timer = Timer.periodic(
      Duration(seconds: AppConsts.counterDuration),
      (timer) {
        setState(() {
          if (_seconds > 0) {
            _seconds--;
          } else {
            _timer!.cancel();
          }
        });
      },
    );
  }

  _increaseTheCounterOfTimesAndStartAgain() {
    setState(() {
      _resendTimes++;
      _seconds = _baseCountDownTime * _resendTimes;
    });
    if (_resendTimes >= 2) {
      context.read<CheckCodeBloc>().isWhatsapp = true;
    }
    _startCountdown();
  }

  _sendWhatsappDone() {
    setState(() {
      context.read<GenerateCodeBloc>()..isSendWhatsappBtnClicked = true;
    });
    _increaseTheCounterOfTimesAndStartAgain();
  }

  @override
  void dispose() {
    _timer!.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: AppPaddingWidget(
        child: BlocConsumer<CheckCodeBloc, CheckCodeState>(
          listener: (context, state) {
            state.whenOrNull(
              checkCodeForSmsSuccess: (response) =>
                  context.read<CheckCodeBloc>()..makeLogin(context),
              checkCodeForWhatsappSuccess: (response) =>
                  context.read<CheckCodeBloc>()..makeLogin(context),
              checkCodeForWhatsappFailed: (failure) =>
                  context.read<CheckCodeBloc>()
                    ..showSmsOrWhatsappCodeCheckFailedMessage(
                      context: context,
                      failure: failure,
                    ),
              checkCodeForSmsFailed: (failure) => context.read<CheckCodeBloc>()
                ..showSmsOrWhatsappCodeCheckFailedMessage(
                  context: context,
                  failure: failure,
                ),
            );
          },
          builder: (context, state) {
            return state.maybeWhen(
              checkCodeForSmsLoading: () => const LoadingIndicatorWidget(
                loadingSize: LoadingSizes.fullScreenAuth,
              ),
              checkCodeForWhatsappLoading: () => const LoadingIndicatorWidget(
                loadingSize: LoadingSizes.fullScreenAuth,
              ),
              orElse: () => BlocConsumer<LoginBloc, LoginState>(
                listener: (context, state) {
                  state.whenOrNull(
                    loginSuccess: (login) => context.read<LoginBloc>()
                      ..goToTheApp(
                        login: login.loginData!,
                        context: context,
                      ),
                    loginFailed: (failure) => context.read<LoginBloc>()
                      ..showLoginFailedMessage(
                        context: context,
                        failure: failure,
                      ),
                  );
                },
                builder: (context, state) {
                  return state.maybeWhen(
                    loginLoading: () => const LoadingIndicatorWidget(
                      loadingSize: LoadingSizes.fullScreenAuth,
                    ),
                    orElse: () => Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        VerifyOtpScreenTitleWidget(),
                        verticalSpacing(21),
                        VerifyOtpScreenSubtitleWidget(),
                        verticalSpacing(47),
                        VerifyOtpScreenPinPutWidget(),
                        verticalSpacing(14),
                        VerifyOtpScreenResendCodeWidget(
                          seconds: _seconds,
                          resendSuccess: (response) =>
                              _increaseTheCounterOfTimesAndStartAgain(),
                        ),
                        verticalSpacing(20),
                        OtpVerifyButtonWidget(
                          sendWhatsappDone: (response) => _sendWhatsappDone(),
                        ),
                      ],
                    ),
                  );
                },
              ),
            );
          },
        ),
      ),
    );
  }
}
