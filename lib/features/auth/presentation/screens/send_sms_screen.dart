import 'package:flutter/material.dart';
import 'package:meedigo_company_sales_person/core/widgets/app_padding_widget.dart';
import 'package:meedigo_company_sales_person/features/auth/presentation/widgets/send_sms_widgets/send_sms_screen_form_widget.dart';
import '../../../../core/helpers/app_sizes.dart';
import '../widgets/send_sms_widgets/send_sms_screen_subtitle_widget.dart';
import '../widgets/send_sms_widgets/send_sms_screen_title_widget.dart';
import '../widgets/send_sms_widgets/send_sms_screen_logo_widget.dart';

class SendSmsScreen extends StatefulWidget {
  const SendSmsScreen({Key? key}) : super(key: key);

  @override
  State<SendSmsScreen> createState() => _SendSmsScreenState();
}

class _SendSmsScreenState extends State<SendSmsScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: AppPaddingWidget(
          top: 103,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              SendSmsScreenLogoWidget(),
              verticalSpacing(15),
              SendSmsScreenTitleWidget(),
              verticalSpacing(22),
              SendSmsScreenSubtitleWidget(),
              verticalSpacing(50),
              SendSmsScreenFormWidget(),
            ],
          ),
        ),
      ),
    );
  }
}
