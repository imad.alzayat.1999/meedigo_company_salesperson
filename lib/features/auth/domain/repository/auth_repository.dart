import 'package:dartz/dartz.dart';
import 'package:meedigo_company_sales_person/core/networking/api_failure.dart';
import 'package:meedigo_company_sales_person/core/networking/api_requests.dart';
import 'package:meedigo_company_sales_person/features/auth/domain/entities/check_code.dart';
import 'package:meedigo_company_sales_person/features/auth/domain/entities/generate_code.dart';
import 'package:meedigo_company_sales_person/features/auth/domain/entities/login.dart';

abstract class AuthRepository {
  Future<Either<Failure, GenerateCodeResponse>> generateCodeForSms({
    required GenerateCodeForSmsAndWhatsappRequestBody
        generateCodeForSmsAndWhatsappRequestBody,
  });
  Future<Either<Failure, GenerateCodeResponse>> generateCodeForWhatsapp({
    required GenerateCodeForSmsAndWhatsappRequestBody
        generateCodeForSmsAndWhatsappRequestBody,
  });
  Future<Either<Failure, CheckCodeResponse>> checkCodeForSms({
    required CheckCodeForSmsAndWhatsappRequestBody
        checkCodeForSmsAndWhatsappRequestBody,
  });
  Future<Either<Failure, CheckCodeResponse>> checkCodeForWhatsapp({
    required CheckCodeForSmsAndWhatsappRequestBody
        checkCodeForSmsAndWhatsappRequestBody,
  });
  Future<Either<Failure, LoginResponse>> login({
    required LoginRequestBody loginRequestBody,
  });
}
