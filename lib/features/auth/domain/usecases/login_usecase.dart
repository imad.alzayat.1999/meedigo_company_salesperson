import 'package:dartz/dartz.dart';
import 'package:meedigo_company_sales_person/core/networking/api_failure.dart';
import 'package:meedigo_company_sales_person/core/networking/api_requests.dart';
import 'package:meedigo_company_sales_person/core/usecase/base_usecase.dart';
import 'package:meedigo_company_sales_person/features/auth/domain/entities/login.dart';
import 'package:meedigo_company_sales_person/features/auth/domain/repository/auth_repository.dart';

class LoginUseCase extends BaseUseCase<LoginResponse, LoginRequestBody> {
  final AuthRepository authRepository;

  LoginUseCase({required this.authRepository});

  @override
  Future<Either<Failure, LoginResponse>> call(
    LoginRequestBody parameters,
  ) async {
    return await authRepository.login(
      loginRequestBody: parameters,
    );
  }
}
