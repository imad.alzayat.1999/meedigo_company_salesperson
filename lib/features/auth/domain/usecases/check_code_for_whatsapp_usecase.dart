import 'package:dartz/dartz.dart';
import 'package:meedigo_company_sales_person/core/networking/api_failure.dart';
import 'package:meedigo_company_sales_person/core/networking/api_requests.dart';
import 'package:meedigo_company_sales_person/core/usecase/base_usecase.dart';
import 'package:meedigo_company_sales_person/features/auth/domain/entities/check_code.dart';
import 'package:meedigo_company_sales_person/features/auth/domain/repository/auth_repository.dart';

class CheckCodeForWhatsappUseCase extends BaseUseCase<CheckCodeResponse,
    CheckCodeForSmsAndWhatsappRequestBody> {
  final AuthRepository authRepository;

  CheckCodeForWhatsappUseCase({
    required this.authRepository,
  });

  @override
  Future<Either<Failure, CheckCodeResponse>> call(
    CheckCodeForSmsAndWhatsappRequestBody parameters,
  ) async {
    return await authRepository.checkCodeForWhatsapp(
      checkCodeForSmsAndWhatsappRequestBody: parameters,
    );
  }
}
