import 'package:dartz/dartz.dart';
import 'package:meedigo_company_sales_person/core/networking/api_failure.dart';
import 'package:meedigo_company_sales_person/core/networking/api_requests.dart';
import 'package:meedigo_company_sales_person/core/usecase/base_usecase.dart';
import 'package:meedigo_company_sales_person/features/auth/domain/entities/generate_code.dart';
import 'package:meedigo_company_sales_person/features/auth/domain/repository/auth_repository.dart';

class GenerateCodeForWhatsappUseCase extends BaseUseCase<GenerateCodeResponse,
    GenerateCodeForSmsAndWhatsappRequestBody> {
  final AuthRepository authRepository;

  GenerateCodeForWhatsappUseCase({
    required this.authRepository,
  });

  @override
  Future<Either<Failure, GenerateCodeResponse>> call(
    GenerateCodeForSmsAndWhatsappRequestBody parameters,
  ) async {
    return await authRepository.generateCodeForWhatsapp(
      generateCodeForSmsAndWhatsappRequestBody: parameters,
    );
  }
}
