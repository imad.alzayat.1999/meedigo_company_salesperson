import 'package:equatable/equatable.dart';

class CheckCodeResponse extends Equatable {
  final CheckCodeData? checkCodeData;

  CheckCodeResponse({required this.checkCodeData});

  @override
  // TODO: implement props
  List<Object?> get props => [checkCodeData];
}

class CheckCodeData extends Equatable {
  final bool verificationsCheck;
  final String verificationsWay;

  CheckCodeData({
    required this.verificationsCheck,
    required this.verificationsWay,
  });

  @override
  // TODO: implement props
  List<Object?> get props => [
        verificationsCheck,
        verificationsWay,
      ];
}
