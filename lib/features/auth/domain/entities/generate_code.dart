import 'package:equatable/equatable.dart';

class GenerateCodeResponse extends Equatable {
  final String message;

  GenerateCodeResponse({required this.message});

  @override
  // TODO: implement props
  List<Object?> get props => [message];
}
