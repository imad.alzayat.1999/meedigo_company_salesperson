import 'package:equatable/equatable.dart';

class LoginResponse extends Equatable {
  final LoginData? loginData;

  LoginResponse({required this.loginData});

  @override
  List<Object?> get props => [loginData];
}

class LoginData extends Equatable {
  final int salesPersonId;
  final String token;
  final String type;
  final int companyId;
  final String companyName;
  final int allowOrder;
  final int active;
  final List<String> representatives;

  LoginData({
    required this.salesPersonId,
    required this.token,
    required this.type,
    required this.companyId,
    required this.companyName,
    required this.allowOrder,
    required this.active,
    required this.representatives,
  });

  factory LoginData.fromJson(Map<String, dynamic> json) => LoginData(
        salesPersonId: json["salesperson_id"],
        token: json["token"],
        type: json["type"],
        companyId: json["company_id"],
        active: json["active"] == null ? null : json["active"],
        allowOrder: json["allow_order"] == null ? null : json["allow_order"],
        companyName: json["company_name"] == null ? null : json["company_name"],
        representatives: json["representatives"] == null
            ? []
            : List<String>.from(
                json["representatives"].map((x) => x),
              ),
      );

  Map<String, dynamic> toJson() => {
        "token": "Bearer ${token}",
        "salesperson_id": salesPersonId,
        "type": type,
        "company_id": companyId,
        "company_name": companyName,
        "allow_order": allowOrder,
        "active": active,
        "representatives": List<dynamic>.from(representatives.map((x) => x)),
      };

  @override
  // TODO: implement props
  List<Object?> get props => [
        salesPersonId,
        token,
        type,
        companyId,
        companyName,
        active,
        allowOrder,
        representatives,
      ];
}
