import 'package:dartz/dartz.dart';
import 'package:meedigo_company_sales_person/core/networking/api_failure.dart';
import 'package:meedigo_company_sales_person/core/networking/api_requests.dart';
import 'package:meedigo_company_sales_person/core/usecase/base_usecase.dart';
import 'package:meedigo_company_sales_person/features/attendance/domain/entities/attendance_response.dart';
import 'package:meedigo_company_sales_person/features/attendance/domain/repository/attendance_repository.dart';

class CreateLocationUseCase
    extends BaseUseCase<AttendanceResponse, AttendanceRequestBody> {
  final AttendanceRepository attendanceRepository;

  CreateLocationUseCase({
    required this.attendanceRepository,
  });

  @override
  Future<Either<Failure, AttendanceResponse>> call(
    AttendanceRequestBody parameters,
  ) async {
    return await attendanceRepository.createLocation(
      attendanceRequestBody: parameters,
    );
  }
}
