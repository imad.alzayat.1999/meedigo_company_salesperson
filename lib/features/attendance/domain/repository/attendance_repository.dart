import 'package:dartz/dartz.dart';
import 'package:meedigo_company_sales_person/core/networking/api_failure.dart';
import 'package:meedigo_company_sales_person/core/networking/api_requests.dart';
import 'package:meedigo_company_sales_person/features/attendance/domain/entities/attendance_response.dart';

abstract class AttendanceRepository {
  Future<Either<Failure, AttendanceResponse>> checkIn({
    required AttendanceRequestBody attendanceRequestBody,
  });
  Future<Either<Failure, AttendanceResponse>> checkOut({
    required AttendanceRequestBody attendanceRequestBody,
  });
  Future<Either<Failure, AttendanceResponse>> createLocation({
    required AttendanceRequestBody attendanceRequestBody,
  });
}
