import 'package:equatable/equatable.dart';

class AttendanceResponse extends Equatable {
  final String message;

  AttendanceResponse({required this.message});

  @override
  // TODO: implement props
  List<Object?> get props => [message];
}
