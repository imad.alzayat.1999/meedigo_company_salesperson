import 'package:dartz/dartz.dart';
import 'package:meedigo_company_sales_person/core/mappers/attendance_mapper.dart';
import 'package:meedigo_company_sales_person/core/networking/api_error_model.dart';
import 'package:meedigo_company_sales_person/core/networking/api_failure.dart';
import 'package:meedigo_company_sales_person/core/networking/api_requests.dart';
import 'package:meedigo_company_sales_person/features/attendance/data/datasources/attendance_remote_datasource.dart';
import 'package:meedigo_company_sales_person/features/attendance/domain/entities/attendance_response.dart';
import 'package:meedigo_company_sales_person/features/attendance/domain/repository/attendance_repository.dart';

class AttendanceRepositoryImpl implements AttendanceRepository {
  final AttendanceRemoteDataSource attendanceRemoteDataSource;

  AttendanceRepositoryImpl({
    required this.attendanceRemoteDataSource,
  });

  @override
  Future<Either<Failure, AttendanceResponse>> checkIn({
    required AttendanceRequestBody attendanceRequestBody,
  }) async {
    try {
      final result = await attendanceRemoteDataSource.checkIn(
        attendanceRequestBody: attendanceRequestBody,
      );
      return Right(result.toDomain());
    } on ApiErrorModel catch (e) {
      return Left(
        ServerFailure(
          errorMessage: e.message!,
          statusCode: e.code!,
        ),
      );
    }
  }

  @override
  Future<Either<Failure, AttendanceResponse>> checkOut({
    required AttendanceRequestBody attendanceRequestBody,
  }) async {
    try {
      final result = await attendanceRemoteDataSource.checkout(
        attendanceRequestBody: attendanceRequestBody,
      );
      return Right(result.toDomain());
    } on ApiErrorModel catch (e) {
      return Left(
        ServerFailure(
          errorMessage: e.message!,
          statusCode: e.code!,
        ),
      );
    }
  }

  @override
  Future<Either<Failure, AttendanceResponse>> createLocation({
    required AttendanceRequestBody attendanceRequestBody,
  }) async {
    try {
      final result = await attendanceRemoteDataSource.createLocation(
        attendanceRequestBody: attendanceRequestBody,
      );
      return Right(result.toDomain());
    } on ApiErrorModel catch (e) {
      return Left(
        ServerFailure(
          errorMessage: e.message!,
          statusCode: e.code!,
        ),
      );
    }
  }
}
