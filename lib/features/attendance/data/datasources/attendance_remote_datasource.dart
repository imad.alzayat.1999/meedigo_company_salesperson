import 'package:dio/dio.dart';
import 'package:meedigo_company_sales_person/core/networking/api_error_handler.dart';
import 'package:meedigo_company_sales_person/core/networking/api_requests.dart';
import 'package:meedigo_company_sales_person/core/networking/api_services.dart';
import 'package:meedigo_company_sales_person/features/attendance/data/models/attendance_response_model.dart';

abstract class AttendanceRemoteDataSource {
  Future<AttendanceResponseModel> checkIn({
    required AttendanceRequestBody attendanceRequestBody,
  });
  Future<AttendanceResponseModel> checkout({
    required AttendanceRequestBody attendanceRequestBody,
  });
  Future<AttendanceResponseModel> createLocation({
    required AttendanceRequestBody attendanceRequestBody,
  });
}

class AttendanceRemoteDataSourceImpl implements AttendanceRemoteDataSource {
  final ApiService apiService;

  AttendanceRemoteDataSourceImpl({
    required this.apiService,
  });

  @override
  Future<AttendanceResponseModel> checkIn({
    required AttendanceRequestBody attendanceRequestBody,
  }) async {
    try {
      final result = await apiService.checkIn(
        attendanceRequestBody: attendanceRequestBody,
      );
      return result;
    } on DioException catch (e) {
      throw handleError(e);
    }
  }

  @override
  Future<AttendanceResponseModel> checkout({
    required AttendanceRequestBody attendanceRequestBody,
  }) async {
    try {
      final result = await apiService.checkOut(
        attendanceRequestBody: attendanceRequestBody,
      );
      return result;
    } on DioException catch (e) {
      throw handleError(e);
    }
  }

  @override
  Future<AttendanceResponseModel> createLocation({
    required AttendanceRequestBody attendanceRequestBody,
  }) async {
    try {
      final result = await apiService.createLocation(
        attendanceRequestBody: attendanceRequestBody,
      );
      return result;
    } on DioException catch (e) {
      throw handleError(e);
    }
  }
}
