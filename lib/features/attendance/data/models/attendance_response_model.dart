import 'package:json_annotation/json_annotation.dart';
part 'attendance_response_model.g.dart';

@JsonSerializable()
class AttendanceResponseModel {
  @JsonKey(name: "message")
  final String? message;

  AttendanceResponseModel({required this.message});

  factory AttendanceResponseModel.fromJson(Map<String, dynamic> json) =>
      _$AttendanceResponseModelFromJson(json);

  Map<String , dynamic> toJson() => _$AttendanceResponseModelToJson(this);
}