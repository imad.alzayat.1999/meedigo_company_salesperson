import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:meedigo_company_sales_person/core/widgets/app_padding_widget.dart';
import '../../../../core/helpers/app_sizes.dart';
import '../widgets/check_in_screen_content_widget.dart';
import '../widgets/check_in_screen_fingerprint_icon_widget.dart';
import '../widgets/check_in_screen_verify_button_widget.dart';

class CheckInScreen extends StatelessWidget {
  const CheckInScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return CheckInScreenContent();
  }
}

class CheckInScreenContent extends StatefulWidget {
  const CheckInScreenContent({
    Key? key,
  }) : super(key: key);

  @override
  State<CheckInScreenContent> createState() => _CheckInScreenContentState();
}

class _CheckInScreenContentState extends State<CheckInScreenContent> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: AppPaddingWidget(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            CheckInFingerprintIconWidget(),
            verticalSpacing(20),
            CheckInContentWidget(),
            verticalSpacing(50),
            CheckInScreenVerifyButtonWidget(),
          ],
        ),
      ),
    );
  }
}
