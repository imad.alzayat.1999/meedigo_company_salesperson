part of 'attendance_bloc.dart';

@freezed
class AttendanceState with _$AttendanceState {
  const factory AttendanceState.initial() = _Initial;

  /// checkin request states
  const factory AttendanceState.checkInLoading() = _CheckInLoading;
  const factory AttendanceState.checkInDone() = _CheckInDone;
  const factory AttendanceState.checkInFailed(
    Failure failure,
  ) = _CheckInFailed;

  /// checkout request states
  const factory AttendanceState.checkOutLoading() = _CheckOutLoading;
  const factory AttendanceState.checkOutDone() = _CheckOutDone;
  const factory AttendanceState.checkOutFailed(
    Failure failure,
  ) = _CheckOutFailed;

  /// create location request states
  const factory AttendanceState.createLocationLoading() =
      _CreateLocationLoading;
  const factory AttendanceState.createLocationDone() = _CreateLocationDone;
  const factory AttendanceState.createLocationFailed(
    Failure failure,
  ) = _CreateLocationFailed;
}
