import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:geolocator/geolocator.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_consts.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_extensions.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_functions.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_preferences.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_strings.dart';
import 'package:meedigo_company_sales_person/core/networking/api_failure.dart';
import 'package:meedigo_company_sales_person/core/networking/api_requests.dart';
import 'package:meedigo_company_sales_person/core/routing/app_routes.dart';
import 'package:meedigo_company_sales_person/features/attendance/domain/usecases/checkin_usecase.dart';
import 'package:meedigo_company_sales_person/features/attendance/domain/usecases/checkout_usecase.dart';
import 'package:meedigo_company_sales_person/features/attendance/domain/usecases/create_location_usecase.dart';
import 'package:meedigo_company_sales_person/features/auth/domain/entities/login.dart';

part 'attendance_event.dart';
part 'attendance_state.dart';
part 'attendance_bloc.freezed.dart';

class AttendanceBloc extends Bloc<AttendanceEvent, AttendanceState> {
  final CheckInUseCase checkInUseCase;
  final CheckOutUseCase checkOutUseCase;
  final CreateLocationUseCase createLocationUseCase;
  late LocationSettings locationSettings;
  StreamSubscription<Position>? positionStream;

  AttendanceBloc({
    required this.checkInUseCase,
    required this.checkOutUseCase,
    required this.createLocationUseCase,
  }) : super(AttendanceState.initial()) {
    on<AttendanceEvent>(
      (event, emit) async {
        await event.when(
          checkIn: (request) async {
            emit(AttendanceState.checkInLoading());
            final failureOrCheckIn = await checkInUseCase(request);
            failureOrCheckIn.fold(
              (l) => emit(AttendanceState.checkInFailed(l)),
              (r) => emit(AttendanceState.checkInDone()),
            );
          },
          checkOut: (request) async {
            emit(AttendanceState.checkOutLoading());
            final failureOrCheckOut = await checkOutUseCase(request);
            failureOrCheckOut.fold(
              (l) => emit(AttendanceState.checkOutFailed(l)),
              (r) => emit(AttendanceState.checkOutDone()),
            );
          },
          createLocation: (request) async {
            emit(AttendanceState.createLocationLoading());
            final failureOrCreateLocation = await createLocationUseCase(
              request,
            );
            failureOrCreateLocation.fold(
              (l) => emit(AttendanceState.createLocationFailed(l)),
              (r) => emit(AttendanceState.createLocationDone()),
            );
          },
        );
      },
    );
  }

  /// track user location
  trackUserLocation() {
    LoginData loginData = AppPreferences.getUserInfo();
    if (positionStream == null) {
      final _positionStream = Geolocator.getPositionStream(
        locationSettings: AppConsts.getLocationSettingsForTracking(),
      );
      positionStream = _positionStream.listen((position) {
        add(
          AttendanceEvent.createLocation(
            AttendanceRequestBody(
              latitude: position.latitude.toString(),
              longitude: position.longitude.toString(),
              salespersonId: loginData.salesPersonId,
            ),
          ),
        );
      });
    } else {
      positionStream = Geolocator.getPositionStream(
        locationSettings: AppConsts.getLocationSettingsForTracking(),
      ).listen((Position position) {
        add(
          AttendanceEvent.createLocation(
            AttendanceRequestBody(
              latitude: position.latitude.toString(),
              longitude: position.longitude.toString(),
              salespersonId: loginData.salesPersonId,
            ),
          ),
        );
      });
    }
  }

  /// cancel tracking
  cancelTracking() {
    if (positionStream != null) {
      positionStream!.cancel();
    }
  }

  /// function to make attendance
  makeAttendance({
    required BuildContext context,
    required Position position,
  }) {
    LoginData login = AppPreferences.getUserInfo();
    AttendanceRequestBody attendanceRequest = AttendanceRequestBody(
      latitude: position.latitude.toString(),
      longitude: position.longitude.toString(),
      salespersonId: login.salesPersonId,
    );
    add(AttendanceEvent.checkIn(attendanceRequest));
  }

  /// checkout functionality
  makeCheckout({
    required BuildContext context,
    required Position position,
  }) {
    LoginData login = AppPreferences.getUserInfo();
    AttendanceRequestBody attendanceRequest = AttendanceRequestBody(
      latitude: position.latitude.toString(),
      longitude: position.longitude.toString(),
      salespersonId: login.salesPersonId,
    );
    add(AttendanceEvent.checkOut(attendanceRequest));
  }

  /// if checkout done , then route to checkin page
  cancelTrackingAndBackToAttendanceRoute(
    BuildContext context,
  ) {
    cancelTracking();
    AppPreferences.clearCheckInData();
    context.pushNamedAndRemoveUntil(
      AppRoutes.checkInRoute,
    );
  }

  /// else show failure message
  showAttendanceFailureMessage({
    required BuildContext context,
    required Failure failure,
  }) {
    AppFunctions.showFailedMessage(
      context: context,
      failure: failure,
    );
  }

  /// handle success state of attendance registration
  attendanceRegistrationSuccess({
    required BuildContext context,
    required List products,
  }) {
    AppFunctions.showSuccessMessage(
      context: context,
      message: AppStrings.attendanceDoneString,
    );
    AppPreferences.passCheckIn();
    trackUserLocation();
    if (products.isEmpty) {
      context.pushNamedAndRemoveUntil(AppRoutes.mainRoute);
    } else {
      // final product = products[0];
      // context.pushNamedAndRemoveUntil(
      //   AppRoutes.pharmaciesByWarehouseRoute,
      //   arguments: PharmacyContentRouteParameters(
      //     marketId: product.marketId,
      //     pharmacyName: product.marketName,
      //   ),
      // );
    }
  }
}
