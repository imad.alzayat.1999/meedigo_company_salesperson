// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'attendance_bloc.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

/// @nodoc
mixin _$AttendanceEvent {
  AttendanceRequestBody get attendanceRequestBody =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(AttendanceRequestBody attendanceRequestBody)
        checkIn,
    required TResult Function(AttendanceRequestBody attendanceRequestBody)
        checkOut,
    required TResult Function(AttendanceRequestBody attendanceRequestBody)
        createLocation,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(AttendanceRequestBody attendanceRequestBody)? checkIn,
    TResult? Function(AttendanceRequestBody attendanceRequestBody)? checkOut,
    TResult? Function(AttendanceRequestBody attendanceRequestBody)?
        createLocation,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(AttendanceRequestBody attendanceRequestBody)? checkIn,
    TResult Function(AttendanceRequestBody attendanceRequestBody)? checkOut,
    TResult Function(AttendanceRequestBody attendanceRequestBody)?
        createLocation,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_CheckIn value) checkIn,
    required TResult Function(_CheckOut value) checkOut,
    required TResult Function(_CreateLocation value) createLocation,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_CheckIn value)? checkIn,
    TResult? Function(_CheckOut value)? checkOut,
    TResult? Function(_CreateLocation value)? createLocation,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_CheckIn value)? checkIn,
    TResult Function(_CheckOut value)? checkOut,
    TResult Function(_CreateLocation value)? createLocation,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $AttendanceEventCopyWith<AttendanceEvent> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $AttendanceEventCopyWith<$Res> {
  factory $AttendanceEventCopyWith(
          AttendanceEvent value, $Res Function(AttendanceEvent) then) =
      _$AttendanceEventCopyWithImpl<$Res, AttendanceEvent>;
  @useResult
  $Res call({AttendanceRequestBody attendanceRequestBody});
}

/// @nodoc
class _$AttendanceEventCopyWithImpl<$Res, $Val extends AttendanceEvent>
    implements $AttendanceEventCopyWith<$Res> {
  _$AttendanceEventCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? attendanceRequestBody = null,
  }) {
    return _then(_value.copyWith(
      attendanceRequestBody: null == attendanceRequestBody
          ? _value.attendanceRequestBody
          : attendanceRequestBody // ignore: cast_nullable_to_non_nullable
              as AttendanceRequestBody,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$CheckInImplCopyWith<$Res>
    implements $AttendanceEventCopyWith<$Res> {
  factory _$$CheckInImplCopyWith(
          _$CheckInImpl value, $Res Function(_$CheckInImpl) then) =
      __$$CheckInImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({AttendanceRequestBody attendanceRequestBody});
}

/// @nodoc
class __$$CheckInImplCopyWithImpl<$Res>
    extends _$AttendanceEventCopyWithImpl<$Res, _$CheckInImpl>
    implements _$$CheckInImplCopyWith<$Res> {
  __$$CheckInImplCopyWithImpl(
      _$CheckInImpl _value, $Res Function(_$CheckInImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? attendanceRequestBody = null,
  }) {
    return _then(_$CheckInImpl(
      null == attendanceRequestBody
          ? _value.attendanceRequestBody
          : attendanceRequestBody // ignore: cast_nullable_to_non_nullable
              as AttendanceRequestBody,
    ));
  }
}

/// @nodoc

class _$CheckInImpl implements _CheckIn {
  const _$CheckInImpl(this.attendanceRequestBody);

  @override
  final AttendanceRequestBody attendanceRequestBody;

  @override
  String toString() {
    return 'AttendanceEvent.checkIn(attendanceRequestBody: $attendanceRequestBody)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$CheckInImpl &&
            (identical(other.attendanceRequestBody, attendanceRequestBody) ||
                other.attendanceRequestBody == attendanceRequestBody));
  }

  @override
  int get hashCode => Object.hash(runtimeType, attendanceRequestBody);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$CheckInImplCopyWith<_$CheckInImpl> get copyWith =>
      __$$CheckInImplCopyWithImpl<_$CheckInImpl>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(AttendanceRequestBody attendanceRequestBody)
        checkIn,
    required TResult Function(AttendanceRequestBody attendanceRequestBody)
        checkOut,
    required TResult Function(AttendanceRequestBody attendanceRequestBody)
        createLocation,
  }) {
    return checkIn(attendanceRequestBody);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(AttendanceRequestBody attendanceRequestBody)? checkIn,
    TResult? Function(AttendanceRequestBody attendanceRequestBody)? checkOut,
    TResult? Function(AttendanceRequestBody attendanceRequestBody)?
        createLocation,
  }) {
    return checkIn?.call(attendanceRequestBody);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(AttendanceRequestBody attendanceRequestBody)? checkIn,
    TResult Function(AttendanceRequestBody attendanceRequestBody)? checkOut,
    TResult Function(AttendanceRequestBody attendanceRequestBody)?
        createLocation,
    required TResult orElse(),
  }) {
    if (checkIn != null) {
      return checkIn(attendanceRequestBody);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_CheckIn value) checkIn,
    required TResult Function(_CheckOut value) checkOut,
    required TResult Function(_CreateLocation value) createLocation,
  }) {
    return checkIn(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_CheckIn value)? checkIn,
    TResult? Function(_CheckOut value)? checkOut,
    TResult? Function(_CreateLocation value)? createLocation,
  }) {
    return checkIn?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_CheckIn value)? checkIn,
    TResult Function(_CheckOut value)? checkOut,
    TResult Function(_CreateLocation value)? createLocation,
    required TResult orElse(),
  }) {
    if (checkIn != null) {
      return checkIn(this);
    }
    return orElse();
  }
}

abstract class _CheckIn implements AttendanceEvent {
  const factory _CheckIn(final AttendanceRequestBody attendanceRequestBody) =
      _$CheckInImpl;

  @override
  AttendanceRequestBody get attendanceRequestBody;
  @override
  @JsonKey(ignore: true)
  _$$CheckInImplCopyWith<_$CheckInImpl> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$CheckOutImplCopyWith<$Res>
    implements $AttendanceEventCopyWith<$Res> {
  factory _$$CheckOutImplCopyWith(
          _$CheckOutImpl value, $Res Function(_$CheckOutImpl) then) =
      __$$CheckOutImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({AttendanceRequestBody attendanceRequestBody});
}

/// @nodoc
class __$$CheckOutImplCopyWithImpl<$Res>
    extends _$AttendanceEventCopyWithImpl<$Res, _$CheckOutImpl>
    implements _$$CheckOutImplCopyWith<$Res> {
  __$$CheckOutImplCopyWithImpl(
      _$CheckOutImpl _value, $Res Function(_$CheckOutImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? attendanceRequestBody = null,
  }) {
    return _then(_$CheckOutImpl(
      null == attendanceRequestBody
          ? _value.attendanceRequestBody
          : attendanceRequestBody // ignore: cast_nullable_to_non_nullable
              as AttendanceRequestBody,
    ));
  }
}

/// @nodoc

class _$CheckOutImpl implements _CheckOut {
  const _$CheckOutImpl(this.attendanceRequestBody);

  @override
  final AttendanceRequestBody attendanceRequestBody;

  @override
  String toString() {
    return 'AttendanceEvent.checkOut(attendanceRequestBody: $attendanceRequestBody)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$CheckOutImpl &&
            (identical(other.attendanceRequestBody, attendanceRequestBody) ||
                other.attendanceRequestBody == attendanceRequestBody));
  }

  @override
  int get hashCode => Object.hash(runtimeType, attendanceRequestBody);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$CheckOutImplCopyWith<_$CheckOutImpl> get copyWith =>
      __$$CheckOutImplCopyWithImpl<_$CheckOutImpl>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(AttendanceRequestBody attendanceRequestBody)
        checkIn,
    required TResult Function(AttendanceRequestBody attendanceRequestBody)
        checkOut,
    required TResult Function(AttendanceRequestBody attendanceRequestBody)
        createLocation,
  }) {
    return checkOut(attendanceRequestBody);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(AttendanceRequestBody attendanceRequestBody)? checkIn,
    TResult? Function(AttendanceRequestBody attendanceRequestBody)? checkOut,
    TResult? Function(AttendanceRequestBody attendanceRequestBody)?
        createLocation,
  }) {
    return checkOut?.call(attendanceRequestBody);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(AttendanceRequestBody attendanceRequestBody)? checkIn,
    TResult Function(AttendanceRequestBody attendanceRequestBody)? checkOut,
    TResult Function(AttendanceRequestBody attendanceRequestBody)?
        createLocation,
    required TResult orElse(),
  }) {
    if (checkOut != null) {
      return checkOut(attendanceRequestBody);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_CheckIn value) checkIn,
    required TResult Function(_CheckOut value) checkOut,
    required TResult Function(_CreateLocation value) createLocation,
  }) {
    return checkOut(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_CheckIn value)? checkIn,
    TResult? Function(_CheckOut value)? checkOut,
    TResult? Function(_CreateLocation value)? createLocation,
  }) {
    return checkOut?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_CheckIn value)? checkIn,
    TResult Function(_CheckOut value)? checkOut,
    TResult Function(_CreateLocation value)? createLocation,
    required TResult orElse(),
  }) {
    if (checkOut != null) {
      return checkOut(this);
    }
    return orElse();
  }
}

abstract class _CheckOut implements AttendanceEvent {
  const factory _CheckOut(final AttendanceRequestBody attendanceRequestBody) =
      _$CheckOutImpl;

  @override
  AttendanceRequestBody get attendanceRequestBody;
  @override
  @JsonKey(ignore: true)
  _$$CheckOutImplCopyWith<_$CheckOutImpl> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$CreateLocationImplCopyWith<$Res>
    implements $AttendanceEventCopyWith<$Res> {
  factory _$$CreateLocationImplCopyWith(_$CreateLocationImpl value,
          $Res Function(_$CreateLocationImpl) then) =
      __$$CreateLocationImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({AttendanceRequestBody attendanceRequestBody});
}

/// @nodoc
class __$$CreateLocationImplCopyWithImpl<$Res>
    extends _$AttendanceEventCopyWithImpl<$Res, _$CreateLocationImpl>
    implements _$$CreateLocationImplCopyWith<$Res> {
  __$$CreateLocationImplCopyWithImpl(
      _$CreateLocationImpl _value, $Res Function(_$CreateLocationImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? attendanceRequestBody = null,
  }) {
    return _then(_$CreateLocationImpl(
      null == attendanceRequestBody
          ? _value.attendanceRequestBody
          : attendanceRequestBody // ignore: cast_nullable_to_non_nullable
              as AttendanceRequestBody,
    ));
  }
}

/// @nodoc

class _$CreateLocationImpl implements _CreateLocation {
  const _$CreateLocationImpl(this.attendanceRequestBody);

  @override
  final AttendanceRequestBody attendanceRequestBody;

  @override
  String toString() {
    return 'AttendanceEvent.createLocation(attendanceRequestBody: $attendanceRequestBody)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$CreateLocationImpl &&
            (identical(other.attendanceRequestBody, attendanceRequestBody) ||
                other.attendanceRequestBody == attendanceRequestBody));
  }

  @override
  int get hashCode => Object.hash(runtimeType, attendanceRequestBody);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$CreateLocationImplCopyWith<_$CreateLocationImpl> get copyWith =>
      __$$CreateLocationImplCopyWithImpl<_$CreateLocationImpl>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(AttendanceRequestBody attendanceRequestBody)
        checkIn,
    required TResult Function(AttendanceRequestBody attendanceRequestBody)
        checkOut,
    required TResult Function(AttendanceRequestBody attendanceRequestBody)
        createLocation,
  }) {
    return createLocation(attendanceRequestBody);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(AttendanceRequestBody attendanceRequestBody)? checkIn,
    TResult? Function(AttendanceRequestBody attendanceRequestBody)? checkOut,
    TResult? Function(AttendanceRequestBody attendanceRequestBody)?
        createLocation,
  }) {
    return createLocation?.call(attendanceRequestBody);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(AttendanceRequestBody attendanceRequestBody)? checkIn,
    TResult Function(AttendanceRequestBody attendanceRequestBody)? checkOut,
    TResult Function(AttendanceRequestBody attendanceRequestBody)?
        createLocation,
    required TResult orElse(),
  }) {
    if (createLocation != null) {
      return createLocation(attendanceRequestBody);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_CheckIn value) checkIn,
    required TResult Function(_CheckOut value) checkOut,
    required TResult Function(_CreateLocation value) createLocation,
  }) {
    return createLocation(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_CheckIn value)? checkIn,
    TResult? Function(_CheckOut value)? checkOut,
    TResult? Function(_CreateLocation value)? createLocation,
  }) {
    return createLocation?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_CheckIn value)? checkIn,
    TResult Function(_CheckOut value)? checkOut,
    TResult Function(_CreateLocation value)? createLocation,
    required TResult orElse(),
  }) {
    if (createLocation != null) {
      return createLocation(this);
    }
    return orElse();
  }
}

abstract class _CreateLocation implements AttendanceEvent {
  const factory _CreateLocation(
      final AttendanceRequestBody attendanceRequestBody) = _$CreateLocationImpl;

  @override
  AttendanceRequestBody get attendanceRequestBody;
  @override
  @JsonKey(ignore: true)
  _$$CreateLocationImplCopyWith<_$CreateLocationImpl> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
mixin _$AttendanceState {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() checkInLoading,
    required TResult Function() checkInDone,
    required TResult Function(Failure failure) checkInFailed,
    required TResult Function() checkOutLoading,
    required TResult Function() checkOutDone,
    required TResult Function(Failure failure) checkOutFailed,
    required TResult Function() createLocationLoading,
    required TResult Function() createLocationDone,
    required TResult Function(Failure failure) createLocationFailed,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? checkInLoading,
    TResult? Function()? checkInDone,
    TResult? Function(Failure failure)? checkInFailed,
    TResult? Function()? checkOutLoading,
    TResult? Function()? checkOutDone,
    TResult? Function(Failure failure)? checkOutFailed,
    TResult? Function()? createLocationLoading,
    TResult? Function()? createLocationDone,
    TResult? Function(Failure failure)? createLocationFailed,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? checkInLoading,
    TResult Function()? checkInDone,
    TResult Function(Failure failure)? checkInFailed,
    TResult Function()? checkOutLoading,
    TResult Function()? checkOutDone,
    TResult Function(Failure failure)? checkOutFailed,
    TResult Function()? createLocationLoading,
    TResult Function()? createLocationDone,
    TResult Function(Failure failure)? createLocationFailed,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_CheckInLoading value) checkInLoading,
    required TResult Function(_CheckInDone value) checkInDone,
    required TResult Function(_CheckInFailed value) checkInFailed,
    required TResult Function(_CheckOutLoading value) checkOutLoading,
    required TResult Function(_CheckOutDone value) checkOutDone,
    required TResult Function(_CheckOutFailed value) checkOutFailed,
    required TResult Function(_CreateLocationLoading value)
        createLocationLoading,
    required TResult Function(_CreateLocationDone value) createLocationDone,
    required TResult Function(_CreateLocationFailed value) createLocationFailed,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_CheckInLoading value)? checkInLoading,
    TResult? Function(_CheckInDone value)? checkInDone,
    TResult? Function(_CheckInFailed value)? checkInFailed,
    TResult? Function(_CheckOutLoading value)? checkOutLoading,
    TResult? Function(_CheckOutDone value)? checkOutDone,
    TResult? Function(_CheckOutFailed value)? checkOutFailed,
    TResult? Function(_CreateLocationLoading value)? createLocationLoading,
    TResult? Function(_CreateLocationDone value)? createLocationDone,
    TResult? Function(_CreateLocationFailed value)? createLocationFailed,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_CheckInLoading value)? checkInLoading,
    TResult Function(_CheckInDone value)? checkInDone,
    TResult Function(_CheckInFailed value)? checkInFailed,
    TResult Function(_CheckOutLoading value)? checkOutLoading,
    TResult Function(_CheckOutDone value)? checkOutDone,
    TResult Function(_CheckOutFailed value)? checkOutFailed,
    TResult Function(_CreateLocationLoading value)? createLocationLoading,
    TResult Function(_CreateLocationDone value)? createLocationDone,
    TResult Function(_CreateLocationFailed value)? createLocationFailed,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $AttendanceStateCopyWith<$Res> {
  factory $AttendanceStateCopyWith(
          AttendanceState value, $Res Function(AttendanceState) then) =
      _$AttendanceStateCopyWithImpl<$Res, AttendanceState>;
}

/// @nodoc
class _$AttendanceStateCopyWithImpl<$Res, $Val extends AttendanceState>
    implements $AttendanceStateCopyWith<$Res> {
  _$AttendanceStateCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$InitialImplCopyWith<$Res> {
  factory _$$InitialImplCopyWith(
          _$InitialImpl value, $Res Function(_$InitialImpl) then) =
      __$$InitialImplCopyWithImpl<$Res>;
}

/// @nodoc
class __$$InitialImplCopyWithImpl<$Res>
    extends _$AttendanceStateCopyWithImpl<$Res, _$InitialImpl>
    implements _$$InitialImplCopyWith<$Res> {
  __$$InitialImplCopyWithImpl(
      _$InitialImpl _value, $Res Function(_$InitialImpl) _then)
      : super(_value, _then);
}

/// @nodoc

class _$InitialImpl implements _Initial {
  const _$InitialImpl();

  @override
  String toString() {
    return 'AttendanceState.initial()';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$InitialImpl);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() checkInLoading,
    required TResult Function() checkInDone,
    required TResult Function(Failure failure) checkInFailed,
    required TResult Function() checkOutLoading,
    required TResult Function() checkOutDone,
    required TResult Function(Failure failure) checkOutFailed,
    required TResult Function() createLocationLoading,
    required TResult Function() createLocationDone,
    required TResult Function(Failure failure) createLocationFailed,
  }) {
    return initial();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? checkInLoading,
    TResult? Function()? checkInDone,
    TResult? Function(Failure failure)? checkInFailed,
    TResult? Function()? checkOutLoading,
    TResult? Function()? checkOutDone,
    TResult? Function(Failure failure)? checkOutFailed,
    TResult? Function()? createLocationLoading,
    TResult? Function()? createLocationDone,
    TResult? Function(Failure failure)? createLocationFailed,
  }) {
    return initial?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? checkInLoading,
    TResult Function()? checkInDone,
    TResult Function(Failure failure)? checkInFailed,
    TResult Function()? checkOutLoading,
    TResult Function()? checkOutDone,
    TResult Function(Failure failure)? checkOutFailed,
    TResult Function()? createLocationLoading,
    TResult Function()? createLocationDone,
    TResult Function(Failure failure)? createLocationFailed,
    required TResult orElse(),
  }) {
    if (initial != null) {
      return initial();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_CheckInLoading value) checkInLoading,
    required TResult Function(_CheckInDone value) checkInDone,
    required TResult Function(_CheckInFailed value) checkInFailed,
    required TResult Function(_CheckOutLoading value) checkOutLoading,
    required TResult Function(_CheckOutDone value) checkOutDone,
    required TResult Function(_CheckOutFailed value) checkOutFailed,
    required TResult Function(_CreateLocationLoading value)
        createLocationLoading,
    required TResult Function(_CreateLocationDone value) createLocationDone,
    required TResult Function(_CreateLocationFailed value) createLocationFailed,
  }) {
    return initial(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_CheckInLoading value)? checkInLoading,
    TResult? Function(_CheckInDone value)? checkInDone,
    TResult? Function(_CheckInFailed value)? checkInFailed,
    TResult? Function(_CheckOutLoading value)? checkOutLoading,
    TResult? Function(_CheckOutDone value)? checkOutDone,
    TResult? Function(_CheckOutFailed value)? checkOutFailed,
    TResult? Function(_CreateLocationLoading value)? createLocationLoading,
    TResult? Function(_CreateLocationDone value)? createLocationDone,
    TResult? Function(_CreateLocationFailed value)? createLocationFailed,
  }) {
    return initial?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_CheckInLoading value)? checkInLoading,
    TResult Function(_CheckInDone value)? checkInDone,
    TResult Function(_CheckInFailed value)? checkInFailed,
    TResult Function(_CheckOutLoading value)? checkOutLoading,
    TResult Function(_CheckOutDone value)? checkOutDone,
    TResult Function(_CheckOutFailed value)? checkOutFailed,
    TResult Function(_CreateLocationLoading value)? createLocationLoading,
    TResult Function(_CreateLocationDone value)? createLocationDone,
    TResult Function(_CreateLocationFailed value)? createLocationFailed,
    required TResult orElse(),
  }) {
    if (initial != null) {
      return initial(this);
    }
    return orElse();
  }
}

abstract class _Initial implements AttendanceState {
  const factory _Initial() = _$InitialImpl;
}

/// @nodoc
abstract class _$$CheckInLoadingImplCopyWith<$Res> {
  factory _$$CheckInLoadingImplCopyWith(_$CheckInLoadingImpl value,
          $Res Function(_$CheckInLoadingImpl) then) =
      __$$CheckInLoadingImplCopyWithImpl<$Res>;
}

/// @nodoc
class __$$CheckInLoadingImplCopyWithImpl<$Res>
    extends _$AttendanceStateCopyWithImpl<$Res, _$CheckInLoadingImpl>
    implements _$$CheckInLoadingImplCopyWith<$Res> {
  __$$CheckInLoadingImplCopyWithImpl(
      _$CheckInLoadingImpl _value, $Res Function(_$CheckInLoadingImpl) _then)
      : super(_value, _then);
}

/// @nodoc

class _$CheckInLoadingImpl implements _CheckInLoading {
  const _$CheckInLoadingImpl();

  @override
  String toString() {
    return 'AttendanceState.checkInLoading()';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$CheckInLoadingImpl);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() checkInLoading,
    required TResult Function() checkInDone,
    required TResult Function(Failure failure) checkInFailed,
    required TResult Function() checkOutLoading,
    required TResult Function() checkOutDone,
    required TResult Function(Failure failure) checkOutFailed,
    required TResult Function() createLocationLoading,
    required TResult Function() createLocationDone,
    required TResult Function(Failure failure) createLocationFailed,
  }) {
    return checkInLoading();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? checkInLoading,
    TResult? Function()? checkInDone,
    TResult? Function(Failure failure)? checkInFailed,
    TResult? Function()? checkOutLoading,
    TResult? Function()? checkOutDone,
    TResult? Function(Failure failure)? checkOutFailed,
    TResult? Function()? createLocationLoading,
    TResult? Function()? createLocationDone,
    TResult? Function(Failure failure)? createLocationFailed,
  }) {
    return checkInLoading?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? checkInLoading,
    TResult Function()? checkInDone,
    TResult Function(Failure failure)? checkInFailed,
    TResult Function()? checkOutLoading,
    TResult Function()? checkOutDone,
    TResult Function(Failure failure)? checkOutFailed,
    TResult Function()? createLocationLoading,
    TResult Function()? createLocationDone,
    TResult Function(Failure failure)? createLocationFailed,
    required TResult orElse(),
  }) {
    if (checkInLoading != null) {
      return checkInLoading();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_CheckInLoading value) checkInLoading,
    required TResult Function(_CheckInDone value) checkInDone,
    required TResult Function(_CheckInFailed value) checkInFailed,
    required TResult Function(_CheckOutLoading value) checkOutLoading,
    required TResult Function(_CheckOutDone value) checkOutDone,
    required TResult Function(_CheckOutFailed value) checkOutFailed,
    required TResult Function(_CreateLocationLoading value)
        createLocationLoading,
    required TResult Function(_CreateLocationDone value) createLocationDone,
    required TResult Function(_CreateLocationFailed value) createLocationFailed,
  }) {
    return checkInLoading(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_CheckInLoading value)? checkInLoading,
    TResult? Function(_CheckInDone value)? checkInDone,
    TResult? Function(_CheckInFailed value)? checkInFailed,
    TResult? Function(_CheckOutLoading value)? checkOutLoading,
    TResult? Function(_CheckOutDone value)? checkOutDone,
    TResult? Function(_CheckOutFailed value)? checkOutFailed,
    TResult? Function(_CreateLocationLoading value)? createLocationLoading,
    TResult? Function(_CreateLocationDone value)? createLocationDone,
    TResult? Function(_CreateLocationFailed value)? createLocationFailed,
  }) {
    return checkInLoading?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_CheckInLoading value)? checkInLoading,
    TResult Function(_CheckInDone value)? checkInDone,
    TResult Function(_CheckInFailed value)? checkInFailed,
    TResult Function(_CheckOutLoading value)? checkOutLoading,
    TResult Function(_CheckOutDone value)? checkOutDone,
    TResult Function(_CheckOutFailed value)? checkOutFailed,
    TResult Function(_CreateLocationLoading value)? createLocationLoading,
    TResult Function(_CreateLocationDone value)? createLocationDone,
    TResult Function(_CreateLocationFailed value)? createLocationFailed,
    required TResult orElse(),
  }) {
    if (checkInLoading != null) {
      return checkInLoading(this);
    }
    return orElse();
  }
}

abstract class _CheckInLoading implements AttendanceState {
  const factory _CheckInLoading() = _$CheckInLoadingImpl;
}

/// @nodoc
abstract class _$$CheckInDoneImplCopyWith<$Res> {
  factory _$$CheckInDoneImplCopyWith(
          _$CheckInDoneImpl value, $Res Function(_$CheckInDoneImpl) then) =
      __$$CheckInDoneImplCopyWithImpl<$Res>;
}

/// @nodoc
class __$$CheckInDoneImplCopyWithImpl<$Res>
    extends _$AttendanceStateCopyWithImpl<$Res, _$CheckInDoneImpl>
    implements _$$CheckInDoneImplCopyWith<$Res> {
  __$$CheckInDoneImplCopyWithImpl(
      _$CheckInDoneImpl _value, $Res Function(_$CheckInDoneImpl) _then)
      : super(_value, _then);
}

/// @nodoc

class _$CheckInDoneImpl implements _CheckInDone {
  const _$CheckInDoneImpl();

  @override
  String toString() {
    return 'AttendanceState.checkInDone()';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$CheckInDoneImpl);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() checkInLoading,
    required TResult Function() checkInDone,
    required TResult Function(Failure failure) checkInFailed,
    required TResult Function() checkOutLoading,
    required TResult Function() checkOutDone,
    required TResult Function(Failure failure) checkOutFailed,
    required TResult Function() createLocationLoading,
    required TResult Function() createLocationDone,
    required TResult Function(Failure failure) createLocationFailed,
  }) {
    return checkInDone();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? checkInLoading,
    TResult? Function()? checkInDone,
    TResult? Function(Failure failure)? checkInFailed,
    TResult? Function()? checkOutLoading,
    TResult? Function()? checkOutDone,
    TResult? Function(Failure failure)? checkOutFailed,
    TResult? Function()? createLocationLoading,
    TResult? Function()? createLocationDone,
    TResult? Function(Failure failure)? createLocationFailed,
  }) {
    return checkInDone?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? checkInLoading,
    TResult Function()? checkInDone,
    TResult Function(Failure failure)? checkInFailed,
    TResult Function()? checkOutLoading,
    TResult Function()? checkOutDone,
    TResult Function(Failure failure)? checkOutFailed,
    TResult Function()? createLocationLoading,
    TResult Function()? createLocationDone,
    TResult Function(Failure failure)? createLocationFailed,
    required TResult orElse(),
  }) {
    if (checkInDone != null) {
      return checkInDone();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_CheckInLoading value) checkInLoading,
    required TResult Function(_CheckInDone value) checkInDone,
    required TResult Function(_CheckInFailed value) checkInFailed,
    required TResult Function(_CheckOutLoading value) checkOutLoading,
    required TResult Function(_CheckOutDone value) checkOutDone,
    required TResult Function(_CheckOutFailed value) checkOutFailed,
    required TResult Function(_CreateLocationLoading value)
        createLocationLoading,
    required TResult Function(_CreateLocationDone value) createLocationDone,
    required TResult Function(_CreateLocationFailed value) createLocationFailed,
  }) {
    return checkInDone(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_CheckInLoading value)? checkInLoading,
    TResult? Function(_CheckInDone value)? checkInDone,
    TResult? Function(_CheckInFailed value)? checkInFailed,
    TResult? Function(_CheckOutLoading value)? checkOutLoading,
    TResult? Function(_CheckOutDone value)? checkOutDone,
    TResult? Function(_CheckOutFailed value)? checkOutFailed,
    TResult? Function(_CreateLocationLoading value)? createLocationLoading,
    TResult? Function(_CreateLocationDone value)? createLocationDone,
    TResult? Function(_CreateLocationFailed value)? createLocationFailed,
  }) {
    return checkInDone?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_CheckInLoading value)? checkInLoading,
    TResult Function(_CheckInDone value)? checkInDone,
    TResult Function(_CheckInFailed value)? checkInFailed,
    TResult Function(_CheckOutLoading value)? checkOutLoading,
    TResult Function(_CheckOutDone value)? checkOutDone,
    TResult Function(_CheckOutFailed value)? checkOutFailed,
    TResult Function(_CreateLocationLoading value)? createLocationLoading,
    TResult Function(_CreateLocationDone value)? createLocationDone,
    TResult Function(_CreateLocationFailed value)? createLocationFailed,
    required TResult orElse(),
  }) {
    if (checkInDone != null) {
      return checkInDone(this);
    }
    return orElse();
  }
}

abstract class _CheckInDone implements AttendanceState {
  const factory _CheckInDone() = _$CheckInDoneImpl;
}

/// @nodoc
abstract class _$$CheckInFailedImplCopyWith<$Res> {
  factory _$$CheckInFailedImplCopyWith(
          _$CheckInFailedImpl value, $Res Function(_$CheckInFailedImpl) then) =
      __$$CheckInFailedImplCopyWithImpl<$Res>;
  @useResult
  $Res call({Failure failure});
}

/// @nodoc
class __$$CheckInFailedImplCopyWithImpl<$Res>
    extends _$AttendanceStateCopyWithImpl<$Res, _$CheckInFailedImpl>
    implements _$$CheckInFailedImplCopyWith<$Res> {
  __$$CheckInFailedImplCopyWithImpl(
      _$CheckInFailedImpl _value, $Res Function(_$CheckInFailedImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? failure = null,
  }) {
    return _then(_$CheckInFailedImpl(
      null == failure
          ? _value.failure
          : failure // ignore: cast_nullable_to_non_nullable
              as Failure,
    ));
  }
}

/// @nodoc

class _$CheckInFailedImpl implements _CheckInFailed {
  const _$CheckInFailedImpl(this.failure);

  @override
  final Failure failure;

  @override
  String toString() {
    return 'AttendanceState.checkInFailed(failure: $failure)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$CheckInFailedImpl &&
            (identical(other.failure, failure) || other.failure == failure));
  }

  @override
  int get hashCode => Object.hash(runtimeType, failure);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$CheckInFailedImplCopyWith<_$CheckInFailedImpl> get copyWith =>
      __$$CheckInFailedImplCopyWithImpl<_$CheckInFailedImpl>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() checkInLoading,
    required TResult Function() checkInDone,
    required TResult Function(Failure failure) checkInFailed,
    required TResult Function() checkOutLoading,
    required TResult Function() checkOutDone,
    required TResult Function(Failure failure) checkOutFailed,
    required TResult Function() createLocationLoading,
    required TResult Function() createLocationDone,
    required TResult Function(Failure failure) createLocationFailed,
  }) {
    return checkInFailed(failure);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? checkInLoading,
    TResult? Function()? checkInDone,
    TResult? Function(Failure failure)? checkInFailed,
    TResult? Function()? checkOutLoading,
    TResult? Function()? checkOutDone,
    TResult? Function(Failure failure)? checkOutFailed,
    TResult? Function()? createLocationLoading,
    TResult? Function()? createLocationDone,
    TResult? Function(Failure failure)? createLocationFailed,
  }) {
    return checkInFailed?.call(failure);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? checkInLoading,
    TResult Function()? checkInDone,
    TResult Function(Failure failure)? checkInFailed,
    TResult Function()? checkOutLoading,
    TResult Function()? checkOutDone,
    TResult Function(Failure failure)? checkOutFailed,
    TResult Function()? createLocationLoading,
    TResult Function()? createLocationDone,
    TResult Function(Failure failure)? createLocationFailed,
    required TResult orElse(),
  }) {
    if (checkInFailed != null) {
      return checkInFailed(failure);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_CheckInLoading value) checkInLoading,
    required TResult Function(_CheckInDone value) checkInDone,
    required TResult Function(_CheckInFailed value) checkInFailed,
    required TResult Function(_CheckOutLoading value) checkOutLoading,
    required TResult Function(_CheckOutDone value) checkOutDone,
    required TResult Function(_CheckOutFailed value) checkOutFailed,
    required TResult Function(_CreateLocationLoading value)
        createLocationLoading,
    required TResult Function(_CreateLocationDone value) createLocationDone,
    required TResult Function(_CreateLocationFailed value) createLocationFailed,
  }) {
    return checkInFailed(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_CheckInLoading value)? checkInLoading,
    TResult? Function(_CheckInDone value)? checkInDone,
    TResult? Function(_CheckInFailed value)? checkInFailed,
    TResult? Function(_CheckOutLoading value)? checkOutLoading,
    TResult? Function(_CheckOutDone value)? checkOutDone,
    TResult? Function(_CheckOutFailed value)? checkOutFailed,
    TResult? Function(_CreateLocationLoading value)? createLocationLoading,
    TResult? Function(_CreateLocationDone value)? createLocationDone,
    TResult? Function(_CreateLocationFailed value)? createLocationFailed,
  }) {
    return checkInFailed?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_CheckInLoading value)? checkInLoading,
    TResult Function(_CheckInDone value)? checkInDone,
    TResult Function(_CheckInFailed value)? checkInFailed,
    TResult Function(_CheckOutLoading value)? checkOutLoading,
    TResult Function(_CheckOutDone value)? checkOutDone,
    TResult Function(_CheckOutFailed value)? checkOutFailed,
    TResult Function(_CreateLocationLoading value)? createLocationLoading,
    TResult Function(_CreateLocationDone value)? createLocationDone,
    TResult Function(_CreateLocationFailed value)? createLocationFailed,
    required TResult orElse(),
  }) {
    if (checkInFailed != null) {
      return checkInFailed(this);
    }
    return orElse();
  }
}

abstract class _CheckInFailed implements AttendanceState {
  const factory _CheckInFailed(final Failure failure) = _$CheckInFailedImpl;

  Failure get failure;
  @JsonKey(ignore: true)
  _$$CheckInFailedImplCopyWith<_$CheckInFailedImpl> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$CheckOutLoadingImplCopyWith<$Res> {
  factory _$$CheckOutLoadingImplCopyWith(_$CheckOutLoadingImpl value,
          $Res Function(_$CheckOutLoadingImpl) then) =
      __$$CheckOutLoadingImplCopyWithImpl<$Res>;
}

/// @nodoc
class __$$CheckOutLoadingImplCopyWithImpl<$Res>
    extends _$AttendanceStateCopyWithImpl<$Res, _$CheckOutLoadingImpl>
    implements _$$CheckOutLoadingImplCopyWith<$Res> {
  __$$CheckOutLoadingImplCopyWithImpl(
      _$CheckOutLoadingImpl _value, $Res Function(_$CheckOutLoadingImpl) _then)
      : super(_value, _then);
}

/// @nodoc

class _$CheckOutLoadingImpl implements _CheckOutLoading {
  const _$CheckOutLoadingImpl();

  @override
  String toString() {
    return 'AttendanceState.checkOutLoading()';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$CheckOutLoadingImpl);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() checkInLoading,
    required TResult Function() checkInDone,
    required TResult Function(Failure failure) checkInFailed,
    required TResult Function() checkOutLoading,
    required TResult Function() checkOutDone,
    required TResult Function(Failure failure) checkOutFailed,
    required TResult Function() createLocationLoading,
    required TResult Function() createLocationDone,
    required TResult Function(Failure failure) createLocationFailed,
  }) {
    return checkOutLoading();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? checkInLoading,
    TResult? Function()? checkInDone,
    TResult? Function(Failure failure)? checkInFailed,
    TResult? Function()? checkOutLoading,
    TResult? Function()? checkOutDone,
    TResult? Function(Failure failure)? checkOutFailed,
    TResult? Function()? createLocationLoading,
    TResult? Function()? createLocationDone,
    TResult? Function(Failure failure)? createLocationFailed,
  }) {
    return checkOutLoading?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? checkInLoading,
    TResult Function()? checkInDone,
    TResult Function(Failure failure)? checkInFailed,
    TResult Function()? checkOutLoading,
    TResult Function()? checkOutDone,
    TResult Function(Failure failure)? checkOutFailed,
    TResult Function()? createLocationLoading,
    TResult Function()? createLocationDone,
    TResult Function(Failure failure)? createLocationFailed,
    required TResult orElse(),
  }) {
    if (checkOutLoading != null) {
      return checkOutLoading();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_CheckInLoading value) checkInLoading,
    required TResult Function(_CheckInDone value) checkInDone,
    required TResult Function(_CheckInFailed value) checkInFailed,
    required TResult Function(_CheckOutLoading value) checkOutLoading,
    required TResult Function(_CheckOutDone value) checkOutDone,
    required TResult Function(_CheckOutFailed value) checkOutFailed,
    required TResult Function(_CreateLocationLoading value)
        createLocationLoading,
    required TResult Function(_CreateLocationDone value) createLocationDone,
    required TResult Function(_CreateLocationFailed value) createLocationFailed,
  }) {
    return checkOutLoading(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_CheckInLoading value)? checkInLoading,
    TResult? Function(_CheckInDone value)? checkInDone,
    TResult? Function(_CheckInFailed value)? checkInFailed,
    TResult? Function(_CheckOutLoading value)? checkOutLoading,
    TResult? Function(_CheckOutDone value)? checkOutDone,
    TResult? Function(_CheckOutFailed value)? checkOutFailed,
    TResult? Function(_CreateLocationLoading value)? createLocationLoading,
    TResult? Function(_CreateLocationDone value)? createLocationDone,
    TResult? Function(_CreateLocationFailed value)? createLocationFailed,
  }) {
    return checkOutLoading?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_CheckInLoading value)? checkInLoading,
    TResult Function(_CheckInDone value)? checkInDone,
    TResult Function(_CheckInFailed value)? checkInFailed,
    TResult Function(_CheckOutLoading value)? checkOutLoading,
    TResult Function(_CheckOutDone value)? checkOutDone,
    TResult Function(_CheckOutFailed value)? checkOutFailed,
    TResult Function(_CreateLocationLoading value)? createLocationLoading,
    TResult Function(_CreateLocationDone value)? createLocationDone,
    TResult Function(_CreateLocationFailed value)? createLocationFailed,
    required TResult orElse(),
  }) {
    if (checkOutLoading != null) {
      return checkOutLoading(this);
    }
    return orElse();
  }
}

abstract class _CheckOutLoading implements AttendanceState {
  const factory _CheckOutLoading() = _$CheckOutLoadingImpl;
}

/// @nodoc
abstract class _$$CheckOutDoneImplCopyWith<$Res> {
  factory _$$CheckOutDoneImplCopyWith(
          _$CheckOutDoneImpl value, $Res Function(_$CheckOutDoneImpl) then) =
      __$$CheckOutDoneImplCopyWithImpl<$Res>;
}

/// @nodoc
class __$$CheckOutDoneImplCopyWithImpl<$Res>
    extends _$AttendanceStateCopyWithImpl<$Res, _$CheckOutDoneImpl>
    implements _$$CheckOutDoneImplCopyWith<$Res> {
  __$$CheckOutDoneImplCopyWithImpl(
      _$CheckOutDoneImpl _value, $Res Function(_$CheckOutDoneImpl) _then)
      : super(_value, _then);
}

/// @nodoc

class _$CheckOutDoneImpl implements _CheckOutDone {
  const _$CheckOutDoneImpl();

  @override
  String toString() {
    return 'AttendanceState.checkOutDone()';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$CheckOutDoneImpl);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() checkInLoading,
    required TResult Function() checkInDone,
    required TResult Function(Failure failure) checkInFailed,
    required TResult Function() checkOutLoading,
    required TResult Function() checkOutDone,
    required TResult Function(Failure failure) checkOutFailed,
    required TResult Function() createLocationLoading,
    required TResult Function() createLocationDone,
    required TResult Function(Failure failure) createLocationFailed,
  }) {
    return checkOutDone();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? checkInLoading,
    TResult? Function()? checkInDone,
    TResult? Function(Failure failure)? checkInFailed,
    TResult? Function()? checkOutLoading,
    TResult? Function()? checkOutDone,
    TResult? Function(Failure failure)? checkOutFailed,
    TResult? Function()? createLocationLoading,
    TResult? Function()? createLocationDone,
    TResult? Function(Failure failure)? createLocationFailed,
  }) {
    return checkOutDone?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? checkInLoading,
    TResult Function()? checkInDone,
    TResult Function(Failure failure)? checkInFailed,
    TResult Function()? checkOutLoading,
    TResult Function()? checkOutDone,
    TResult Function(Failure failure)? checkOutFailed,
    TResult Function()? createLocationLoading,
    TResult Function()? createLocationDone,
    TResult Function(Failure failure)? createLocationFailed,
    required TResult orElse(),
  }) {
    if (checkOutDone != null) {
      return checkOutDone();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_CheckInLoading value) checkInLoading,
    required TResult Function(_CheckInDone value) checkInDone,
    required TResult Function(_CheckInFailed value) checkInFailed,
    required TResult Function(_CheckOutLoading value) checkOutLoading,
    required TResult Function(_CheckOutDone value) checkOutDone,
    required TResult Function(_CheckOutFailed value) checkOutFailed,
    required TResult Function(_CreateLocationLoading value)
        createLocationLoading,
    required TResult Function(_CreateLocationDone value) createLocationDone,
    required TResult Function(_CreateLocationFailed value) createLocationFailed,
  }) {
    return checkOutDone(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_CheckInLoading value)? checkInLoading,
    TResult? Function(_CheckInDone value)? checkInDone,
    TResult? Function(_CheckInFailed value)? checkInFailed,
    TResult? Function(_CheckOutLoading value)? checkOutLoading,
    TResult? Function(_CheckOutDone value)? checkOutDone,
    TResult? Function(_CheckOutFailed value)? checkOutFailed,
    TResult? Function(_CreateLocationLoading value)? createLocationLoading,
    TResult? Function(_CreateLocationDone value)? createLocationDone,
    TResult? Function(_CreateLocationFailed value)? createLocationFailed,
  }) {
    return checkOutDone?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_CheckInLoading value)? checkInLoading,
    TResult Function(_CheckInDone value)? checkInDone,
    TResult Function(_CheckInFailed value)? checkInFailed,
    TResult Function(_CheckOutLoading value)? checkOutLoading,
    TResult Function(_CheckOutDone value)? checkOutDone,
    TResult Function(_CheckOutFailed value)? checkOutFailed,
    TResult Function(_CreateLocationLoading value)? createLocationLoading,
    TResult Function(_CreateLocationDone value)? createLocationDone,
    TResult Function(_CreateLocationFailed value)? createLocationFailed,
    required TResult orElse(),
  }) {
    if (checkOutDone != null) {
      return checkOutDone(this);
    }
    return orElse();
  }
}

abstract class _CheckOutDone implements AttendanceState {
  const factory _CheckOutDone() = _$CheckOutDoneImpl;
}

/// @nodoc
abstract class _$$CheckOutFailedImplCopyWith<$Res> {
  factory _$$CheckOutFailedImplCopyWith(_$CheckOutFailedImpl value,
          $Res Function(_$CheckOutFailedImpl) then) =
      __$$CheckOutFailedImplCopyWithImpl<$Res>;
  @useResult
  $Res call({Failure failure});
}

/// @nodoc
class __$$CheckOutFailedImplCopyWithImpl<$Res>
    extends _$AttendanceStateCopyWithImpl<$Res, _$CheckOutFailedImpl>
    implements _$$CheckOutFailedImplCopyWith<$Res> {
  __$$CheckOutFailedImplCopyWithImpl(
      _$CheckOutFailedImpl _value, $Res Function(_$CheckOutFailedImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? failure = null,
  }) {
    return _then(_$CheckOutFailedImpl(
      null == failure
          ? _value.failure
          : failure // ignore: cast_nullable_to_non_nullable
              as Failure,
    ));
  }
}

/// @nodoc

class _$CheckOutFailedImpl implements _CheckOutFailed {
  const _$CheckOutFailedImpl(this.failure);

  @override
  final Failure failure;

  @override
  String toString() {
    return 'AttendanceState.checkOutFailed(failure: $failure)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$CheckOutFailedImpl &&
            (identical(other.failure, failure) || other.failure == failure));
  }

  @override
  int get hashCode => Object.hash(runtimeType, failure);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$CheckOutFailedImplCopyWith<_$CheckOutFailedImpl> get copyWith =>
      __$$CheckOutFailedImplCopyWithImpl<_$CheckOutFailedImpl>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() checkInLoading,
    required TResult Function() checkInDone,
    required TResult Function(Failure failure) checkInFailed,
    required TResult Function() checkOutLoading,
    required TResult Function() checkOutDone,
    required TResult Function(Failure failure) checkOutFailed,
    required TResult Function() createLocationLoading,
    required TResult Function() createLocationDone,
    required TResult Function(Failure failure) createLocationFailed,
  }) {
    return checkOutFailed(failure);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? checkInLoading,
    TResult? Function()? checkInDone,
    TResult? Function(Failure failure)? checkInFailed,
    TResult? Function()? checkOutLoading,
    TResult? Function()? checkOutDone,
    TResult? Function(Failure failure)? checkOutFailed,
    TResult? Function()? createLocationLoading,
    TResult? Function()? createLocationDone,
    TResult? Function(Failure failure)? createLocationFailed,
  }) {
    return checkOutFailed?.call(failure);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? checkInLoading,
    TResult Function()? checkInDone,
    TResult Function(Failure failure)? checkInFailed,
    TResult Function()? checkOutLoading,
    TResult Function()? checkOutDone,
    TResult Function(Failure failure)? checkOutFailed,
    TResult Function()? createLocationLoading,
    TResult Function()? createLocationDone,
    TResult Function(Failure failure)? createLocationFailed,
    required TResult orElse(),
  }) {
    if (checkOutFailed != null) {
      return checkOutFailed(failure);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_CheckInLoading value) checkInLoading,
    required TResult Function(_CheckInDone value) checkInDone,
    required TResult Function(_CheckInFailed value) checkInFailed,
    required TResult Function(_CheckOutLoading value) checkOutLoading,
    required TResult Function(_CheckOutDone value) checkOutDone,
    required TResult Function(_CheckOutFailed value) checkOutFailed,
    required TResult Function(_CreateLocationLoading value)
        createLocationLoading,
    required TResult Function(_CreateLocationDone value) createLocationDone,
    required TResult Function(_CreateLocationFailed value) createLocationFailed,
  }) {
    return checkOutFailed(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_CheckInLoading value)? checkInLoading,
    TResult? Function(_CheckInDone value)? checkInDone,
    TResult? Function(_CheckInFailed value)? checkInFailed,
    TResult? Function(_CheckOutLoading value)? checkOutLoading,
    TResult? Function(_CheckOutDone value)? checkOutDone,
    TResult? Function(_CheckOutFailed value)? checkOutFailed,
    TResult? Function(_CreateLocationLoading value)? createLocationLoading,
    TResult? Function(_CreateLocationDone value)? createLocationDone,
    TResult? Function(_CreateLocationFailed value)? createLocationFailed,
  }) {
    return checkOutFailed?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_CheckInLoading value)? checkInLoading,
    TResult Function(_CheckInDone value)? checkInDone,
    TResult Function(_CheckInFailed value)? checkInFailed,
    TResult Function(_CheckOutLoading value)? checkOutLoading,
    TResult Function(_CheckOutDone value)? checkOutDone,
    TResult Function(_CheckOutFailed value)? checkOutFailed,
    TResult Function(_CreateLocationLoading value)? createLocationLoading,
    TResult Function(_CreateLocationDone value)? createLocationDone,
    TResult Function(_CreateLocationFailed value)? createLocationFailed,
    required TResult orElse(),
  }) {
    if (checkOutFailed != null) {
      return checkOutFailed(this);
    }
    return orElse();
  }
}

abstract class _CheckOutFailed implements AttendanceState {
  const factory _CheckOutFailed(final Failure failure) = _$CheckOutFailedImpl;

  Failure get failure;
  @JsonKey(ignore: true)
  _$$CheckOutFailedImplCopyWith<_$CheckOutFailedImpl> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$CreateLocationLoadingImplCopyWith<$Res> {
  factory _$$CreateLocationLoadingImplCopyWith(
          _$CreateLocationLoadingImpl value,
          $Res Function(_$CreateLocationLoadingImpl) then) =
      __$$CreateLocationLoadingImplCopyWithImpl<$Res>;
}

/// @nodoc
class __$$CreateLocationLoadingImplCopyWithImpl<$Res>
    extends _$AttendanceStateCopyWithImpl<$Res, _$CreateLocationLoadingImpl>
    implements _$$CreateLocationLoadingImplCopyWith<$Res> {
  __$$CreateLocationLoadingImplCopyWithImpl(_$CreateLocationLoadingImpl _value,
      $Res Function(_$CreateLocationLoadingImpl) _then)
      : super(_value, _then);
}

/// @nodoc

class _$CreateLocationLoadingImpl implements _CreateLocationLoading {
  const _$CreateLocationLoadingImpl();

  @override
  String toString() {
    return 'AttendanceState.createLocationLoading()';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$CreateLocationLoadingImpl);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() checkInLoading,
    required TResult Function() checkInDone,
    required TResult Function(Failure failure) checkInFailed,
    required TResult Function() checkOutLoading,
    required TResult Function() checkOutDone,
    required TResult Function(Failure failure) checkOutFailed,
    required TResult Function() createLocationLoading,
    required TResult Function() createLocationDone,
    required TResult Function(Failure failure) createLocationFailed,
  }) {
    return createLocationLoading();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? checkInLoading,
    TResult? Function()? checkInDone,
    TResult? Function(Failure failure)? checkInFailed,
    TResult? Function()? checkOutLoading,
    TResult? Function()? checkOutDone,
    TResult? Function(Failure failure)? checkOutFailed,
    TResult? Function()? createLocationLoading,
    TResult? Function()? createLocationDone,
    TResult? Function(Failure failure)? createLocationFailed,
  }) {
    return createLocationLoading?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? checkInLoading,
    TResult Function()? checkInDone,
    TResult Function(Failure failure)? checkInFailed,
    TResult Function()? checkOutLoading,
    TResult Function()? checkOutDone,
    TResult Function(Failure failure)? checkOutFailed,
    TResult Function()? createLocationLoading,
    TResult Function()? createLocationDone,
    TResult Function(Failure failure)? createLocationFailed,
    required TResult orElse(),
  }) {
    if (createLocationLoading != null) {
      return createLocationLoading();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_CheckInLoading value) checkInLoading,
    required TResult Function(_CheckInDone value) checkInDone,
    required TResult Function(_CheckInFailed value) checkInFailed,
    required TResult Function(_CheckOutLoading value) checkOutLoading,
    required TResult Function(_CheckOutDone value) checkOutDone,
    required TResult Function(_CheckOutFailed value) checkOutFailed,
    required TResult Function(_CreateLocationLoading value)
        createLocationLoading,
    required TResult Function(_CreateLocationDone value) createLocationDone,
    required TResult Function(_CreateLocationFailed value) createLocationFailed,
  }) {
    return createLocationLoading(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_CheckInLoading value)? checkInLoading,
    TResult? Function(_CheckInDone value)? checkInDone,
    TResult? Function(_CheckInFailed value)? checkInFailed,
    TResult? Function(_CheckOutLoading value)? checkOutLoading,
    TResult? Function(_CheckOutDone value)? checkOutDone,
    TResult? Function(_CheckOutFailed value)? checkOutFailed,
    TResult? Function(_CreateLocationLoading value)? createLocationLoading,
    TResult? Function(_CreateLocationDone value)? createLocationDone,
    TResult? Function(_CreateLocationFailed value)? createLocationFailed,
  }) {
    return createLocationLoading?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_CheckInLoading value)? checkInLoading,
    TResult Function(_CheckInDone value)? checkInDone,
    TResult Function(_CheckInFailed value)? checkInFailed,
    TResult Function(_CheckOutLoading value)? checkOutLoading,
    TResult Function(_CheckOutDone value)? checkOutDone,
    TResult Function(_CheckOutFailed value)? checkOutFailed,
    TResult Function(_CreateLocationLoading value)? createLocationLoading,
    TResult Function(_CreateLocationDone value)? createLocationDone,
    TResult Function(_CreateLocationFailed value)? createLocationFailed,
    required TResult orElse(),
  }) {
    if (createLocationLoading != null) {
      return createLocationLoading(this);
    }
    return orElse();
  }
}

abstract class _CreateLocationLoading implements AttendanceState {
  const factory _CreateLocationLoading() = _$CreateLocationLoadingImpl;
}

/// @nodoc
abstract class _$$CreateLocationDoneImplCopyWith<$Res> {
  factory _$$CreateLocationDoneImplCopyWith(_$CreateLocationDoneImpl value,
          $Res Function(_$CreateLocationDoneImpl) then) =
      __$$CreateLocationDoneImplCopyWithImpl<$Res>;
}

/// @nodoc
class __$$CreateLocationDoneImplCopyWithImpl<$Res>
    extends _$AttendanceStateCopyWithImpl<$Res, _$CreateLocationDoneImpl>
    implements _$$CreateLocationDoneImplCopyWith<$Res> {
  __$$CreateLocationDoneImplCopyWithImpl(_$CreateLocationDoneImpl _value,
      $Res Function(_$CreateLocationDoneImpl) _then)
      : super(_value, _then);
}

/// @nodoc

class _$CreateLocationDoneImpl implements _CreateLocationDone {
  const _$CreateLocationDoneImpl();

  @override
  String toString() {
    return 'AttendanceState.createLocationDone()';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$CreateLocationDoneImpl);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() checkInLoading,
    required TResult Function() checkInDone,
    required TResult Function(Failure failure) checkInFailed,
    required TResult Function() checkOutLoading,
    required TResult Function() checkOutDone,
    required TResult Function(Failure failure) checkOutFailed,
    required TResult Function() createLocationLoading,
    required TResult Function() createLocationDone,
    required TResult Function(Failure failure) createLocationFailed,
  }) {
    return createLocationDone();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? checkInLoading,
    TResult? Function()? checkInDone,
    TResult? Function(Failure failure)? checkInFailed,
    TResult? Function()? checkOutLoading,
    TResult? Function()? checkOutDone,
    TResult? Function(Failure failure)? checkOutFailed,
    TResult? Function()? createLocationLoading,
    TResult? Function()? createLocationDone,
    TResult? Function(Failure failure)? createLocationFailed,
  }) {
    return createLocationDone?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? checkInLoading,
    TResult Function()? checkInDone,
    TResult Function(Failure failure)? checkInFailed,
    TResult Function()? checkOutLoading,
    TResult Function()? checkOutDone,
    TResult Function(Failure failure)? checkOutFailed,
    TResult Function()? createLocationLoading,
    TResult Function()? createLocationDone,
    TResult Function(Failure failure)? createLocationFailed,
    required TResult orElse(),
  }) {
    if (createLocationDone != null) {
      return createLocationDone();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_CheckInLoading value) checkInLoading,
    required TResult Function(_CheckInDone value) checkInDone,
    required TResult Function(_CheckInFailed value) checkInFailed,
    required TResult Function(_CheckOutLoading value) checkOutLoading,
    required TResult Function(_CheckOutDone value) checkOutDone,
    required TResult Function(_CheckOutFailed value) checkOutFailed,
    required TResult Function(_CreateLocationLoading value)
        createLocationLoading,
    required TResult Function(_CreateLocationDone value) createLocationDone,
    required TResult Function(_CreateLocationFailed value) createLocationFailed,
  }) {
    return createLocationDone(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_CheckInLoading value)? checkInLoading,
    TResult? Function(_CheckInDone value)? checkInDone,
    TResult? Function(_CheckInFailed value)? checkInFailed,
    TResult? Function(_CheckOutLoading value)? checkOutLoading,
    TResult? Function(_CheckOutDone value)? checkOutDone,
    TResult? Function(_CheckOutFailed value)? checkOutFailed,
    TResult? Function(_CreateLocationLoading value)? createLocationLoading,
    TResult? Function(_CreateLocationDone value)? createLocationDone,
    TResult? Function(_CreateLocationFailed value)? createLocationFailed,
  }) {
    return createLocationDone?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_CheckInLoading value)? checkInLoading,
    TResult Function(_CheckInDone value)? checkInDone,
    TResult Function(_CheckInFailed value)? checkInFailed,
    TResult Function(_CheckOutLoading value)? checkOutLoading,
    TResult Function(_CheckOutDone value)? checkOutDone,
    TResult Function(_CheckOutFailed value)? checkOutFailed,
    TResult Function(_CreateLocationLoading value)? createLocationLoading,
    TResult Function(_CreateLocationDone value)? createLocationDone,
    TResult Function(_CreateLocationFailed value)? createLocationFailed,
    required TResult orElse(),
  }) {
    if (createLocationDone != null) {
      return createLocationDone(this);
    }
    return orElse();
  }
}

abstract class _CreateLocationDone implements AttendanceState {
  const factory _CreateLocationDone() = _$CreateLocationDoneImpl;
}

/// @nodoc
abstract class _$$CreateLocationFailedImplCopyWith<$Res> {
  factory _$$CreateLocationFailedImplCopyWith(_$CreateLocationFailedImpl value,
          $Res Function(_$CreateLocationFailedImpl) then) =
      __$$CreateLocationFailedImplCopyWithImpl<$Res>;
  @useResult
  $Res call({Failure failure});
}

/// @nodoc
class __$$CreateLocationFailedImplCopyWithImpl<$Res>
    extends _$AttendanceStateCopyWithImpl<$Res, _$CreateLocationFailedImpl>
    implements _$$CreateLocationFailedImplCopyWith<$Res> {
  __$$CreateLocationFailedImplCopyWithImpl(_$CreateLocationFailedImpl _value,
      $Res Function(_$CreateLocationFailedImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? failure = null,
  }) {
    return _then(_$CreateLocationFailedImpl(
      null == failure
          ? _value.failure
          : failure // ignore: cast_nullable_to_non_nullable
              as Failure,
    ));
  }
}

/// @nodoc

class _$CreateLocationFailedImpl implements _CreateLocationFailed {
  const _$CreateLocationFailedImpl(this.failure);

  @override
  final Failure failure;

  @override
  String toString() {
    return 'AttendanceState.createLocationFailed(failure: $failure)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$CreateLocationFailedImpl &&
            (identical(other.failure, failure) || other.failure == failure));
  }

  @override
  int get hashCode => Object.hash(runtimeType, failure);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$CreateLocationFailedImplCopyWith<_$CreateLocationFailedImpl>
      get copyWith =>
          __$$CreateLocationFailedImplCopyWithImpl<_$CreateLocationFailedImpl>(
              this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() checkInLoading,
    required TResult Function() checkInDone,
    required TResult Function(Failure failure) checkInFailed,
    required TResult Function() checkOutLoading,
    required TResult Function() checkOutDone,
    required TResult Function(Failure failure) checkOutFailed,
    required TResult Function() createLocationLoading,
    required TResult Function() createLocationDone,
    required TResult Function(Failure failure) createLocationFailed,
  }) {
    return createLocationFailed(failure);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? checkInLoading,
    TResult? Function()? checkInDone,
    TResult? Function(Failure failure)? checkInFailed,
    TResult? Function()? checkOutLoading,
    TResult? Function()? checkOutDone,
    TResult? Function(Failure failure)? checkOutFailed,
    TResult? Function()? createLocationLoading,
    TResult? Function()? createLocationDone,
    TResult? Function(Failure failure)? createLocationFailed,
  }) {
    return createLocationFailed?.call(failure);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? checkInLoading,
    TResult Function()? checkInDone,
    TResult Function(Failure failure)? checkInFailed,
    TResult Function()? checkOutLoading,
    TResult Function()? checkOutDone,
    TResult Function(Failure failure)? checkOutFailed,
    TResult Function()? createLocationLoading,
    TResult Function()? createLocationDone,
    TResult Function(Failure failure)? createLocationFailed,
    required TResult orElse(),
  }) {
    if (createLocationFailed != null) {
      return createLocationFailed(failure);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_CheckInLoading value) checkInLoading,
    required TResult Function(_CheckInDone value) checkInDone,
    required TResult Function(_CheckInFailed value) checkInFailed,
    required TResult Function(_CheckOutLoading value) checkOutLoading,
    required TResult Function(_CheckOutDone value) checkOutDone,
    required TResult Function(_CheckOutFailed value) checkOutFailed,
    required TResult Function(_CreateLocationLoading value)
        createLocationLoading,
    required TResult Function(_CreateLocationDone value) createLocationDone,
    required TResult Function(_CreateLocationFailed value) createLocationFailed,
  }) {
    return createLocationFailed(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_CheckInLoading value)? checkInLoading,
    TResult? Function(_CheckInDone value)? checkInDone,
    TResult? Function(_CheckInFailed value)? checkInFailed,
    TResult? Function(_CheckOutLoading value)? checkOutLoading,
    TResult? Function(_CheckOutDone value)? checkOutDone,
    TResult? Function(_CheckOutFailed value)? checkOutFailed,
    TResult? Function(_CreateLocationLoading value)? createLocationLoading,
    TResult? Function(_CreateLocationDone value)? createLocationDone,
    TResult? Function(_CreateLocationFailed value)? createLocationFailed,
  }) {
    return createLocationFailed?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_CheckInLoading value)? checkInLoading,
    TResult Function(_CheckInDone value)? checkInDone,
    TResult Function(_CheckInFailed value)? checkInFailed,
    TResult Function(_CheckOutLoading value)? checkOutLoading,
    TResult Function(_CheckOutDone value)? checkOutDone,
    TResult Function(_CheckOutFailed value)? checkOutFailed,
    TResult Function(_CreateLocationLoading value)? createLocationLoading,
    TResult Function(_CreateLocationDone value)? createLocationDone,
    TResult Function(_CreateLocationFailed value)? createLocationFailed,
    required TResult orElse(),
  }) {
    if (createLocationFailed != null) {
      return createLocationFailed(this);
    }
    return orElse();
  }
}

abstract class _CreateLocationFailed implements AttendanceState {
  const factory _CreateLocationFailed(final Failure failure) =
      _$CreateLocationFailedImpl;

  Failure get failure;
  @JsonKey(ignore: true)
  _$$CreateLocationFailedImplCopyWith<_$CreateLocationFailedImpl>
      get copyWith => throw _privateConstructorUsedError;
}
