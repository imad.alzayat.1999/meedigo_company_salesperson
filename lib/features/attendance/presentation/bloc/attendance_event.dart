part of 'attendance_bloc.dart';

@freezed
class AttendanceEvent with _$AttendanceEvent {
  const factory AttendanceEvent.checkIn(
    AttendanceRequestBody attendanceRequestBody,
  ) = _CheckIn;
  const factory AttendanceEvent.checkOut(
    AttendanceRequestBody attendanceRequestBody,
  ) = _CheckOut;
  const factory AttendanceEvent.createLocation(
    AttendanceRequestBody attendanceRequestBody,
  ) = _CreateLocation;
}
