import 'package:flutter/material.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_extensions.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_sizes.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_strings.dart';

import '../../../../core/theming/app_colors.dart';
import '../../../../core/theming/app_styles.dart';

class CheckInContentWidget extends StatelessWidget {
  const CheckInContentWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Text(
          context.translate(
            AppStrings.attendanceTitleString,
          ),
          textAlign: TextAlign.center,
          style: AppStyles.bold(
            fontSize: 20,
            context: context,
          ),
        ),
        verticalSpacing(15),
        Text(
          context.translate(
            AppStrings.attendanceContentString,
          ),
          textAlign: TextAlign.center,
          style: AppStyles.medium(
            fontSize: 16,
            textColor: AppColors.kSecondaryText,
            height: 1.5,
            context: context,
          ),
        ),
      ],
    );
  }
}
