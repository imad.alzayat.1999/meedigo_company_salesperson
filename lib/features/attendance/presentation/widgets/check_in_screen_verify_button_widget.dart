import 'package:flutter/material.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_extensions.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_strings.dart';
import '../../../../core/widgets/button_widget.dart';

class CheckInScreenVerifyButtonWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ButtonWidget(
      onPressFunction: () {},
      btnText: context.translate(
        AppStrings.attendanceBtnString,
      ),
    );
  }
}
