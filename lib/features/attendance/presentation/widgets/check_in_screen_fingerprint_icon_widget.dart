import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

import '../../../../core/helpers/app_sizes.dart';
import '../../../../core/helpers/app_assets.dart';
import '../../../../core/theming/app_colors.dart';

class CheckInFingerprintIconWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: getHeight(300),
      width: getHeight(300),
      child: SvgPicture.asset(
        AppIcons.fingerprintIcon,
        color: AppColors.kPrimaryText,
      ),
    );
  }
}
