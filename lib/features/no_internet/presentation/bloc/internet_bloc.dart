import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:meedigo_company_sales_person/core/di/dependency_injection.dart';
import 'package:meedigo_company_sales_person/core/networking/network_info.dart';

part 'internet_event.dart';
part 'internet_state.dart';
part 'internet_bloc.freezed.dart';

class InternetBloc extends Bloc<InternetEvent, InternetState> {
  StreamSubscription? _subscription;
  InternetBloc() : super(InternetState.disconnected()) {
    on<InternetEvent>((event, emit) {
      event.when(
        connect: () => emit(
          InternetState.connected(),
        ),
        disconnect: () => emit(
          InternetState.disconnected(),
        ),
      );
    });

    _subscription = getIt<Connectivity>()
        .onConnectivityChanged
        .listen((ConnectivityResult result) {
      if (result == ConnectivityResult.wifi ||
          result == ConnectivityResult.mobile) {
        add(InternetEvent.connect());
      } else {
        add(InternetEvent.disconnect());
      }
    });
  }

  /// function to try connect with the internet
  tryConnect() async {
    final connectivityResult = await getIt<NetworkInfo>().connectivityResult;
    if (connectivityResult != ConnectivityResult.none) {
      add(const InternetEvent.connect());
    } else {
      add(const InternetEvent.disconnect());
    }
  }

  @override
  Future<void> close() {
    _subscription!.cancel();
    return super.close();
  }
}
