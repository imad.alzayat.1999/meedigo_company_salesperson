part of 'internet_bloc.dart';

@freezed
class InternetEvent with _$InternetEvent {
  const factory InternetEvent.connect() = _Connect;
  const factory InternetEvent.disconnect() = _Disconnect;
}