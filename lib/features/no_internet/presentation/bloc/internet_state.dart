part of 'internet_bloc.dart';

@freezed
class InternetState with _$InternetState {
  const factory InternetState.connected() = _Connected;
  const factory InternetState.disconnected() = _Discounnected;
}
