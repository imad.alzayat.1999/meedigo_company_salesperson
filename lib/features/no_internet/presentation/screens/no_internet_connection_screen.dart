import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_sizes.dart';
import 'package:meedigo_company_sales_person/core/widgets/app_padding_widget.dart';
import 'package:meedigo_company_sales_person/features/no_internet/presentation/bloc/internet_bloc.dart';
import 'package:meedigo_company_sales_person/features/no_internet/presentation/widgets/no_internet_animation_widget.dart';
import 'package:meedigo_company_sales_person/features/no_internet/presentation/widgets/no_internet_content_widget.dart';
import 'package:meedigo_company_sales_person/features/no_internet/presentation/widgets/no_internet_try_again_button_widget.dart';

class NoInternetConnectionScreen extends StatelessWidget {
  final Widget screen;
  const NoInternetConnectionScreen({
    Key? key,
    required this.screen,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocBuilder<InternetBloc, InternetState>(
        builder: (context, state) {
          return state.maybeWhen(
            connected: () => screen,
            orElse: () => AppPaddingWidget(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  NoInternetAnimationWidget(),
                  verticalSpacing(40),
                  NoInternetContentWidget(),
                  verticalSpacing(50),
                  NoInternetTryAgainButtonWidget(),
                ],
              ),
            ),
          );
        },
      ),
    );
  }
}
