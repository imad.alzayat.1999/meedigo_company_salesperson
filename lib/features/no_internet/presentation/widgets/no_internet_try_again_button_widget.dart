import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_extensions.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_strings.dart';
import 'package:meedigo_company_sales_person/features/no_internet/presentation/bloc/internet_bloc.dart';

import '../../../../core/widgets/button_widget.dart';

class NoInternetTryAgainButtonWidget extends StatelessWidget {
  const NoInternetTryAgainButtonWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return ButtonWidget(
      onPressFunction: () => context.read<InternetBloc>()..tryConnect(),
      btnText: context.translate(
        AppStrings.tryAgainBtnString,
      ),
    );
  }
}
