import 'package:flutter/material.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_extensions.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_strings.dart';

import '../../../../core/theming/app_colors.dart';
import '../../../../core/theming/app_styles.dart';

class NoInternetContentWidget extends StatelessWidget {
  const NoInternetContentWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return Text(
      context.translate(
        AppStrings.noInternetContentString,
      ),
      textAlign: TextAlign.center,
      style: AppStyles.semiBold(
        fontSize: 18,
        textColor: AppColors.kSecondaryText,
        height: 1.5,
        context: context,
      ),
    );
  }
}
