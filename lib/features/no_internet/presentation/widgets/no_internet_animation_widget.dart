import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_assets.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_sizes.dart';


class NoInternetAnimationWidget extends StatelessWidget {
  const NoInternetAnimationWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return Lottie.asset(
      AppJsons.noInternetJson,
      width: getHeight(300),
      height: getHeight(300),
    );
  }
}
