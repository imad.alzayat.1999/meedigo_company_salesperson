import 'package:flutter/material.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_sizes.dart';
import 'package:meedigo_company_sales_person/core/widgets/app_padding_widget.dart';
import 'package:meedigo_company_sales_person/features/payment/presentation/widgets/create_payment_placeholder_image_widget.dart';
import 'package:meedigo_company_sales_person/features/payment/presentation/widgets/create_payment_title_widget.dart';

import '../widgets/create_payment_button_widget.dart';
import '../widgets/payment_amount_input_field_widget.dart';
import '../widgets/payment_note_input_field_widget.dart';

class MakePaymentScreen extends StatefulWidget {
  const MakePaymentScreen({super.key});

  @override
  State<MakePaymentScreen> createState() => _MakePaymentScreenState();
}

class _MakePaymentScreenState extends State<MakePaymentScreen> {
  var _paymentAmountController = TextEditingController();
  var _paymentNoteController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: AppPaddingWidget(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            CreatePaymentPlaceholderImageWidget(),
            verticalSpacing(20),
            CreatePaymentTitleWidget(),
            verticalSpacing(40),
            PaymentAmountInputFieldWidget(
              textEditingController: _paymentAmountController,
            ),
            verticalSpacing(15),
            PaymentNoteInputFieldWidget(
              textEditingController: _paymentNoteController,
            ),
            verticalSpacing(45),
            CreatePaymentButtonWidget(
              onPressFunction: () {},
            ),
          ],
        ),
      ),
    );
  }
}
