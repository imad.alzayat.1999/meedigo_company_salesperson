import 'package:flutter/material.dart';

import '../../../../core/widgets/button_widget.dart';

class CreatePaymentButtonWidget extends StatelessWidget {
  final void Function()? onPressFunction;
  const CreatePaymentButtonWidget({
    super.key,
    required this.onPressFunction,
  });

  @override
  Widget build(BuildContext context) {
    return ButtonWidget(
      onPressFunction: onPressFunction,
      btnText: "Create payment",
    );
  }
}
