import 'package:flutter/material.dart';
import '../../../../core/theming/app_styles.dart';

class CreatePaymentTitleWidget extends StatelessWidget {
  const CreatePaymentTitleWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return Text(
      "Create payment",
      style: AppStyles.bold(
        fontSize: 20,
        context: context,
      ),
    );
  }
}
