import 'package:flutter/material.dart';
import '../../../../core/widgets/text_field_widget.dart';

class PaymentNoteInputFieldWidget extends StatelessWidget {
  final TextEditingController textEditingController;
  const PaymentNoteInputFieldWidget({
    super.key,
    required this.textEditingController,
  });

  @override
  Widget build(BuildContext context) {
    return TextFieldWidget(
      labelText: "Payment note",
      textEditingController: textEditingController,
      hintText: "Type payment note here",
      textInputType: TextInputType.text,
      maxLines: 4,
    );
  }
}
