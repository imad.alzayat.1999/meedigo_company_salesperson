import 'package:flutter/material.dart';

import '../../../../core/widgets/text_field_widget.dart';

class PaymentAmountInputFieldWidget extends StatelessWidget {
  final TextEditingController textEditingController;
  const PaymentAmountInputFieldWidget({
    super.key,
    required this.textEditingController,
  });

  @override
  Widget build(BuildContext context) {
    return TextFieldWidget(
      labelText: "Payment amount",
      textEditingController: textEditingController,
      hintText: "Enter the payment amount",
      textInputType: TextInputType.number,
    );
  }
}
