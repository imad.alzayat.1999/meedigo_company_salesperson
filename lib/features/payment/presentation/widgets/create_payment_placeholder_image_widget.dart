import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_sizes.dart';

import '../../../../core/helpers/app_assets.dart';

class CreatePaymentPlaceholderImageWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: getWidth(300),
      height: getHeight(200),
      child: SvgPicture.asset(
        AppImages.makePaymentImage,
      ),
    );
  }
}
