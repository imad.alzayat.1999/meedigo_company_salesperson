import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_sizes.dart';
import '../../../../core/helpers/app_assets.dart';
import '../../../../core/theming/app_colors.dart';

class EnableLocationPinIconWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: getRadius(250),
      width: getRadius(250),
      child: SvgPicture.asset(
        AppIcons.locationPinIcon,
        color: AppColors.kPrimaryText,
      ),
    );
  }
}
