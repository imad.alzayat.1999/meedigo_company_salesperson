import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:geolocator/geolocator.dart';
import 'package:meedigo_company_sales_person/core/common/bloc/location/location_bloc.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_extensions.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_strings.dart';
import '../../../../core/widgets/button_widget.dart';

class EnableLocationScreenButtonWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocConsumer<LocationBloc, LocationState>(
      listener: (context, state) {
        state.whenOrNull(
          granted: () => context.read<LocationBloc>()
            ..goToTheNextPage(
              context: context,
            ),
          denied: () => Geolocator.openAppSettings(),
        );
      },
      builder: (context, state) {
        return state.maybeWhen(
          orElse: () => ButtonWidget(
            onPressFunction: () => context.read<LocationBloc>()
              ..add(
                LocationEvent.checkLocationPermission(),
              ),
            btnText: context.translate(
              AppStrings.locationPermissionEnableBtnString,
            ),
          ),
        );
      },
    );
  }
}
