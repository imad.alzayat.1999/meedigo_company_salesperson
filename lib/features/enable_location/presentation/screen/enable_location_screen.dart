import 'package:flutter/material.dart';
import 'package:meedigo_company_sales_person/core/widgets/app_padding_widget.dart';
import 'package:meedigo_company_sales_person/features/enable_location/presentation/widgets/enable_location_pin_icon_widget.dart';
import 'package:meedigo_company_sales_person/features/enable_location/presentation/widgets/enable_location_screen_button_widget.dart';
import 'package:meedigo_company_sales_person/features/enable_location/presentation/widgets/enable_location_screen_content_widget.dart';

import '../../../../core/helpers/app_sizes.dart';

class EnableLocationScreen extends StatelessWidget {
  const EnableLocationScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: AppPaddingWidget(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            EnableLocationPinIconWidget(),
            verticalSpacing(34),
            EnableLocationScreenContentWidget(),
            verticalSpacing(45),
            EnableLocationScreenButtonWidget(),
          ],
        ),
      ),
    );
  }
}
