import 'package:flutter/material.dart';
import 'package:meedigo_company_sales_person/features/warehouses/presentation/widgets/warehouse_card_widget.dart';

class WarehousesScreen extends StatefulWidget {
  const WarehousesScreen({super.key});

  @override
  State<WarehousesScreen> createState() => _WarehousesScreenState();
}

class _WarehousesScreenState extends State<WarehousesScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: WarehousesListWidget(),
    );
  }
}
