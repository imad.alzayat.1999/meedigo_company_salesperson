import 'package:flutter/material.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_sizes.dart';
import 'package:meedigo_company_sales_person/core/theming/app_colors.dart';
import 'package:meedigo_company_sales_person/core/theming/app_styles.dart';

class WarehousesListWidget extends StatelessWidget {
  const WarehousesListWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      keyboardDismissBehavior: ScrollViewKeyboardDismissBehavior.onDrag,
      itemCount: 10,
      physics: const BouncingScrollPhysics(),
      padding: EdgeInsets.only(
        left: getWidth(16),
        right: getWidth(16),
        top: getHeight(15),
      ),
      itemBuilder: (context, index) => Padding(
        padding: EdgeInsets.only(
          bottom: getHeight(15),
        ),
        child: WarehouseCardWidget(),
      ),
    );
  }
}

class WarehouseCardWidget extends StatelessWidget {
  const WarehouseCardWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: getHeight(56),
      decoration: BoxDecoration(
        color: AppColors.kDarkSecondary,
        borderRadius: BorderRadius.circular(
          getRadius(8),
        ),
      ),
      child: Center(
        child: Text(
          "Warehouse Name",
          style: AppStyles.semiBold(
            fontSize: 20,
            context: context,
          ),
        ),
      ),
    );
  }
}
