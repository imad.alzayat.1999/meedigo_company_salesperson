import 'package:flutter/material.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_extensions.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_strings.dart';
import 'package:meedigo_company_sales_person/core/theming/app_styles.dart';
import 'package:meedigo_company_sales_person/core/widgets/delete_order_and_order_product_button_widget.dart';
import '../../../../../core/helpers/app_sizes.dart';
import '../../../../orders/presentation/widgets/common_widgets/order_card_widget.dart';

class OfflineOrderDetailsProductsWidget extends StatefulWidget {
  @override
  State<OfflineOrderDetailsProductsWidget> createState() =>
      _OfflineOrderDetailsProductsWidgetState();
}

class _OfflineOrderDetailsProductsWidgetState
    extends State<OfflineOrderDetailsProductsWidget> {
  // List<Cart> _products = [];
  //
  // @override
  // void initState() {
  //   // TODO: implement initState
  //   super.initState();
  //   _products = widget.savedOrder.products;
  // }
  //
  // /// on delete item
  // _onDeleteItem({required int index, required BuildContext context}) {
  //   context.pop();
  //   if (_products.length != 1) {
  //     widget.savedOrder.products.removeWhere(
  //       (element) => element.productId == index,
  //     );
  //     setState(() {
  //       _products = widget.savedOrder.products;
  //     });
  //     SavedOrder _order = SavedOrder(
  //       id: widget.savedOrder.id,
  //       orderNote: widget.savedOrder.orderNote,
  //       companyName: widget.savedOrder.companyName,
  //       products: _products,
  //       marketId: widget.savedOrder.marketId,
  //       marketName: widget.savedOrder.marketName,
  //       creationDate: widget.savedOrder.creationDate,
  //     );
  //     EditSavedOrderParameters _parameters = EditSavedOrderParameters(
  //       savedOrder: _order,
  //       index: widget.orderItemIndex,
  //     );
  //     context.read<OfflineOrdersBloc>()
  //       ..add(
  //         OfflineOrdersEvent.editOfflineOrder(_parameters),
  //       );
  //   } else {
  //     context.read<OfflineOrdersBloc>()
  //       ..add(
  //         OfflineOrdersEvent.deleteOfflineOrder(index),
  //       );
  //   }
  // }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Align(
          alignment: AlignmentDirectional.centerStart,
          child: Text(
            context.translate(
              AppStrings.numberOfOrderProductsString,
              args: {
                "number_of_products_per_order": "12",
              },
            ),
            style: AppStyles.semiBold(
              fontSize: 18,
              context: context,
            ),
          ),
        ),
        verticalSpacing(15),
        ListView.builder(
          shrinkWrap: true,
          physics: NeverScrollableScrollPhysics(),
          itemBuilder: (context, index) {
            return Padding(
              padding: EdgeInsets.only(
                bottom: getHeight(15),
              ),
              child: SizedBox(
                height: getHeight(113),
                child: Stack(
                  children: [
                    OrderCardWidget(),
                    DeleteOrderAndOrderProductButtonWidget(
                      onPressFunction: () {},
                    ),
                  ],
                ),
              ),
            );
          },
          itemCount: 10,
        ),
      ],
    );
  }
}
