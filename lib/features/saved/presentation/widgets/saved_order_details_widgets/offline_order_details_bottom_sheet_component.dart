import 'package:flutter/material.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_extensions.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_strings.dart';
import '../../../../../core/widgets/bottom_sheet_button_confirm_widget.dart';

class OfflineOrderDetailsBottomSheetWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BottomSheetButtonConfirmWidget(
      btnTitle: context.translate(
        AppStrings.confirmBtnString,
      ),
      onPressFunction: () {},
    );
  }
}
