import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_extensions.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_strings.dart';
import '../../../../../core/helpers/app_sizes.dart';
import '../../../../../core/helpers/app_assets.dart';
import '../../../../../core/theming/app_colors.dart';
import '../../../../../core/theming/app_fonts.dart';
import '../../../../../core/theming/app_styles.dart';

class OfflineOrderDetailsHeaderWidgets extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        FirstSectionOfHeader(),
        SecondSectionOfHeader(),
      ],
    );
  }
}

class RefreshButtonWidget extends StatelessWidget {
  final void Function()? onPressFunction;
  const RefreshButtonWidget({
    super.key,
    required this.onPressFunction,
  });

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: getWidth(116),
      height: getHeight(27),
      child: ElevatedButton(
        style: ElevatedButton.styleFrom(
          elevation: 0,
          backgroundColor: AppColors.kDarkPrimary,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(
              getRadius(5),
            ),
          ),
        ),
        onPressed: onPressFunction,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            SizedBox(
              width: getRadius(22),
              height: getRadius(22),
              child: SvgPicture.asset(
                AppIcons.refreshIcon,
                color: AppColors.kWhite,
              ),
            ),
            horizontalSpacing(9),
            Text(
              context.translate(
                AppStrings.refreshBtnString,
              ),
              style: AppStyles.semiBold(
                fontSize: 14,
                textColor: AppColors.kWhite,
                context: context,
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class FirstSectionOfHeader extends StatelessWidget {
  const FirstSectionOfHeader({super.key});

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(
            context.translate(
              AppStrings.orderNumberCardString,
              args: {
                "order_id": "1200",
              },
            ),
            style: AppStyles.bold(
              fontSize: 20,
              context: context,
            ),
          ),
          verticalSpacing(22),
          Text(
            "Company Name",
            style: AppStyles.medium(
              fontSize: 16,
              textColor: AppColors.kSecondaryText,
              context: context,
              height: 1.2,
            ),
          ),
        ],
      ),
    );
  }
}

class SecondSectionOfHeader extends StatelessWidget {
  const SecondSectionOfHeader({super.key});

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Text(
          "5 days ago",
          style: AppStyles.medium(
            fontSize: 16,
            textColor: AppColors.kSecondaryText,
            context: context,
          ),
        ),
        verticalSpacing(22),
        RefreshButtonWidget(
          onPressFunction: () {},
        ),
      ],
    );
  }
}
