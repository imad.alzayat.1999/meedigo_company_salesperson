import 'package:flutter/material.dart';
import 'package:meedigo_company_sales_person/core/widgets/delete_order_and_order_product_button_widget.dart';
import 'package:meedigo_company_sales_person/features/orders/presentation/widgets/common_widgets/order_card_widget.dart';

import '../../../../../core/helpers/app_sizes.dart';

class OfflineOrderCardWidget extends StatelessWidget {
  final void Function()? onPressFunction;
  final void Function()? onDeleteFunction;

  const OfflineOrderCardWidget({
    super.key,
    required this.onPressFunction,
    required this.onDeleteFunction,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      height: getHeight(100),
      child: Stack(
        children: [
          OrderCardWidget(),
          DeleteOrderAndOrderProductButtonWidget(
            onPressFunction: () {},
          ),
        ],
      ),
    );
  }
}
