import 'package:flutter/material.dart';
import '../../../../core/helpers/app_sizes.dart';
import '../widgets/saved_orders_screen_widgets/offline_order_card_widget.dart';

class SavedOrderScreen extends StatelessWidget {
  const SavedOrderScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView.builder(
        shrinkWrap: true,
        itemBuilder: (context, index) {
          return Padding(
            padding: EdgeInsets.only(
              bottom: getHeight(15),
            ),
            child: OfflineOrderCardWidget(
              onDeleteFunction: () {},
              onPressFunction: () {},
            ),
          );
        },
        padding: EdgeInsets.only(
          left: getWidth(16),
          right: getWidth(16),
          top: getHeight(15),
        ),
        itemCount: 15,
      ),
    );
  }
}
