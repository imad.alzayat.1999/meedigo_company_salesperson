import 'package:flutter/material.dart';

import '../../../../core/helpers/app_sizes.dart';
import '../../../../core/widgets/appbar_widget.dart';
import '../../../no_internet/presentation/screens/no_internet_connection_screen.dart';
import '../widgets/saved_order_details_widgets/offline_order_details_bottom_sheet_component.dart';
import '../widgets/saved_order_details_widgets/offline_order_details_header_component.dart';
import '../widgets/saved_order_details_widgets/offline_order_details_products_widget.dart';

class SavedOrderDetailsScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return PopScope(
      onPopInvoked: (bool) {},
      child: NoInternetConnectionScreen(
        screen: Scaffold(
          appBar: appbarWidget(
            title: "#Order Id",
            context: context,
          ),
          body: ListView(
            children: [
              OfflineOrderDetailsHeaderWidgets(),
              Padding(
                padding: EdgeInsets.only(
                  bottom: getHeight(15),
                  top: getHeight(18),
                ),
                child: Divider(),
              ),
              Padding(
                padding: EdgeInsets.only(
                  bottom: getHeight(85),
                ),
                child: OfflineOrderDetailsProductsWidget(),
              ),
            ],
          ),
          bottomSheet: OfflineOrderDetailsBottomSheetWidget(),
        ),
      ),
    );
  }
}
