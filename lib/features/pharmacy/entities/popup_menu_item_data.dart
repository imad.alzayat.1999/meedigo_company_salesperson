import 'package:equatable/equatable.dart';

class PopupMenuItemData extends Equatable{
  final String title;
  final String iconPath;
  final String routeUrl;

  PopupMenuItemData({
    required this.title,
    required this.iconPath,
    required this.routeUrl,
  });

  @override
  // TODO: implement props
  List<Object?> get props => [title , iconPath , routeUrl];
}