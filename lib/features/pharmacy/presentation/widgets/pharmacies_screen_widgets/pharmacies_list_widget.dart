import 'package:flutter/material.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_sizes.dart';
import 'package:meedigo_company_sales_person/core/widgets/pharmacy_card_widget.dart';

class PharmaciesListScreen extends StatelessWidget {
  const PharmaciesListScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      padding: EdgeInsets.only(
        top: getHeight(15),
        left: getWidth(16),
        right: getWidth(16),
      ),
      physics: const BouncingScrollPhysics(),
      itemCount: 10,
      itemBuilder: (context, index) => Padding(
        padding: EdgeInsets.only(
          bottom: getHeight(15),
        ),
        child: PharmacyCardWidget(),
      ),
    );
  }
}
