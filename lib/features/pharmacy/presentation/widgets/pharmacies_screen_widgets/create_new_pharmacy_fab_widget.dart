import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import '../../../../../core/helpers/app_assets.dart';
import '../../../../../core/routing/app_routes.dart';
import '../../../../../core/theming/app_colors.dart';

class CreateNewPharmacyFabWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return FloatingActionButton(
      backgroundColor: AppColors.kPrimary,
      onPressed: () {},
      child: Center(
        child: SvgPicture.asset(
          AppIcons.userAddIcon,
          color: AppColors.kWhite,
        ),
      ),
    );
  }
}
