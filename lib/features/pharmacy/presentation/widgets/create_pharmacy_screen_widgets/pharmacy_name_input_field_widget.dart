import 'package:flutter/material.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_extensions.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_strings.dart';
import '../../../../../core/widgets/text_field_widget.dart';

class PharmacyNameInputFieldWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return TextFieldWidget(
      textEditingController: TextEditingController(),
      textInputType: TextInputType.text,
      hintText: context.translate(
        AppStrings.pharmacyNameLabelTextString,
      ),
      onValidate: (value) {},
      labelText: context.translate(
        AppStrings.pharmacyNameLabelTextString,
      ),
    );
  }
}
