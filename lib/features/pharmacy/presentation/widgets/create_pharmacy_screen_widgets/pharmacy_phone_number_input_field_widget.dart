import 'package:flutter/material.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_extensions.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_strings.dart';
import '../../../../../core/widgets/text_field_widget.dart';

class PharmacyPhoneNumberInputFieldWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return TextFieldWidget(
      textEditingController: TextEditingController(),
      textInputType: TextInputType.phone,
      onValidate: (value) {},
      labelText: context.translate(
        AppStrings.pharmacyPhoneNumberLabelTextString,
      ),
    );
  }
}
