import 'package:flutter/material.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_extensions.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_strings.dart';
import '../../../../../core/widgets/button_widget.dart';

class CreatePharmacyButtonWidget extends StatelessWidget {
  final void Function()? onPressFunction;
  const CreatePharmacyButtonWidget({
    super.key,
    required this.onPressFunction,
  });

  @override
  Widget build(BuildContext context) {
    return ButtonWidget(
      onPressFunction: onPressFunction,
      btnText: context.translate(
        AppStrings.createBtnString,
      ),
    );
  }
}
