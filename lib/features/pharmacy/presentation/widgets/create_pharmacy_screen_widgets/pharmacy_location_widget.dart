import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_extensions.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_strings.dart';
import '../../../../../core/helpers/app_assets.dart';
import '../../../../../core/helpers/app_sizes.dart';
import '../../../../../core/theming/app_colors.dart';
import '../../../../../core/theming/app_styles.dart';
import '../../../../../core/widgets/text_field_widget.dart';

class PharmacyLocationInput extends StatelessWidget {
  final void Function(String)? onChange;

  const PharmacyLocationInput({
    super.key,
    required this.onChange,
  });

  @override
  Widget build(BuildContext context) {
    return TextFieldWidget(
      labelText: context.translate(
        AppStrings.pharmacyLocationLabelTextString,
      ),
      onChange: onChange,
      textEditingController: TextEditingController(),
      isSearch: true,
      onValidate: (value) {},
    );
  }
}

class PredictionsResults extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: getHeight(196),
      child: ListView.builder(
        itemBuilder: (context, index) => Padding(
          padding: EdgeInsets.only(
            bottom: getHeight(14),
          ),
          child: PredictionCardWidget(),
        ),
        physics: const BouncingScrollPhysics(),
        itemCount: 5,
      ),
    );
  }
}

class PredictionCardWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: getHeight(45),
      decoration: BoxDecoration(
        color: AppColors.kSecondary,
        borderRadius: BorderRadius.circular(
          getRadius(8),
        ),
      ),
      child: Material(
        color: AppColors.kTransparent,
        child: InkWell(
          borderRadius: BorderRadius.circular(
            getRadius(8),
          ),
          onTap: () {},
          child: Center(
            child: Row(
              children: [
                SizedBox(
                  height: getHeight(30),
                  width: getHeight(30),
                  child: SvgPicture.asset(
                    AppIcons.locationPinIcon,
                    color: AppColors.kPrimaryText,
                  ),
                ),
                horizontalSpacing(10),
                Expanded(
                  child: Text(
                    "Pharmacy location",
                    style: AppStyles.medium(
                      fontSize: 14,
                      context: context,
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class PharmacyLocationWidget extends StatefulWidget {
  const PharmacyLocationWidget({super.key});

  @override
  State<PharmacyLocationWidget> createState() => _PharmacyLocationWidgetState();
}

class _PharmacyLocationWidgetState extends State<PharmacyLocationWidget> {
  bool _isSearch = false;

  _onChangeTextField(String query) async {
    if (query.isEmpty) {
      setState(() {
        _isSearch = false;
      });
    } else {
      setState(() {
        _isSearch = true;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        PharmacyLocationInput(
          onChange: (value) => _onChangeTextField(value),
        ),
        verticalSpacing(6),
        !_isSearch
            ? Container()
            : SizedBox(
                height: getHeight(300),
                child: PredictionsResults(),
              ),
      ],
    );
  }
}
