import 'package:flutter/material.dart';
import 'package:meedigo_company_sales_person/core/widgets/medicines/list_of_medicines_widget.dart';


class MedicineOffersTabWidget extends StatelessWidget {
  const MedicineOffersTabWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return ListOfMedicinesWidget();
  }
}
