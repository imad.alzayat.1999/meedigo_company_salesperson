import 'package:flutter/material.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_sizes.dart';
import 'package:meedigo_company_sales_person/core/widgets/medicines/list_of_medicines_widget.dart';
import 'package:meedigo_company_sales_person/features/pharmacy/presentation/widgets/pharmacy_products_screen_widgets/filter_on_brand_button_widget.dart';

class MedicinesTabWidget extends StatefulWidget {
  const MedicinesTabWidget({super.key});

  @override
  State<MedicinesTabWidget> createState() => _MedicinesTabWidgetState();
}

class _MedicinesTabWidgetState extends State<MedicinesTabWidget> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        FilterOnBrandButtonWidget(),
        verticalSpacing(15),
        Expanded(
          child: MedicinesByBrandWidget(),
        ),
      ],
    );
  }
}

class MedicinesByBrandWidget extends StatefulWidget {
  const MedicinesByBrandWidget({super.key});

  @override
  State<MedicinesByBrandWidget> createState() => _MedicinesByBrandWidgetState();
}

class _MedicinesByBrandWidgetState extends State<MedicinesByBrandWidget> {
  @override
  Widget build(BuildContext context) {
    return ListOfMedicinesWidget();
  }
}
