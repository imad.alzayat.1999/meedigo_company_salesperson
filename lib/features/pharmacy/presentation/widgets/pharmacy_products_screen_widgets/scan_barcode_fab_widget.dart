import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

import '../../../../../core/helpers/app_assets.dart';
import '../../../../../core/theming/app_colors.dart';

class ScanBarcodeFabWidget extends StatelessWidget {
  const ScanBarcodeFabWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return FloatingActionButton(
      backgroundColor: AppColors.kPrimary,
      onPressed: () {},
      child: SvgPicture.asset(
        AppIcons.barcodeIcon,
        color: AppColors.kWhite,
      ),
    );
  }
}
