import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_assets.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_extensions.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_sizes.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_strings.dart';
import 'package:meedigo_company_sales_person/core/theming/app_colors.dart';
import 'package:meedigo_company_sales_person/core/theming/app_styles.dart';

class FilterOnBrandButtonWidget extends StatelessWidget {
  const FilterOnBrandButtonWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: getHeight(40),
      child: ElevatedButton(
        style: ElevatedButton.styleFrom(
          backgroundColor: AppColors.kPrimary,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(
              getRadius(10),
            ),
          ),
        ),
        onPressed: () {},
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              context.translate(
                AppStrings.brandFilterBtnString,
              ),
              style: AppStyles.semiBold(
                fontSize: 16,
                textColor: AppColors.kWhite,
                context: context,
              ),
            ),
            SizedBox(
              width: getRadius(18),
              height: getRadius(18),
              child: SvgPicture.asset(
                AppIcons.arrowDownIcon,
                color: AppColors.kWhite,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
