import 'package:flutter/material.dart';
import 'package:meedigo_company_sales_person/core/widgets/accessories/list_of_accessories_widget.dart';

import '../../../../../core/helpers/app_sizes.dart';
import 'filter_on_brand_button_widget.dart';

class AccessoryTabWidget extends StatefulWidget {
  const AccessoryTabWidget({super.key});

  @override
  State<AccessoryTabWidget> createState() => _AccessoryTabWidgetState();
}

class _AccessoryTabWidgetState extends State<AccessoryTabWidget> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        FilterOnBrandButtonWidget(),
        verticalSpacing(15),
        Expanded(
          child: AccessoryByBrandWidget(),
        ),
      ],
    );
  }
}

class AccessoryByBrandWidget extends StatefulWidget {
  const AccessoryByBrandWidget({super.key});

  @override
  State<AccessoryByBrandWidget> createState() => _AccessoryByBrandWidgetState();
}

class _AccessoryByBrandWidgetState extends State<AccessoryByBrandWidget> {
  @override
  Widget build(BuildContext context) {
    return ListOfAccessoriesWidget();
  }
}
