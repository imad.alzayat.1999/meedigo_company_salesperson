import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_assets.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_extensions.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_sizes.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_strings.dart';
import 'package:meedigo_company_sales_person/core/theming/app_colors.dart';
import 'package:meedigo_company_sales_person/core/theming/app_styles.dart';
import 'package:meedigo_company_sales_person/core/widgets/app_padding_widget.dart';
import 'package:meedigo_company_sales_person/core/widgets/button_widget.dart';

class SaveCartConfirmationBottomSheet extends StatelessWidget {
  final void Function()? noFunction;
  final void Function()? yesFunction;

  const SaveCartConfirmationBottomSheet({
    super.key,
    required this.noFunction,
    required this.yesFunction,
  });

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: getHeight(168),
      child: AppPaddingWidget(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Row(
              children: [
                SizedBox(
                  width: getRadius(30),
                  height: getRadius(30),
                  child: SvgPicture.asset(
                    AppIcons.attentionCartIcon,
                    color: AppColors.kPrimaryText,
                  ),
                ),
                Text(
                  context.translate(
                    AppStrings.attentionTitleString,
                  ),
                  style: AppStyles.bold(
                    fontSize: 20,
                    context: context,
                  ),
                ),
              ],
            ),
            verticalSpacing(15),
            Text(
              context.translate(
                AppStrings.attentionCartDeletionQuestionString,
              ),
              style: AppStyles.medium(
                fontSize: 16,
                context: context,
              ),
            ),
            verticalSpacing(29),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                ButtonWidget(
                  btnWidth: getWidth(165),
                  btnHeight: getHeight(35),
                  borderRadius: 25,
                  btnTextColor: AppColors.kPrimary,
                  btnColor: AppColors.kWhite,
                  btnText: context.translate(
                    AppStrings.noBtnString,
                  ),
                  onPressFunction: noFunction,
                ),
                ButtonWidget(
                  btnWidth: getWidth(165),
                  btnHeight: getHeight(35),
                  borderRadius: 25,
                  btnText: context.translate(
                    AppStrings.yesBtnString,
                  ),
                  onPressFunction: yesFunction,
                ),
              ],
            )
          ],
        ),
      ),
    );
  }
}
