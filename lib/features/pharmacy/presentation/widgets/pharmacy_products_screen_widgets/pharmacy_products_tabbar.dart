import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:meedigo_company_sales_person/core/common/entities/tabbar_data.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_extensions.dart';
import 'package:meedigo_company_sales_person/core/utils/custom_tabbar_indicator.dart';

import '../../../../../core/helpers/app_consts.dart';
import '../../../../../core/helpers/app_sizes.dart';
import '../../../../../core/theming/app_colors.dart';
import '../../../../../core/theming/app_styles.dart';

PreferredSizeWidget? pharmacyProductsTabBarWidget({
  required BuildContext context,
  required void Function(int)? onTap,
}) {
  return TabBar(
    onTap: onTap,
    indicator: CustomTabBarIndicator(),
    tabAlignment: TabAlignment.center,
    indicatorWeight: 5,
    isScrollable: true,
    padding: EdgeInsets.zero,
    labelColor: AppColors.kWhite,
    labelStyle: AppStyles.medium(
      fontSize: 14,
      context: context,
    ),
    physics: const ClampingScrollPhysics(),
    unselectedLabelColor: AppColors.kWhite,
    unselectedLabelStyle: AppStyles.medium(
      fontSize: 14,
      context: context,
    ),
    tabs: AppConsts.pharmacyProductsTabBarItems
        .map(
          (e) => tabItem(
            tabBarData: e,
            context: context,
          ),
        )
        .toList(),
  );
}

Tab tabItem({
  required TabBarData tabBarData,
  required BuildContext context,
}) {
  return Tab(
    child: Padding(
      padding: EdgeInsets.zero,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          SizedBox(
            width: getHeight(16),
            height: getHeight(16),
            child: SvgPicture.asset(
              tabBarData.iconPath,
              color: AppColors.kWhite,
            ),
          ),
          horizontalSpacing(5),
          Text(context.translate(tabBarData.key)),
        ],
      ),
    ),
  );
}
