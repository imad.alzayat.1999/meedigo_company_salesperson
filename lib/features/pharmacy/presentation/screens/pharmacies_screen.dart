import 'package:flutter/material.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_extensions.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_strings.dart';
import 'package:meedigo_company_sales_person/core/widgets/appbar_widget.dart';
import 'package:meedigo_company_sales_person/features/pharmacy/presentation/widgets/pharmacies_screen_widgets/create_new_pharmacy_fab_widget.dart';

import '../widgets/pharmacies_screen_widgets/pharmacies_list_widget.dart';

class PharmaciesScreen extends StatelessWidget {
  const PharmaciesScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: appbarWidget(
        title: context.translate(
          AppStrings.pharmaciesTitleString,
        ),
        context: context,
      ),
      body: PharmaciesListScreen(),
      floatingActionButton: CreateNewPharmacyFabWidget(),
      floatingActionButtonLocation: FloatingActionButtonLocation.startFloat,
    );
  }
}
