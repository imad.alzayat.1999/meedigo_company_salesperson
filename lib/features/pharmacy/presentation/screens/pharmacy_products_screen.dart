import 'package:flutter/material.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_extensions.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_strings.dart';
import 'package:meedigo_company_sales_person/features/pharmacy/presentation/widgets/pharmacy_products_screen_widgets/pharmacy_products_tabbar.dart';
import 'package:meedigo_company_sales_person/features/pharmacy/presentation/widgets/pharmacy_products_screen_widgets/scan_barcode_fab_widget.dart';

import '../../../../core/widgets/appbar_widget.dart';
import '../widgets/pharmacy_products_screen_widgets/accessory_tab_widget.dart';
import '../widgets/pharmacy_products_screen_widgets/medicine_offers_tab_widget.dart';
import '../widgets/pharmacy_products_screen_widgets/medicines_tab_widget.dart';

class PharmacyProductsScreen extends StatefulWidget {
  const PharmacyProductsScreen({super.key});

  @override
  State<PharmacyProductsScreen> createState() => _PharmacyProductsScreenState();
}

class _PharmacyProductsScreenState extends State<PharmacyProductsScreen> {
  int _tabIndex = 0;

  /// function to change tabBar index
  _changeTabBarIndex(int index) {
    setState(() {
      _tabIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 3,
      initialIndex: _tabIndex,
      child: PopScope(
        onPopInvoked: (bool) {},
        child: Scaffold(
          resizeToAvoidBottomInset: true,
          appBar: appbarWidget(
            context: context,
            isCompaniesPage: true,
            title: context.translate(
              AppStrings.productsTitleString,
            ),
            onTap: () {},
            bottom: pharmacyProductsTabBarWidget(
              context: context,
              onTap: (index) => _changeTabBarIndex(index),
            ),
          ),
          floatingActionButton: ScanBarcodeFabWidget(),
          floatingActionButtonLocation: FloatingActionButtonLocation.startFloat,
          body: TabBarView(
            children: [
              MedicinesTabWidget(),
              MedicineOffersTabWidget(),
              AccessoryTabWidget(),
            ],
          ),
        ),
      ),
    );
  }
}
