import 'package:flutter/material.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_extensions.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_sizes.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_strings.dart';
import 'package:meedigo_company_sales_person/core/widgets/app_padding_widget.dart';
import 'package:meedigo_company_sales_person/core/widgets/appbar_widget.dart';
import 'package:meedigo_company_sales_person/features/pharmacy/presentation/widgets/create_pharmacy_screen_widgets/create_pharmacy_button_widget.dart';
import 'package:meedigo_company_sales_person/features/pharmacy/presentation/widgets/create_pharmacy_screen_widgets/pharmacy_name_input_field_widget.dart';
import 'package:meedigo_company_sales_person/features/pharmacy/presentation/widgets/create_pharmacy_screen_widgets/pharmacy_phone_number_input_field_widget.dart';

import '../widgets/create_pharmacy_screen_widgets/pharmacy_location_widget.dart';

class CreatePharmacyScreen extends StatelessWidget {
  const CreatePharmacyScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: appbarWidget(
        title: context.translate(
          AppStrings.createNewPharmacyTitleString,
        ),
        context: context,
      ),
      resizeToAvoidBottomInset: true,
      body: SafeArea(
        child: AppPaddingWidget(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              PharmacyNameInputFieldWidget(),
              verticalSpacing(15),
              PharmacyPhoneNumberInputFieldWidget(),
              verticalSpacing(15),
              PharmacyLocationWidget(),
              Padding(
                padding: EdgeInsets.only(
                  bottom: getHeight(15),
                  top: getHeight(10),
                ),
                child: CreatePharmacyButtonWidget(onPressFunction: () {}),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
