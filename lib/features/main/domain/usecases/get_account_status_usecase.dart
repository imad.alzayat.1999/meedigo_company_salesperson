import 'package:dartz/dartz.dart';
import 'package:meedigo_company_sales_person/core/networking/api_failure.dart';
import 'package:meedigo_company_sales_person/core/usecase/base_usecase.dart';
import 'package:meedigo_company_sales_person/features/main/domain/entities/account_status_response.dart';
import 'package:meedigo_company_sales_person/features/main/domain/repository/main_repository.dart';

class GetAccountStatusUseCase
    extends BaseUseCase<AccountStatusResponse, NoParameters> {
  final MainRepository mainRepository;

  GetAccountStatusUseCase({required this.mainRepository});

  @override
  Future<Either<Failure, AccountStatusResponse>> call(
    NoParameters parameters,
  ) async {
    return await mainRepository.getAccountStatus();
  }
}
