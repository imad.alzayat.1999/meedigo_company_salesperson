import 'package:dartz/dartz.dart';
import 'package:meedigo_company_sales_person/core/networking/api_failure.dart';
import 'package:meedigo_company_sales_person/core/networking/api_requests.dart';
import 'package:meedigo_company_sales_person/core/usecase/base_usecase.dart';
import 'package:meedigo_company_sales_person/features/main/domain/entities/update_fcm_token_response.dart';
import 'package:meedigo_company_sales_person/features/main/domain/repository/main_repository.dart';

class UpdateFcmTokenUseCase
    extends BaseUseCase<UpdateFcmTokenResponse, UpdateFcmTokenRequestBody> {
  final MainRepository mainRepository;

  UpdateFcmTokenUseCase({
    required this.mainRepository,
  });

  @override
  Future<Either<Failure, UpdateFcmTokenResponse>> call(
    UpdateFcmTokenRequestBody parameters,
  ) async {
    return await mainRepository.updateFcmToken(
      updateFcmTokenRequestBody: parameters,
    );
  }
}
