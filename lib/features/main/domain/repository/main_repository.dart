import 'package:dartz/dartz.dart';
import 'package:meedigo_company_sales_person/core/networking/api_failure.dart';
import 'package:meedigo_company_sales_person/core/networking/api_requests.dart';
import 'package:meedigo_company_sales_person/features/main/domain/entities/account_status_response.dart';
import 'package:meedigo_company_sales_person/features/main/domain/entities/update_fcm_token_response.dart';

abstract class MainRepository {
  Future<Either<Failure, UpdateFcmTokenResponse>> updateFcmToken({
    required UpdateFcmTokenRequestBody updateFcmTokenRequestBody,
  });
  Future<Either<Failure, AccountStatusResponse>> getAccountStatus();
}
