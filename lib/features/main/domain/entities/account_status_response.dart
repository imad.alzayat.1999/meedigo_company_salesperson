class AccountStatusResponse {
  final AccountStatusData? accountStatusData;

  AccountStatusResponse({
    required this.accountStatusData,
  });
}

class AccountStatusData {
  final int allowOrder;
  final int active;

  AccountStatusData({
    required this.allowOrder,
    required this.active,
  });
}