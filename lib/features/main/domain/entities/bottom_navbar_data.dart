import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';

class BottomNavBarData extends Equatable{
  final String text;
  final String iconPath;
  final Widget screen;

  BottomNavBarData({
    required this.text,
    required this.iconPath,
    required this.screen,
  });

  @override
  // TODO: implement props
  List<Object?> get props => [text , iconPath , screen];
}
