class UpdateFcmTokenResponse {
  final String message;

  UpdateFcmTokenResponse({required this.message});
}