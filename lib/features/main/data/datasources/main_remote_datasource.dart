import 'package:dio/dio.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_preferences.dart';
import 'package:meedigo_company_sales_person/core/networking/api_error_handler.dart';
import 'package:meedigo_company_sales_person/core/networking/api_services.dart';
import 'package:meedigo_company_sales_person/core/networking/api_requests.dart';
import 'package:meedigo_company_sales_person/features/main/data/models/update_fcm_token_response_model.dart';
import '../models/account_status_response_model.dart';

abstract class MainRemoteDataSource {
  Future<AccountStatusResponseModel> getAccountStatus();
  Future<UpdateFcmTokenResponseModel> updateFcmToken({
    required UpdateFcmTokenRequestBody updateFcmTokenRequestBody,
  });
}

class MainRemoteDataSourceImpl implements MainRemoteDataSource {
  final ApiService apiService;

  MainRemoteDataSourceImpl({required this.apiService});

  @override
  Future<AccountStatusResponseModel> getAccountStatus() async {
    final loginData = AppPreferences.getUserInfo();
    try {
      final result = await apiService.getAccountStatus(
        id: loginData.salesPersonId.toString(),
      );
      return result;
    } on DioException catch (e) {
      throw handleError(e);
    }
  }

  @override
  Future<UpdateFcmTokenResponseModel> updateFcmToken({
    required UpdateFcmTokenRequestBody updateFcmTokenRequestBody,
  }) async {
    final loginData = AppPreferences.getUserInfo();
    try {
      final result = await apiService.updateFcmToken(
        updateFcmTokenRequestBody: updateFcmTokenRequestBody,
        id: loginData.salesPersonId,
      );
      return result;
    } on DioException catch (e) {
      throw handleError(e);
    }
  }
}
