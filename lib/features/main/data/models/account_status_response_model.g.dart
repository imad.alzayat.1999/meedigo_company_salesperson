// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'account_status_response_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

AccountStatusResponseModel _$AccountStatusResponseModelFromJson(
        Map<String, dynamic> json) =>
    AccountStatusResponseModel(
      accountStatusDataModel: json['data'] == null
          ? null
          : AccountStatusDataModel.fromJson(
              json['data'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$AccountStatusResponseModelToJson(
        AccountStatusResponseModel instance) =>
    <String, dynamic>{
      'data': instance.accountStatusDataModel,
    };

AccountStatusDataModel _$AccountStatusDataModelFromJson(
        Map<String, dynamic> json) =>
    AccountStatusDataModel(
      allowOrder: (json['allow_order'] as num?)?.toInt(),
      active: (json['active'] as num?)?.toInt(),
    );

Map<String, dynamic> _$AccountStatusDataModelToJson(
        AccountStatusDataModel instance) =>
    <String, dynamic>{
      'allow_order': instance.allowOrder,
      'active': instance.active,
    };
