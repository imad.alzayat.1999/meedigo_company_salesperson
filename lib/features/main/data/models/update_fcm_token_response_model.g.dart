// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'update_fcm_token_response_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

UpdateFcmTokenResponseModel _$UpdateFcmTokenResponseModelFromJson(
        Map<String, dynamic> json) =>
    UpdateFcmTokenResponseModel(
      message: json['message'] as String?,
    );

Map<String, dynamic> _$UpdateFcmTokenResponseModelToJson(
        UpdateFcmTokenResponseModel instance) =>
    <String, dynamic>{
      'message': instance.message,
    };
