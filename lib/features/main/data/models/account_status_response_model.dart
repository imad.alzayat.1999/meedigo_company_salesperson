import 'package:json_annotation/json_annotation.dart';

part 'account_status_response_model.g.dart';

@JsonSerializable()
class AccountStatusResponseModel {
  @JsonKey(name: "data")
  final AccountStatusDataModel? accountStatusDataModel;

  AccountStatusResponseModel({
    required this.accountStatusDataModel,
  });

  factory AccountStatusResponseModel.fromJson(Map<String, dynamic> json) =>
      _$AccountStatusResponseModelFromJson(json);

  Map<String, dynamic> toJson() => _$AccountStatusResponseModelToJson(this);
}

@JsonSerializable()
class AccountStatusDataModel {
  @JsonKey(name: "allow_order")
  final int? allowOrder;

  @JsonKey(name: "active")
  final int? active;

  AccountStatusDataModel({
    required this.allowOrder,
    required this.active,
  });

  factory AccountStatusDataModel.fromJson(Map<String, dynamic> json) =>
      _$AccountStatusDataModelFromJson(json);

  Map<String, dynamic> toJson() => _$AccountStatusDataModelToJson(this);
}
