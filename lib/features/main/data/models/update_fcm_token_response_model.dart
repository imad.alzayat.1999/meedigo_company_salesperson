import 'package:json_annotation/json_annotation.dart';

part 'update_fcm_token_response_model.g.dart';

@JsonSerializable()
class UpdateFcmTokenResponseModel {
  @JsonKey(name: "message")
  final String? message;

  UpdateFcmTokenResponseModel({required this.message});

  factory UpdateFcmTokenResponseModel.fromJson(Map<String, dynamic> json) =>
      _$UpdateFcmTokenResponseModelFromJson(json);

  Map<String, dynamic> toJson() => _$UpdateFcmTokenResponseModelToJson(this);
}
