import 'package:dartz/dartz.dart';
import 'package:meedigo_company_sales_person/core/mappers/account_status_mapper.dart';
import 'package:meedigo_company_sales_person/core/networking/api_error_model.dart';
import 'package:meedigo_company_sales_person/core/networking/api_failure.dart';
import 'package:meedigo_company_sales_person/features/main/data/datasources/main_remote_datasource.dart';
import 'package:meedigo_company_sales_person/features/main/domain/entities/account_status_response.dart';
import 'package:meedigo_company_sales_person/core/mappers/update_fcm_token_mapper.dart';
import 'package:meedigo_company_sales_person/core/networking/api_requests.dart';
import 'package:meedigo_company_sales_person/features/main/domain/entities/update_fcm_token_response.dart';
import 'package:meedigo_company_sales_person/features/main/domain/repository/main_repository.dart';

class MainRepositoryImpl implements MainRepository {
  final MainRemoteDataSource mainRemoteDataSource;

  MainRepositoryImpl({required this.mainRemoteDataSource});

  @override
  Future<Either<Failure, AccountStatusResponse>> getAccountStatus() async {
    try {
      final result = await mainRemoteDataSource.getAccountStatus();
      return Right(result.toDomain());
    } on ApiErrorModel catch (e) {
      return Left(
        ServerFailure(
          errorMessage: e.message,
          statusCode: e.code,
        ),
      );
    }
  }

  @override
  Future<Either<Failure, UpdateFcmTokenResponse>> updateFcmToken({
    required UpdateFcmTokenRequestBody updateFcmTokenRequestBody,
  }) async {
    try {
      final result = await mainRemoteDataSource.updateFcmToken(
        updateFcmTokenRequestBody: updateFcmTokenRequestBody,
      );
      return Right(result.toDomain());
    } on ApiErrorModel catch (e) {
      return Left(
        ServerFailure(
          errorMessage: e.message,
          statusCode: e.code,
        ),
      );
    }
  }
}
