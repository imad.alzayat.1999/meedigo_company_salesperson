part of 'update_fcm_token_bloc.dart';

@freezed
class UpdateFcmTokenEvent with _$UpdateFcmTokenEvent {
  const factory UpdateFcmTokenEvent.updateFcmToken(
    UpdateFcmTokenRequestBody updateFcmTokenRequestBody,
  ) = _UpdateFcmToken;
}
