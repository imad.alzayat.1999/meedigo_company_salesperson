part of 'update_fcm_token_bloc.dart';

@freezed
class UpdateFcmTokenState with _$UpdateFcmTokenState {
  const factory UpdateFcmTokenState.initial() = _Initial;

  /// update fcm token request states
  const factory UpdateFcmTokenState.updateFcmLoading() = _UpdateFcmLoading;
  const factory UpdateFcmTokenState.updateFcmSuccess(
    UpdateFcmTokenResponse updateFcmTokenResponse,
  ) = _UpdateFcmSuccess;
  const factory UpdateFcmTokenState.updateFcmFailed(
    Failure failure,
  ) = _UpdateFcmFailed;
}
