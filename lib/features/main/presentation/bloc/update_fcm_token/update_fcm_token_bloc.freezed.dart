// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'update_fcm_token_bloc.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

/// @nodoc
mixin _$UpdateFcmTokenEvent {
  UpdateFcmTokenRequestBody get updateFcmTokenRequestBody =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(
            UpdateFcmTokenRequestBody updateFcmTokenRequestBody)
        updateFcmToken,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(UpdateFcmTokenRequestBody updateFcmTokenRequestBody)?
        updateFcmToken,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(UpdateFcmTokenRequestBody updateFcmTokenRequestBody)?
        updateFcmToken,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_UpdateFcmToken value) updateFcmToken,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_UpdateFcmToken value)? updateFcmToken,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_UpdateFcmToken value)? updateFcmToken,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $UpdateFcmTokenEventCopyWith<UpdateFcmTokenEvent> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $UpdateFcmTokenEventCopyWith<$Res> {
  factory $UpdateFcmTokenEventCopyWith(
          UpdateFcmTokenEvent value, $Res Function(UpdateFcmTokenEvent) then) =
      _$UpdateFcmTokenEventCopyWithImpl<$Res, UpdateFcmTokenEvent>;
  @useResult
  $Res call({UpdateFcmTokenRequestBody updateFcmTokenRequestBody});
}

/// @nodoc
class _$UpdateFcmTokenEventCopyWithImpl<$Res, $Val extends UpdateFcmTokenEvent>
    implements $UpdateFcmTokenEventCopyWith<$Res> {
  _$UpdateFcmTokenEventCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? updateFcmTokenRequestBody = null,
  }) {
    return _then(_value.copyWith(
      updateFcmTokenRequestBody: null == updateFcmTokenRequestBody
          ? _value.updateFcmTokenRequestBody
          : updateFcmTokenRequestBody // ignore: cast_nullable_to_non_nullable
              as UpdateFcmTokenRequestBody,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$UpdateFcmTokenImplCopyWith<$Res>
    implements $UpdateFcmTokenEventCopyWith<$Res> {
  factory _$$UpdateFcmTokenImplCopyWith(_$UpdateFcmTokenImpl value,
          $Res Function(_$UpdateFcmTokenImpl) then) =
      __$$UpdateFcmTokenImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({UpdateFcmTokenRequestBody updateFcmTokenRequestBody});
}

/// @nodoc
class __$$UpdateFcmTokenImplCopyWithImpl<$Res>
    extends _$UpdateFcmTokenEventCopyWithImpl<$Res, _$UpdateFcmTokenImpl>
    implements _$$UpdateFcmTokenImplCopyWith<$Res> {
  __$$UpdateFcmTokenImplCopyWithImpl(
      _$UpdateFcmTokenImpl _value, $Res Function(_$UpdateFcmTokenImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? updateFcmTokenRequestBody = null,
  }) {
    return _then(_$UpdateFcmTokenImpl(
      null == updateFcmTokenRequestBody
          ? _value.updateFcmTokenRequestBody
          : updateFcmTokenRequestBody // ignore: cast_nullable_to_non_nullable
              as UpdateFcmTokenRequestBody,
    ));
  }
}

/// @nodoc

class _$UpdateFcmTokenImpl implements _UpdateFcmToken {
  const _$UpdateFcmTokenImpl(this.updateFcmTokenRequestBody);

  @override
  final UpdateFcmTokenRequestBody updateFcmTokenRequestBody;

  @override
  String toString() {
    return 'UpdateFcmTokenEvent.updateFcmToken(updateFcmTokenRequestBody: $updateFcmTokenRequestBody)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$UpdateFcmTokenImpl &&
            (identical(other.updateFcmTokenRequestBody,
                    updateFcmTokenRequestBody) ||
                other.updateFcmTokenRequestBody == updateFcmTokenRequestBody));
  }

  @override
  int get hashCode => Object.hash(runtimeType, updateFcmTokenRequestBody);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$UpdateFcmTokenImplCopyWith<_$UpdateFcmTokenImpl> get copyWith =>
      __$$UpdateFcmTokenImplCopyWithImpl<_$UpdateFcmTokenImpl>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(
            UpdateFcmTokenRequestBody updateFcmTokenRequestBody)
        updateFcmToken,
  }) {
    return updateFcmToken(updateFcmTokenRequestBody);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(UpdateFcmTokenRequestBody updateFcmTokenRequestBody)?
        updateFcmToken,
  }) {
    return updateFcmToken?.call(updateFcmTokenRequestBody);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(UpdateFcmTokenRequestBody updateFcmTokenRequestBody)?
        updateFcmToken,
    required TResult orElse(),
  }) {
    if (updateFcmToken != null) {
      return updateFcmToken(updateFcmTokenRequestBody);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_UpdateFcmToken value) updateFcmToken,
  }) {
    return updateFcmToken(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_UpdateFcmToken value)? updateFcmToken,
  }) {
    return updateFcmToken?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_UpdateFcmToken value)? updateFcmToken,
    required TResult orElse(),
  }) {
    if (updateFcmToken != null) {
      return updateFcmToken(this);
    }
    return orElse();
  }
}

abstract class _UpdateFcmToken implements UpdateFcmTokenEvent {
  const factory _UpdateFcmToken(
          final UpdateFcmTokenRequestBody updateFcmTokenRequestBody) =
      _$UpdateFcmTokenImpl;

  @override
  UpdateFcmTokenRequestBody get updateFcmTokenRequestBody;
  @override
  @JsonKey(ignore: true)
  _$$UpdateFcmTokenImplCopyWith<_$UpdateFcmTokenImpl> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
mixin _$UpdateFcmTokenState {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() updateFcmLoading,
    required TResult Function(UpdateFcmTokenResponse updateFcmTokenResponse)
        updateFcmSuccess,
    required TResult Function(Failure failure) updateFcmFailed,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? updateFcmLoading,
    TResult? Function(UpdateFcmTokenResponse updateFcmTokenResponse)?
        updateFcmSuccess,
    TResult? Function(Failure failure)? updateFcmFailed,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? updateFcmLoading,
    TResult Function(UpdateFcmTokenResponse updateFcmTokenResponse)?
        updateFcmSuccess,
    TResult Function(Failure failure)? updateFcmFailed,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_UpdateFcmLoading value) updateFcmLoading,
    required TResult Function(_UpdateFcmSuccess value) updateFcmSuccess,
    required TResult Function(_UpdateFcmFailed value) updateFcmFailed,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_UpdateFcmLoading value)? updateFcmLoading,
    TResult? Function(_UpdateFcmSuccess value)? updateFcmSuccess,
    TResult? Function(_UpdateFcmFailed value)? updateFcmFailed,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_UpdateFcmLoading value)? updateFcmLoading,
    TResult Function(_UpdateFcmSuccess value)? updateFcmSuccess,
    TResult Function(_UpdateFcmFailed value)? updateFcmFailed,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $UpdateFcmTokenStateCopyWith<$Res> {
  factory $UpdateFcmTokenStateCopyWith(
          UpdateFcmTokenState value, $Res Function(UpdateFcmTokenState) then) =
      _$UpdateFcmTokenStateCopyWithImpl<$Res, UpdateFcmTokenState>;
}

/// @nodoc
class _$UpdateFcmTokenStateCopyWithImpl<$Res, $Val extends UpdateFcmTokenState>
    implements $UpdateFcmTokenStateCopyWith<$Res> {
  _$UpdateFcmTokenStateCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$InitialImplCopyWith<$Res> {
  factory _$$InitialImplCopyWith(
          _$InitialImpl value, $Res Function(_$InitialImpl) then) =
      __$$InitialImplCopyWithImpl<$Res>;
}

/// @nodoc
class __$$InitialImplCopyWithImpl<$Res>
    extends _$UpdateFcmTokenStateCopyWithImpl<$Res, _$InitialImpl>
    implements _$$InitialImplCopyWith<$Res> {
  __$$InitialImplCopyWithImpl(
      _$InitialImpl _value, $Res Function(_$InitialImpl) _then)
      : super(_value, _then);
}

/// @nodoc

class _$InitialImpl implements _Initial {
  const _$InitialImpl();

  @override
  String toString() {
    return 'UpdateFcmTokenState.initial()';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$InitialImpl);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() updateFcmLoading,
    required TResult Function(UpdateFcmTokenResponse updateFcmTokenResponse)
        updateFcmSuccess,
    required TResult Function(Failure failure) updateFcmFailed,
  }) {
    return initial();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? updateFcmLoading,
    TResult? Function(UpdateFcmTokenResponse updateFcmTokenResponse)?
        updateFcmSuccess,
    TResult? Function(Failure failure)? updateFcmFailed,
  }) {
    return initial?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? updateFcmLoading,
    TResult Function(UpdateFcmTokenResponse updateFcmTokenResponse)?
        updateFcmSuccess,
    TResult Function(Failure failure)? updateFcmFailed,
    required TResult orElse(),
  }) {
    if (initial != null) {
      return initial();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_UpdateFcmLoading value) updateFcmLoading,
    required TResult Function(_UpdateFcmSuccess value) updateFcmSuccess,
    required TResult Function(_UpdateFcmFailed value) updateFcmFailed,
  }) {
    return initial(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_UpdateFcmLoading value)? updateFcmLoading,
    TResult? Function(_UpdateFcmSuccess value)? updateFcmSuccess,
    TResult? Function(_UpdateFcmFailed value)? updateFcmFailed,
  }) {
    return initial?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_UpdateFcmLoading value)? updateFcmLoading,
    TResult Function(_UpdateFcmSuccess value)? updateFcmSuccess,
    TResult Function(_UpdateFcmFailed value)? updateFcmFailed,
    required TResult orElse(),
  }) {
    if (initial != null) {
      return initial(this);
    }
    return orElse();
  }
}

abstract class _Initial implements UpdateFcmTokenState {
  const factory _Initial() = _$InitialImpl;
}

/// @nodoc
abstract class _$$UpdateFcmLoadingImplCopyWith<$Res> {
  factory _$$UpdateFcmLoadingImplCopyWith(_$UpdateFcmLoadingImpl value,
          $Res Function(_$UpdateFcmLoadingImpl) then) =
      __$$UpdateFcmLoadingImplCopyWithImpl<$Res>;
}

/// @nodoc
class __$$UpdateFcmLoadingImplCopyWithImpl<$Res>
    extends _$UpdateFcmTokenStateCopyWithImpl<$Res, _$UpdateFcmLoadingImpl>
    implements _$$UpdateFcmLoadingImplCopyWith<$Res> {
  __$$UpdateFcmLoadingImplCopyWithImpl(_$UpdateFcmLoadingImpl _value,
      $Res Function(_$UpdateFcmLoadingImpl) _then)
      : super(_value, _then);
}

/// @nodoc

class _$UpdateFcmLoadingImpl implements _UpdateFcmLoading {
  const _$UpdateFcmLoadingImpl();

  @override
  String toString() {
    return 'UpdateFcmTokenState.updateFcmLoading()';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$UpdateFcmLoadingImpl);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() updateFcmLoading,
    required TResult Function(UpdateFcmTokenResponse updateFcmTokenResponse)
        updateFcmSuccess,
    required TResult Function(Failure failure) updateFcmFailed,
  }) {
    return updateFcmLoading();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? updateFcmLoading,
    TResult? Function(UpdateFcmTokenResponse updateFcmTokenResponse)?
        updateFcmSuccess,
    TResult? Function(Failure failure)? updateFcmFailed,
  }) {
    return updateFcmLoading?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? updateFcmLoading,
    TResult Function(UpdateFcmTokenResponse updateFcmTokenResponse)?
        updateFcmSuccess,
    TResult Function(Failure failure)? updateFcmFailed,
    required TResult orElse(),
  }) {
    if (updateFcmLoading != null) {
      return updateFcmLoading();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_UpdateFcmLoading value) updateFcmLoading,
    required TResult Function(_UpdateFcmSuccess value) updateFcmSuccess,
    required TResult Function(_UpdateFcmFailed value) updateFcmFailed,
  }) {
    return updateFcmLoading(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_UpdateFcmLoading value)? updateFcmLoading,
    TResult? Function(_UpdateFcmSuccess value)? updateFcmSuccess,
    TResult? Function(_UpdateFcmFailed value)? updateFcmFailed,
  }) {
    return updateFcmLoading?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_UpdateFcmLoading value)? updateFcmLoading,
    TResult Function(_UpdateFcmSuccess value)? updateFcmSuccess,
    TResult Function(_UpdateFcmFailed value)? updateFcmFailed,
    required TResult orElse(),
  }) {
    if (updateFcmLoading != null) {
      return updateFcmLoading(this);
    }
    return orElse();
  }
}

abstract class _UpdateFcmLoading implements UpdateFcmTokenState {
  const factory _UpdateFcmLoading() = _$UpdateFcmLoadingImpl;
}

/// @nodoc
abstract class _$$UpdateFcmSuccessImplCopyWith<$Res> {
  factory _$$UpdateFcmSuccessImplCopyWith(_$UpdateFcmSuccessImpl value,
          $Res Function(_$UpdateFcmSuccessImpl) then) =
      __$$UpdateFcmSuccessImplCopyWithImpl<$Res>;
  @useResult
  $Res call({UpdateFcmTokenResponse updateFcmTokenResponse});
}

/// @nodoc
class __$$UpdateFcmSuccessImplCopyWithImpl<$Res>
    extends _$UpdateFcmTokenStateCopyWithImpl<$Res, _$UpdateFcmSuccessImpl>
    implements _$$UpdateFcmSuccessImplCopyWith<$Res> {
  __$$UpdateFcmSuccessImplCopyWithImpl(_$UpdateFcmSuccessImpl _value,
      $Res Function(_$UpdateFcmSuccessImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? updateFcmTokenResponse = null,
  }) {
    return _then(_$UpdateFcmSuccessImpl(
      null == updateFcmTokenResponse
          ? _value.updateFcmTokenResponse
          : updateFcmTokenResponse // ignore: cast_nullable_to_non_nullable
              as UpdateFcmTokenResponse,
    ));
  }
}

/// @nodoc

class _$UpdateFcmSuccessImpl implements _UpdateFcmSuccess {
  const _$UpdateFcmSuccessImpl(this.updateFcmTokenResponse);

  @override
  final UpdateFcmTokenResponse updateFcmTokenResponse;

  @override
  String toString() {
    return 'UpdateFcmTokenState.updateFcmSuccess(updateFcmTokenResponse: $updateFcmTokenResponse)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$UpdateFcmSuccessImpl &&
            (identical(other.updateFcmTokenResponse, updateFcmTokenResponse) ||
                other.updateFcmTokenResponse == updateFcmTokenResponse));
  }

  @override
  int get hashCode => Object.hash(runtimeType, updateFcmTokenResponse);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$UpdateFcmSuccessImplCopyWith<_$UpdateFcmSuccessImpl> get copyWith =>
      __$$UpdateFcmSuccessImplCopyWithImpl<_$UpdateFcmSuccessImpl>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() updateFcmLoading,
    required TResult Function(UpdateFcmTokenResponse updateFcmTokenResponse)
        updateFcmSuccess,
    required TResult Function(Failure failure) updateFcmFailed,
  }) {
    return updateFcmSuccess(updateFcmTokenResponse);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? updateFcmLoading,
    TResult? Function(UpdateFcmTokenResponse updateFcmTokenResponse)?
        updateFcmSuccess,
    TResult? Function(Failure failure)? updateFcmFailed,
  }) {
    return updateFcmSuccess?.call(updateFcmTokenResponse);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? updateFcmLoading,
    TResult Function(UpdateFcmTokenResponse updateFcmTokenResponse)?
        updateFcmSuccess,
    TResult Function(Failure failure)? updateFcmFailed,
    required TResult orElse(),
  }) {
    if (updateFcmSuccess != null) {
      return updateFcmSuccess(updateFcmTokenResponse);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_UpdateFcmLoading value) updateFcmLoading,
    required TResult Function(_UpdateFcmSuccess value) updateFcmSuccess,
    required TResult Function(_UpdateFcmFailed value) updateFcmFailed,
  }) {
    return updateFcmSuccess(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_UpdateFcmLoading value)? updateFcmLoading,
    TResult? Function(_UpdateFcmSuccess value)? updateFcmSuccess,
    TResult? Function(_UpdateFcmFailed value)? updateFcmFailed,
  }) {
    return updateFcmSuccess?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_UpdateFcmLoading value)? updateFcmLoading,
    TResult Function(_UpdateFcmSuccess value)? updateFcmSuccess,
    TResult Function(_UpdateFcmFailed value)? updateFcmFailed,
    required TResult orElse(),
  }) {
    if (updateFcmSuccess != null) {
      return updateFcmSuccess(this);
    }
    return orElse();
  }
}

abstract class _UpdateFcmSuccess implements UpdateFcmTokenState {
  const factory _UpdateFcmSuccess(
          final UpdateFcmTokenResponse updateFcmTokenResponse) =
      _$UpdateFcmSuccessImpl;

  UpdateFcmTokenResponse get updateFcmTokenResponse;
  @JsonKey(ignore: true)
  _$$UpdateFcmSuccessImplCopyWith<_$UpdateFcmSuccessImpl> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$UpdateFcmFailedImplCopyWith<$Res> {
  factory _$$UpdateFcmFailedImplCopyWith(_$UpdateFcmFailedImpl value,
          $Res Function(_$UpdateFcmFailedImpl) then) =
      __$$UpdateFcmFailedImplCopyWithImpl<$Res>;
  @useResult
  $Res call({Failure failure});
}

/// @nodoc
class __$$UpdateFcmFailedImplCopyWithImpl<$Res>
    extends _$UpdateFcmTokenStateCopyWithImpl<$Res, _$UpdateFcmFailedImpl>
    implements _$$UpdateFcmFailedImplCopyWith<$Res> {
  __$$UpdateFcmFailedImplCopyWithImpl(
      _$UpdateFcmFailedImpl _value, $Res Function(_$UpdateFcmFailedImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? failure = null,
  }) {
    return _then(_$UpdateFcmFailedImpl(
      null == failure
          ? _value.failure
          : failure // ignore: cast_nullable_to_non_nullable
              as Failure,
    ));
  }
}

/// @nodoc

class _$UpdateFcmFailedImpl implements _UpdateFcmFailed {
  const _$UpdateFcmFailedImpl(this.failure);

  @override
  final Failure failure;

  @override
  String toString() {
    return 'UpdateFcmTokenState.updateFcmFailed(failure: $failure)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$UpdateFcmFailedImpl &&
            (identical(other.failure, failure) || other.failure == failure));
  }

  @override
  int get hashCode => Object.hash(runtimeType, failure);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$UpdateFcmFailedImplCopyWith<_$UpdateFcmFailedImpl> get copyWith =>
      __$$UpdateFcmFailedImplCopyWithImpl<_$UpdateFcmFailedImpl>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() updateFcmLoading,
    required TResult Function(UpdateFcmTokenResponse updateFcmTokenResponse)
        updateFcmSuccess,
    required TResult Function(Failure failure) updateFcmFailed,
  }) {
    return updateFcmFailed(failure);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? updateFcmLoading,
    TResult? Function(UpdateFcmTokenResponse updateFcmTokenResponse)?
        updateFcmSuccess,
    TResult? Function(Failure failure)? updateFcmFailed,
  }) {
    return updateFcmFailed?.call(failure);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? updateFcmLoading,
    TResult Function(UpdateFcmTokenResponse updateFcmTokenResponse)?
        updateFcmSuccess,
    TResult Function(Failure failure)? updateFcmFailed,
    required TResult orElse(),
  }) {
    if (updateFcmFailed != null) {
      return updateFcmFailed(failure);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_UpdateFcmLoading value) updateFcmLoading,
    required TResult Function(_UpdateFcmSuccess value) updateFcmSuccess,
    required TResult Function(_UpdateFcmFailed value) updateFcmFailed,
  }) {
    return updateFcmFailed(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_UpdateFcmLoading value)? updateFcmLoading,
    TResult? Function(_UpdateFcmSuccess value)? updateFcmSuccess,
    TResult? Function(_UpdateFcmFailed value)? updateFcmFailed,
  }) {
    return updateFcmFailed?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_UpdateFcmLoading value)? updateFcmLoading,
    TResult Function(_UpdateFcmSuccess value)? updateFcmSuccess,
    TResult Function(_UpdateFcmFailed value)? updateFcmFailed,
    required TResult orElse(),
  }) {
    if (updateFcmFailed != null) {
      return updateFcmFailed(this);
    }
    return orElse();
  }
}

abstract class _UpdateFcmFailed implements UpdateFcmTokenState {
  const factory _UpdateFcmFailed(final Failure failure) = _$UpdateFcmFailedImpl;

  Failure get failure;
  @JsonKey(ignore: true)
  _$$UpdateFcmFailedImplCopyWith<_$UpdateFcmFailedImpl> get copyWith =>
      throw _privateConstructorUsedError;
}
