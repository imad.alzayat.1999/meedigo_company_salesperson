import 'package:bloc/bloc.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:meedigo_company_sales_person/core/networking/api_failure.dart';
import 'package:meedigo_company_sales_person/core/networking/api_requests.dart';
import 'package:meedigo_company_sales_person/features/main/domain/entities/update_fcm_token_response.dart';

import '../../../domain/usecases/update_fcm_token_usecase.dart';

part 'update_fcm_token_event.dart';
part 'update_fcm_token_state.dart';
part 'update_fcm_token_bloc.freezed.dart';

class UpdateFcmTokenBloc
    extends Bloc<UpdateFcmTokenEvent, UpdateFcmTokenState> {
  final UpdateFcmTokenUseCase updateFcmTokenUseCase;
  UpdateFcmTokenBloc({
    required this.updateFcmTokenUseCase,
  }) : super(const UpdateFcmTokenState.initial()) {
    on<UpdateFcmTokenEvent>((event, emit) async {
      await event.when(updateFcmToken: (body) async {
        emit(const UpdateFcmTokenState.updateFcmLoading());
        final failureOrUpdateFcmToken = await updateFcmTokenUseCase(body);
        failureOrUpdateFcmToken.fold(
          (l) => emit(
            UpdateFcmTokenState.updateFcmFailed(l),
          ),
          (r) => emit(
            UpdateFcmTokenState.updateFcmSuccess(r),
          ),
        );
      });
    });
  }

  /// function to call update fcm token event
  makeUpdateFcmToken() async {
    final firebaseToken = await FirebaseMessaging.instance.getToken();
    UpdateFcmTokenRequestBody body = UpdateFcmTokenRequestBody(
      firebaseToken: firebaseToken ?? "",
    );
    add(UpdateFcmTokenEvent.updateFcmToken(body));
  }
}
