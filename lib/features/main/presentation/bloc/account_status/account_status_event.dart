part of 'account_status_bloc.dart';

@freezed
class AccountStatusEvent with _$AccountStatusEvent {
  const factory AccountStatusEvent.getAccountStatus() = _GetAccountStatus;
}
