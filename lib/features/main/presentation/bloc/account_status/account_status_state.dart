part of 'account_status_bloc.dart';

@freezed
class AccountStatusState with _$AccountStatusState {
  const factory AccountStatusState.initial() = _Initial;

  /// get account status request states
  const factory AccountStatusState.getAccountStatusLoading() =
      _GetAccountStatusLoading;
  const factory AccountStatusState.getAccountStatusSuccess(
    AccountStatusData accountStatusData,
  ) = _GetAccountStatusSuccess;
  const factory AccountStatusState.getAccountStatusFailed(
    Failure failure,
  ) = _GetAccountStatusFailed;
}
