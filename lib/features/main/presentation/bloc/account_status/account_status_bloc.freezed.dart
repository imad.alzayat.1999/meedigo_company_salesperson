// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'account_status_bloc.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

/// @nodoc
mixin _$AccountStatusEvent {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() getAccountStatus,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? getAccountStatus,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? getAccountStatus,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_GetAccountStatus value) getAccountStatus,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_GetAccountStatus value)? getAccountStatus,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_GetAccountStatus value)? getAccountStatus,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $AccountStatusEventCopyWith<$Res> {
  factory $AccountStatusEventCopyWith(
          AccountStatusEvent value, $Res Function(AccountStatusEvent) then) =
      _$AccountStatusEventCopyWithImpl<$Res, AccountStatusEvent>;
}

/// @nodoc
class _$AccountStatusEventCopyWithImpl<$Res, $Val extends AccountStatusEvent>
    implements $AccountStatusEventCopyWith<$Res> {
  _$AccountStatusEventCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$GetAccountStatusImplCopyWith<$Res> {
  factory _$$GetAccountStatusImplCopyWith(_$GetAccountStatusImpl value,
          $Res Function(_$GetAccountStatusImpl) then) =
      __$$GetAccountStatusImplCopyWithImpl<$Res>;
}

/// @nodoc
class __$$GetAccountStatusImplCopyWithImpl<$Res>
    extends _$AccountStatusEventCopyWithImpl<$Res, _$GetAccountStatusImpl>
    implements _$$GetAccountStatusImplCopyWith<$Res> {
  __$$GetAccountStatusImplCopyWithImpl(_$GetAccountStatusImpl _value,
      $Res Function(_$GetAccountStatusImpl) _then)
      : super(_value, _then);
}

/// @nodoc

class _$GetAccountStatusImpl implements _GetAccountStatus {
  const _$GetAccountStatusImpl();

  @override
  String toString() {
    return 'AccountStatusEvent.getAccountStatus()';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$GetAccountStatusImpl);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() getAccountStatus,
  }) {
    return getAccountStatus();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? getAccountStatus,
  }) {
    return getAccountStatus?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? getAccountStatus,
    required TResult orElse(),
  }) {
    if (getAccountStatus != null) {
      return getAccountStatus();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_GetAccountStatus value) getAccountStatus,
  }) {
    return getAccountStatus(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_GetAccountStatus value)? getAccountStatus,
  }) {
    return getAccountStatus?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_GetAccountStatus value)? getAccountStatus,
    required TResult orElse(),
  }) {
    if (getAccountStatus != null) {
      return getAccountStatus(this);
    }
    return orElse();
  }
}

abstract class _GetAccountStatus implements AccountStatusEvent {
  const factory _GetAccountStatus() = _$GetAccountStatusImpl;
}

/// @nodoc
mixin _$AccountStatusState {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() getAccountStatusLoading,
    required TResult Function(AccountStatusData accountStatusData)
        getAccountStatusSuccess,
    required TResult Function(Failure failure) getAccountStatusFailed,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? getAccountStatusLoading,
    TResult? Function(AccountStatusData accountStatusData)?
        getAccountStatusSuccess,
    TResult? Function(Failure failure)? getAccountStatusFailed,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? getAccountStatusLoading,
    TResult Function(AccountStatusData accountStatusData)?
        getAccountStatusSuccess,
    TResult Function(Failure failure)? getAccountStatusFailed,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_GetAccountStatusLoading value)
        getAccountStatusLoading,
    required TResult Function(_GetAccountStatusSuccess value)
        getAccountStatusSuccess,
    required TResult Function(_GetAccountStatusFailed value)
        getAccountStatusFailed,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_GetAccountStatusLoading value)? getAccountStatusLoading,
    TResult? Function(_GetAccountStatusSuccess value)? getAccountStatusSuccess,
    TResult? Function(_GetAccountStatusFailed value)? getAccountStatusFailed,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_GetAccountStatusLoading value)? getAccountStatusLoading,
    TResult Function(_GetAccountStatusSuccess value)? getAccountStatusSuccess,
    TResult Function(_GetAccountStatusFailed value)? getAccountStatusFailed,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $AccountStatusStateCopyWith<$Res> {
  factory $AccountStatusStateCopyWith(
          AccountStatusState value, $Res Function(AccountStatusState) then) =
      _$AccountStatusStateCopyWithImpl<$Res, AccountStatusState>;
}

/// @nodoc
class _$AccountStatusStateCopyWithImpl<$Res, $Val extends AccountStatusState>
    implements $AccountStatusStateCopyWith<$Res> {
  _$AccountStatusStateCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$InitialImplCopyWith<$Res> {
  factory _$$InitialImplCopyWith(
          _$InitialImpl value, $Res Function(_$InitialImpl) then) =
      __$$InitialImplCopyWithImpl<$Res>;
}

/// @nodoc
class __$$InitialImplCopyWithImpl<$Res>
    extends _$AccountStatusStateCopyWithImpl<$Res, _$InitialImpl>
    implements _$$InitialImplCopyWith<$Res> {
  __$$InitialImplCopyWithImpl(
      _$InitialImpl _value, $Res Function(_$InitialImpl) _then)
      : super(_value, _then);
}

/// @nodoc

class _$InitialImpl implements _Initial {
  const _$InitialImpl();

  @override
  String toString() {
    return 'AccountStatusState.initial()';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$InitialImpl);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() getAccountStatusLoading,
    required TResult Function(AccountStatusData accountStatusData)
        getAccountStatusSuccess,
    required TResult Function(Failure failure) getAccountStatusFailed,
  }) {
    return initial();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? getAccountStatusLoading,
    TResult? Function(AccountStatusData accountStatusData)?
        getAccountStatusSuccess,
    TResult? Function(Failure failure)? getAccountStatusFailed,
  }) {
    return initial?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? getAccountStatusLoading,
    TResult Function(AccountStatusData accountStatusData)?
        getAccountStatusSuccess,
    TResult Function(Failure failure)? getAccountStatusFailed,
    required TResult orElse(),
  }) {
    if (initial != null) {
      return initial();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_GetAccountStatusLoading value)
        getAccountStatusLoading,
    required TResult Function(_GetAccountStatusSuccess value)
        getAccountStatusSuccess,
    required TResult Function(_GetAccountStatusFailed value)
        getAccountStatusFailed,
  }) {
    return initial(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_GetAccountStatusLoading value)? getAccountStatusLoading,
    TResult? Function(_GetAccountStatusSuccess value)? getAccountStatusSuccess,
    TResult? Function(_GetAccountStatusFailed value)? getAccountStatusFailed,
  }) {
    return initial?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_GetAccountStatusLoading value)? getAccountStatusLoading,
    TResult Function(_GetAccountStatusSuccess value)? getAccountStatusSuccess,
    TResult Function(_GetAccountStatusFailed value)? getAccountStatusFailed,
    required TResult orElse(),
  }) {
    if (initial != null) {
      return initial(this);
    }
    return orElse();
  }
}

abstract class _Initial implements AccountStatusState {
  const factory _Initial() = _$InitialImpl;
}

/// @nodoc
abstract class _$$GetAccountStatusLoadingImplCopyWith<$Res> {
  factory _$$GetAccountStatusLoadingImplCopyWith(
          _$GetAccountStatusLoadingImpl value,
          $Res Function(_$GetAccountStatusLoadingImpl) then) =
      __$$GetAccountStatusLoadingImplCopyWithImpl<$Res>;
}

/// @nodoc
class __$$GetAccountStatusLoadingImplCopyWithImpl<$Res>
    extends _$AccountStatusStateCopyWithImpl<$Res,
        _$GetAccountStatusLoadingImpl>
    implements _$$GetAccountStatusLoadingImplCopyWith<$Res> {
  __$$GetAccountStatusLoadingImplCopyWithImpl(
      _$GetAccountStatusLoadingImpl _value,
      $Res Function(_$GetAccountStatusLoadingImpl) _then)
      : super(_value, _then);
}

/// @nodoc

class _$GetAccountStatusLoadingImpl implements _GetAccountStatusLoading {
  const _$GetAccountStatusLoadingImpl();

  @override
  String toString() {
    return 'AccountStatusState.getAccountStatusLoading()';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$GetAccountStatusLoadingImpl);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() getAccountStatusLoading,
    required TResult Function(AccountStatusData accountStatusData)
        getAccountStatusSuccess,
    required TResult Function(Failure failure) getAccountStatusFailed,
  }) {
    return getAccountStatusLoading();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? getAccountStatusLoading,
    TResult? Function(AccountStatusData accountStatusData)?
        getAccountStatusSuccess,
    TResult? Function(Failure failure)? getAccountStatusFailed,
  }) {
    return getAccountStatusLoading?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? getAccountStatusLoading,
    TResult Function(AccountStatusData accountStatusData)?
        getAccountStatusSuccess,
    TResult Function(Failure failure)? getAccountStatusFailed,
    required TResult orElse(),
  }) {
    if (getAccountStatusLoading != null) {
      return getAccountStatusLoading();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_GetAccountStatusLoading value)
        getAccountStatusLoading,
    required TResult Function(_GetAccountStatusSuccess value)
        getAccountStatusSuccess,
    required TResult Function(_GetAccountStatusFailed value)
        getAccountStatusFailed,
  }) {
    return getAccountStatusLoading(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_GetAccountStatusLoading value)? getAccountStatusLoading,
    TResult? Function(_GetAccountStatusSuccess value)? getAccountStatusSuccess,
    TResult? Function(_GetAccountStatusFailed value)? getAccountStatusFailed,
  }) {
    return getAccountStatusLoading?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_GetAccountStatusLoading value)? getAccountStatusLoading,
    TResult Function(_GetAccountStatusSuccess value)? getAccountStatusSuccess,
    TResult Function(_GetAccountStatusFailed value)? getAccountStatusFailed,
    required TResult orElse(),
  }) {
    if (getAccountStatusLoading != null) {
      return getAccountStatusLoading(this);
    }
    return orElse();
  }
}

abstract class _GetAccountStatusLoading implements AccountStatusState {
  const factory _GetAccountStatusLoading() = _$GetAccountStatusLoadingImpl;
}

/// @nodoc
abstract class _$$GetAccountStatusSuccessImplCopyWith<$Res> {
  factory _$$GetAccountStatusSuccessImplCopyWith(
          _$GetAccountStatusSuccessImpl value,
          $Res Function(_$GetAccountStatusSuccessImpl) then) =
      __$$GetAccountStatusSuccessImplCopyWithImpl<$Res>;
  @useResult
  $Res call({AccountStatusData accountStatusData});
}

/// @nodoc
class __$$GetAccountStatusSuccessImplCopyWithImpl<$Res>
    extends _$AccountStatusStateCopyWithImpl<$Res,
        _$GetAccountStatusSuccessImpl>
    implements _$$GetAccountStatusSuccessImplCopyWith<$Res> {
  __$$GetAccountStatusSuccessImplCopyWithImpl(
      _$GetAccountStatusSuccessImpl _value,
      $Res Function(_$GetAccountStatusSuccessImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? accountStatusData = null,
  }) {
    return _then(_$GetAccountStatusSuccessImpl(
      null == accountStatusData
          ? _value.accountStatusData
          : accountStatusData // ignore: cast_nullable_to_non_nullable
              as AccountStatusData,
    ));
  }
}

/// @nodoc

class _$GetAccountStatusSuccessImpl implements _GetAccountStatusSuccess {
  const _$GetAccountStatusSuccessImpl(this.accountStatusData);

  @override
  final AccountStatusData accountStatusData;

  @override
  String toString() {
    return 'AccountStatusState.getAccountStatusSuccess(accountStatusData: $accountStatusData)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$GetAccountStatusSuccessImpl &&
            (identical(other.accountStatusData, accountStatusData) ||
                other.accountStatusData == accountStatusData));
  }

  @override
  int get hashCode => Object.hash(runtimeType, accountStatusData);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$GetAccountStatusSuccessImplCopyWith<_$GetAccountStatusSuccessImpl>
      get copyWith => __$$GetAccountStatusSuccessImplCopyWithImpl<
          _$GetAccountStatusSuccessImpl>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() getAccountStatusLoading,
    required TResult Function(AccountStatusData accountStatusData)
        getAccountStatusSuccess,
    required TResult Function(Failure failure) getAccountStatusFailed,
  }) {
    return getAccountStatusSuccess(accountStatusData);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? getAccountStatusLoading,
    TResult? Function(AccountStatusData accountStatusData)?
        getAccountStatusSuccess,
    TResult? Function(Failure failure)? getAccountStatusFailed,
  }) {
    return getAccountStatusSuccess?.call(accountStatusData);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? getAccountStatusLoading,
    TResult Function(AccountStatusData accountStatusData)?
        getAccountStatusSuccess,
    TResult Function(Failure failure)? getAccountStatusFailed,
    required TResult orElse(),
  }) {
    if (getAccountStatusSuccess != null) {
      return getAccountStatusSuccess(accountStatusData);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_GetAccountStatusLoading value)
        getAccountStatusLoading,
    required TResult Function(_GetAccountStatusSuccess value)
        getAccountStatusSuccess,
    required TResult Function(_GetAccountStatusFailed value)
        getAccountStatusFailed,
  }) {
    return getAccountStatusSuccess(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_GetAccountStatusLoading value)? getAccountStatusLoading,
    TResult? Function(_GetAccountStatusSuccess value)? getAccountStatusSuccess,
    TResult? Function(_GetAccountStatusFailed value)? getAccountStatusFailed,
  }) {
    return getAccountStatusSuccess?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_GetAccountStatusLoading value)? getAccountStatusLoading,
    TResult Function(_GetAccountStatusSuccess value)? getAccountStatusSuccess,
    TResult Function(_GetAccountStatusFailed value)? getAccountStatusFailed,
    required TResult orElse(),
  }) {
    if (getAccountStatusSuccess != null) {
      return getAccountStatusSuccess(this);
    }
    return orElse();
  }
}

abstract class _GetAccountStatusSuccess implements AccountStatusState {
  const factory _GetAccountStatusSuccess(
          final AccountStatusData accountStatusData) =
      _$GetAccountStatusSuccessImpl;

  AccountStatusData get accountStatusData;
  @JsonKey(ignore: true)
  _$$GetAccountStatusSuccessImplCopyWith<_$GetAccountStatusSuccessImpl>
      get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$GetAccountStatusFailedImplCopyWith<$Res> {
  factory _$$GetAccountStatusFailedImplCopyWith(
          _$GetAccountStatusFailedImpl value,
          $Res Function(_$GetAccountStatusFailedImpl) then) =
      __$$GetAccountStatusFailedImplCopyWithImpl<$Res>;
  @useResult
  $Res call({Failure failure});
}

/// @nodoc
class __$$GetAccountStatusFailedImplCopyWithImpl<$Res>
    extends _$AccountStatusStateCopyWithImpl<$Res, _$GetAccountStatusFailedImpl>
    implements _$$GetAccountStatusFailedImplCopyWith<$Res> {
  __$$GetAccountStatusFailedImplCopyWithImpl(
      _$GetAccountStatusFailedImpl _value,
      $Res Function(_$GetAccountStatusFailedImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? failure = null,
  }) {
    return _then(_$GetAccountStatusFailedImpl(
      null == failure
          ? _value.failure
          : failure // ignore: cast_nullable_to_non_nullable
              as Failure,
    ));
  }
}

/// @nodoc

class _$GetAccountStatusFailedImpl implements _GetAccountStatusFailed {
  const _$GetAccountStatusFailedImpl(this.failure);

  @override
  final Failure failure;

  @override
  String toString() {
    return 'AccountStatusState.getAccountStatusFailed(failure: $failure)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$GetAccountStatusFailedImpl &&
            (identical(other.failure, failure) || other.failure == failure));
  }

  @override
  int get hashCode => Object.hash(runtimeType, failure);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$GetAccountStatusFailedImplCopyWith<_$GetAccountStatusFailedImpl>
      get copyWith => __$$GetAccountStatusFailedImplCopyWithImpl<
          _$GetAccountStatusFailedImpl>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() getAccountStatusLoading,
    required TResult Function(AccountStatusData accountStatusData)
        getAccountStatusSuccess,
    required TResult Function(Failure failure) getAccountStatusFailed,
  }) {
    return getAccountStatusFailed(failure);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? getAccountStatusLoading,
    TResult? Function(AccountStatusData accountStatusData)?
        getAccountStatusSuccess,
    TResult? Function(Failure failure)? getAccountStatusFailed,
  }) {
    return getAccountStatusFailed?.call(failure);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? getAccountStatusLoading,
    TResult Function(AccountStatusData accountStatusData)?
        getAccountStatusSuccess,
    TResult Function(Failure failure)? getAccountStatusFailed,
    required TResult orElse(),
  }) {
    if (getAccountStatusFailed != null) {
      return getAccountStatusFailed(failure);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_GetAccountStatusLoading value)
        getAccountStatusLoading,
    required TResult Function(_GetAccountStatusSuccess value)
        getAccountStatusSuccess,
    required TResult Function(_GetAccountStatusFailed value)
        getAccountStatusFailed,
  }) {
    return getAccountStatusFailed(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_GetAccountStatusLoading value)? getAccountStatusLoading,
    TResult? Function(_GetAccountStatusSuccess value)? getAccountStatusSuccess,
    TResult? Function(_GetAccountStatusFailed value)? getAccountStatusFailed,
  }) {
    return getAccountStatusFailed?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_GetAccountStatusLoading value)? getAccountStatusLoading,
    TResult Function(_GetAccountStatusSuccess value)? getAccountStatusSuccess,
    TResult Function(_GetAccountStatusFailed value)? getAccountStatusFailed,
    required TResult orElse(),
  }) {
    if (getAccountStatusFailed != null) {
      return getAccountStatusFailed(this);
    }
    return orElse();
  }
}

abstract class _GetAccountStatusFailed implements AccountStatusState {
  const factory _GetAccountStatusFailed(final Failure failure) =
      _$GetAccountStatusFailedImpl;

  Failure get failure;
  @JsonKey(ignore: true)
  _$$GetAccountStatusFailedImplCopyWith<_$GetAccountStatusFailedImpl>
      get copyWith => throw _privateConstructorUsedError;
}
