import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_extensions.dart';
import 'package:meedigo_company_sales_person/core/networking/api_failure.dart';
import 'package:meedigo_company_sales_person/core/routing/app_routes.dart';
import 'package:meedigo_company_sales_person/core/usecase/base_usecase.dart';
import 'package:meedigo_company_sales_person/features/auth/domain/entities/login.dart';
import 'package:meedigo_company_sales_person/features/main/domain/entities/account_status_response.dart';

import '../../../../../core/helpers/app_preferences.dart';
import '../../../domain/usecases/get_account_status_usecase.dart';

part 'account_status_event.dart';
part 'account_status_state.dart';
part 'account_status_bloc.freezed.dart';

class AccountStatusBloc extends Bloc<AccountStatusEvent, AccountStatusState> {
  final GetAccountStatusUseCase getAccountStatusUseCase;
  AccountStatusBloc({
    required this.getAccountStatusUseCase,
  }) : super(const AccountStatusState.initial()) {
    on<AccountStatusEvent>((event, emit) async {
      await event.when(getAccountStatus: () async {
        emit(AccountStatusState.getAccountStatusLoading());
        final failureOrAccountStatus = await getAccountStatusUseCase(
          const NoParameters(),
        );
        failureOrAccountStatus.fold(
          (l) => emit(
            AccountStatusState.getAccountStatusFailed(l),
          ),
          (r) => emit(
            AccountStatusState.getAccountStatusSuccess(
              r.accountStatusData!,
            ),
          ),
        );
      });
    });
  }

    /// function to check if the account is active
  /// then don't do anything
  /// else route to account disabled screen
  handleAccountStatusDone({
    required AccountStatusData accountStatus,
    required BuildContext context,
  }) {
    LoginData auth = AppPreferences.getUserInfo();
    if (accountStatus.active == 0) {
      AppPreferences.saveLoginData(
        LoginData(
          salesPersonId: auth.salesPersonId,
          token: auth.token,
          allowOrder: auth.allowOrder,
          active: auth.active,
          companyId: auth.companyId,
          companyName: auth.companyName,
          type: auth.type,
          representatives: auth.representatives,
        ),
      );
      context.pushNamedAndRemoveUntil(
        AppRoutes.accountBlockedRoute,
      );
    } else {
      AppPreferences.saveLoginData(
        LoginData(
          salesPersonId: auth.salesPersonId,
          token: auth.token,
          allowOrder: auth.allowOrder,
          active: auth.active,
          companyId: auth.companyId,
          companyName: auth.companyName,
          type: auth.type,
          representatives: auth.representatives,
        ),
      );
    }
  }
}
