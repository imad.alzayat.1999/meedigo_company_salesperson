import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_extensions.dart';
import 'package:meedigo_company_sales_person/core/widgets/appbar_widget.dart';
import 'package:meedigo_company_sales_person/features/attendance/presentation/bloc/attendance_bloc.dart';
import 'package:meedigo_company_sales_person/features/main/presentation/bloc/account_status/account_status_bloc.dart';
import 'package:meedigo_company_sales_person/features/main/presentation/bloc/update_fcm_token/update_fcm_token_bloc.dart';
import '../../../../core/helpers/app_consts.dart';
import '../widgets/bottom_navbar_widget.dart';

class MainScreen extends StatefulWidget {
  const MainScreen({Key? key}) : super(key: key);

  @override
  State<MainScreen> createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen> {
  int _currentIndex = 0;
  PageController? _pageController;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _pageController = PageController(
      initialPage: _currentIndex,
    );
    context.read<AttendanceBloc>()..trackUserLocation();
    context.read<AccountStatusBloc>()
      ..add(
        AccountStatusEvent.getAccountStatus(),
      );
    context.read<UpdateFcmTokenBloc>()..makeUpdateFcmToken();
  }

  /// function to change page view index
  _selectItem(int index) {
    setState(() {
      _currentIndex = index;
    });
  }

  /// function to tap on item on the bottom navigation bar
  _tapOnItem(int index) {
    setState(() {
      _currentIndex = index;
      _pageController!.animateToPage(
        index,
        duration: Duration(
          milliseconds: AppConsts.pageViewDuration,
        ),
        curve: Curves.linear,
      );
    });
  }

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<AccountStatusBloc, AccountStatusState>(
      listener: (context, state) {
        state.whenOrNull(
          getAccountStatusSuccess: (accountStatus) =>
              context.read<AccountStatusBloc>()
                ..handleAccountStatusDone(
                  accountStatus: accountStatus,
                  context: context,
                ),
        );
      },
      builder: (context, state) {
        return Scaffold(
          appBar: appbarWidget(
            title: context.translate(
              AppConsts.bottomNavBarItems[_currentIndex].text,
            ),
            context: context,
          ),
          body: SizedBox.expand(
            child: PageView.builder(
              controller: _pageController,
              onPageChanged: _selectItem,
              physics: const NeverScrollableScrollPhysics(),
              itemBuilder: (context, index) =>
                  AppConsts.bottomNavBarItems[index].screen,
            ),
          ),
          bottomNavigationBar: BottomNavbarWidget(
            currentIndex: _currentIndex,
            onTap: (index) => _tapOnItem(index),
          ),
        );
      },
    );
  }
}
