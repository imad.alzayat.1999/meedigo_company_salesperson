import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_consts.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_extensions.dart';
import '../../../../core/helpers/app_sizes.dart';
import '../../../../core/theming/app_colors.dart';
import '../../../../core/theming/app_styles.dart';
import '../../domain/entities/bottom_navbar_data.dart';

class BottomNavbarWidget extends StatelessWidget {
  final int currentIndex;
  final void Function(int)? onTap;

  const BottomNavbarWidget({
    Key? key,
    required this.currentIndex,
    required this.onTap,
  });

  @override
  Widget build(BuildContext context) {
    return BottomNavigationBar(
      currentIndex: currentIndex,
      onTap: onTap,
      items: AppConsts.bottomNavBarItems
          .map(
            (item) => getBottomNavigationBarItem(
              context: context,
              bottomNavBarData: item,
              hasMatched: currentIndex ==
                  AppConsts.bottomNavBarItems.indexOf(
                    item,
                  ),
            ),
          )
          .toList(),
    );
  }
}

/// function to get bottom navigation bar item
BottomNavigationBarItem getBottomNavigationBarItem({
  required BottomNavBarData bottomNavBarData,
  required bool hasMatched,
  required BuildContext context,
}) {
  return BottomNavigationBarItem(
    icon: SizedBox(
      height: getRadius(28),
      width: getRadius(28),
      child: SvgPicture.asset(
        bottomNavBarData.iconPath,
        color: hasMatched ? AppColors.kActivated : AppColors.kWhite,
      ),
    ),
    label: context.translate(bottomNavBarData.text),
  );
}

class SavedBadge extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Positioned(
      right: 15,
      left: 0,
      child: Container(
        width: getHeight(20),
        height: getHeight(20),
        decoration: BoxDecoration(
          color: AppColors.kSecondary,
          shape: BoxShape.circle,
        ),
        child: Center(
          child: Text(
            "10",
            style: AppStyles.bold(
              fontSize: 12,
              context: context,
            ),
          ),
        ),
      ),
    );
  }
}
