import 'package:equatable/equatable.dart';

class OnBoarding extends Equatable{
  final String title;
  final String description;
  final String imagePath;

  OnBoarding({
    required this.title,
    required this.description,
    required this.imagePath,
  });

  @override
  // TODO: implement props
  List<Object?> get props => [title , description , imagePath];
}

