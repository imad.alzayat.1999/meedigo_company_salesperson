import 'package:flutter/material.dart';
import 'package:meedigo_company_sales_person/core/widgets/app_padding_widget.dart';

import '../../../../core/helpers/app_sizes.dart';
import '../../../../core/helpers/app_consts.dart';
import '../widgets/on_boarding_actions_widget.dart';
import '../widgets/on_boarding_indicators_widget.dart';
import '../widgets/on_boarding_page_widget.dart';

class OnBoardingScreen extends StatefulWidget {
  const OnBoardingScreen({Key? key}) : super(key: key);

  @override
  State<OnBoardingScreen> createState() => _OnBoardingScreenState();
}

class _OnBoardingScreenState extends State<OnBoardingScreen> {
  int _currentIndex = 0;
  PageController? _pageController;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _pageController = PageController(initialPage: 0);
  }

  /// function to change index of onBoarding page
  _changeOnBoardingPageIndex(int index) {
    setState(() {
      _currentIndex = index;
    });
  }

  /// function to go to next page of the page view via page controller
  _goToTheNextPage() {
    _pageController!.nextPage(
      duration: Duration(
        milliseconds: AppConsts.onBoardingDuration,
      ),
      curve: Curves.easeIn,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: AppPaddingWidget(
        child: Column(
          children: [
            Expanded(
              child: PageView.builder(
                controller: _pageController,
                itemBuilder: (context, index) => OnBoardingPageWidget(
                  onBoarding: AppConsts.onBoardingData[index],
                ),
                itemCount: AppConsts.onBoardingData.length,
                onPageChanged: (index) => _changeOnBoardingPageIndex(index),
              ),
            ),
            OnBoardingIndicatorsWidget(currentIndex: _currentIndex),
            verticalSpacing(34),
            OnBoardingActionsWidget(
              currentIndex: _currentIndex,
              onPress: () => _goToTheNextPage(),
            ),
          ],
        ),
      ),
    );
  }
}
