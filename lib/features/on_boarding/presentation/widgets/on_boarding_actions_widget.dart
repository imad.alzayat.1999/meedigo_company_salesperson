import 'package:flutter/material.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_extensions.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_preferences.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_strings.dart';

import '../../../../core/helpers/app_consts.dart';
import '../../../../core/routing/app_routes.dart';
import '../../../../core/theming/app_colors.dart';
import '../../../../core/theming/app_styles.dart';
import '../../../../core/widgets/button_widget.dart';

class OnBoardingActionsWidget extends StatelessWidget {
  final int currentIndex;
  final void Function()? onPress;

  OnBoardingActionsWidget({
    required this.currentIndex,
    required this.onPress,
  });

  @override
  Widget build(BuildContext context) {
    if (currentIndex == AppConsts.onBoardingData.length - 1) {
      return ButtonWidget(
        onPressFunction: () => _markAsPassedAndGoToSendSmsScreen(
          context: context,
        ),
        btnText: context.translate(
          AppStrings.getStartedBtnString,
        ),
      );
    } else {
      return Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          TextButton(
            onPressed: () => _markAsPassedAndGoToSendSmsScreen(
              context: context,
            ),
            child: Text(
              context.translate(
                AppStrings.skipBtnString,
              ),
              style: AppStyles.bold(
                fontSize: 18,
                textColor: AppColors.kPrimary,
                context: context,
              ),
            ),
          ),
          ButtonWidget(
            btnWidth: 115,
            onPressFunction: onPress,
            btnText: context.translate(
              AppStrings.nextBtnString,
            ),
          ),
        ],
      );
    }
  }

  /// function to mark for [onBoardingKey] as passed
  /// then save the results into local storage
  /// and route to send sms screen
  _markAsPassedAndGoToSendSmsScreen({
    required BuildContext context,
  }) {
    AppPreferences.passOnBoardingPage();
    context.pushNamed(AppRoutes.sendSmsRoute);
  }
}
