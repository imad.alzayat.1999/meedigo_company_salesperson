import 'package:flutter/material.dart';
import '../../../../core/helpers/app_sizes.dart';
import '../../../../core/helpers/app_consts.dart';
import '../../../../core/theming/app_colors.dart';

class OnBoardingIndicatorsWidget extends StatelessWidget {
  final int currentIndex;
  const OnBoardingIndicatorsWidget({required this.currentIndex});

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: List.generate(
        AppConsts.onBoardingData.length,
        (index) => AnimatedContainer(
          duration: const Duration(
            milliseconds: AppConsts.animatedOnBoardingIndicatorDuration,
          ),
          height: getRadius(12),
          width: currentIndex == index ? getWidth(40) : getRadius(12),
          margin: EdgeInsets.only(
            right: getWidth(14),
          ),
          decoration: BoxDecoration(
            color: currentIndex == index
                ? AppColors.kPrimary
                : AppColors.kPrimary.withOpacity(0.5),
            borderRadius: BorderRadius.all(
              Radius.circular(
                getRadius(15),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
