import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_extensions.dart';
import '../../../../core/helpers/app_sizes.dart';
import '../../../../core/theming/app_colors.dart';
import '../../../../core/theming/app_styles.dart';
import '../../domain/entities/on_boarding.dart';

class OnBoardingPageWidget extends StatelessWidget {
  final OnBoarding onBoarding;

  const OnBoardingPageWidget({required this.onBoarding});

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        SizedBox(
          height: getHeight(300),
          width: getWidth(200),
          child: SvgPicture.asset(
            onBoarding.imagePath,
          ),
        ),
        verticalSpacing(40),
        Text(
          context.translate(onBoarding.title),
          textAlign: TextAlign.center,
          style: AppStyles.bold(
            textColor: AppColors.kPrimaryText,
            fontSize: 20,
            context: context,
          ),
        ),
        verticalSpacing(30),
        Text(
          context.translate(onBoarding.description),
          textAlign: TextAlign.center,
          style: AppStyles.medium(
            fontSize: 16,
            textColor: AppColors.kSecondaryText,
            context: context,
            height: 2,
          ),
        )
      ],
    );
  }
}
