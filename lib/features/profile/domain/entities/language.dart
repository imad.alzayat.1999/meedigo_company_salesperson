import 'package:equatable/equatable.dart';

class Language extends Equatable {
  final String iconPath;
  final String key;
  bool hasMatch;

  Language({
    required this.iconPath,
    required this.key,
    required this.hasMatch,
  });

  @override
  // TODO: implement props
  List<Object?> get props => [iconPath , key , hasMatch];
}
