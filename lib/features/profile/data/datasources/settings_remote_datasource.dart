import 'package:meedigo_company_sales_person/core/networking/api_services.dart';

abstract class SettingsRemoteDataSource {
  
}

class SettingsRemoteDataSourceImpl implements SettingsRemoteDataSource {
  final ApiService apiService;

  SettingsRemoteDataSourceImpl({
    required this.apiService,
  });
}
