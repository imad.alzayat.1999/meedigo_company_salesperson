import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_extensions.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_strings.dart';
import 'package:meedigo_company_sales_person/features/profile/presentation/widgets/profile_widgets/section_widget.dart';
import '../../../../../core/helpers/app_sizes.dart';
import '../../../../../core/helpers/app_assets.dart';
import '../../../../../core/theming/app_colors.dart';
import '../../../../../core/theming/app_styles.dart';

class WelcomeHeaderWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  context.translate(
                    AppStrings.welcomeSalespersonString,
                    args: {
                      "salesperson": "My Salesperson",
                    },
                  ),
                  style: AppStyles.bold(
                    fontSize: 20,
                    context: context,
                  ),
                ),
                verticalSpacing(15),
                Text(
                  context.translate(
                    AppStrings.welcomeBackString,
                  ),
                  style: AppStyles.semiBold(
                    fontSize: 18,
                    textColor: AppColors.kSecondaryText,
                    context: context,
                  ),
                ),
              ],
            ),
            SizedBox(
              width: getRadius(90),
              height: getRadius(90),
              child: SvgPicture.asset(
                AppIcons.handshakeIcon,
                color: AppColors.kPrimaryText,
              ),
            )
          ],
        ),
        verticalSpacing(45),
        ChoiceWidget(
          iconPath: AppIcons.userIcon,
          sectionTitle: context.translate(
            AppStrings.personalInformationChoiceString,
          ),
          onPressFunction: () {},
        ),
        verticalSpacing(15),
        ChoiceWidget(
          iconPath: AppIcons.qrCodeIcon,
          sectionTitle: context.translate(
            AppStrings.scanQrString,
          ),
          onPressFunction: () {},
        ),
      ],
    );
  }
}
