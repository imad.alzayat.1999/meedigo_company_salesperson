import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:meedigo_company_sales_person/core/common/bloc/location/location_bloc.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_assets.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_extensions.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_functions.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_strings.dart';
import 'package:meedigo_company_sales_person/core/helpers/enums.dart';
import 'package:meedigo_company_sales_person/core/widgets/loading_indicator_widget.dart';
import 'package:meedigo_company_sales_person/features/attendance/presentation/bloc/attendance_bloc.dart';
import 'package:meedigo_company_sales_person/features/profile/presentation/widgets/profile_widgets/section_widget.dart';

class CheckoutChoiceWithBlocBuilderWidget extends StatelessWidget {
  const CheckoutChoiceWithBlocBuilderWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<LocationBloc, LocationState>(
      listener: (context, state) {
        state.whenOrNull(
          locationDone: (position) => context.read<AttendanceBloc>()
            ..makeCheckout(
              context: context,
              position: position,
            ),
          locationFailed: (message) => AppFunctions.showFailedMessage(
            context: context,
            message: message,
          ),
        );
      },
      builder: (context, state) {
        return state.maybeWhen(
          locationLoading: () => const LoadingIndicatorWidget(
            loadingSize: LoadingSizes.button,
          ),
          orElse: () => BlocConsumer<AttendanceBloc, AttendanceState>(
            listener: (context, state) {
              state.whenOrNull(
                checkOutDone: () => context.read<AttendanceBloc>()
                  ..cancelTrackingAndBackToAttendanceRoute(
                    context,
                  ),
                checkOutFailed: (failure) => context.read<AttendanceBloc>()
                  ..showAttendanceFailureMessage(
                    context: context,
                    failure: failure,
                  ),
              );
            },
            builder: (context, state) {
              return state.maybeWhen(
                checkOutLoading: () => const LoadingIndicatorWidget(
                  loadingSize: LoadingSizes.button,
                ),
                orElse: () => ChoiceWidget(
                  iconPath: AppIcons.checkoutIcon,
                  sectionTitle: context.translate(
                    AppStrings.checkoutChoiceString,
                  ),
                  onPressFunction: () => context.read<LocationBloc>()
                    ..add(
                      LocationEvent.getCurrentLocation(),
                    ),
                ),
              );
            },
          ),
        );
      },
    );
  }
}
