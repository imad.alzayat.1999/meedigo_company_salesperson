import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

import '../../../../../core/helpers/app_sizes.dart';
import '../../../../../core/helpers/app_assets.dart';
import '../../../../../core/theming/app_colors.dart';
import '../../../../../core/theming/app_styles.dart';

class ChoiceWidget extends StatelessWidget {
  final String iconPath;
  final String sectionTitle;
  final void Function()? onPressFunction;

  const ChoiceWidget({
    Key? key,
    required this.iconPath,
    required this.sectionTitle,
    required this.onPressFunction,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: MediaQuery.of(context).size.width,
      height: getHeight(50),
      child: ElevatedButton(
        onPressed: onPressFunction,
        style: ElevatedButton.styleFrom(
          padding: EdgeInsets.zero,
          backgroundColor: AppColors.kPrimary,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(
              getRadius(12),
            ),
          ),
        ),
        child: Padding(
          padding: EdgeInsets.symmetric(
            horizontal: getWidth(10),
          ),
          child: Row(
            children: [
              SizedBox(
                width: getHeight(35),
                height: getHeight(35),
                child: SvgPicture.asset(
                  iconPath,
                  color: AppColors.kWhite,
                ),
              ),
              horizontalSpacing(6),
              Expanded(
                child: Text(
                  sectionTitle,
                  style: AppStyles.semiBold(
                    fontSize: 18,
                    textColor: AppColors.kWhite,
                    context: context,
                  ),
                ),
              ),
              SizedBox(
                width: getHeight(35),
                height: getHeight(35),
                child: SvgPicture.asset(
                  AppIcons.arrowRightIcon,
                  color: AppColors.kWhite,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
