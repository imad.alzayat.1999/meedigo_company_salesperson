import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_extensions.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_strings.dart';
import 'package:meedigo_company_sales_person/core/theming/app_colors.dart';

import '../../../../../core/helpers/app_assets.dart';
import '../../../../../core/helpers/app_sizes.dart';
import '../../../../../core/theming/app_styles.dart';

class ContactUsWidget extends StatelessWidget {
  const ContactUsWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return ContactUsTypes();
  }
}

class ContactUsTypes extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Align(
          alignment: AlignmentDirectional.centerStart,
          child: Text(
            context.translate(
              AppStrings.contactUsTitleString,
            ),
            style: AppStyles.bold(
              fontSize: 20,
              context: context,
              textColor: AppColors.kSecondaryText,
            ),
          ),
        ),
        verticalSpacing(13),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            ContactUsButton(
              onPressFunction: () => {},
              iconPath: AppIcons.whatsappIcon,
            ),
            ContactUsButton(
              onPressFunction: () => {},
              iconPath: AppIcons.phoneIcon,
            ),
            ContactUsButton(
              onPressFunction: () => {},
              iconPath: AppIcons.facebookIcon,
            ),
            ContactUsButton(
              onPressFunction: () => {},
              iconPath: AppIcons.instagramIcon,
            ),
          ],
        ),
      ],
    );
  }
}

class ContactUsButton extends StatelessWidget {
  final void Function()? onPressFunction;
  final String iconPath;
  const ContactUsButton({
    super.key,
    required this.onPressFunction,
    required this.iconPath,
  });

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: getWidth(58),
      height: getHeight(58),
      child: ElevatedButton(
        onPressed: onPressFunction,
        style: ElevatedButton.styleFrom(
          elevation: 0,
          shape: CircleBorder(),
          backgroundColor: AppColors.kSecondary,
          padding: EdgeInsets.all(getRadius(2)),
        ),
        child: SvgPicture.asset(
          iconPath,
          color: AppColors.kPrimaryText,
        ),
      ),
    );
  }
}
