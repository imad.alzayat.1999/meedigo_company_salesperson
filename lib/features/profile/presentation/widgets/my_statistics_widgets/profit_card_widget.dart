import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_extensions.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_strings.dart';

import '../../../../../core/helpers/app_assets.dart';
import '../../../../../core/helpers/app_sizes.dart';
import '../../../../../core/theming/app_colors.dart';
import '../../../../../core/theming/app_styles.dart';

class ProfitCardWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: getHeight(100),
      padding: EdgeInsets.symmetric(
        horizontal: getWidth(8),
      ),
      decoration: BoxDecoration(
        color: AppColors.kSecondary,
        borderRadius: BorderRadius.circular(
          getRadius(10),
        ),
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  context.translate(
                    AppStrings.sellingProfitString,
                  ),
                  style: AppStyles.medium(
                    fontSize: 12,
                    context: context,
                  ),
                ),
                verticalSpacing(14),
                Text(
                  "14,000",
                  style: AppStyles.bold(
                    fontSize: 25,
                    context: context,
                  ),
                ),
              ],
            ),
          ),
          SizedBox(
            height: getHeight(65),
            width: getHeight(65),
            child: SvgPicture.asset(
              AppIcons.trendIcon,
              color: AppColors.kPrimaryText,
            ),
          )
        ],
      ),
    );
  }
}
