import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_assets.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_extensions.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_strings.dart';
import '../../../../../core/helpers/app_consts.dart';
import '../../../../../core/helpers/app_sizes.dart';
import '../../../../../core/theming/app_colors.dart';
import '../../../../../core/theming/app_styles.dart';

class GridOfStatisticsWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GridView.builder(
      gridDelegate: AppConsts.myStatisticsGridDelegate,
      shrinkWrap: true,
      physics: const NeverScrollableScrollPhysics(),
      itemCount: 4,
      itemBuilder: (context, index) => StatisticsCard(
        index: index,
      ),
    );
  }
}

class StatisticsCard extends StatelessWidget {
  final int index;

  const StatisticsCard({
    super.key,
    required this.index,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: AppColors.kSecondary,
        borderRadius: BorderRadius.circular(
          getRadius(10),
        ),
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          SizedBox(
            width: getRadius(46),
            height: getRadius(46),
            child: SvgPicture.asset(
              AppIcons.totalDeliveredOrdersIcon,
              color: AppColors.kPrimaryText,
            ),
          ),
          verticalSpacing(8),
          Text(
            context.translate(
              AppStrings.overallPriceOfDeliveredOrdersString,
            ),
            textAlign: TextAlign.center,
            style: AppStyles.medium(
              fontSize: 12,
              context: context,
            ),
          ),
          verticalSpacing(15),
          Text(
            "1,500,000",
            style: AppStyles.bold(
              fontSize: 20,
              context: context,
            ),
          ),
        ],
      ),
    );
  }
}
