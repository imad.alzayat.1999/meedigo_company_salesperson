import 'package:flutter/material.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_extensions.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_strings.dart';

import '../../../../../core/helpers/app_sizes.dart';
import '../../../../../core/theming/app_colors.dart';
import '../../../../../core/theming/app_styles.dart';

class AccountBlockedContentWidget extends StatelessWidget {
  const AccountBlockedContentWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Text(
          context.translate(
            AppStrings.accountBlockedTitleString,
          ),
          textAlign: TextAlign.center,
          style: AppStyles.bold(
            fontSize: 20,
            height: 1.5,
            context: context,
          ),
        ),
        verticalSpacing(27),
        Text(
          context.translate(
            AppStrings.accountBlockedContentString,
          ),
          textAlign: TextAlign.center,
          style: AppStyles.medium(
            fontSize: 14,
            textColor: AppColors.kSecondaryText,
            height: 1.5,
            context: context,
          ),
        ),
      ],
    );
  }
}
