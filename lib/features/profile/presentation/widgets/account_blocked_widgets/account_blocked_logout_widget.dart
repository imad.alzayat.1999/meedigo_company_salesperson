import 'package:flutter/material.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_extensions.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_strings.dart';
import '../../../../../core/widgets/button_widget.dart';

class AccountBlockedLogoutWidget extends StatelessWidget {
  final void Function()? onPressFunction;
  const AccountBlockedLogoutWidget({
    Key? key,
    required this.onPressFunction,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ButtonWidget(
      onPressFunction: onPressFunction,
      btnText: context.translate(
        AppStrings.logoutBtnString,
      ),
    );
  }
}
