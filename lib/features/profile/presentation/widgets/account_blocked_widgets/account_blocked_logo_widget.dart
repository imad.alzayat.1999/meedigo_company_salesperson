import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';
import '../../../../../core/helpers/app_sizes.dart';
import '../../../../../core/helpers/app_assets.dart';

class AccountBlockedLogoWidget extends StatelessWidget {
  const AccountBlockedLogoWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: getRadius(250),
      width: getRadius(250),
      child: Lottie.asset(AppJsons.blockedJson),
    );
  }
}
