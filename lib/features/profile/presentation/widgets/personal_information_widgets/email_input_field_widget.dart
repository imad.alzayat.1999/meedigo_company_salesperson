import 'package:flutter/material.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_extensions.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_strings.dart';
import '../../../../../core/widgets/text_field_widget.dart';

class EmailInputFieldWidget extends StatelessWidget {
  final bool isReadOnly;
  final TextEditingController textEditingController;

  const EmailInputFieldWidget({
    super.key,
    required this.isReadOnly,
    required this.textEditingController,
  });

  @override
  Widget build(BuildContext context) {
    return TextFieldWidget(
      labelText: context.translate(
        AppStrings.emailLabelTextString,
      ),
      textEditingController: textEditingController,
      isReadOnly: isReadOnly,
      hintText: context.translate(
        AppStrings.emailHintTextString,
      ),
      textInputType: TextInputType.emailAddress,
      onChange: (value) {},
    );
  }
}
