import 'package:flutter/material.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_extensions.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_strings.dart';
import '../../../../../core/widgets/text_field_widget.dart';

class NameInputFieldWidget extends StatelessWidget {
  final bool isReadOnly;
  final TextEditingController textEditingController;
  const NameInputFieldWidget({
    super.key,
    required this.isReadOnly,
    required this.textEditingController,
  });

  @override
  Widget build(BuildContext context) {
    return TextFieldWidget(
      labelText: context.translate(
        AppStrings.nameLabelTextString,
      ),
      hintText: context.translate(
        AppStrings.nameHintTextString,
      ),
      textEditingController: textEditingController,
      isReadOnly: isReadOnly,
      textInputType: TextInputType.text,
      onChange: (value) {},
    );
  }
}
