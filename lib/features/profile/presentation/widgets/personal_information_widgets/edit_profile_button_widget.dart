import 'package:flutter/material.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_extensions.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_strings.dart';
import '../../../../../core/widgets/button_widget.dart';

class EditProfileButtonWidget extends StatelessWidget {
  final bool isReadOnly;
  final void Function()? onPressFunction;
  const EditProfileButtonWidget({
    super.key,
    required this.isReadOnly,
    required this.onPressFunction,
  });

  @override
  Widget build(BuildContext context) {
    return ButtonWidget(
      onPressFunction: onPressFunction,
      btnText: isReadOnly
          ? context.translate(
              AppStrings.editInformationBtnString,
            )
          : context.translate(
              AppStrings.saveInformationBtnString,
            ),
    );
  }
}
