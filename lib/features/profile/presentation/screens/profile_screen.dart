import 'package:flutter/material.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_extensions.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_strings.dart';
import 'package:meedigo_company_sales_person/core/widgets/app_padding_widget.dart';
import 'package:meedigo_company_sales_person/features/profile/presentation/widgets/profile_widgets/checkout_choice_with_bloc_builder_widget.dart';
import '../../../../core/helpers/app_sizes.dart';
import '../../../../core/helpers/app_assets.dart';
import '../widgets/profile_widgets/contact_us_widget.dart';
import '../widgets/profile_widgets/section_widget.dart';
import '../widgets/profile_widgets/welcome_header_widget.dart';

class ProfileScreen extends StatelessWidget {
  const ProfileScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: AppPaddingWidget(
        top: 15,
        child: SingleChildScrollView(
          child: Column(
            children: [
              WelcomeHeaderWidget(),
              verticalSpacing(15),
              ChoiceWidget(
                iconPath: AppIcons.languageIcon,
                sectionTitle: context.translate(
                  AppStrings.languageChoiceString,
                ),
                onPressFunction: () {},
              ),
              verticalSpacing(15),
              ChoiceWidget(
                iconPath: AppIcons.trendIcon,
                sectionTitle: context.translate(
                  AppStrings.myStatisticsChoiceString,
                ),
                onPressFunction: () {},
              ),
              verticalSpacing(15),
              CheckoutChoiceWithBlocBuilderWidget(),
              verticalSpacing(15),
              ChoiceWidget(
                iconPath: AppIcons.logoutIcon,
                sectionTitle: context.translate(
                  AppStrings.logoutBtnString,
                ),
                onPressFunction: () {},
              ),
              verticalSpacing(30),
              ContactUsWidget(),
            ],
          ),
        ),
      ),
    );
  }
}
