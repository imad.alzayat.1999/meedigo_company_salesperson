import 'package:flutter/material.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_extensions.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_strings.dart';
import 'package:meedigo_company_sales_person/core/widgets/app_padding_widget.dart';
import '../../../../core/helpers/app_sizes.dart';
import '../../../../core/widgets/appbar_widget.dart';
import '../../../no_internet/presentation/screens/no_internet_connection_screen.dart';
import '../widgets/personal_information_widgets/edit_profile_button_widget.dart';
import '../widgets/personal_information_widgets/email_input_field_widget.dart';
import '../widgets/personal_information_widgets/name_input_field_widget.dart';
import '../widgets/personal_information_widgets/phone_input_field_widget.dart';

class PersonalInformationScreen extends StatefulWidget {
  @override
  State<PersonalInformationScreen> createState() =>
      _PersonalInformationScreenState();
}

class _PersonalInformationScreenState extends State<PersonalInformationScreen> {
  var _nameController = TextEditingController();
  var _emailController = TextEditingController();
  var _phoneController = TextEditingController();
  bool _isReadOnly = false;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  /// change type of text fields
  _changeTypeOfTextFields() {
    setState(() {
      _isReadOnly = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    return NoInternetConnectionScreen(
      screen: Scaffold(
        extendBody: true,
        appBar: appbarWidget(
          title: context.translate(
            AppStrings.personalInformationTitleString,
          ),
          context: context,
          isMainPages: true,
        ),
        body: AppPaddingWidget(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              NameInputFieldWidget(
                isReadOnly: _isReadOnly,
                textEditingController: _nameController,
              ),
              verticalSpacing(25),
              EmailInputFieldWidget(
                isReadOnly: _isReadOnly,
                textEditingController: _emailController,
              ),
              verticalSpacing(25),
              PhoneInputFieldWidget(
                isReadOnly: _isReadOnly,
                textEditingController: _phoneController,
              ),
              verticalSpacing(82),
              EditProfileButtonWidget(
                isReadOnly: _isReadOnly,
                onPressFunction: () => _changeTypeOfTextFields(),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
