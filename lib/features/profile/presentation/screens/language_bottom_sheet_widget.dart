import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_phoenix/flutter_phoenix.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_consts.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_extensions.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_strings.dart';
import 'package:meedigo_company_sales_person/core/widgets/app_padding_widget.dart';

import '../../../../core/helpers/app_assets.dart';
import '../../../../core/helpers/app_sizes.dart';
import '../../../../core/theming/app_colors.dart';
import '../../../../core/theming/app_styles.dart';
import '../../../../core/widgets/button_widget.dart';
import '../../domain/entities/language.dart';

class LanguageBottomSheetWidget extends StatefulWidget {
  final String languageCode;
  const LanguageBottomSheetWidget({
    super.key,
    required this.languageCode,
  });

  @override
  State<LanguageBottomSheetWidget> createState() =>
      _LanguageBottomSheetWidgetState();
}

class _LanguageBottomSheetWidgetState extends State<LanguageBottomSheetWidget> {
  final List<Language> _langs = [];

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _init();
  }

  _init() {
    _langs.add(
      Language(
        iconPath: AppIcons.enIcon,
        key: AppConsts.en,
        hasMatch: widget.languageCode == AppConsts.en ? true : false,
      ),
    );
    _langs.add(
      Language(
        iconPath: AppIcons.arIcon,
        key: AppConsts.ar,
        hasMatch: widget.languageCode == AppConsts.ar ? true : false,
      ),
    );
  }

  /// select language
  _selectLanguage({required Language language}) {
    if (language.hasMatch == false) {
      setState(() {
        _langs.forEach(
          (element) => element.hasMatch = false,
        );
        language.hasMatch = true;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: getHeight(275),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.only(
        topLeft: Radius.circular(getRadius(40)),
        topRight: Radius.circular(getRadius(40)),
      )),
      child: AppPaddingWidget(
        top: 13,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Container(
              width: getWidth(53),
              height: getHeight(6),
              decoration: BoxDecoration(
                color: AppColors.kPrimary.withOpacity(0.6),
                borderRadius: BorderRadius.circular(
                  getRadius(8),
                ),
              ),
            ),
            verticalSpacing(22),
            Text(
              context.translate(
                AppStrings.languageBottomSheetTitleString,
              ),
              style: AppStyles.bold(
                fontSize: 18,
                context: context,
              ),
            ),
            verticalSpacing(29),
            ListView.separated(
              shrinkWrap: true,
              physics: const NeverScrollableScrollPhysics(),
              itemBuilder: (context, index) => LanguageItem(
                language: _langs[index],
                onTapFunction: () => _selectLanguage(
                  language: _langs[index],
                ),
              ),
              separatorBuilder: (context, index) => verticalSpacing(20),
              itemCount: _langs.length,
            ),
          ],
        ),
      ),
    );
  }
}

class LanguageItem extends StatelessWidget {
  final Language language;
  final void Function()? onTapFunction;
  const LanguageItem({
    super.key,
    required this.language,
    required this.onTapFunction,
  });

  @override
  Widget build(BuildContext context) {
    return AnimatedContainer(
      duration: const Duration(
        milliseconds: AppConsts.animatedContainerDuration,
      ),
      curve: Curves.easeIn,
      width: MediaQuery.of(context).size.width,
      height: getHeight(45),
      padding: EdgeInsets.symmetric(
        horizontal: getWidth(17),
      ),
      decoration: BoxDecoration(
        color: language.hasMatch ? AppColors.kPrimary : AppColors.kSecondary,
        borderRadius: BorderRadius.circular(
          getRadius(12),
        ),
      ),
      child: Material(
        color: AppColors.kTransparent,
        child: InkWell(
          borderRadius: BorderRadius.circular(
            getRadius(12),
          ),
          onTap: onTapFunction,
          child: Row(
            children: [
              SizedBox(
                width: getHeight(28),
                height: getHeight(28),
                child: SvgPicture.asset(
                  language.iconPath,
                ),
              ),
              horizontalSpacing(8),
              Expanded(
                child: Text(
                  language.key,
                  style: AppStyles.semiBold(
                    fontSize: 18,
                    textColor: language.hasMatch
                        ? AppColors.kWhite
                        : AppColors.kPrimaryText,
                    context: context,
                  ),
                ),
              ),
              language.hasMatch
                  ? SizedBox(
                      width: getHeight(25),
                      height: getHeight(25),
                      child: SvgPicture.asset(
                        AppIcons.checkIcon,
                        color: AppColors.kWhite,
                      ),
                    )
                  : Container(),
            ],
          ),
        ),
      ),
    );
  }
}
