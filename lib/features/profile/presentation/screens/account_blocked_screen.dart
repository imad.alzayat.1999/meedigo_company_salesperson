import 'package:flutter/material.dart';
import 'package:meedigo_company_sales_person/core/widgets/app_padding_widget.dart';
import '../../../../core/helpers/app_sizes.dart';
import '../widgets/account_blocked_widgets/account_blocked_content_widget.dart';
import '../widgets/account_blocked_widgets/account_blocked_logo_widget.dart';
import '../widgets/account_blocked_widgets/account_blocked_logout_widget.dart';

class AccountBlockedScreen extends StatelessWidget {
  const AccountBlockedScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: AppPaddingWidget(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            AccountBlockedLogoWidget(),
            verticalSpacing(30),
            AccountBlockedContentWidget(),
            verticalSpacing(43),
            AccountBlockedLogoutWidget(
              onPressFunction: () {},
            ),
          ],
        ),
      ),
    );
  }
}
