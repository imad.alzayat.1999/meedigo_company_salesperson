import 'package:flutter/material.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_extensions.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_sizes.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_strings.dart';
import 'package:meedigo_company_sales_person/core/widgets/app_padding_widget.dart';
import '../../../../core/widgets/appbar_widget.dart';
import '../../../no_internet/presentation/screens/no_internet_connection_screen.dart';
import '../widgets/my_statistics_widgets/grid_of_statistics_widget.dart';
import '../widgets/my_statistics_widgets/profit_card_widget.dart';

class MyStatisticsScreen extends StatefulWidget {
  const MyStatisticsScreen({super.key});

  @override
  State<MyStatisticsScreen> createState() => _MyStatisticsScreenState();
}

class _MyStatisticsScreenState extends State<MyStatisticsScreen> {
  @override
  Widget build(BuildContext context) {
    return NoInternetConnectionScreen(
      screen: Scaffold(
        appBar: appbarWidget(
          title: context.translate(
            AppStrings.myStatisticsTitleString,
          ),
          context: context,
        ),
        body: AppPaddingWidget(
          top: 45,
          child: Column(
            children: [
              ProfitCardWidget(),
              verticalSpacing(15),
              GridOfStatisticsWidget(),
            ],
          ),
        ),
      ),
    );
  }
}
