import 'package:flutter/material.dart';
import '../../../no_internet/presentation/screens/no_internet_connection_screen.dart';
import '../widgets/notifications_card_widget.dart';

class NotificationsScreen extends StatelessWidget {
  const NotificationsScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return NoInternetConnectionScreen(
      screen: Scaffold(
        body: NotificationsContent(),
      ),
    );
  }
}

class NotificationsContent extends StatefulWidget {
  const NotificationsContent({super.key});

  @override
  State<NotificationsContent> createState() => _NotificationsContentState();
}

class _NotificationsContentState extends State<NotificationsContent> {
  @override
  Widget build(BuildContext context) {
    return ListOfNotifications();
  }
}
