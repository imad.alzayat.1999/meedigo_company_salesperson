import 'package:flutter/material.dart';

import '../../../../core/helpers/app_sizes.dart';
import '../../../../core/theming/app_colors.dart';
import '../../../../core/theming/app_styles.dart';

class ListOfNotifications extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ListView.separated(
      itemBuilder: (context, index) => NotificationCardWidget(),
      padding: EdgeInsets.only(
        left: getWidth(16),
        right: getWidth(16),
        top: getHeight(15),
      ),
      separatorBuilder: (context, index) => Divider(
        color: AppColors.kPrimaryText,
      ),
      shrinkWrap: true,
      itemCount: 10,
    );
  }
}

class NotificationCardWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(
          getRadius(10),
        ),
        color: AppColors.kSecondary,
      ),
      child: Padding(
        padding: EdgeInsets.symmetric(
          horizontal: getWidth(15),
          vertical: getHeight(8),
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  "Notifications title",
                  style: AppStyles.bold(
                    fontSize: 14,
                    textColor: AppColors.kSecondaryText,
                    context: context,
                  ),
                ),
                Text(
                  "3 hours ago",
                  style: AppStyles.medium(
                    fontSize: 10,
                    textColor: AppColors.kPrimaryText,
                    context: context,
                  ),
                ),
              ],
            ),
            verticalSpacing(10),
            SizedBox(
              width: getWidth(150),
              child: Text(
                "Notifications message",
                maxLines: 2,
                overflow: TextOverflow.ellipsis,
                style: AppStyles.regular(
                  fontSize: 10,
                  textColor: AppColors.kSecondaryText,
                  context: context,
                  height: 1.2,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
