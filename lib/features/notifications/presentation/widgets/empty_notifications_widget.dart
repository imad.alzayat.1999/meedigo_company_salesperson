import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';

import '../../../../core/helpers/app_assets.dart';
import '../../../../core/helpers/app_sizes.dart';
import '../../../../core/theming/app_styles.dart';

class EmptyNotificationsWidget extends StatelessWidget {
  const EmptyNotificationsWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Lottie.asset(
          AppJsons.noNotificationsJson,
          width: getWidth(350),
          height: getHeight(350),
        ),
        verticalSpacing(15),
        Text(
          "No notifications found",
          textAlign: TextAlign.center,
          style: AppStyles.medium(
            fontSize: 16,
            context: context,
          ),
        ),
      ],
    );
  }
}
