import 'package:flutter/material.dart';
import 'package:meedigo_company_sales_person/core/widgets/pharmacy_card_widget.dart';

class SearchForPharmacyResultsWidget extends StatelessWidget {
  const SearchForPharmacyResultsWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      physics: const BouncingScrollPhysics(),
      keyboardDismissBehavior: ScrollViewKeyboardDismissBehavior.onDrag,
      itemCount: 10,
      itemBuilder: (context, index) => PharmacyCardWidget(),
    );
  }
}
