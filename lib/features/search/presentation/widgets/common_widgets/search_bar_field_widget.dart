import 'package:flutter/material.dart';
import '../../../../../core/widgets/text_field_widget.dart';

class SearchBarFieldWidget extends StatelessWidget {
  final TextEditingController textEditingController;
  final String hintText;
  final void Function(String)? onChange;

  const SearchBarFieldWidget({
    super.key,
    required this.textEditingController,
    required this.onChange,
    required this.hintText,
  });

  @override
  Widget build(BuildContext context) {
    return TextFieldWidget(
      hintText: hintText,
      textEditingController: textEditingController,
      isSearch: true,
      onChange: onChange,
    );
  }
}
