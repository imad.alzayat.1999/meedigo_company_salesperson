import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_extensions.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_sizes.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_strings.dart';

import '../../../../../core/helpers/app_assets.dart';
import '../../../../../core/theming/app_colors.dart';
import '../../../../../core/theming/app_styles.dart';

class SearchPlaceholderWidget extends StatelessWidget {
  const SearchPlaceholderWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        SizedBox(
          height: getRadius(280),
          width: getRadius(280),
          child: SvgPicture.asset(
            AppIcons.searchIcon,
            color: AppColors.kPrimary,
          ),
        ),
        verticalSpacing(16),
        Text(
          context.translate(
            AppStrings.searchPlaceholderString,
          ),
          textAlign: TextAlign.center,
          style: AppStyles.bold(
            fontSize: 20,
            height: 1.5,
            context: context,
          ),
        ),
      ],
    );
  }
}
