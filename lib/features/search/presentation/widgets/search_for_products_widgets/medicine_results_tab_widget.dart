import 'package:flutter/material.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_extensions.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_strings.dart';
import 'package:meedigo_company_sales_person/core/widgets/medicines/medicine_card_widget.dart';

import '../../../../../core/helpers/app_sizes.dart';
import '../../../../../core/widgets/app_padding_widget.dart';
import '../common_widgets/search_bar_field_widget.dart';
import '../common_widgets/search_placeholder_widget.dart';

class MedicineResultsTabWidget extends StatefulWidget {
  const MedicineResultsTabWidget({super.key});

  @override
  State<MedicineResultsTabWidget> createState() =>
      _MedicineResultsTabWidgetState();
}

class _MedicineResultsTabWidgetState extends State<MedicineResultsTabWidget>
    with AutomaticKeepAliveClientMixin {
  bool _isSearch = false;

  @override
  Widget build(BuildContext context) {
    return AppPaddingWidget(
      child: Column(
        children: [
          SearchBarFieldWidget(
            textEditingController: TextEditingController(),
            hintText: context.translate(
              AppStrings.searchForMedicineHintTextString,
            ),
            onChange: (value) {},
          ),
          verticalSpacing(15),
          Expanded(
            child: !_isSearch
                ? SearchPlaceholderWidget()
                : ListView.builder(
                    physics: const BouncingScrollPhysics(),
                    itemBuilder: (context, index) => MedicineCardWidget(),
                    itemCount: 10,
                  ),
          ),
        ],
      ),
    );
  }

  @override
  // TODO: implement wantKeepAlive
  bool get wantKeepAlive => true;
}
