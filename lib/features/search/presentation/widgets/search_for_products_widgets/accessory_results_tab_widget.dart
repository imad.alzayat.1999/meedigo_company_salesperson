import 'package:flutter/material.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_extensions.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_strings.dart';

import '../../../../../core/helpers/app_sizes.dart';
import '../../../../../core/widgets/accessories/accessory_card_widget.dart';
import '../../../../../core/widgets/app_padding_widget.dart';
import '../common_widgets/search_bar_field_widget.dart';
import '../common_widgets/search_placeholder_widget.dart';

class AccessoryResultsTabWidget extends StatefulWidget {
  const AccessoryResultsTabWidget({super.key});

  @override
  State<AccessoryResultsTabWidget> createState() =>
      _AccessoryResultsTabWidgetState();
}

class _AccessoryResultsTabWidgetState extends State<AccessoryResultsTabWidget>
    with AutomaticKeepAliveClientMixin {
  bool _isSearch = false;
  @override
  Widget build(BuildContext context) {
    return AppPaddingWidget(
      child: Column(
        children: [
          SearchBarFieldWidget(
            textEditingController: TextEditingController(),
            hintText: context.translate(
              AppStrings.searchForAccessoryHintTextString,
            ),
            onChange: (value) {},
          ),
          verticalSpacing(15),
          Expanded(
            child: !_isSearch
                ? SearchPlaceholderWidget()
                : ListView.builder(
                    physics: const BouncingScrollPhysics(),
                    itemBuilder: (context, index) => AccessoryCardWidget(),
                    itemCount: 10,
                  ),
          ),
        ],
      ),
    );
  }

  @override
  // TODO: implement wantKeepAlive
  bool get wantKeepAlive => true;
}
