import 'package:flutter/material.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_extensions.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_sizes.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_strings.dart';
import 'package:meedigo_company_sales_person/core/widgets/app_padding_widget.dart';
import 'package:meedigo_company_sales_person/core/widgets/appbar_widget.dart';
import 'package:meedigo_company_sales_person/features/search/presentation/widgets/common_widgets/search_bar_field_widget.dart';
import 'package:meedigo_company_sales_person/features/search/presentation/widgets/common_widgets/search_placeholder_widget.dart';
import 'package:meedigo_company_sales_person/features/search/presentation/widgets/search_for_pharmacy_widgets/search_for_pharmacy_results_widget.dart';

class SearchForPharmacyScreen extends StatefulWidget {
  const SearchForPharmacyScreen({super.key});

  @override
  State<SearchForPharmacyScreen> createState() =>
      _SearchForPharmacyScreenState();
}

class _SearchForPharmacyScreenState extends State<SearchForPharmacyScreen> {
  bool _isSearch = false;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: appbarWidget(
        title: context.translate(
          AppStrings.searchTitleString,
        ),
        context: context,
      ),
      body: AppPaddingWidget(
        child: Column(
          children: [
            SearchBarFieldWidget(
              textEditingController: TextEditingController(),
              hintText: context.translate(
                AppStrings.searchForPharmacyHintTextString,
              ),
              onChange: (value) {},
            ),
            verticalSpacing(15),
            Expanded(
              child: !_isSearch
                  ? SearchPlaceholderWidget()
                  : SearchForPharmacyResultsWidget(),
            ),
          ],
        ),
      ),
    );
  }
}
