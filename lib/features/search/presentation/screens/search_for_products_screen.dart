import 'package:flutter/material.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_consts.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_extensions.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_strings.dart';
import 'package:meedigo_company_sales_person/core/widgets/appbar_widget.dart';
import 'package:meedigo_company_sales_person/features/search/presentation/widgets/search_for_products_widgets/accessory_results_tab_widget.dart';
import 'package:meedigo_company_sales_person/features/search/presentation/widgets/search_for_products_widgets/medicine_results_tab_widget.dart';
import 'package:meedigo_company_sales_person/features/search/presentation/widgets/search_for_products_widgets/search_for_products_tabbar_widget.dart';

class SearchForProductsScreen extends StatefulWidget {
  const SearchForProductsScreen({super.key});

  @override
  State<SearchForProductsScreen> createState() =>
      _SearchForProductsScreenState();
}

class _SearchForProductsScreenState extends State<SearchForProductsScreen> {
  int _currentIndex = 0;

  /// change tabBar index
  _changeTabBarIndex(int index) {
    setState(() {
      _currentIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: AppConsts.barcodeTabBarItems.length,
      initialIndex: _currentIndex,
      child: Scaffold(
        appBar: appbarWidget(
          title: context.translate(
            AppStrings.searchTitleString,
          ),
          context: context,
          bottom: searchForProductsTabBarWidget(
            context: context,
            onTabFunction: (index) => _changeTabBarIndex(index),
          ),
        ),
        body: TabBarView(
          children: [
            MedicineResultsTabWidget(),
            AccessoryResultsTabWidget(),
          ],
        ),
      ),
    );
  }
}
