import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

import '../../../../../core/theming/app_colors.dart';

class PharmacyLocationMapWidget extends StatefulWidget {
  @override
  State<PharmacyLocationMapWidget> createState() =>
      _PharmacyLocationMapWidgetState();
}

class _PharmacyLocationMapWidgetState extends State<PharmacyLocationMapWidget> {
  GoogleMapController? _googleMapController;
  Set<Polyline> polylines = Set();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: GoogleMap(
        myLocationEnabled: true,
        mapType: MapType.hybrid,
        polylines: polylines,
        initialCameraPosition: CameraPosition(
          target: LatLng(0, 0),
          zoom: 17,
        ),
      ),
    );
  }
}
