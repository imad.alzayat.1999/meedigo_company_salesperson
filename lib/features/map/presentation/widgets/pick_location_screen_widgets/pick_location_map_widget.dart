import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:meedigo_company_sales_person/features/map/presentation/widgets/pick_location_screen_widgets/pick_location_button_widget.dart';

class PickLocationMapWidget extends StatefulWidget {
  @override
  State<PickLocationMapWidget> createState() => _PickLocationMapWidgetState();
}

class _PickLocationMapWidgetState extends State<PickLocationMapWidget> {
  final Completer<GoogleMapController> _controller =
      Completer<GoogleMapController>();

  @override
  Widget build(BuildContext context) {
    return Stack(
      alignment: AlignmentDirectional.bottomStart,
      children: [
        GoogleMap(
          myLocationEnabled: true,
          mapType: MapType.hybrid,
          onMapCreated: (GoogleMapController controller) {
            _controller.complete(controller);
          },
          initialCameraPosition: CameraPosition(
            target: LatLng(0, 0),
            zoom: 17,
          ),
        ),
        PickLocationButtonWidget(),
      ],
    );
  }
}
