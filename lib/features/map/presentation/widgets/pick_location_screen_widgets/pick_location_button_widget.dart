import 'package:flutter/material.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_extensions.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_sizes.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_strings.dart';
import 'package:meedigo_company_sales_person/core/theming/app_colors.dart';
import 'package:meedigo_company_sales_person/core/widgets/app_padding_widget.dart';
import '../../../../../core/widgets/button_widget.dart';

class PickLocationButtonWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return AppPaddingWidget(
      bottom: 26,
      child: SizedBox(
        width: getWidth(250),
        child: ButtonWidget(
          btnColor: AppColors.kPrimary,
          btnTextColor: AppColors.kWhite,
          onPressFunction: () {},
          btnText: context.translate(
            AppStrings.pickLocationBtnString,
          ),
        ),
      ),
    );
  }
}
