import 'package:flutter/material.dart';
import 'package:meedigo_company_sales_person/features/map/presentation/widgets/pharmacy_location_screen_widgets/pharmacy_location_map_widget.dart';

class PharmacyLocationScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: PharmacyLocationMapWidget(),
    );
  }
}
