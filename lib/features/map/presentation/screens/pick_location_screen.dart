import 'package:flutter/material.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_extensions.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_strings.dart';
import 'package:meedigo_company_sales_person/core/widgets/appbar_widget.dart';
import '../widgets/pick_location_screen_widgets/pick_location_map_widget.dart';

class PickLocationScreen extends StatelessWidget {
  const PickLocationScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: appbarWidget(
        title: context.translate(
          AppStrings.pickLocationTitleString,
        ),
        context: context,
      ),
      body: PickLocationMapWidget(),
    );
  }
}
