import 'package:flutter/material.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_extensions.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_strings.dart';
import '../../../../../core/theming/app_styles.dart';

class UpdateVersionDialogTitleWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Text(
      context.translate(
        AppStrings.updateVersionDialogTitleString,
      ),
      style: AppStyles.bold(
        fontSize: 16,
        context: context,
      ),
    );
  }
}
