import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_extensions.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_strings.dart';
import 'package:meedigo_company_sales_person/features/versioning/domain/entities/versioning_response.dart';
import 'package:meedigo_company_sales_person/features/versioning/presentation/bloc/versioning_bloc.dart';

import '../../../../../core/helpers/app_sizes.dart';
import '../../../../../core/theming/app_colors.dart';
import '../../../../../core/widgets/button_widget.dart';

class UpdateVersionDialogActionsWidget extends StatelessWidget {
  final VersioningData versioningData;

  const UpdateVersionDialogActionsWidget({
    super.key,
    required this.versioningData,
  });

  @override
  Widget build(BuildContext context) {
    if (versioningData.directUrl.isEmpty) {
      return Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            UpdateButtonWidget(
              onPressFunction: () => context.read<VersioningBloc>()
                ..routeToPlayStoreOrDirectDownloadUrl(
                  downloadRouteUrl: versioningData.platformUrl,
                ),
              btnText: context.read<VersioningBloc>().getDownloadString(),
            ),
            verticalSpacing(10),
            UpdateButtonWidget(
              onPressFunction: () => context.pop(),
              btnText: AppStrings.cancelString,
              btnColor: AppColors.kDisabled,
            ),
          ],
        ),
      );
    } else if (versioningData.platformUrl.isEmpty) {
      return Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            UpdateButtonWidget(
              onPressFunction: () => context.read<VersioningBloc>()
                ..routeToPlayStoreOrDirectDownloadUrl(
                  downloadRouteUrl: versioningData.directUrl,
                ),
              btnText: AppStrings.updateVersionDialogDirectDownloadBtnString,
            ),
            verticalSpacing(10),
            UpdateButtonWidget(
              onPressFunction: () => context.pop(),
              btnText: AppStrings.cancelString,
              btnColor: AppColors.kDisabled,
            ),
          ],
        ),
      );
    } else {
      return Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            UpdateButtonWidget(
              onPressFunction: () => context.read<VersioningBloc>()
                ..routeToPlayStoreOrDirectDownloadUrl(
                  downloadRouteUrl: versioningData.platformUrl,
                ),
              btnText: context.read<VersioningBloc>().getDownloadString(),
            ),
            verticalSpacing(10),
            UpdateButtonWidget(
              onPressFunction: () => context.read<VersioningBloc>()
                ..routeToPlayStoreOrDirectDownloadUrl(
                  downloadRouteUrl: versioningData.directUrl,
                ),
              btnText: AppStrings.updateVersionDialogDirectDownloadBtnString,
            ),
            verticalSpacing(10),
            UpdateButtonWidget(
              onPressFunction: () => context.pop(),
              btnText: AppStrings.cancelString,
              btnColor: AppColors.kDisabled,
            ),
          ],
        ),
      );
    }
  }
}

class UpdateButtonWidget extends StatelessWidget {
  final void Function()? onPressFunction;
  final String btnText;
  final Color btnColor;
  final double btnFontSize;

  const UpdateButtonWidget({
    Key? key,
    required this.onPressFunction,
    required this.btnText,
    this.btnFontSize = 14,
    this.btnColor = AppColors.kPrimary,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ButtonWidget(
      btnHeight: 40,
      btnWidth: 250,
      borderRadius: 10,
      btnColor: btnColor,
      btnFontSize: btnFontSize,
      onPressFunction: onPressFunction,
      btnText: context.translate(btnText),
    );
  }
}
