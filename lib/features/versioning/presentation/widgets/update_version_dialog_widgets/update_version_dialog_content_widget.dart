import 'package:flutter/material.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_extensions.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_strings.dart';
import '../../../../../core/theming/app_colors.dart';
import '../../../../../core/theming/app_styles.dart';

class UpdateVersionDialogContentWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Text(
      context.translate(
        AppStrings.updateVersionDialogContentString,
        args: {
          "number_of_days": 5,
        },
      ),
      textAlign: TextAlign.center,
      style: AppStyles.medium(
        fontSize: 14,
        context: context,
        textColor: AppColors.kSecondaryText,
        height: 1.5,
      ),
    );
  }
}
