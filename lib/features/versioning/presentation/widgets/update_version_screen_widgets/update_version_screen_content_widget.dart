import 'package:flutter/material.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_extensions.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_strings.dart';
import '../../../../../core/theming/app_colors.dart';
import '../../../../../core/theming/app_styles.dart';

class UpdateVersionScreenContentWidget extends StatelessWidget {
  const UpdateVersionScreenContentWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Text(
      context.translate(
        AppStrings.updateVersionContentString,
      ),
      textAlign: TextAlign.center,
      style: AppStyles.semiBold(
        fontSize: 16,
        height: 1.5,
        context: context,
        textColor: AppColors.kSecondaryText,
      ),
    );
  }
}
