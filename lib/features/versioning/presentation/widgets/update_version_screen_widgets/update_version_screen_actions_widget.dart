import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_extensions.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_strings.dart';
import 'package:meedigo_company_sales_person/core/theming/app_colors.dart';
import 'package:meedigo_company_sales_person/features/versioning/domain/entities/versioning_response.dart';
import 'package:meedigo_company_sales_person/features/versioning/presentation/bloc/versioning_bloc.dart';
import '../../../../../core/helpers/app_sizes.dart';
import '../../../../../core/theming/app_styles.dart';
import '../../../../../core/widgets/button_widget.dart';

class UpdateVersionScreenActionsWidget extends StatelessWidget {
  final VersioningData versioningData;

  const UpdateVersionScreenActionsWidget({
    super.key,
    required this.versioningData,
  });

  @override
  Widget build(BuildContext context) {
    if (versioningData.platformUrl.isEmpty) {
      return ButtonWidget(
        onPressFunction: () => context.read<VersioningBloc>()
          ..routeToPlayStoreOrDirectDownloadUrl(
            downloadRouteUrl: versioningData.directUrl,
          ),
        btnText: context.translate(
          AppStrings.updateVersionDirectDownloadBtnString,
        ),
      );
    } else if (versioningData.directUrl.isEmpty) {
      return ButtonWidget(
        onPressFunction: () => context.read<VersioningBloc>()
          ..routeToPlayStoreOrDirectDownloadUrl(
            downloadRouteUrl: versioningData.platformUrl,
          ),
        btnText: context.translate(
          context.read<VersioningBloc>().getDownloadString(),
        ),
      );
    }else{
      return Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          ButtonWidget(
            onPressFunction: () => context.read<VersioningBloc>()
              ..routeToPlayStoreOrDirectDownloadUrl(
                downloadRouteUrl: versioningData.platformUrl,
              ),
            btnText: context.translate(
              context.read<VersioningBloc>().getDownloadString(),
            ),
          ),
          Padding(
            padding: EdgeInsets.symmetric(
              vertical: getHeight(10),
            ),
            child: Text(
              context.translate(
                AppStrings.orString,
              ),
              style: AppStyles.extraBold(
                fontSize: 16,
                textColor: AppColors.kSecondaryText,
                context: context,
              ),
            ),
          ),
          ButtonWidget(
            onPressFunction: () => context.read<VersioningBloc>()
              ..routeToPlayStoreOrDirectDownloadUrl(
                downloadRouteUrl: versioningData.directUrl,
              ),
            btnText: context.translate(
              AppStrings.updateVersionDirectDownloadBtnString,
            ),
          ),
        ],
      );
    }
  }
}
