import 'package:flutter/material.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_sizes.dart';

import '../../../../../core/helpers/app_assets.dart';

class UpdateVersionScreenLogoWidget extends StatelessWidget {
  const UpdateVersionScreenLogoWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: getRadius(200),
      height: getRadius(200),
      child: ClipRRect(
        borderRadius: BorderRadius.circular(
          getRadius(20),
        ),
        child: Image.asset(
          AppImages.meedigoLogoImage,
        ),
      ),
    );
  }
}
