import 'package:flutter/material.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_extensions.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_strings.dart';
import '../../../../../core/theming/app_styles.dart';

class UpdateVersionScreenTitleWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Text(
      context.translate(
        AppStrings.updateVersionTitleString,
      ),
      style: AppStyles.bold(
        fontSize: 24,
        context: context,
      ),
    );
  }
}
