part of 'versioning_bloc.dart';

@freezed
class VersioningState with _$VersioningState {
  const factory VersioningState.initial() = _Initial;

  /// update app version event states
  const factory VersioningState.checkUpdateLoading() = _CheckUpdateLoading;
  const factory VersioningState.checkUpdateDone(
    VersioningResponse versioningResponse,
  ) = _CheckUpdateDone;
  const factory VersioningState.checkUpdateFailed(
    Failure failure,
  ) = _CheckUpdateFailed;
}
