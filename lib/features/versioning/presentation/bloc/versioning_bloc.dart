import 'dart:async';
import 'dart:io';

import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:geolocator/geolocator.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_extensions.dart';
import 'package:meedigo_company_sales_person/core/networking/api_query_parameters.dart';
import 'package:meedigo_company_sales_person/core/routing/route_parameters.dart';
import 'package:meedigo_company_sales_person/features/auth/domain/entities/login.dart';
import 'package:meedigo_company_sales_person/features/versioning/domain/entities/versioning_response.dart';
import 'package:meedigo_company_sales_person/features/versioning/domain/usecases/update_app_usecase.dart';
import 'package:package_info_plus/package_info_plus.dart';

import '../../../../core/helpers/app_consts.dart';
import '../../../../core/helpers/app_functions.dart';
import '../../../../core/helpers/app_preferences.dart';
import '../../../../core/helpers/app_strings.dart';
import '../../../../core/networking/api_failure.dart';
import '../../../../core/routing/app_routes.dart';
import '../screens/update_version_dialog.dart';

part 'versioning_event.dart';
part 'versioning_state.dart';
part 'versioning_bloc.freezed.dart';

class VersioningBloc extends Bloc<VersioningEvent, VersioningState> {
  final UpdateAppUseCase updateAppUseCase;

  Timer? _timer;
  VersioningBloc({
    required this.updateAppUseCase,
  }) : super(VersioningState.initial()) {
    on<VersioningEvent>((event, emit) async {
      await event.when(
        checkAppUpdate: (parameters) async {
          emit(VersioningState.checkUpdateLoading());
          final failureOrCheckVersion = await updateAppUseCase(
            parameters,
          );
          failureOrCheckVersion.fold(
            (l) => emit(
              VersioningState.checkUpdateFailed(l),
            ),
            (r) => emit(
              VersioningState.checkUpdateDone(r),
            ),
          );
        },
      );
    });
  }

  /// function to get the next page by on boarding status ,
  /// here if the user is passed the on boarding pages
  /// then the login page will be displayed for the user after splash screen
  /// else after splash screen the on boarding pages will be displayed for the user
  getTheNextScreenAfterOnBoarding({
    required List products,
    required BuildContext context,
  }) {
    bool _isOnBoarding = AppPreferences.getOnBoarding();
    bool _isAuthenticated = AppPreferences.isUserAuthenticated();
    bool _isCheckIn = AppPreferences.getCheckIn();
    LoginData login = AppPreferences.getUserInfo();
    if (_isAuthenticated == false) {
      if (_isOnBoarding == true) {
        context.pushNamedAndRemoveUntil(AppRoutes.sendSmsRoute);
      } else {
        context.pushNamedAndRemoveUntil(AppRoutes.onBoardingRoute);
      }
    } else {
      if (_isCheckIn == false) {
        context.pushNamedAndRemoveUntil(AppRoutes.checkInRoute);
      } else {
        if (login.active == 0) {
          context.pushNamedAndRemoveUntil(AppRoutes.accountBlockedRoute);
        } else {
          if (products.isEmpty) {
            context.pushNamedAndRemoveUntil(AppRoutes.mainRoute);
          } else {
            // final product = products[0];
            // context.pushNamedAndRemoveUntil(
            //   AppRoutes.pharmacyContentRoute,
            //   arguments: PharmacyContentRouteParameters(
            //     marketId: product.marketId,
            //     pharmacyName: product.marketName,
            //   ),
            // );
          }
        }
      }
    }
  }

  /// function to check if the server version of app
  /// equals the app version then call get user location
  checkAndRouteToNextPage({
    required BuildContext context,
    required VersioningData versioningData,
    required String appVersion,
    required List products,
  }) {
    _timer = Timer.periodic(
      Duration(
        seconds: AppConsts.splashScreenDuration,
      ),
      (timer) {
        handleVersionProcessing(
          context: context,
          versioningData: versioningData,
          products: products,
        );
      },
    );
  }

  /// function to call check if there is
  /// available update for the application event
  checkAvailableUpdate() async {
    final packageInfo = await PackageInfo.fromPlatform();
    UpdateAppRequestQueryParameters queryParameters =
        UpdateAppRequestQueryParameters(
      platform: AppFunctions.getCurrentPlatform(),
      version: packageInfo.version,
    );
    add(VersioningEvent.checkAppUpdate(queryParameters));
  }

  /// handle version processing
  handleVersionProcessing({
    required BuildContext context,
    required VersioningData versioningData,
    required List products,
  }) {
    if (versioningData.hasUpdate) {
      if (versioningData.required == 1) {
        context.pushNamedAndRemoveUntil(
          AppRoutes.updateVersionRoute,
          arguments: UpdateAppScreenRouteParameters(
            versioningData: versioningData,
          ),
        );
      } else {
        WidgetsBinding.instance.addPostFrameCallback(
          (callback) {
            showDialog(
              context: context,
              barrierDismissible: false,
              builder: (context) => VersionCheckDialog(
                parameters: UpdateAppDialogRouteParameters(
                  versioningData: versioningData,
                ),
              ),
            );
          },
        );
        getUserLocation(products: products, context: context);
      }
    } else {
      getUserLocation(products: products, context: context);
    }
  }

  /// function to handle failure state of check app version
  handleFailedState({
    required Failure failure,
    required BuildContext context,
    required List products,
  }) {
    _timer = Timer.periodic(
      Duration(
        seconds: AppConsts.splashScreenDuration,
      ),
      (timer) {
        if (failure.errorMessage == AppStrings.noInternetErrorString) {
          final versionInfo = AppPreferences.getVersionInfo();
          if (versionInfo == null) {
            getUserLocation(context: context, products: products);
          } else {
            handleVersionProcessing(
              context: context,
              versioningData: versionInfo.versioningData!,
              products: products,
            );
          }
        } else {
          exitTheApplication();
        }
      },
    );
  }

  ///  check if location permission enabled
  ///  then route to enable location page
  ///  else route to on boarding page
  getUserLocation({
    required List products,
    required BuildContext context,
  }) async {
    LocationPermission permission = await Geolocator.checkPermission();
    if (permission == LocationPermission.always ||
        permission == LocationPermission.whileInUse) {
      getTheNextScreenAfterOnBoarding(
        products: products,
        context: context,
      );
    } else {
      context.pushNamedAndRemoveUntil(
        AppRoutes.locationPermissionRoute,
      );
    }
  }

  /// function to exit the application when there is no internet connection
  static exitTheApplication() {
    if (Platform.isAndroid) {
      SystemNavigator.pop();
    } else if (Platform.isIOS) {
      exit(0);
    }
  }

  String versionCheckDialogContent({
    required BuildContext context,
    required String dateRange,
  }) {
    DateTime range = dateRange.toDate();
    DateTime currentDate = DateTime.now().format().toDate();
    Duration difference = range.difference(currentDate);
    return context.translate(
      AppStrings.updateVersionDialogContentString,
      args: {
        "number_of_days": difference.inDays,
      },
    );
  }

  String getDownloadString() {
    if (Platform.isAndroid) {
      return AppStrings.updateVersionGooglePlayBtnString;
    } else {
      return AppStrings.updateVersionAppStoreBtnString;
    }
  }

  /// function to route to play store url
  /// or to the direct download url
  routeToPlayStoreOrDirectDownloadUrl({
    required String downloadRouteUrl,
  }) {
    AppPreferences.setLastCleared(value: false);
    AppFunctions.launchExternalUrl(downloadRouteUrl);
  }

  @override
  Future<void> close() {
    _timer!.cancel();
    return super.close();
  }
}
