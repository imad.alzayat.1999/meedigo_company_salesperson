// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'versioning_bloc.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

/// @nodoc
mixin _$VersioningEvent {
  UpdateAppRequestQueryParameters get queryParameters =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(UpdateAppRequestQueryParameters queryParameters)
        checkAppUpdate,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(UpdateAppRequestQueryParameters queryParameters)?
        checkAppUpdate,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(UpdateAppRequestQueryParameters queryParameters)?
        checkAppUpdate,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_CheckAppUpdate value) checkAppUpdate,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_CheckAppUpdate value)? checkAppUpdate,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_CheckAppUpdate value)? checkAppUpdate,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $VersioningEventCopyWith<VersioningEvent> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $VersioningEventCopyWith<$Res> {
  factory $VersioningEventCopyWith(
          VersioningEvent value, $Res Function(VersioningEvent) then) =
      _$VersioningEventCopyWithImpl<$Res, VersioningEvent>;
  @useResult
  $Res call({UpdateAppRequestQueryParameters queryParameters});
}

/// @nodoc
class _$VersioningEventCopyWithImpl<$Res, $Val extends VersioningEvent>
    implements $VersioningEventCopyWith<$Res> {
  _$VersioningEventCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? queryParameters = null,
  }) {
    return _then(_value.copyWith(
      queryParameters: null == queryParameters
          ? _value.queryParameters
          : queryParameters // ignore: cast_nullable_to_non_nullable
              as UpdateAppRequestQueryParameters,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$CheckAppUpdateImplCopyWith<$Res>
    implements $VersioningEventCopyWith<$Res> {
  factory _$$CheckAppUpdateImplCopyWith(_$CheckAppUpdateImpl value,
          $Res Function(_$CheckAppUpdateImpl) then) =
      __$$CheckAppUpdateImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({UpdateAppRequestQueryParameters queryParameters});
}

/// @nodoc
class __$$CheckAppUpdateImplCopyWithImpl<$Res>
    extends _$VersioningEventCopyWithImpl<$Res, _$CheckAppUpdateImpl>
    implements _$$CheckAppUpdateImplCopyWith<$Res> {
  __$$CheckAppUpdateImplCopyWithImpl(
      _$CheckAppUpdateImpl _value, $Res Function(_$CheckAppUpdateImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? queryParameters = null,
  }) {
    return _then(_$CheckAppUpdateImpl(
      null == queryParameters
          ? _value.queryParameters
          : queryParameters // ignore: cast_nullable_to_non_nullable
              as UpdateAppRequestQueryParameters,
    ));
  }
}

/// @nodoc

class _$CheckAppUpdateImpl implements _CheckAppUpdate {
  const _$CheckAppUpdateImpl(this.queryParameters);

  @override
  final UpdateAppRequestQueryParameters queryParameters;

  @override
  String toString() {
    return 'VersioningEvent.checkAppUpdate(queryParameters: $queryParameters)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$CheckAppUpdateImpl &&
            (identical(other.queryParameters, queryParameters) ||
                other.queryParameters == queryParameters));
  }

  @override
  int get hashCode => Object.hash(runtimeType, queryParameters);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$CheckAppUpdateImplCopyWith<_$CheckAppUpdateImpl> get copyWith =>
      __$$CheckAppUpdateImplCopyWithImpl<_$CheckAppUpdateImpl>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(UpdateAppRequestQueryParameters queryParameters)
        checkAppUpdate,
  }) {
    return checkAppUpdate(queryParameters);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(UpdateAppRequestQueryParameters queryParameters)?
        checkAppUpdate,
  }) {
    return checkAppUpdate?.call(queryParameters);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(UpdateAppRequestQueryParameters queryParameters)?
        checkAppUpdate,
    required TResult orElse(),
  }) {
    if (checkAppUpdate != null) {
      return checkAppUpdate(queryParameters);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_CheckAppUpdate value) checkAppUpdate,
  }) {
    return checkAppUpdate(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_CheckAppUpdate value)? checkAppUpdate,
  }) {
    return checkAppUpdate?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_CheckAppUpdate value)? checkAppUpdate,
    required TResult orElse(),
  }) {
    if (checkAppUpdate != null) {
      return checkAppUpdate(this);
    }
    return orElse();
  }
}

abstract class _CheckAppUpdate implements VersioningEvent {
  const factory _CheckAppUpdate(
          final UpdateAppRequestQueryParameters queryParameters) =
      _$CheckAppUpdateImpl;

  @override
  UpdateAppRequestQueryParameters get queryParameters;
  @override
  @JsonKey(ignore: true)
  _$$CheckAppUpdateImplCopyWith<_$CheckAppUpdateImpl> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
mixin _$VersioningState {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() checkUpdateLoading,
    required TResult Function(VersioningResponse versioningResponse)
        checkUpdateDone,
    required TResult Function(Failure failure) checkUpdateFailed,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? checkUpdateLoading,
    TResult? Function(VersioningResponse versioningResponse)? checkUpdateDone,
    TResult? Function(Failure failure)? checkUpdateFailed,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? checkUpdateLoading,
    TResult Function(VersioningResponse versioningResponse)? checkUpdateDone,
    TResult Function(Failure failure)? checkUpdateFailed,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_CheckUpdateLoading value) checkUpdateLoading,
    required TResult Function(_CheckUpdateDone value) checkUpdateDone,
    required TResult Function(_CheckUpdateFailed value) checkUpdateFailed,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_CheckUpdateLoading value)? checkUpdateLoading,
    TResult? Function(_CheckUpdateDone value)? checkUpdateDone,
    TResult? Function(_CheckUpdateFailed value)? checkUpdateFailed,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_CheckUpdateLoading value)? checkUpdateLoading,
    TResult Function(_CheckUpdateDone value)? checkUpdateDone,
    TResult Function(_CheckUpdateFailed value)? checkUpdateFailed,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $VersioningStateCopyWith<$Res> {
  factory $VersioningStateCopyWith(
          VersioningState value, $Res Function(VersioningState) then) =
      _$VersioningStateCopyWithImpl<$Res, VersioningState>;
}

/// @nodoc
class _$VersioningStateCopyWithImpl<$Res, $Val extends VersioningState>
    implements $VersioningStateCopyWith<$Res> {
  _$VersioningStateCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$InitialImplCopyWith<$Res> {
  factory _$$InitialImplCopyWith(
          _$InitialImpl value, $Res Function(_$InitialImpl) then) =
      __$$InitialImplCopyWithImpl<$Res>;
}

/// @nodoc
class __$$InitialImplCopyWithImpl<$Res>
    extends _$VersioningStateCopyWithImpl<$Res, _$InitialImpl>
    implements _$$InitialImplCopyWith<$Res> {
  __$$InitialImplCopyWithImpl(
      _$InitialImpl _value, $Res Function(_$InitialImpl) _then)
      : super(_value, _then);
}

/// @nodoc

class _$InitialImpl implements _Initial {
  const _$InitialImpl();

  @override
  String toString() {
    return 'VersioningState.initial()';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$InitialImpl);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() checkUpdateLoading,
    required TResult Function(VersioningResponse versioningResponse)
        checkUpdateDone,
    required TResult Function(Failure failure) checkUpdateFailed,
  }) {
    return initial();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? checkUpdateLoading,
    TResult? Function(VersioningResponse versioningResponse)? checkUpdateDone,
    TResult? Function(Failure failure)? checkUpdateFailed,
  }) {
    return initial?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? checkUpdateLoading,
    TResult Function(VersioningResponse versioningResponse)? checkUpdateDone,
    TResult Function(Failure failure)? checkUpdateFailed,
    required TResult orElse(),
  }) {
    if (initial != null) {
      return initial();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_CheckUpdateLoading value) checkUpdateLoading,
    required TResult Function(_CheckUpdateDone value) checkUpdateDone,
    required TResult Function(_CheckUpdateFailed value) checkUpdateFailed,
  }) {
    return initial(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_CheckUpdateLoading value)? checkUpdateLoading,
    TResult? Function(_CheckUpdateDone value)? checkUpdateDone,
    TResult? Function(_CheckUpdateFailed value)? checkUpdateFailed,
  }) {
    return initial?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_CheckUpdateLoading value)? checkUpdateLoading,
    TResult Function(_CheckUpdateDone value)? checkUpdateDone,
    TResult Function(_CheckUpdateFailed value)? checkUpdateFailed,
    required TResult orElse(),
  }) {
    if (initial != null) {
      return initial(this);
    }
    return orElse();
  }
}

abstract class _Initial implements VersioningState {
  const factory _Initial() = _$InitialImpl;
}

/// @nodoc
abstract class _$$CheckUpdateLoadingImplCopyWith<$Res> {
  factory _$$CheckUpdateLoadingImplCopyWith(_$CheckUpdateLoadingImpl value,
          $Res Function(_$CheckUpdateLoadingImpl) then) =
      __$$CheckUpdateLoadingImplCopyWithImpl<$Res>;
}

/// @nodoc
class __$$CheckUpdateLoadingImplCopyWithImpl<$Res>
    extends _$VersioningStateCopyWithImpl<$Res, _$CheckUpdateLoadingImpl>
    implements _$$CheckUpdateLoadingImplCopyWith<$Res> {
  __$$CheckUpdateLoadingImplCopyWithImpl(_$CheckUpdateLoadingImpl _value,
      $Res Function(_$CheckUpdateLoadingImpl) _then)
      : super(_value, _then);
}

/// @nodoc

class _$CheckUpdateLoadingImpl implements _CheckUpdateLoading {
  const _$CheckUpdateLoadingImpl();

  @override
  String toString() {
    return 'VersioningState.checkUpdateLoading()';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$CheckUpdateLoadingImpl);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() checkUpdateLoading,
    required TResult Function(VersioningResponse versioningResponse)
        checkUpdateDone,
    required TResult Function(Failure failure) checkUpdateFailed,
  }) {
    return checkUpdateLoading();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? checkUpdateLoading,
    TResult? Function(VersioningResponse versioningResponse)? checkUpdateDone,
    TResult? Function(Failure failure)? checkUpdateFailed,
  }) {
    return checkUpdateLoading?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? checkUpdateLoading,
    TResult Function(VersioningResponse versioningResponse)? checkUpdateDone,
    TResult Function(Failure failure)? checkUpdateFailed,
    required TResult orElse(),
  }) {
    if (checkUpdateLoading != null) {
      return checkUpdateLoading();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_CheckUpdateLoading value) checkUpdateLoading,
    required TResult Function(_CheckUpdateDone value) checkUpdateDone,
    required TResult Function(_CheckUpdateFailed value) checkUpdateFailed,
  }) {
    return checkUpdateLoading(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_CheckUpdateLoading value)? checkUpdateLoading,
    TResult? Function(_CheckUpdateDone value)? checkUpdateDone,
    TResult? Function(_CheckUpdateFailed value)? checkUpdateFailed,
  }) {
    return checkUpdateLoading?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_CheckUpdateLoading value)? checkUpdateLoading,
    TResult Function(_CheckUpdateDone value)? checkUpdateDone,
    TResult Function(_CheckUpdateFailed value)? checkUpdateFailed,
    required TResult orElse(),
  }) {
    if (checkUpdateLoading != null) {
      return checkUpdateLoading(this);
    }
    return orElse();
  }
}

abstract class _CheckUpdateLoading implements VersioningState {
  const factory _CheckUpdateLoading() = _$CheckUpdateLoadingImpl;
}

/// @nodoc
abstract class _$$CheckUpdateDoneImplCopyWith<$Res> {
  factory _$$CheckUpdateDoneImplCopyWith(_$CheckUpdateDoneImpl value,
          $Res Function(_$CheckUpdateDoneImpl) then) =
      __$$CheckUpdateDoneImplCopyWithImpl<$Res>;
  @useResult
  $Res call({VersioningResponse versioningResponse});
}

/// @nodoc
class __$$CheckUpdateDoneImplCopyWithImpl<$Res>
    extends _$VersioningStateCopyWithImpl<$Res, _$CheckUpdateDoneImpl>
    implements _$$CheckUpdateDoneImplCopyWith<$Res> {
  __$$CheckUpdateDoneImplCopyWithImpl(
      _$CheckUpdateDoneImpl _value, $Res Function(_$CheckUpdateDoneImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? versioningResponse = null,
  }) {
    return _then(_$CheckUpdateDoneImpl(
      null == versioningResponse
          ? _value.versioningResponse
          : versioningResponse // ignore: cast_nullable_to_non_nullable
              as VersioningResponse,
    ));
  }
}

/// @nodoc

class _$CheckUpdateDoneImpl implements _CheckUpdateDone {
  const _$CheckUpdateDoneImpl(this.versioningResponse);

  @override
  final VersioningResponse versioningResponse;

  @override
  String toString() {
    return 'VersioningState.checkUpdateDone(versioningResponse: $versioningResponse)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$CheckUpdateDoneImpl &&
            (identical(other.versioningResponse, versioningResponse) ||
                other.versioningResponse == versioningResponse));
  }

  @override
  int get hashCode => Object.hash(runtimeType, versioningResponse);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$CheckUpdateDoneImplCopyWith<_$CheckUpdateDoneImpl> get copyWith =>
      __$$CheckUpdateDoneImplCopyWithImpl<_$CheckUpdateDoneImpl>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() checkUpdateLoading,
    required TResult Function(VersioningResponse versioningResponse)
        checkUpdateDone,
    required TResult Function(Failure failure) checkUpdateFailed,
  }) {
    return checkUpdateDone(versioningResponse);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? checkUpdateLoading,
    TResult? Function(VersioningResponse versioningResponse)? checkUpdateDone,
    TResult? Function(Failure failure)? checkUpdateFailed,
  }) {
    return checkUpdateDone?.call(versioningResponse);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? checkUpdateLoading,
    TResult Function(VersioningResponse versioningResponse)? checkUpdateDone,
    TResult Function(Failure failure)? checkUpdateFailed,
    required TResult orElse(),
  }) {
    if (checkUpdateDone != null) {
      return checkUpdateDone(versioningResponse);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_CheckUpdateLoading value) checkUpdateLoading,
    required TResult Function(_CheckUpdateDone value) checkUpdateDone,
    required TResult Function(_CheckUpdateFailed value) checkUpdateFailed,
  }) {
    return checkUpdateDone(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_CheckUpdateLoading value)? checkUpdateLoading,
    TResult? Function(_CheckUpdateDone value)? checkUpdateDone,
    TResult? Function(_CheckUpdateFailed value)? checkUpdateFailed,
  }) {
    return checkUpdateDone?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_CheckUpdateLoading value)? checkUpdateLoading,
    TResult Function(_CheckUpdateDone value)? checkUpdateDone,
    TResult Function(_CheckUpdateFailed value)? checkUpdateFailed,
    required TResult orElse(),
  }) {
    if (checkUpdateDone != null) {
      return checkUpdateDone(this);
    }
    return orElse();
  }
}

abstract class _CheckUpdateDone implements VersioningState {
  const factory _CheckUpdateDone(final VersioningResponse versioningResponse) =
      _$CheckUpdateDoneImpl;

  VersioningResponse get versioningResponse;
  @JsonKey(ignore: true)
  _$$CheckUpdateDoneImplCopyWith<_$CheckUpdateDoneImpl> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$CheckUpdateFailedImplCopyWith<$Res> {
  factory _$$CheckUpdateFailedImplCopyWith(_$CheckUpdateFailedImpl value,
          $Res Function(_$CheckUpdateFailedImpl) then) =
      __$$CheckUpdateFailedImplCopyWithImpl<$Res>;
  @useResult
  $Res call({Failure failure});
}

/// @nodoc
class __$$CheckUpdateFailedImplCopyWithImpl<$Res>
    extends _$VersioningStateCopyWithImpl<$Res, _$CheckUpdateFailedImpl>
    implements _$$CheckUpdateFailedImplCopyWith<$Res> {
  __$$CheckUpdateFailedImplCopyWithImpl(_$CheckUpdateFailedImpl _value,
      $Res Function(_$CheckUpdateFailedImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? failure = null,
  }) {
    return _then(_$CheckUpdateFailedImpl(
      null == failure
          ? _value.failure
          : failure // ignore: cast_nullable_to_non_nullable
              as Failure,
    ));
  }
}

/// @nodoc

class _$CheckUpdateFailedImpl implements _CheckUpdateFailed {
  const _$CheckUpdateFailedImpl(this.failure);

  @override
  final Failure failure;

  @override
  String toString() {
    return 'VersioningState.checkUpdateFailed(failure: $failure)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$CheckUpdateFailedImpl &&
            (identical(other.failure, failure) || other.failure == failure));
  }

  @override
  int get hashCode => Object.hash(runtimeType, failure);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$CheckUpdateFailedImplCopyWith<_$CheckUpdateFailedImpl> get copyWith =>
      __$$CheckUpdateFailedImplCopyWithImpl<_$CheckUpdateFailedImpl>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() checkUpdateLoading,
    required TResult Function(VersioningResponse versioningResponse)
        checkUpdateDone,
    required TResult Function(Failure failure) checkUpdateFailed,
  }) {
    return checkUpdateFailed(failure);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? checkUpdateLoading,
    TResult? Function(VersioningResponse versioningResponse)? checkUpdateDone,
    TResult? Function(Failure failure)? checkUpdateFailed,
  }) {
    return checkUpdateFailed?.call(failure);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? checkUpdateLoading,
    TResult Function(VersioningResponse versioningResponse)? checkUpdateDone,
    TResult Function(Failure failure)? checkUpdateFailed,
    required TResult orElse(),
  }) {
    if (checkUpdateFailed != null) {
      return checkUpdateFailed(failure);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_CheckUpdateLoading value) checkUpdateLoading,
    required TResult Function(_CheckUpdateDone value) checkUpdateDone,
    required TResult Function(_CheckUpdateFailed value) checkUpdateFailed,
  }) {
    return checkUpdateFailed(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_CheckUpdateLoading value)? checkUpdateLoading,
    TResult? Function(_CheckUpdateDone value)? checkUpdateDone,
    TResult? Function(_CheckUpdateFailed value)? checkUpdateFailed,
  }) {
    return checkUpdateFailed?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_CheckUpdateLoading value)? checkUpdateLoading,
    TResult Function(_CheckUpdateDone value)? checkUpdateDone,
    TResult Function(_CheckUpdateFailed value)? checkUpdateFailed,
    required TResult orElse(),
  }) {
    if (checkUpdateFailed != null) {
      return checkUpdateFailed(this);
    }
    return orElse();
  }
}

abstract class _CheckUpdateFailed implements VersioningState {
  const factory _CheckUpdateFailed(final Failure failure) =
      _$CheckUpdateFailedImpl;

  Failure get failure;
  @JsonKey(ignore: true)
  _$$CheckUpdateFailedImplCopyWith<_$CheckUpdateFailedImpl> get copyWith =>
      throw _privateConstructorUsedError;
}
