import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:meedigo_company_sales_person/core/routing/route_parameters.dart';
import 'package:meedigo_company_sales_person/features/versioning/presentation/bloc/versioning_bloc.dart';
import 'package:meedigo_company_sales_person/features/versioning/presentation/widgets/update_version_dialog_widgets/update_version_dialog_actions_widget.dart';
import '../../../../core/di/dependency_injection.dart';
import '../../../../core/helpers/app_sizes.dart';
import '../widgets/update_version_dialog_widgets/update_version_dialog_content_widget.dart';
import '../widgets/update_version_dialog_widgets/update_version_dialog_title_widget.dart';

class VersionCheckDialog extends StatelessWidget {
  final UpdateAppDialogRouteParameters parameters;

  const VersionCheckDialog({
    super.key,
    required this.parameters,
  });

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => getIt<VersioningBloc>(),
      child: Dialog(
        child: SizedBox(
          height: getHeight(280),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              UpdateVersionDialogTitleWidget(),
              verticalSpacing(27),
              UpdateVersionDialogContentWidget(),
              verticalSpacing(15),
              UpdateVersionDialogActionsWidget(
                versioningData: parameters.versioningData,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
