import 'package:flutter/material.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_sizes.dart';
import 'package:meedigo_company_sales_person/core/routing/route_parameters.dart';
import 'package:meedigo_company_sales_person/core/widgets/app_padding_widget.dart';
import 'package:meedigo_company_sales_person/features/versioning/presentation/widgets/update_version_screen_widgets/update_version_screen_actions_widget.dart';
import 'package:meedigo_company_sales_person/features/versioning/presentation/widgets/update_version_screen_widgets/update_version_screen_content_widget.dart';
import 'package:meedigo_company_sales_person/features/versioning/presentation/widgets/update_version_screen_widgets/update_version_screen_logo_widget.dart';
import 'package:meedigo_company_sales_person/features/versioning/presentation/widgets/update_version_screen_widgets/update_version_screen_title_widget.dart';

class UpdateVersionScreen extends StatelessWidget {
  final UpdateAppScreenRouteParameters parameters;

  const UpdateVersionScreen({
    super.key,
    required this.parameters,
  });

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: AppPaddingWidget(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            UpdateVersionScreenLogoWidget(),
            verticalSpacing(35),
            UpdateVersionScreenTitleWidget(),
            verticalSpacing(35),
            UpdateVersionScreenContentWidget(),
            verticalSpacing(75),
            UpdateVersionScreenActionsWidget(
              versioningData: parameters.versioningData,
            ),
          ],
        ),
      ),
    );
  }
}
