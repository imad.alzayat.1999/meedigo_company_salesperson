import 'package:equatable/equatable.dart';

class VersioningResponse extends Equatable {
  final VersioningData? versioningData;

  VersioningResponse({required this.versioningData});

  @override
  List<Object?> get props => [versioningData];
}

class VersioningData extends Equatable {
  final bool hasUpdate;
  final int required;
  final String dateRange;
  final String platformUrl;
  final String directUrl;

  VersioningData({
    required this.required,
    required this.hasUpdate,
    required this.dateRange,
    required this.directUrl,
    required this.platformUrl,
  });

  @override
  List<Object?> get props => [
        required,
        hasUpdate,
        dateRange,
        directUrl,
        platformUrl,
      ];
}
