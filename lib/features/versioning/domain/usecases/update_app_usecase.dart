import 'package:dartz/dartz.dart';
import 'package:meedigo_company_sales_person/core/networking/api_query_parameters.dart';
import 'package:meedigo_company_sales_person/features/versioning/domain/entities/versioning_response.dart';
import 'package:meedigo_company_sales_person/features/versioning/domain/repository/versioning_repository.dart';

import '../../../../core/networking/api_failure.dart';
import '../../../../core/usecase/base_usecase.dart';

class UpdateAppUseCase
    extends BaseUseCase<VersioningResponse, UpdateAppRequestQueryParameters> {
  final VersioningRepository versioningRepository;

  UpdateAppUseCase({required this.versioningRepository});

  @override
  Future<Either<Failure, VersioningResponse>> call(
    UpdateAppRequestQueryParameters parameters,
  ) async {
    return await versioningRepository.updateApp(
      queryParameters: parameters,
    );
  }
}
