import 'package:dartz/dartz.dart';
import 'package:meedigo_company_sales_person/core/networking/api_query_parameters.dart';
import 'package:meedigo_company_sales_person/features/versioning/domain/entities/versioning_response.dart';

import '../../../../core/networking/api_failure.dart';

abstract class VersioningRepository {
  Future<Either<Failure, VersioningResponse>> updateApp({
    required UpdateAppRequestQueryParameters queryParameters,
  });
}
