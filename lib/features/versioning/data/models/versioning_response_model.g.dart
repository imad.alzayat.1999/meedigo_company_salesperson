// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'versioning_response_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

VersioningResponseModel _$VersioningResponseModelFromJson(
        Map<String, dynamic> json) =>
    VersioningResponseModel(
      versioningDataModel: json['data'] == null
          ? null
          : VersioningDataModel.fromJson(json['data'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$VersioningResponseModelToJson(
        VersioningResponseModel instance) =>
    <String, dynamic>{
      'data': instance.versioningDataModel,
    };

VersioningDataModel _$VersioningDataModelFromJson(Map<String, dynamic> json) =>
    VersioningDataModel(
      required: (json['required'] as num?)?.toInt(),
      hasUpdate: json['has_update'] as bool?,
      dateRange: json['date_range'] as String?,
      directUrl: json['direct_url'] as String?,
      platformUrl: json['platform_url'] as String?,
    );

Map<String, dynamic> _$VersioningDataModelToJson(
        VersioningDataModel instance) =>
    <String, dynamic>{
      'has_update': instance.hasUpdate,
      'required': instance.required,
      'date_range': instance.dateRange,
      'platform_url': instance.platformUrl,
      'direct_url': instance.directUrl,
    };
