import 'package:json_annotation/json_annotation.dart';
part 'versioning_response_model.g.dart';

@JsonSerializable()
class VersioningResponseModel {
  @JsonKey(name: "data")
  final VersioningDataModel? versioningDataModel;

  VersioningResponseModel({
    required this.versioningDataModel,
  });

  factory VersioningResponseModel.fromJson(Map<String, dynamic> json) =>
      _$VersioningResponseModelFromJson(json);

  Map<String, dynamic> toJson() => _$VersioningResponseModelToJson(this);
}

@JsonSerializable()
class VersioningDataModel {
  @JsonKey(name: "has_update")
  final bool? hasUpdate;

  @JsonKey(name: "required")
  final int? required;

  @JsonKey(name: "date_range")
  final String? dateRange;

  @JsonKey(name: "platform_url")
  final String? platformUrl;

  @JsonKey(name: "direct_url")
  final String? directUrl;

  VersioningDataModel({
    required this.required,
    required this.hasUpdate,
    required this.dateRange,
    required this.directUrl,
    required this.platformUrl,
  });

  factory VersioningDataModel.fromJson(Map<String, dynamic> json) =>
      _$VersioningDataModelFromJson(json);

  Map<String, dynamic> toJson() => _$VersioningDataModelToJson(this);
}
