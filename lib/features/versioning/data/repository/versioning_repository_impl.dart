import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:dartz/dartz.dart';
import 'package:meedigo_company_sales_person/core/mappers/update_app_mappers.dart';
import 'package:meedigo_company_sales_person/core/networking/api_error_model.dart';
import 'package:meedigo_company_sales_person/core/networking/api_failure.dart';
import 'package:meedigo_company_sales_person/core/networking/api_query_parameters.dart';
import 'package:meedigo_company_sales_person/core/networking/network_info.dart';
import 'package:meedigo_company_sales_person/features/versioning/data/datasources/versioning_remote_datasource.dart';
import 'package:meedigo_company_sales_person/features/versioning/domain/entities/versioning_response.dart';
import '../../../../core/networking/network_info.dart';
import '../../domain/repository/versioning_repository.dart';
import '../datasources/versioning_local_datasource.dart';

class VersioningRepositoryImpl implements VersioningRepository {
  final VersioningRemoteDataSource versioningRemoteDataSource;
  final VersioningLocalDataSource versioningLocalDataSource;
  final NetworkInfo networkInfo;

  VersioningRepositoryImpl({
    required this.versioningRemoteDataSource,
    required this.versioningLocalDataSource,
    required this.networkInfo,
  });

  @override
  Future<Either<Failure, VersioningResponse>> updateApp({
    required UpdateAppRequestQueryParameters queryParameters,
  }) async {
    if (await networkInfo.connectivityResult != ConnectivityResult.none) {
      try {
        final result = await versioningRemoteDataSource.updateApp(
          queryParameters: queryParameters,
        );
        versioningLocalDataSource.cacheVersioningResponse(
          versioningResponseModel: result,
        );
        return Right(result.toDomain());
      } on ApiErrorModel catch (e) {
        return Left(
          ServerFailure(
            statusCode: e.code,
            errorMessage: e.message,
          ),
        );
      }
    } else {
      try {
        final result =
            await versioningLocalDataSource.getCachedVersioningResponse();
        return Right(result.toDomain());
      } on LocalException catch (e) {
        return Left(LocalFailure(e.message));
      }
    }
  }
}
