import 'dart:convert';

import 'package:dartz/dartz.dart';
import 'package:meedigo_company_sales_person/core/networking/api_error_model.dart';
import 'package:meedigo_company_sales_person/features/versioning/data/models/versioning_response_model.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../../../core/helpers/app_consts.dart';
import '../../../../core/helpers/app_strings.dart';

abstract class VersioningLocalDataSource {
  Future<Unit> cacheVersioningResponse({
    required VersioningResponseModel versioningResponseModel,
  });

  Future<VersioningResponseModel> getCachedVersioningResponse();
}

class VersioningLocalDataSourceImpl implements VersioningLocalDataSource {
  final SharedPreferences sharedPreferences;

  VersioningLocalDataSourceImpl({
    required this.sharedPreferences,
  });

  @override
  Future<Unit> cacheVersioningResponse({
    required VersioningResponseModel versioningResponseModel,
  }) {
    final convertToJson = versioningResponseModel.toJson();
    final encodeTheConvertedData = jsonEncode(convertToJson);
    sharedPreferences.setString(
      AppConsts.versioningKey,
      encodeTheConvertedData,
    );
    return Future.value(unit);
  }

  @override
  Future<VersioningResponseModel> getCachedVersioningResponse() {
    try {
      final cachedData = sharedPreferences.getString(
        AppConsts.versioningKey,
      );
      if (cachedData == null) {
        throw LocalException(
          message: AppStrings.noInternetErrorString,
        );
      } else {
        final decodeTheResult = jsonDecode(cachedData);
        return Future.value(
          VersioningResponseModel.fromJson(
            decodeTheResult,
          ),
        );
      }
    } catch (_) {
      throw LocalException(
        message: AppStrings.noInternetErrorString,
      );
    }
  }
}
