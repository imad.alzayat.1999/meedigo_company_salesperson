import 'package:dio/dio.dart';
import 'package:meedigo_company_sales_person/core/networking/api_query_parameters.dart';
import 'package:meedigo_company_sales_person/core/networking/api_services.dart';
import 'package:package_info_plus/package_info_plus.dart';

import '../../../../core/helpers/app_functions.dart';
import '../../../../core/networking/api_error_handler.dart';
import '../models/versioning_response_model.dart';

abstract class VersioningRemoteDataSource {
  Future<VersioningResponseModel> updateApp({
    required UpdateAppRequestQueryParameters queryParameters,
  });
}

class VersioningRemoteDataSourceImpl implements VersioningRemoteDataSource {
  final ApiService apiService;

  VersioningRemoteDataSourceImpl({required this.apiService});

  @override
  Future<VersioningResponseModel> updateApp({
    required UpdateAppRequestQueryParameters queryParameters,
  }) async {
    final info = await PackageInfo.fromPlatform();
    try {
      final response = await apiService.updateApp(
        version: info.version,
        appPlatform: queryParameters.platform,
        appVersion: info.version,
        appName: queryParameters.appName,
        platform: AppFunctions.getCurrentPlatform(),
      );
      return response;
    } on DioException catch (e) {
      throw handleError(e);
    }
  }
}
