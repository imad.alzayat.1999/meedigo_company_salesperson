import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_extensions.dart';
import '../../../../../core/helpers/app_sizes.dart';
import '../../../../../core/helpers/app_consts.dart';
import '../../../../../core/theming/app_colors.dart';
import '../../../../../core/theming/app_styles.dart';
import '../../../../../core/utils/custom_tabbar_indicator.dart';

PreferredSizeWidget? ordersTabBarWidget({
  required BuildContext context,
  required void Function(int)? onTabFunction,
}) {
  return TabBar(
    onTap: onTabFunction,
    indicatorPadding: EdgeInsets.only(top: getRadius(1)),
    indicator: CustomTabBarIndicator(),
    indicatorWeight: 5,
    labelColor: AppColors.kWhite,
    labelStyle: AppStyles.medium(
      fontSize: 14,
      context: context,
    ),
    unselectedLabelColor: AppColors.kWhite,
    unselectedLabelStyle: AppStyles.medium(
      fontSize: 14,
      context: context,
    ),
    tabs: AppConsts.ordersTabBarItems
        .map(
          (e) => tabItem(
            tabIcon: e.iconPath,
            tabTitle: e.key,
            context: context,
          ),
        )
        .toList(),
  );
}

Tab tabItem({
  required String tabIcon,
  required String tabTitle,
  required BuildContext context,
}) {
  return Tab(
    child: Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        SizedBox(
          width: getHeight(20),
          height: getHeight(20),
          child: SvgPicture.asset(
            tabIcon,
            color: AppColors.kWhite,
          ),
        ),
        horizontalSpacing(5),
        Text(context.translate(tabTitle)),
      ],
    ),
  );
}