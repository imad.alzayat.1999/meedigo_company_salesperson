import 'package:flutter/material.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_extensions.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_strings.dart';
import '../../../../../core/helpers/app_sizes.dart';
import '../../../../../core/theming/app_colors.dart';
import '../../../../../core/theming/app_styles.dart';

class OrderCardWidget extends StatelessWidget {
  final void Function()? onPressFunction;

  const OrderCardWidget({super.key, this.onPressFunction});

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      decoration: BoxDecoration(
        color: AppColors.kSecondary,
        borderRadius: BorderRadius.circular(
          getRadius(12),
        ),
      ),
      child: Material(
        color: AppColors.kTransparent,
        child: InkWell(
          onTap: onPressFunction,
          borderRadius: BorderRadius.circular(
            getRadius(12),
          ),
          child: Padding(
            padding: EdgeInsets.symmetric(
              horizontal: getWidth(14),
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      context.translate(
                        AppStrings.orderNumberCardString,
                        args: {
                          "order_id": "1220",
                        },
                      ),
                      style: AppStyles.bold(
                        fontSize: 14,
                        textColor: AppColors.kSecondaryText,
                        context: context,
                      ),
                    ),
                    Text(
                      "10 hours ago",
                      style: AppStyles.medium(
                        fontSize: 10,
                        textColor: AppColors.kSecondaryText,
                        context: context,
                      ),
                    ),
                  ],
                ),
                verticalSpacing(7),
                Text(
                  context.translate(
                    AppStrings.orderSourceOrderCardString,
                    args: {
                      "source": "Company name",
                    },
                  ),
                  style: AppStyles.regular(
                    fontSize: 14,
                    context: context,
                  ),
                ),
                verticalSpacing(7),
                Text(
                  context.translate(
                    AppStrings.orderDestinationOrderCardString,
                    args: {
                      "destination": "Market name",
                    },
                  ),
                  style: AppStyles.regular(
                    fontSize: 14,
                    context: context,
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
