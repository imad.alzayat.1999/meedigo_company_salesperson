import 'package:flutter/material.dart';
import '../../../../../core/helpers/app_sizes.dart';
import 'order_card_widget.dart';

class OrdersListWidget extends StatelessWidget {
  const OrdersListWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      shrinkWrap: true,
      itemBuilder: (context, index) {
        return Padding(
          padding: EdgeInsets.symmetric(
            vertical: getHeight(10),
          ),
          child: OrderCardWidget(
            onPressFunction: () {},
          ),
        );
      },
      padding: EdgeInsets.only(
        left: getWidth(16),
        right: getWidth(16),
        top: getHeight(15),
      ),
      itemCount: 10,
    );
  }
}
