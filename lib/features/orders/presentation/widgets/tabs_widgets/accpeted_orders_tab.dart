import 'package:flutter/material.dart';

import '../common_widgets/orders_list_widget.dart';

class AcceptedOrdersTab extends StatefulWidget {
  @override
  State<AcceptedOrdersTab> createState() => _AcceptedOrdersTabState();
}

class _AcceptedOrdersTabState extends State<AcceptedOrdersTab> {

  @override
  Widget build(BuildContext context) {
    return OrdersListWidget();
  }
}
