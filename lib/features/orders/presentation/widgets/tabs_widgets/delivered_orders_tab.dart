import 'package:flutter/material.dart';
import '../common_widgets/orders_list_widget.dart';

class DeliveredOrdersTab extends StatefulWidget {
  @override
  State<DeliveredOrdersTab> createState() => _DeliveredOrdersTabState();
}

class _DeliveredOrdersTabState extends State<DeliveredOrdersTab> {
  @override
  Widget build(BuildContext context) {
    return OrdersListWidget();
  }
}
