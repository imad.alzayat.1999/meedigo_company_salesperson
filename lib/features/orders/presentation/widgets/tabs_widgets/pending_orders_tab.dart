import 'package:flutter/material.dart';
import '../common_widgets/orders_list_widget.dart';

class PendingOrdersTab extends StatefulWidget {
  @override
  State<PendingOrdersTab> createState() => _PendingOrdersTabState();
}

class _PendingOrdersTabState extends State<PendingOrdersTab> {
  @override
  Widget build(BuildContext context) {
    return OrdersListWidget();
  }
}
