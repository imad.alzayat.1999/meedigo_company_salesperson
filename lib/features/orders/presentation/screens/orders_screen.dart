import 'package:flutter/material.dart';
import 'package:meedigo_company_sales_person/features/orders/presentation/widgets/common_widgets/orders_tabbar_widget.dart';
import '../../../../core/helpers/app_consts.dart';
import '../../../../core/widgets/appbar_widget.dart';
import '../../../no_internet/presentation/screens/no_internet_connection_screen.dart';
import '../widgets/tabs_widgets/accpeted_orders_tab.dart';
import '../widgets/tabs_widgets/delivered_orders_tab.dart';
import '../widgets/tabs_widgets/pending_orders_tab.dart';

class OrdersScreen extends StatelessWidget {
  const OrdersScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return OrdersScreenContent();
  }
}

class OrdersScreenContent extends StatefulWidget {
  @override
  State<OrdersScreenContent> createState() => _OrdersScreenContentState();
}

class _OrdersScreenContentState extends State<OrdersScreenContent> {
  int _currentIndex = 0;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  /// select tab and render orders by selected tab
  _select({required int index, required BuildContext context}) {
    setState(() {
      _currentIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: AppConsts.ordersTabBarItems.length,
      initialIndex: _currentIndex,
      child: NoInternetConnectionScreen(
        screen: Scaffold(
          appBar: appbarWidget(
            title: "Orders",
            context: context,
            isMainPages: true,
            bottom: ordersTabBarWidget(
              context: context,
              onTabFunction: (index) => _select(
                index: index,
                context: context,
              ),
            ),
          ),
          body: TabBarView(
            children: [
              PendingOrdersTab(),
              AcceptedOrdersTab(),
              DeliveredOrdersTab(),
            ],
          ),
        ),
      ),
    );
  }
}
