import 'package:flutter/material.dart';
import '../../../../core/helpers/app_sizes.dart';
import '../../../../core/widgets/accessories/accessory_card_widget.dart';

class AccessoriesByBarcodeTabWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      padding: EdgeInsets.only(
        top: getHeight(15),
        left: getWidth(16),
        right: getWidth(16),
      ),
      itemBuilder: (context, index) {
        return Padding(
          padding: EdgeInsets.only(
            bottom: getHeight(15),
          ),
          child: AccessoryCardWidget(),
        );
      },
      shrinkWrap: true,
      itemCount: 10,
    );
  }
}
