import 'package:flutter/material.dart';
import '../../../../core/helpers/app_sizes.dart';
import '../../../../core/widgets/medicines/medicine_card_widget.dart';

class MedicinesByBarcodeTabWidget extends StatelessWidget {
  const MedicinesByBarcodeTabWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      padding: EdgeInsets.only(
        left: getWidth(16),
        right: getWidth(16),
        top: getHeight(15),
      ),
      itemBuilder: (context, index) {
        return Padding(
          padding: EdgeInsets.only(
            bottom: getHeight(15),
          ),
          child: MedicineCardWidget(),
        );
      },
      shrinkWrap: true,
      itemCount: 10,
    );
  }
}