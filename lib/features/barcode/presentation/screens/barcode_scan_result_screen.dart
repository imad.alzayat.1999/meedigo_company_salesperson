import 'package:flutter/material.dart';
import 'package:meedigo_company_sales_person/features/barcode/presentation/widgets/accessories_by_barcode_tab_widget.dart';
import 'package:meedigo_company_sales_person/features/barcode/presentation/widgets/barcode_tabbar_widget.dart';
import '../../../../core/helpers/app_consts.dart';
import '../../../../core/widgets/appbar_widget.dart';
import '../widgets/medicines_by_barcode_tab_widget.dart';

class ProductsByBarcodeScreen extends StatefulWidget {
  @override
  State<ProductsByBarcodeScreen> createState() => _ProductsByBarcodeScreenState();
}

class _ProductsByBarcodeScreenState extends State<ProductsByBarcodeScreen> {
  int _currentIndex = 0;

  /// function to change tabBar index
  _changeTabBarIndex(int index){
    setState(() {
      _currentIndex = index;
    });
  }


  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: AppConsts.barcodeTabBarItems.length,
      initialIndex: _currentIndex,
      child: Scaffold(
        appBar: appbarWidget(
          title: "Barcode Scan",
          context: context,
          bottom: barcodeTabBarWidget(
            context: context,
            onTabFunction: (index) => _changeTabBarIndex(index),
          ),
        ),
        body: TabBarView(
          children: [
            MedicinesByBarcodeTabWidget(),
            AccessoriesByBarcodeTabWidget(),
          ],
        ),
      ),
    );
  }
}