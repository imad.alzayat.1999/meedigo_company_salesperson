import 'package:flutter/material.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_extensions.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_strings.dart';
import 'package:meedigo_company_sales_person/core/widgets/app_padding_widget.dart';
import '../../../../../core/widgets/appbar_widget.dart';
import '../../../../../core/helpers/app_sizes.dart';
import '../../widgets/order_details_screen_widgets/order_details_header_widget.dart';
import '../../widgets/order_details_screen_widgets/order_details_products_widget.dart';
import '../../widgets/order_details_screen_widgets/order_details_total_price_and_deliver_button_widget.dart';

class OrderDetailsScreen extends StatefulWidget {
  @override
  State<OrderDetailsScreen> createState() => _OrderDetailsScreenState();
}

class _OrderDetailsScreenState extends State<OrderDetailsScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: appbarWidget(
        title: context.translate(
          AppStrings.orderDetailsTitleString,
          args: {
            "order_id": "1200",
          },
        ),
        context: context,
        isMainPages: true,
      ),
      body: AppPaddingWidget(
        top: getHeight(15),
        child: ListView(
          children: [
            OrderDetailsHeaderWidget(),
            Padding(
              padding: EdgeInsets.only(
                top: getHeight(18),
                bottom: getHeight(10),
              ),
              child: Divider(),
            ),
            Padding(
              padding: EdgeInsets.only(bottom: getHeight(200)),
              child: OrderProductsWidget(),
            ),
          ],
        ),
      ),
      bottomSheet: OrderDetailsTotalPriceAndDeliverButtonWidget(),
    );
  }
}
