import 'package:flutter/material.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_extensions.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_strings.dart';
import 'package:meedigo_company_sales_person/features/details/presentation/widgets/product_details_widgets/product_details_header_widget.dart';
import 'package:meedigo_company_sales_person/features/details/presentation/widgets/product_details_widgets/similar_products_tab_widget.dart';
import '../../../../../core/widgets/appbar_widget.dart';
import '../../../../../core/helpers/app_sizes.dart';
import '../../widgets/product_details_widgets/add_product_to_cart_widget.dart';
import '../../widgets/product_details_widgets/product_details_tabbar_widget.dart';
import '../../widgets/product_details_widgets/product_more_info_tab_widget.dart';
import '../../widgets/product_details_widgets/product_details_prices_widget.dart';

class ProductDetailsScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ProductDetailsContent();
  }
}

class ProductDetailsContent extends StatefulWidget {
  @override
  State<ProductDetailsContent> createState() => _ProductDetailsContentState();
}

class _ProductDetailsContentState extends State<ProductDetailsContent>
    with TickerProviderStateMixin {
  TabController? _controller;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _controller = TabController(length: 2, vsync: this);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: appbarWidget(
        title: context.translate(
          AppStrings.productDetailsTitleString,
        ),
        context: context,
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          ProductDetailsHeaderWidget(),
          verticalSpacing(30),
          ProductDetailsPricesWidget(),
          verticalSpacing(12),
          DefaultTabController(
            length: 2,
            child: Container(
              width: MediaQuery.of(context).size.width,
              child: productDetailsTabBarWidget(
                context: context,
                controller: _controller,
              ),
            ),
          ),
          Expanded(
            child: TabBarView(
              controller: _controller,
              children: [
                ProductMoreInfoTabWidget(),
                SimilarProductsTabWidget(),
              ],
            ),
          ),
        ],
      ),
      bottomSheet: AddProductToCartWidget(),
    );
  }
}
