import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:meedigo_company_sales_person/core/common/entities/tabbar_data.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_consts.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_extensions.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_sizes.dart';

import '../../../../../core/theming/app_colors.dart';
import '../../../../../core/theming/app_styles.dart';
import '../../../../../core/utils/custom_tabbar_indicator.dart';

Widget? productDetailsTabBarWidget({
  required BuildContext context,
  required TabController? controller,
}) {
  return Center(
    child: TabBar(
      controller: controller,
      indicatorSize: TabBarIndicatorSize.tab,
      indicatorColor: AppColors.kPrimary,
      indicatorPadding: EdgeInsets.only(top: getRadius(1)),
      indicator: CustomTabBarIndicator(),
      isScrollable: true,
      tabAlignment: TabAlignment.center,
      labelColor: AppColors.kPrimary,
      labelStyle: AppStyles.medium(
        fontSize: 14,
        context: context,
      ),
      physics: const NeverScrollableScrollPhysics(),
      unselectedLabelColor: AppColors.kPrimary,
      unselectedLabelStyle: AppStyles.medium(
        fontSize: 14,
        context: context,
      ),
      tabs: AppConsts.productDetailsTabBarItems
          .map(
            (e) => tabItem(tabBarData: e, context: context),
          )
          .toList(),
    ),
  );
}

Widget tabItem({
  required TabBarData tabBarData,
  required BuildContext context,
}) {
  return Tab(
    child: Padding(
      padding: EdgeInsets.symmetric(horizontal: getWidth(20)),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          SizedBox(
            width: getHeight(20),
            height: getHeight(20),
            child: SvgPicture.asset(
              tabBarData.iconPath,
              color: AppColors.kPrimary,
            ),
          ),
          horizontalSpacing(5),
          Text(context.translate(tabBarData.key)),
        ],
      ),
    ),
  );
}
