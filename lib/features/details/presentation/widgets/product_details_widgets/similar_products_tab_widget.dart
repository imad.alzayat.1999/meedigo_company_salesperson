import 'package:flutter/material.dart';
import 'package:meedigo_company_sales_person/core/widgets/app_padding_widget.dart';
import '../../../../../core/widgets/medicines/list_of_medicines_widget.dart';

class SimilarProductsTabWidget extends StatelessWidget {
  const SimilarProductsTabWidget({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return ListOfSimilarProducts();
  }
}

class ListOfSimilarProducts extends StatefulWidget {
  const ListOfSimilarProducts({
    super.key,
  });

  @override
  State<ListOfSimilarProducts> createState() => _ListOfSimilarProductsState();
}

class _ListOfSimilarProductsState extends State<ListOfSimilarProducts> {
  @override
  Widget build(BuildContext context) {
    return AppPaddingWidget(
      top: 15,
      child: ListOfMedicinesWidget(),
    );
  }
}
