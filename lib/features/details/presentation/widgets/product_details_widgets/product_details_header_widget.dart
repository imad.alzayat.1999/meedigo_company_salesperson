import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:meedigo_company_sales_person/core/widgets/app_padding_widget.dart';
import '../../../../../core/helpers/app_assets.dart';
import '../../../../../core/helpers/app_sizes.dart';
import '../../../../../core/theming/app_colors.dart';
import '../../../../../core/theming/app_styles.dart';

class ProductDetailsHeaderWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return AppPaddingWidget(
      top: 15,
      child: Row(
        children: [
          SizedBox(
            width: getWidth(75),
            height: getHeight(80),
            child: ClipRRect(
              borderRadius: BorderRadius.circular(
                getRadius(12),
              ),
              child: Image.asset(
                AppImages.meedigoLogoImage,
                fit: BoxFit.cover,
              ),
            ),
          ),
          horizontalSpacing(12),
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  "Product name",
                  maxLines: 2,
                  overflow: TextOverflow.ellipsis,
                  style: AppStyles.bold(
                    fontSize: 10,
                    context: context,
                    height: 1.2,
                  ),
                ),
                verticalSpacing(12),
                Row(
                  children: [
                    SizedBox(
                      width: getHeight(25),
                      height: getHeight(25),
                      child: SvgPicture.asset(
                        AppIcons.trademarkIcon,
                        color: AppColors.kSecondaryText,
                      ),
                    ),
                    horizontalSpacing(4),
                    Text(
                      "Brand name",
                      style: AppStyles.bold(
                        fontSize: 14,
                        textColor: AppColors.kSecondaryText,
                        context: context,
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}


