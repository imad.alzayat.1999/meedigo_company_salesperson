import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';
import '../../../../../core/helpers/app_sizes.dart';
import '../../../../../core/helpers/app_assets.dart';
import '../../../../../core/theming/app_colors.dart';
import '../../../../../core/theming/app_styles.dart';

class AddProductToCartWidget extends StatefulWidget {
  const AddProductToCartWidget({
    Key? key,
  }) : super(key: key);

  @override
  State<AddProductToCartWidget> createState() => _AddProductToCartWidgetState();
}

class _AddProductToCartWidgetState extends State<AddProductToCartWidget> {
  int _number = 1;
  num _offerWhenEnabled = 0;
  num _offerValue = 0;
  String _offerType = "";
  Timer? _timer;

  // /// assign values
  // _assignValues(Cart cart) {
  //   for (int i = 0; i < cart.offers.length; i++) {
  //     if (AppFunctions.checkIfOfferIsEnabled(
  //       index: i,
  //       quantity: cart.quantity,
  //       offers: cart.offers,
  //     )) {
  //       _mapValuesToOfferValues(offer: cart.offers[i]);
  //     }
  //   }
  // }
  //
  // /// map values to offer values
  // _mapValuesToOfferValues({required Offer offer}) {
  //   _offerType = offer.type;
  //   _offerValue = offer.value;
  //   _offerWhenEnabled = offer.whenEnabled;
  // }
  //
  // /// update product in cart functionality
  // _updateProductInCart({
  //   required Cart cart,
  //   required int index,
  //   required List<Cart> products,
  // }) {
  //   _assignValues(cart);
  //   context.read<CartCubit>()
  //     ..updateProductFromTheCartCard(
  //       cart: cart,
  //       index: index,
  //       offerWhenEnabled: _offerWhenEnabled,
  //       offerValue: _offerValue,
  //       offerType: _offerType,
  //       groupedByProducts: products,
  //     );
  // }
  //
  // /// decrease the counter
  // _decreaseCounter({
  //   required Cart cart,
  //   required int index,
  //   required List<Cart> products,
  // }) {
  //   AppFunctions.vibrate();
  //   if (cart.quantity == 1) {
  //     context.read<CartCubit>().deleteProductFromCart(
  //           delete: DeleteProductFromCartRouteParameters(
  //             index: index,
  //           ),
  //         );
  //   } else {
  //     if (cart.quantity <= cart.minQuantity) {
  //       context.read<CartCubit>().deleteProductFromCart(
  //             delete: DeleteProductFromCartRouteParameters(
  //               index: index,
  //             ),
  //           );
  //     } else {
  //       setState(() {
  //         cart.quantity--;
  //       });
  //       _updateProductInCart(
  //         cart: cart,
  //         index: index,
  //         products: products,
  //       );
  //     }
  //   }
  // }
  //
  // /// increase the counter
  // _increaseCounter({
  //   required Cart cart,
  //   required int index,
  //   required List<Cart> products,
  // }) {
  //   AppFunctions.vibrate();
  //   if (cart.maxQuantity == 0) {
  //     setState(() {
  //       cart.quantity++;
  //     });
  //     _updateProductInCart(
  //       cart: cart,
  //       index: index,
  //       products: products,
  //     );
  //   } else {
  //     if (cart.quantity >= cart.maxQuantity) {
  //       AppFunctions.showWarningMessage(
  //         context: context,
  //         message: AppStrings.maxQuantityWarningString,
  //       );
  //     } else if (cart.quantity >= cart.productQuantity) {
  //       AppFunctions.showWarningMessage(
  //         context: context,
  //         message: AppStrings.warningQuantityRequiredString,
  //       );
  //     } else {
  //       setState(() {
  //         cart.quantity++;
  //       });
  //       _updateProductInCart(
  //         cart: cart,
  //         index: index,
  //         products: products,
  //       );
  //     }
  //   }
  // }
  //
  _onTapCancel() {
    _timer!.cancel();
  }

  // _onTapDownMinus({
  //   required Cart cart,
  //   required int index,
  //   required List<Cart> products,
  // }) {
  //   _timer = Timer.periodic(
  //     Duration(
  //       milliseconds: AppConsts.continuousClickDuration,
  //     ),
  //     (timer) {
  //       _decreaseCounter(
  //         cart: cart,
  //         index: index,
  //         products: products,
  //       );
  //     },
  //   );
  // }

  // _onTapDownPlus({
  //   required Cart cart,
  //   required int index,
  //   required List<Cart> products,
  // }) {
  //   _timer = Timer.periodic(
  //     Duration(
  //       milliseconds: AppConsts.continuousClickDuration,
  //     ),
  //     (timer) {
  //       _increaseCounter(
  //         cart: cart,
  //         index: index,
  //         products: products,
  //       );
  //     },
  //   );
  // }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    _onTapCancel();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: getHeight(80),
      decoration: BoxDecoration(
        color: AppColors.kSecondary,
      ),
      child: Padding(
        padding: EdgeInsets.symmetric(
          horizontal: getWidth(16),
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            CounterButton(
              onTapDown: (details) {},
              onTapCancel: () => _onTapCancel(),
              onTapUp: (details) => _onTapCancel(),
              iconPath: AppIcons.minusIcon,
              onPressFunction: () {},
              isExisted: false,
            ),
            Text(
              _number.toString(),
              style: AppStyles.semiBold(
                fontSize: 20,
                context: context,
              ),
            ),
            CounterButton(
              onTapDown: (details) {},
              onTapCancel: () => _onTapCancel(),
              onTapUp: (details) => _onTapCancel(),
              iconPath: AppIcons.plusIcon,
              onPressFunction: () {},
              isExisted: true,
            ),
          ],
        ),
      ),
    );
  }
}

class CounterButton extends StatelessWidget {
  final String iconPath;
  final void Function()? onPressFunction;
  final void Function()? onTapCancel;
  final void Function(TapDownDetails)? onTapDown;
  final void Function(TapUpDetails)? onTapUp;
  final bool isExisted;

  const CounterButton({
    super.key,
    required this.iconPath,
    required this.onPressFunction,
    required this.onTapCancel,
    required this.onTapUp,
    required this.onTapDown,
    required this.isExisted,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      width: getWidth(114),
      height: getHeight(54),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(
          getRadius(15),
        ),
        color: isExisted ? AppColors.kPrimary : AppColors.kWhite,
      ),
      child: Material(
        color: AppColors.kTransparent,
        child: InkWell(
          borderRadius: BorderRadius.circular(
            getRadius(15),
          ),
          onTap: onPressFunction,
          onTapCancel: onTapCancel,
          onTapDown: onTapDown,
          onTapUp: onTapUp,
          child: Center(
            child: SvgPicture.asset(
              iconPath,
              color: isExisted ? AppColors.kWhite : AppColors.kPrimaryText,
            ),
          ),
        ),
      ),
    );
  }
}
