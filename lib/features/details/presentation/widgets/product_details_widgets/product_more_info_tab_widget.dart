import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_extensions.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_sizes.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_strings.dart';
import 'package:meedigo_company_sales_person/core/widgets/app_padding_widget.dart';
import '../../../../../core/helpers/app_assets.dart';
import '../../../../../core/theming/app_colors.dart';
import '../../../../../core/theming/app_styles.dart';

class ProductMoreInfoTabWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return AppPaddingWidget(
      top: 20,
      child: Column(
        children: [
          MoreDetails(
            value: "FOR /20/CTD TAB.BLISTER",
            title: context.translate(
              AppStrings.compositionProductDetailsString,
            ),
            iconPath: AppIcons.compositionIcon,
          ),
          verticalSpacing(15),
          MoreDetails(
            value: "FOR /20/CTD TAB.BLISTER",
            title: context.translate(
              AppStrings.caliberProductDetailsString,
            ),
            iconPath: AppIcons.caliberIcon,
          ),
          verticalSpacing(15),
          MoreDetails(
            value: "FOR /20/CTD TAB.BLISTER",
            title: context.translate(
              AppStrings.packingProductDetailsString,
            ),
            iconPath: AppIcons.packingIcon,
          ),
          verticalSpacing(15),
          MoreDetails(
            value: "FOR /20/CTD TAB.BLISTER",
            title: context.translate(
              AppStrings.formulaProductDetailsString,
            ),
            iconPath: AppIcons.formulaIcon,
          ),
        ],
      ),
    );
  }
}

class MoreDetails extends StatelessWidget {
  final String iconPath;
  final String title;
  final String value;
  const MoreDetails({
    super.key,
    required this.value,
    required this.title,
    required this.iconPath,
  });

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: getHeight(65),
      child: Card(
        color: AppColors.kPrimary,
        elevation: 3,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(getRadius(12)),
        ),
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: getWidth(10)),
          child: Row(
            children: [
              SizedBox(
                height: getHeight(45),
                width: getWidth(45),
                child: SvgPicture.asset(
                  iconPath,
                  color: AppColors.kWhite,
                ),
              ),
              horizontalSpacing(8),
              Expanded(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      title,
                      style: AppStyles.bold(
                        fontSize: 16,
                        textColor: AppColors.kWhite,
                        context: context,
                      ),
                    ),
                    verticalSpacing(7),
                    Text(
                      value,
                      style: AppStyles.medium(
                        fontSize: 12,
                        textColor: AppColors.kWhite,
                        context: context,
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
