import 'package:flutter/material.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_extensions.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_strings.dart';
import 'package:meedigo_company_sales_person/core/widgets/app_padding_widget.dart';
import '../../../../../core/helpers/app_sizes.dart';
import '../../../../../core/theming/app_colors.dart';
import '../../../../../core/theming/app_styles.dart';

class ProductDetailsPricesWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return AppPaddingWidget(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          PriceCard(
            title: context.translate(
              AppStrings.pharmacistPriceProductDetailsString,
            ),
            value: "10,000",
            isPharmacistPrice: true,
          ),
          PriceCard(
            title: context.translate(
              AppStrings.sellingPriceProductDetailsString,
            ),
            value: "15,000",
          ),
        ],
      ),
    );
  }
}

class PriceCard extends StatelessWidget {
  final String title;
  final String value;
  final bool isPharmacistPrice;

  const PriceCard({
    super.key,
    required this.title,
    required this.value,
    this.isPharmacistPrice = false,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      width: getWidth(154),
      height: getHeight(50),
      decoration: BoxDecoration(
        color: isPharmacistPrice ? AppColors.kWhite : AppColors.kPrimary,
        borderRadius: BorderRadius.circular(
          getRadius(15),
        ),
        border: Border.all(color: AppColors.kPrimary),
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(
            title,
            style: AppStyles.bold(
              fontSize: 14,
              textColor:
                  isPharmacistPrice ? AppColors.kPrimary : AppColors.kWhite,
              context: context,
            ),
          ),
          verticalSpacing(5),
          Text(
            value,
            style: AppStyles.medium(
              fontSize: 12,
              textColor:
                  isPharmacistPrice ? AppColors.kPrimary : AppColors.kWhite,
              context: context,
            ),
          ),
        ],
      ),
    );
  }
}
