import 'package:flutter/material.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_extensions.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_strings.dart';
import 'package:meedigo_company_sales_person/core/theming/app_styles.dart';
import 'package:meedigo_company_sales_person/core/widgets/order_product_card_widget.dart';
import '../../../../../core/helpers/app_sizes.dart';

class OrderProductsWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Align(
          alignment: AlignmentDirectional.centerStart,
          child: Text(
            context.translate(
              AppStrings.numberOfOrderProductsString,
              args: {
                "number_of_products_per_order": 20,
              },
            ),
            style: AppStyles.semiBold(
              fontSize: 18,
              context: context,
            ),
          ),
        ),
        verticalSpacing(15),
        ListView.builder(
          shrinkWrap: true,
          physics: const NeverScrollableScrollPhysics(),
          itemBuilder: (context, index) => Padding(
            padding: EdgeInsets.only(
              bottom: getHeight(15),
            ),
            child: OrderProductCardWidget(),
          ),
          itemCount: 10,
        ),
      ],
    );
  }
}
