import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_assets.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_extensions.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_strings.dart';

import '../../../../../core/helpers/app_sizes.dart';
import '../../../../../core/theming/app_colors.dart';
import '../../../../../core/theming/app_styles.dart';

class OrderDetailsHeaderWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        FirstSectionOfOrderDetailsHeader(),
        SecondSectionOfOrderDetailsHeader(),
      ],
    );
  }
}

class OrderStatus extends StatelessWidget {
  final String iconPath;
  final String state;
  const OrderStatus({
    super.key,
    required this.iconPath,
    required this.state,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: AppColors.kDarkPrimary,
        borderRadius: BorderRadius.circular(getRadius(5)),
      ),
      child: Padding(
        padding: EdgeInsets.symmetric(
          vertical: getHeight(8),
          horizontal: getWidth(10),
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            SizedBox(
              width: getHeight(20),
              height: getHeight(20),
              child: SvgPicture.asset(
                iconPath,
                color: AppColors.kWhite,
              ),
            ),
            horizontalSpacing(8),
            Text(
              state,
              style: AppStyles.semiBold(
                fontSize: 14,
                textColor: AppColors.kWhite,
                context: context,
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class FirstSectionOfOrderDetailsHeader extends StatelessWidget {
  const FirstSectionOfOrderDetailsHeader({super.key});

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Text(
          context.translate(
            AppStrings.orderDetailsTitleString,
            args: {
              "order_id": "100",
            },
          ),
          style: AppStyles.bold(
            fontSize: 20,
            context: context,
          ),
        ),
        verticalSpacing(18),
        SizedBox(
          width: getWidth(150),
          child: Text(
            "Company name",
            style: AppStyles.medium(
              fontSize: 16,
              textColor: AppColors.kSecondaryText,
              context: context,
            ),
          ),
        ),
      ],
    );
  }
}

class SecondSectionOfOrderDetailsHeader extends StatelessWidget {
  const SecondSectionOfOrderDetailsHeader({super.key});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Text(
          "4 days ago",
          style: AppStyles.medium(
            fontSize: 16,
            textColor: AppColors.kSecondaryText,
            context: context,
          ),
        ),
        verticalSpacing(18),
        OrderStatus(
          iconPath: AppIcons.pendingIcon,
          state: context.translate(
            AppStrings.pendingStateString,
          ),
        ),
      ],
    );
  }
}
