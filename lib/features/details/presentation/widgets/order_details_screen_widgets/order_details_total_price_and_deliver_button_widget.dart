import 'package:flutter/material.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_extensions.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_strings.dart';

import '../../../../../core/helpers/app_sizes.dart';
import '../../../../../core/theming/app_colors.dart';
import '../../../../../core/theming/app_styles.dart';
import '../../../../../core/widgets/button_widget.dart';

class OrderDetailsTotalPriceAndDeliverButtonWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: getHeight(112),
      decoration: BoxDecoration(
        color: AppColors.kSecondary,
        boxShadow: <BoxShadow>[
          BoxShadow(
            color: AppColors.kPrimaryText.withOpacity(0.1),
            blurRadius: 5,
          ),
        ],
      ),
      child: Padding(
        padding: EdgeInsets.symmetric(
          horizontal: getWidth(16),
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  context.translate(
                    AppStrings.overallForOrderString,
                  ),
                  style: AppStyles.bold(
                    fontSize: 20,
                    context: context,
                  ),
                ),
                Text(
                  "70,000",
                  style: AppStyles.bold(
                    fontSize: 20,
                    context: context,
                  ),
                ),
              ],
            ),
            verticalSpacing(14),
            ButtonWidget(
              btnText: context.translate(
                AppStrings.deliverBtnString,
              ),
              onPressFunction: () {},
            ),
          ],
        ),
      ),
    );
  }
}
