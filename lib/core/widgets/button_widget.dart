import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

import '../helpers/app_assets.dart';
import '../helpers/app_sizes.dart';
import '../theming/app_colors.dart';
import '../theming/app_styles.dart';

class ButtonWidget extends StatelessWidget {
  final double btnWidth;
  final double btnHeight;
  final String btnText;
  final void Function()? onPressFunction;
  final Color btnColor;
  final Color btnTextColor;
  final double btnFontSize;
  final bool isWhatsapp;
  final bool isOtpProblem;
  final bool isAuth;
  final double borderRadius;

  const ButtonWidget({
    super.key,
    this.btnHeight = 50,
    required this.btnText,
    required this.onPressFunction,
    this.btnWidth = double.infinity,
    this.btnColor = AppColors.kPrimary,
    this.btnTextColor = AppColors.kWhite,
    this.btnFontSize = 16,
    this.isOtpProblem = false,
    this.isWhatsapp = false,
    this.isAuth = false,
    this.borderRadius = 12,
  });

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: btnWidth,
      height: btnHeight,
      child: ElevatedButton(
        onPressed: onPressFunction,
        style: ElevatedButton.styleFrom(
          backgroundColor: btnColor,
          shape: RoundedRectangleBorder(
            side: BorderSide(
              color: AppColors.kPrimary,
            ),
            borderRadius: BorderRadius.circular(
              getRadius(borderRadius),
            ),
          ),
        ),
        child: isAuth
            ? Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  SizedBox(
                    width: getRadius(26),
                    height: getRadius(26),
                    child: SvgPicture.asset(
                      isWhatsapp ? AppIcons.whatsappIcon : AppIcons.smsIcon,
                      color: isWhatsapp ? AppColors.kPrimary : AppColors.kWhite,
                    ),
                  ),
                  horizontalSpacing(5),
                  Text(
                    btnText,
                    style: AppStyles.semiBold(
                      fontSize: 16,
                      textColor: btnTextColor,
                      context: context,
                    ),
                  ),
                ],
              )
            : Text(
                btnText,
                style: AppStyles.semiBold(
                  fontSize: btnFontSize,
                  context: context,
                  textColor: btnTextColor,
                ),
              ),
      ),
    );
  }
}
