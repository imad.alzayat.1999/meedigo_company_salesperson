import 'package:flutter/material.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_extensions.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_sizes.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_strings.dart';
import 'package:meedigo_company_sales_person/core/theming/app_colors.dart';
import 'package:meedigo_company_sales_person/core/theming/app_styles.dart';

class OrderProductCardWidget extends StatelessWidget {
  const OrderProductCardWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      decoration: BoxDecoration(
        color: AppColors.kSecondary,
        borderRadius: BorderRadius.circular(
          getRadius(10),
        ),
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          FittedBox(
            child: Text(
              "Product Name",
              style: AppStyles.bold(
                fontSize: 14,
                context: context,
              ),
            ),
          ),
          verticalSpacing(10),
          Text(
            "15,000 x 5 = 75,000",
            style: AppStyles.medium(
              fontSize: 14,
              context: context,
            ),
          ),
          verticalSpacing(10),
          Text(
            context.translate(
              AppStrings.fixedOfferString,
              args: {
                "required_quantity": "10",
                "offer_quantity": "5",
              },
            ),
            style: AppStyles.medium(
              fontSize: 14,
              textColor: AppColors.kPrimary,
              context: context,
            ),
          ),
          verticalSpacing(10),
          Text(
            context.translate(
              AppStrings.totalForOrderProductString,
              args: {
                "total_product": "50,000",
              },
            ),
            style: AppStyles.bold(
              fontSize: 14,
              context: context,
            ),
          ),
        ],
      ),
    );
  }
}
