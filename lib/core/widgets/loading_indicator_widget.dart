import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_assets.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_sizes.dart';
import 'package:meedigo_company_sales_person/core/theming/app_colors.dart';

import '../helpers/enums.dart';

class LoadingIndicatorWidget extends StatelessWidget {
  final LoadingSizes loadingSize;
  final bool isRefresh;
  final double btnWidth;
  final double btnHeight;

  const LoadingIndicatorWidget({
    required this.loadingSize,
    this.isRefresh = false,
    this.btnWidth = double.infinity,
    this.btnHeight = 50,
  });

  @override
  Widget build(BuildContext context) {
    switch (loadingSize) {
      case LoadingSizes.fullScreen:
        return Center(
          child: Lottie.asset(
            AppJsons.loadingIndicatorJson,
            width: getHeight(250),
            height: getHeight(250),
          ),
        );
      case LoadingSizes.button:
        return Center(
          child: Lottie.asset(
            AppJsons.loadingIndicatorJson,
            width: getHeight(100),
            height: getHeight(100),
          ),
        );
      case LoadingSizes.bottomSheet:
        return Center(
          child: Lottie.asset(
            AppJsons.loadingIndicatorJson,
            width: getHeight(80),
            height: getHeight(80),
          ),
        );
      case LoadingSizes.loadingMore:
        return Center(
          child: Lottie.asset(
            AppJsons.loadingIndicatorJson,
            width: getHeight(125),
            height: getHeight(125),
          ),
        );
      case LoadingSizes.auth:
        return SizedBox(
          width: btnWidth,
          height: getHeight(btnHeight),
          child: ElevatedButton(
            style: ElevatedButton.styleFrom(
              elevation: 0,
              backgroundColor: AppColors.kPrimary,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(
                  isRefresh ? getRadius(5) : getRadius(15),
                ),
              ),
            ),
            onPressed: () {},
            child: Lottie.asset(
              AppJsons.buttonLoadingIndicatorJson,
              width: getWidth(200),
              height: getHeight(200),
            ),
          ),
        );
      case LoadingSizes.fullScreenAuth:
        return Center(
          child: Lottie.asset(
            AppJsons.buttonLoadingIndicatorDarkJson,
            width: getHeight(250),
            height: getHeight(250),
          ),
        );
    }
  }
}