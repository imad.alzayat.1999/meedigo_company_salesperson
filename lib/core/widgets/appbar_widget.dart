import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

import '../helpers/app_assets.dart';
import '../helpers/app_sizes.dart';
import '../theming/app_colors.dart';
import '../theming/app_styles.dart';

/// Implementing appbar for the application
AppBar appbarWidget({
  required String title,
  required BuildContext context,
  bool isHomePage = false,
  bool isCompaniesPage = false,
  bool isMainPages = false,
  bool isCompanyDetailsPage = false,
  bool isCartPage = false,
  bool isAppBarSearch = false,
  void Function()? onTap = null,
  void Function()? backToMainAppBar = null,
  PreferredSizeWidget? bottom,
}) {
  return AppBar(
    automaticallyImplyLeading: false,
    elevation: 0,
    centerTitle: false,
    backgroundColor: AppColors.kPrimary,
    bottom: bottom,
    title: Text(
      title,
      style: AppStyles.bold(
        fontSize: 20,
        textColor: AppColors.kWhite,
        context: context,
      ),
    ),
    actions: [
      SearchActionButtonWidget(
        isTrue: isHomePage || isCompaniesPage || isCompanyDetailsPage,
        onTapFunction: onTap,
      ),
      CartActionButtonWidget(
        isCartPage: isCartPage,
      ),
    ],
  );
}

/// Search action button for appbar widget
class SearchActionButtonWidget extends StatelessWidget {
  final bool isTrue;
  final void Function()? onTapFunction;
  const SearchActionButtonWidget({
    super.key,
    required this.onTapFunction,
    required this.isTrue,
  });

  @override
  Widget build(BuildContext context) {
    if (isTrue) {
      return Padding(
        padding: EdgeInsets.only(right: getWidth(16)),
        child: GestureDetector(
          onTap: onTapFunction,
          child: Container(
            width: getHeight(25),
            height: getHeight(25),
            child: SvgPicture.asset(
              AppIcons.searchIcon,
              color: AppColors.kWhite,
            ),
          ),
        ),
      );
    } else {
      return Container();
    }
  }
}

/// Implementing cart action button for appbar widget
class CartActionButtonWidget extends StatelessWidget {
  final bool isCartPage;

  const CartActionButtonWidget({
    super.key,
    this.isCartPage = false,
  });

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsetsDirectional.only(end: getWidth(16)),
      child: GestureDetector(
        onTap: () {},
        child: Stack(
          alignment: AlignmentDirectional.topStart,
          children: [
            Center(
              child: SizedBox(
                width: getHeight(35),
                height: getHeight(35),
                child: SvgPicture.asset(
                  AppIcons.cartIcon,
                  color: AppColors.kWhite,
                ),
              ),
            ),
            CartIconBadge(),
          ],
        ),
      ),
    );
  }
}

class CartIconBadge extends StatelessWidget {
  const CartIconBadge({super.key});

  @override
  Widget build(BuildContext context) {
    return Positioned(
      top: 8,
      child: Container(
        width: getHeight(20),
        height: getHeight(20),
        decoration: BoxDecoration(
          shape: BoxShape.circle,
          color: AppColors.kWhite,
        ),
        child: Center(
          child: Text(
            "10",
            style: AppStyles.bold(
              fontSize: 14,
              context: context,
            ),
          ),
        ),
      ),
    );
  }
}
