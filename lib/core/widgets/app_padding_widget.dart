import 'package:flutter/material.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_sizes.dart';

class AppPaddingWidget extends StatelessWidget {
  final double left;
  final double right;
  final double top;
  final double bottom;
  final Widget child;

  const AppPaddingWidget({
    super.key,
    this.left = 16,
    this.right = 16,
    this.bottom = 0,
    this.top = 0,
    required this.child,
  });

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(
        left: getWidth(left),
        right: getWidth(right),
        top: getHeight(top),
        bottom: getHeight(bottom),
      ),
      child: child,
    );
  }
}
