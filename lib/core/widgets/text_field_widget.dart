import 'package:flutter/material.dart';
import 'package:flutter_multi_formatter/flutter_multi_formatter.dart';
import 'package:flutter_svg/flutter_svg.dart';

import '../helpers/app_assets.dart';
import '../helpers/app_sizes.dart';
import '../theming/app_colors.dart';
import '../theming/app_styles.dart';

class TextFieldWidget extends StatefulWidget {
  final TextEditingController textEditingController;
  final TextInputType textInputType;
  final bool isSearch;
  final bool isReadOnly;
  final bool isRegisterPage;
  final bool isFromProfile;
  final void Function()? onTapFunction;
  final String? Function(String?)? onValidate;
  final String hintText;
  final void Function(String)? onChange;
  final String labelText;
  final bool autofocus;
  final int maxLines;

  TextFieldWidget({
    Key? key,
    required this.textEditingController,
    this.isSearch = false,
    this.isReadOnly = false,
    this.isRegisterPage = false,
    this.isFromProfile = false,
    this.textInputType = TextInputType.text,
    this.onTapFunction = null,
    this.onValidate = null,
    this.hintText = "",
    this.onChange = null,
    this.labelText = "",
    this.autofocus = false,
    this.maxLines = 1,
  }) : super(key: key);

  @override
  State<TextFieldWidget> createState() => _TextFieldWidgetState();
}

class _TextFieldWidgetState extends State<TextFieldWidget> {
  /// function to get hint text
  String _getHintText() {
    if (widget.textInputType == TextInputType.phone) {
      if (widget.isFromProfile) {
        return "";
      } else {
        return "0 (9xx) xxx-xxx";
      }
    } else {
      if (widget.hintText == "") {
        return "";
      } else {
        return widget.hintText;
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      onChanged: widget.onChange,
      validator: widget.onValidate,
      readOnly: widget.isReadOnly,
      controller: widget.textEditingController,
      onTap: widget.onTapFunction,
      keyboardType: widget.textInputType,
      autofocus: widget.autofocus,
      showCursor: true,
      textDirection: widget.textInputType == TextInputType.phone
          ? TextDirection.ltr
          : null,
      maxLines: widget.maxLines,
      style: AppStyles.medium(
        fontSize: 16,
        textColor: widget.isReadOnly
            ? AppColors.kPrimaryText.withOpacity(0.5)
            : AppColors.kPrimaryText,
        context: context,
      ),
      inputFormatters: widget.textInputType == TextInputType.phone
          ? [MaskedInputFormatter('# (###) ###-###')]
          : null,
      decoration: InputDecoration(
        errorMaxLines: 2,
        labelText: widget.labelText,
        labelStyle: AppStyles.medium(
          fontSize: 16,
          context: context,
          textColor: AppColors.kSecondaryText,
        ),
        hintText: _getHintText(),
        hintTextDirection: widget.textInputType == TextInputType.phone
            ? TextDirection.ltr
            : null,
        hintStyle: AppStyles.medium(
          fontSize: 16,
          context: context,
          textColor: AppColors.kPrimaryText.withOpacity(0.5),
        ),
        prefixIcon: widget.isSearch
            ? Padding(
                padding: EdgeInsets.symmetric(
                  horizontal: getRadius(12),
                  vertical: getRadius(11),
                ),
                child: SvgPicture.asset(
                  AppIcons.searchIcon,
                  color: AppColors.kPrimary,
                ),
              )
            : null,
        errorStyle: AppStyles.medium(
          fontSize: 12,
          textColor: AppColors.kError,
          context: context,
        ),
        isDense: true,
        fillColor: AppColors.kWhite,
        filled: true,
        // focusedBorder: OutlineInputBorder(
        //   borderSide: BorderSide(color: widget.borderColor),
        //   borderRadius: BorderRadius.circular(getRadius(12)),
        // ),
        // enabledBorder: OutlineInputBorder(
        //   borderSide: BorderSide(color: AppColors.kPrimary),
        //   borderRadius: BorderRadius.circular(getRadius(12)),
        // ),
      ),
    );
  }
}
