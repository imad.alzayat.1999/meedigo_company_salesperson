import 'package:flutter/material.dart';
import '../helpers/app_sizes.dart';
import '../theming/app_colors.dart';
import '../theming/app_styles.dart';

class BottomSheetButtonConfirmWidget extends StatelessWidget {
  final String btnTitle;
  final void Function()? onPressFunction;

  const BottomSheetButtonConfirmWidget({
    super.key,
    required this.onPressFunction,
    required this.btnTitle,
  });

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: getHeight(50),
      child: ElevatedButton(
        onPressed: onPressFunction,
        style: ElevatedButton.styleFrom(
          backgroundColor: AppColors.kPrimary,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(
              getRadius(15),
            ),
          ),
        ),
        child: Row(
          children: [
            Text(
              btnTitle,
              style: AppStyles.semiBold(
                fontSize: 16,
                textColor: AppColors.kWhite,
                context: context,
              ),
            ),
            Expanded(
              child: Text(
                "(1) items",
                style: AppStyles.semiBold(
                  fontSize: 12,
                  textColor: AppColors.kWhite,
                  context: context,
                ),
              ),
            ),
            Text(
              "10,000 S.P",
              style: AppStyles.bold(
                fontSize: 20,
                textColor: AppColors.kWhite,
                context: context,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
