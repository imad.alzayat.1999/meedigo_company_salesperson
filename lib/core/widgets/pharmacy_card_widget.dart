import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_assets.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_consts.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_extensions.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_sizes.dart';
import 'package:meedigo_company_sales_person/core/theming/app_colors.dart';
import 'package:meedigo_company_sales_person/core/theming/app_styles.dart';

import '../../features/pharmacy/entities/popup_menu_item_data.dart';

class PharmacyCardWidget extends StatelessWidget {
  const PharmacyCardWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: getHeight(88),
      decoration: BoxDecoration(
        color: AppColors.kSecondary,
        borderRadius: BorderRadius.circular(
          getRadius(8),
        ),
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Expanded(
            child: Padding(
              padding: EdgeInsets.symmetric(
                horizontal: getWidth(9),
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    "Pharmacy Name",
                    style: AppStyles.bold(
                      fontSize: 14,
                      height: 1.8,
                      context: context,
                    ),
                  ),
                  verticalSpacing(10),
                  Row(
                    children: [
                      SizedBox(
                        height: getRadius(15),
                        width: getRadius(15),
                        child: SvgPicture.asset(
                          AppIcons.locationPinIcon,
                          color: AppColors.kSecondaryText,
                        ),
                      ),
                      horizontalSpacing(3),
                      Text(
                        "location",
                        style: AppStyles.regular(
                          fontSize: 12,
                          textColor: AppColors.kSecondaryText,
                          height: 1.8,
                          context: context,
                        ),
                      ),
                    ],
                  ),
                  verticalSpacing(10),
                  Row(
                    children: [
                      SizedBox(
                        height: getRadius(15),
                        width: getRadius(15),
                        child: SvgPicture.asset(
                          AppIcons.phoneIcon,
                          color: AppColors.kSecondaryText,
                        ),
                      ),
                      horizontalSpacing(3),
                      Text(
                        "+963 (956) 759-576",
                        style: AppStyles.regular(
                          fontSize: 12,
                          textColor: AppColors.kSecondaryText,
                          height: 1.8,
                          context: context,
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
          PharmacyOptions(),
        ],
      ),
    );
  }
}

class PharmacyOptions extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return PopupMenuButton<PopupMenuItemData>(
      onSelected: (value) {},
      itemBuilder: (BuildContext context) => [
        popupMenuOption(
          popupMenuItem: AppConsts.popupMenuListItems[0],
          context: context,
        ),
      ],
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(
          getRadius(10),
        ),
      ),
      child: SvgPicture.asset(
        AppIcons.optionsIcon,
        color: AppColors.kPrimaryText,
      ),
    );
  }
}

/// build popup menu option
PopupMenuEntry<PopupMenuItemData> popupMenuOption({
  required PopupMenuItemData popupMenuItem,
  required BuildContext context,
}) {
  return PopupMenuItem(
    value: popupMenuItem,
    child: Row(
      children: [
        SizedBox(
          width: getHeight(14),
          height: getHeight(14),
          child: SvgPicture.asset(popupMenuItem.iconPath),
        ),
        horizontalSpacing(7),
        Text(
          context.translate(
            popupMenuItem.title,
          ),
          style: AppStyles.regular(
            fontSize: 10,
            context: context,
          ),
        ),
      ],
    ),
  );
}
