import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_sizes.dart';
import 'package:meedigo_company_sales_person/core/theming/app_colors.dart';

import '../helpers/app_assets.dart';

class DeleteOrderAndOrderProductButtonWidget extends StatelessWidget {
  final void Function()? onPressFunction;
  const DeleteOrderAndOrderProductButtonWidget({
    super.key,
    required this.onPressFunction,
  });

  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: AlignmentDirectional.bottomEnd,
      child: SizedBox(
        width: getRadius(35),
        height: getRadius(35),
        child: ElevatedButton(
          onPressed: onPressFunction,
          style: ElevatedButton.styleFrom(
            backgroundColor: AppColors.kPrimary,
            padding: EdgeInsets.zero,
            shape: CircleBorder(),
          ),
          child: Padding(
            padding: EdgeInsets.all(getRadius(4)),
            child: SvgPicture.asset(
              AppIcons.trashIcon,
              color: AppColors.kWhite,
            ),
          ),
        ),
      ),
    );
  }
}
