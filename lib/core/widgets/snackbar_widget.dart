import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_assets.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_sizes.dart';

import '../helpers/enums.dart';
import '../theming/app_colors.dart';
import '../theming/app_styles.dart';

/// function to build snackBar content and show it as popup
showSnackBar({
  required BuildContext context,
  required String message,
  required Color snackBarColor,
  required String iconPath,
  required Color iconColor,
  required Color textColor,
}) {
  final snackBar = SnackBar(
    content: Row(
      children: [
        SizedBox(
          width: getRadius(25),
          height: getRadius(25),
          child: SvgPicture.asset(
            iconPath,
            color: iconColor,
          ),
        ),
        horizontalSpacing(20),
        Expanded(
          child: Text(
            message,
            style: AppStyles.medium(
              fontSize: 14,
              textColor: textColor,
              context: context,
            ),
          ),
        ),
      ],
    ),
    behavior: SnackBarBehavior.floating,
    backgroundColor: snackBarColor,
    padding: EdgeInsets.all(getRadius(8)),
    shape: RoundedRectangleBorder(
      borderRadius: BorderRadius.circular(
        getRadius(8),
      ),
    ),
  );
  ScaffoldMessenger.of(context).showSnackBar(snackBar);
}

/// function to show snackBar by the api state
/// if state of api response is success => show snackBar for success state
/// else if state of api response is failed => show snackBar for failed state
/// else show snackBar for warning cases not for api response states
showSnackBarByItsState({
  required SnackBarStates snackBarStates,
  required BuildContext context,
  required String message,
}) {
  switch (snackBarStates) {
    case SnackBarStates.warning:
      showSnackBar(
        context: context,
        message: message,
        snackBarColor: AppColors.kWarning,
        iconPath: AppIcons.warningIcon,
        iconColor: AppColors.kPrimaryText,
        textColor: AppColors.kPrimaryText,
      );
      break;
    case SnackBarStates.success:
      showSnackBar(
        context: context,
        message: message,
        snackBarColor: AppColors.kPrimary,
        iconPath: AppIcons.checkIcon,
        iconColor: AppColors.kWhite,
        textColor: AppColors.kWhite,
      );
      break;
    case SnackBarStates.error:
      showSnackBar(
        context: context,
        message: message,
        snackBarColor: AppColors.kError,
        iconPath: AppIcons.errorIcon,
        iconColor: AppColors.kWhite,
        textColor: AppColors.kWhite,
      );
      break;
  }
}