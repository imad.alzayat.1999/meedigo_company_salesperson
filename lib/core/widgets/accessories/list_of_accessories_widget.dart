import 'package:flutter/material.dart';

import '../../helpers/app_sizes.dart';
import 'accessory_card_widget.dart';

class ListOfAccessoriesWidget extends StatelessWidget {
  const ListOfAccessoriesWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemBuilder: (context, index) => Padding(
        padding: EdgeInsets.only(
          bottom: getHeight(15),
        ),
        child: AccessoryCardWidget(
          isOffer: index % 2 == 0 ? true : false,
        ),
      ),
      itemCount: 15,
      physics: const BouncingScrollPhysics(),
    );
  }
}
