import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_extensions.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_sizes.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_strings.dart';
import 'package:meedigo_company_sales_person/core/theming/app_colors.dart';

import '../../helpers/app_assets.dart';
import '../../theming/app_styles.dart';

class MedicineCardWidget extends StatelessWidget {
  final bool isOffer;

  const MedicineCardWidget({
    super.key,
    this.isOffer = false,
  });

  /// function to build card content
  Widget _buildRow(BuildContext context) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Expanded(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: EdgeInsets.only(
                  top: getHeight(10),
                  left: getWidth(12),
                  right: getWidth(12),
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    CardTitle(
                      productName: "Product name",
                    ),
                    CardContent(
                      content: context.translate(
                        AppStrings.caliberProductCardString,
                        args: {
                          "caliber": "10MG+10 MG/CTD TAB.",
                        },
                      ),
                    ),
                    CardContent(
                      content: context.translate(
                        AppStrings.packingProductCardString,
                        args: {
                          "packing": "FOR /100/ML.SYRUP IN GLASS BOTTLE",
                        },
                      ),
                    ),
                  ],
                ),
              ),
              verticalSpacing(5),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  CardPrice(
                    price: context.translate(
                      AppStrings.sellingPriceProductCardString,
                      args: {
                        "selling_price": "150,000",
                      },
                    ),
                  ),
                  _buildAddToCartSection(),
                ],
              ),
            ],
          ),
        ),
      ],
    );
  }

  Widget _buildAddToCartSection() {
    return SizedBox(
      height: getHeight(40),
      width: getWidth(40),
      child: ElevatedButton(
        onPressed: () {},
        style: ElevatedButton.styleFrom(
          backgroundColor: AppColors.kPrimary,
          padding: EdgeInsets.all(getRadius(4)),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(getRadius(10)),
          ),
        ),
        child: SizedBox(
          height: getHeight(30),
          width: getHeight(30),
          child: SvgPicture.asset(
            AppIcons.plusIcon,
            color: AppColors.kWhite,
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      decoration: BoxDecoration(
        color: AppColors.kSecondary,
        borderRadius: BorderRadius.circular(
          getRadius(12),
        ),
      ),
      child: Material(
        color: AppColors.kTransparent,
        child: InkWell(
          borderRadius: BorderRadius.circular(
            getRadius(12),
          ),
          onTap: () {},
          child: Stack(
            children: [
              _buildRow(context),
              isOffer ? GiftIcon() : Container(),
            ],
          ),
        ),
      ),
    );
  }
}

class GiftIcon extends StatelessWidget {
  const GiftIcon({super.key});

  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: AlignmentDirectional.topEnd,
      child: Container(
        width: getRadius(40),
        height: getRadius(40),
        decoration: BoxDecoration(
          color: AppColors.kDarkPrimary,
          shape: BoxShape.rectangle,
          borderRadius: BorderRadius.only(
            topLeft: Radius.zero,
            topRight: Radius.zero,
            bottomLeft: Radius.circular(
              getRadius(10),
            ),
            bottomRight: Radius.circular(
              getRadius(10),
            ),
          ),
        ),
        child: Padding(
          padding: EdgeInsets.all(getRadius(6)),
          child: SvgPicture.asset(
            AppIcons.giftIcon,
            color: AppColors.kWhite,
          ),
        ),
      ),
    );
  }
}

class CardTitle extends StatelessWidget {
  final String productName;
  const CardTitle({
    super.key,
    required this.productName,
  });

  @override
  Widget build(BuildContext context) {
    return Text(
      productName,
      maxLines: 1,
      overflow: TextOverflow.ellipsis,
      style: AppStyles.bold(
        fontSize: 14,
        context: context,
        height: 1.2,
      ),
    );
  }
}

class CardContent extends StatelessWidget {
  final String content;
  const CardContent({
    super.key,
    required this.content,
  });

  @override
  Widget build(BuildContext context) {
    return Text(
      content,
      maxLines: 2,
      overflow: TextOverflow.ellipsis,
      style: AppStyles.regular(
        textColor: AppColors.kSecondaryText,
        fontSize: 12,
        context: context,
        height: 1.5,
      ),
    );
  }
}

class CardPrice extends StatelessWidget {
  final String price;
  const CardPrice({
    super.key,
    required this.price,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      width: getWidth(150),
      padding: EdgeInsets.all(getRadius(8)),
      decoration: BoxDecoration(
        color: AppColors.kPrimary,
        borderRadius: BorderRadius.only(
          topRight: Radius.circular(
            getRadius(12),
          ),
          bottomLeft: Radius.circular(
            getRadius(12),
          ),
        ),
      ),
      child: Text(
        price,
        maxLines: 2,
        textAlign: TextAlign.center,
        style: AppStyles.semiBold(
          textColor: AppColors.kWhite,
          fontSize: 12,
          height: 1.5,
          context: context,
        ),
      ),
    );
  }
}
