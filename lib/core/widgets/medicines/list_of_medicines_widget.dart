import 'package:flutter/material.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_sizes.dart';
import 'package:meedigo_company_sales_person/core/widgets/medicines/medicine_card_widget.dart';

class ListOfMedicinesWidget extends StatelessWidget {
  const ListOfMedicinesWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemBuilder: (context, index) => Padding(
        padding: EdgeInsets.only(
          bottom: getHeight(15),
        ),
        child: MedicineCardWidget(
          isOffer: index % 2 == 0 ? true : false,
        ),
      ),
      itemCount: 15,
      physics: const BouncingScrollPhysics(),
    );
  }
}
