import 'package:flutter/material.dart';

import '../helpers/app_sizes.dart';
import '../theming/app_colors.dart';

class CustomTabBarIndicator extends Decoration {
  @override
  BoxPainter createBoxPainter([VoidCallback? onChanged]) {
    return _CustomIndicatorPainter(this, onChanged);
  }
}

class _CustomIndicatorPainter extends BoxPainter {
  final CustomTabBarIndicator decoration;

  _CustomIndicatorPainter(this.decoration, VoidCallback? onChanged)
      : super(onChanged);

  @override
  void paint(Canvas canvas, Offset offset, ImageConfiguration configuration) {
    final rect = offset & configuration.size!;
    final paint = Paint();
    paint.color = AppColors.kWhite; // Customize the color of the indicator

    // Adjust the position and size of the indicator as needed
    final indicatorHeight = 4.0;
    final indicatorBottom = rect.bottom - indicatorHeight;

    final indicatorRect = Rect.fromLTRB(
      rect.left,
      indicatorBottom,
      rect.right,
      rect.bottom,
    );

    // Add border radius to the bottom-left and bottom-right corners
    final borderRadius = BorderRadius.circular(
      getRadius(10),
    );

    final indicatorRRect = RRect.fromRectAndCorners(
      indicatorRect,
      bottomLeft: borderRadius.bottomLeft,
      bottomRight: borderRadius.bottomRight,
      topLeft: borderRadius.topLeft,
      topRight: borderRadius.topRight,
    );

    canvas.drawRRect(indicatorRRect, paint);
  }
}
