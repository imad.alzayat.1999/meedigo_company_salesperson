import 'package:flutter/material.dart';
import 'package:meedigo_company_sales_person/core/theming/app_fonts.dart';

import 'app_colors.dart';

/// Implementing all text styles in the application
class AppStyles {
  static TextStyle baseStyle({
    required double fontSize,
    required Color textColor,
    required double height,
    required FontWeight fontWeight,
    required TextDecoration decoration,
    required BuildContext context,
  }) {
    return TextStyle(
      color: textColor,
      height: height,
      fontWeight: fontWeight,
      decoration: decoration,
      fontFamily: AppFontFamilies.englishFontFamily,
      fontSize: fontSize,
    );
    // if (context.isAr()) {
    //   return TextStyle(
    //     color: textColor,
    //     height: height,
    //     fontWeight: fontWeight,
    //     decoration: decoration,
    //     fontFamily: AppFontFamilies.arabicFontFamily,
    //     fontSize: fontSize,
    //   );
    // } else {
    //   return TextStyle(
    //     color: textColor,
    //     height: height,
    //     fontWeight: fontWeight,
    //     decoration: decoration,
    //     fontFamily: AppFontFamilies.englishFontFamily,
    //     fontSize: fontSize,
    //   );
    // }
  }

  static TextStyle regular({
    required double fontSize,
    required BuildContext context,
    Color textColor = AppColors.kPrimaryText,
    double height = 1,
    TextDecoration decoration = TextDecoration.none,
  }) {
    return baseStyle(
      fontSize: fontSize,
      textColor: textColor,
      height: height,
      fontWeight: AppFontWeights.regular,
      decoration: decoration,
      context: context,
    );
  }

  static TextStyle medium({
    required double fontSize,
    required BuildContext context,
    Color textColor = AppColors.kPrimaryText,
    double height = 1,
    TextDecoration decoration = TextDecoration.none,
  }) {
    return baseStyle(
      fontSize: fontSize,
      textColor: textColor,
      height: height,
      fontWeight: AppFontWeights.medium,
      decoration: decoration,
      context: context,
    );
  }

  static TextStyle semiBold({
    required double fontSize,
    required BuildContext context,
    Color textColor = AppColors.kPrimaryText,
    double height = 1,
    TextDecoration decoration = TextDecoration.none,
  }) {
    return baseStyle(
      fontSize: fontSize,
      textColor: textColor,
      height: height,
      fontWeight: AppFontWeights.semiBold,
      decoration: decoration,
      context: context,
    );
  }

  static TextStyle bold({
    required double fontSize,
    required BuildContext context,
    Color textColor = AppColors.kPrimaryText,
    double height = 1,
    TextDecoration decoration = TextDecoration.none,
    double letterSpacing = 0,
  }) {
    return baseStyle(
      context: context,
      fontSize: fontSize,
      textColor: textColor,
      height: height,
      fontWeight: AppFontWeights.bold,
      decoration: decoration,
    );
  }

  static TextStyle extraBold({
    required double fontSize,
    required BuildContext context,
    Color textColor = AppColors.kPrimaryText,
    double height = 1,
    TextDecoration decoration = TextDecoration.none,
  }) {
    return baseStyle(
      context: context,
      fontSize: fontSize,
      textColor: textColor,
      height: height,
      fontWeight: AppFontWeights.extraBold,
      decoration: decoration,
    );
  }
}
