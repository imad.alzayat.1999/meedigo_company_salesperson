import 'package:flutter/material.dart';

/// Implementing all colors in the application
class AppColors{
  static const Color kTransparent = Colors.transparent;
  static const Color kPrimary = Color(0xff0096C7);
  static const Color kDarkPrimary = Color(0xff017DA6);
  static const Color kPrimaryText = Color(0xff000000);
  static const Color kSecondaryText = Color(0xff292526);
  static const Color kWhite = Color(0xffFFFFFF);
  static const Color kSecondary = Color(0xffCAF0F8);
  static const Color kDarkSecondary = Color(0xff82E9FF);
  static const Color kActivated = Color(0xff212F3C);
  static const Color kError = Color(0xffC21818);
  static const Color kWarning = Color(0xffF5FB21);
  static const Color kOutOfStock = Color(0xffEC1C1C);
  static const Color kDisabled = Color(0xffB0B0B0);
}