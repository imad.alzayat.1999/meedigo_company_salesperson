import 'package:flutter/material.dart';
import 'package:meedigo_company_sales_person/core/theming/app_colors.dart';
import 'package:pinput/pinput.dart';

import '../helpers/app_sizes.dart';
import 'app_styles.dart';

class AppTheme {
  /// get main application's theme
  static ThemeData mainApplicationTheme(BuildContext context) => ThemeData(
    scaffoldBackgroundColor: AppColors.kWhite,
    expansionTileTheme: ExpansionTileThemeData(
      backgroundColor: AppColors.kTransparent,
    ),
    dividerTheme: DividerThemeData(
      color: AppColors.kPrimaryText,
    ),
    bottomNavigationBarTheme: BottomNavigationBarThemeData(
      backgroundColor: AppColors.kPrimary,
      showSelectedLabels: true,
      showUnselectedLabels: true,
      type: BottomNavigationBarType.fixed,
      selectedItemColor: AppColors.kActivated,
      unselectedItemColor: AppColors.kWhite,
      selectedLabelStyle: AppStyles.semiBold(
        fontSize: 12,
        textColor: AppColors.kWhite,
        context: context,
      ),
      unselectedLabelStyle: AppStyles.semiBold(
        fontSize: 12,
        textColor: AppColors.kWhite,
        context: context,
      ),
    )
  );

  /// function to get default theme of pin field
  static PinTheme getDefaultPinTheme(BuildContext context) {
    return PinTheme(
      width: getRadius(53),
      height: getRadius(53),
      textStyle: AppStyles.semiBold(
        context: context,
        fontSize: 18,
      ),
      decoration: BoxDecoration(
        color: AppColors.kWhite,
        borderRadius: BorderRadius.circular(
          getRadius(12),
        ),
        border: Border.all(color: AppColors.kPrimary),
      ),
    );
  }

  /// function to get focused state of pin field theme
  static PinTheme getFocusedStateOfPinTheme(BuildContext context) {
    return getDefaultPinTheme(context).copyDecorationWith(
      border: Border.all(
        color: AppColors.kPrimaryText,
        width: 1.5,
      ),
    );
  }

  /// function to get submitted state of pin field theme
  static PinTheme getSubmittedStateOfPinTheme(BuildContext context) {
    return getDefaultPinTheme(context).copyDecorationWith(
      border: Border.all(
        color: AppColors.kSecondaryText,
        width: 1.5,
      ),
    );
  }

  /// function to get submitted state of pin field theme
  static PinTheme getErrorStateOfPinTheme(BuildContext context) {
    return getDefaultPinTheme(context).copyDecorationWith(
      border: Border.all(
        color: AppColors.kError,
        width: 1.5,
      ),
    );
  }
}
