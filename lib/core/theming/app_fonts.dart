import 'package:flutter/material.dart';

/// Implementing all font families in the application
class AppFontFamilies{
  static const String englishFontFamily = "EncodeSans";
  static const String arabicFontFamily = "Tajawal";
}

/// Implementing all font weights in the application
class AppFontWeights{
  static const FontWeight extraLight = FontWeight.w200;
  static const FontWeight light = FontWeight.w300;
  static const FontWeight regular = FontWeight.w400;
  static const FontWeight medium = FontWeight.w500;
  static const FontWeight semiBold = FontWeight.w600;
  static const FontWeight bold = FontWeight.w700;
  static const FontWeight extraBold = FontWeight.w800;
}