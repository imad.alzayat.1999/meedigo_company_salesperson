// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'location_bloc.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

/// @nodoc
mixin _$LocationEvent {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() getCurrentLocation,
    required TResult Function() checkLocationPermission,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? getCurrentLocation,
    TResult? Function()? checkLocationPermission,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? getCurrentLocation,
    TResult Function()? checkLocationPermission,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_GetCurrentLocation value) getCurrentLocation,
    required TResult Function(_CheckLocationPermission value)
        checkLocationPermission,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_GetCurrentLocation value)? getCurrentLocation,
    TResult? Function(_CheckLocationPermission value)? checkLocationPermission,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_GetCurrentLocation value)? getCurrentLocation,
    TResult Function(_CheckLocationPermission value)? checkLocationPermission,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $LocationEventCopyWith<$Res> {
  factory $LocationEventCopyWith(
          LocationEvent value, $Res Function(LocationEvent) then) =
      _$LocationEventCopyWithImpl<$Res, LocationEvent>;
}

/// @nodoc
class _$LocationEventCopyWithImpl<$Res, $Val extends LocationEvent>
    implements $LocationEventCopyWith<$Res> {
  _$LocationEventCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$GetCurrentLocationImplCopyWith<$Res> {
  factory _$$GetCurrentLocationImplCopyWith(_$GetCurrentLocationImpl value,
          $Res Function(_$GetCurrentLocationImpl) then) =
      __$$GetCurrentLocationImplCopyWithImpl<$Res>;
}

/// @nodoc
class __$$GetCurrentLocationImplCopyWithImpl<$Res>
    extends _$LocationEventCopyWithImpl<$Res, _$GetCurrentLocationImpl>
    implements _$$GetCurrentLocationImplCopyWith<$Res> {
  __$$GetCurrentLocationImplCopyWithImpl(_$GetCurrentLocationImpl _value,
      $Res Function(_$GetCurrentLocationImpl) _then)
      : super(_value, _then);
}

/// @nodoc

class _$GetCurrentLocationImpl implements _GetCurrentLocation {
  const _$GetCurrentLocationImpl();

  @override
  String toString() {
    return 'LocationEvent.getCurrentLocation()';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$GetCurrentLocationImpl);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() getCurrentLocation,
    required TResult Function() checkLocationPermission,
  }) {
    return getCurrentLocation();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? getCurrentLocation,
    TResult? Function()? checkLocationPermission,
  }) {
    return getCurrentLocation?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? getCurrentLocation,
    TResult Function()? checkLocationPermission,
    required TResult orElse(),
  }) {
    if (getCurrentLocation != null) {
      return getCurrentLocation();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_GetCurrentLocation value) getCurrentLocation,
    required TResult Function(_CheckLocationPermission value)
        checkLocationPermission,
  }) {
    return getCurrentLocation(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_GetCurrentLocation value)? getCurrentLocation,
    TResult? Function(_CheckLocationPermission value)? checkLocationPermission,
  }) {
    return getCurrentLocation?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_GetCurrentLocation value)? getCurrentLocation,
    TResult Function(_CheckLocationPermission value)? checkLocationPermission,
    required TResult orElse(),
  }) {
    if (getCurrentLocation != null) {
      return getCurrentLocation(this);
    }
    return orElse();
  }
}

abstract class _GetCurrentLocation implements LocationEvent {
  const factory _GetCurrentLocation() = _$GetCurrentLocationImpl;
}

/// @nodoc
abstract class _$$CheckLocationPermissionImplCopyWith<$Res> {
  factory _$$CheckLocationPermissionImplCopyWith(
          _$CheckLocationPermissionImpl value,
          $Res Function(_$CheckLocationPermissionImpl) then) =
      __$$CheckLocationPermissionImplCopyWithImpl<$Res>;
}

/// @nodoc
class __$$CheckLocationPermissionImplCopyWithImpl<$Res>
    extends _$LocationEventCopyWithImpl<$Res, _$CheckLocationPermissionImpl>
    implements _$$CheckLocationPermissionImplCopyWith<$Res> {
  __$$CheckLocationPermissionImplCopyWithImpl(
      _$CheckLocationPermissionImpl _value,
      $Res Function(_$CheckLocationPermissionImpl) _then)
      : super(_value, _then);
}

/// @nodoc

class _$CheckLocationPermissionImpl implements _CheckLocationPermission {
  const _$CheckLocationPermissionImpl();

  @override
  String toString() {
    return 'LocationEvent.checkLocationPermission()';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$CheckLocationPermissionImpl);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() getCurrentLocation,
    required TResult Function() checkLocationPermission,
  }) {
    return checkLocationPermission();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? getCurrentLocation,
    TResult? Function()? checkLocationPermission,
  }) {
    return checkLocationPermission?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? getCurrentLocation,
    TResult Function()? checkLocationPermission,
    required TResult orElse(),
  }) {
    if (checkLocationPermission != null) {
      return checkLocationPermission();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_GetCurrentLocation value) getCurrentLocation,
    required TResult Function(_CheckLocationPermission value)
        checkLocationPermission,
  }) {
    return checkLocationPermission(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_GetCurrentLocation value)? getCurrentLocation,
    TResult? Function(_CheckLocationPermission value)? checkLocationPermission,
  }) {
    return checkLocationPermission?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_GetCurrentLocation value)? getCurrentLocation,
    TResult Function(_CheckLocationPermission value)? checkLocationPermission,
    required TResult orElse(),
  }) {
    if (checkLocationPermission != null) {
      return checkLocationPermission(this);
    }
    return orElse();
  }
}

abstract class _CheckLocationPermission implements LocationEvent {
  const factory _CheckLocationPermission() = _$CheckLocationPermissionImpl;
}

/// @nodoc
mixin _$LocationState {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() locationLoading,
    required TResult Function(Position position) locationDone,
    required TResult Function(String errorMessage) locationFailed,
    required TResult Function() granted,
    required TResult Function() denied,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? locationLoading,
    TResult? Function(Position position)? locationDone,
    TResult? Function(String errorMessage)? locationFailed,
    TResult? Function()? granted,
    TResult? Function()? denied,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? locationLoading,
    TResult Function(Position position)? locationDone,
    TResult Function(String errorMessage)? locationFailed,
    TResult Function()? granted,
    TResult Function()? denied,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_LocationLoading value) locationLoading,
    required TResult Function(_LocationDone value) locationDone,
    required TResult Function(_LocationFailed value) locationFailed,
    required TResult Function(_Granted value) granted,
    required TResult Function(_Denied value) denied,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_LocationLoading value)? locationLoading,
    TResult? Function(_LocationDone value)? locationDone,
    TResult? Function(_LocationFailed value)? locationFailed,
    TResult? Function(_Granted value)? granted,
    TResult? Function(_Denied value)? denied,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_LocationLoading value)? locationLoading,
    TResult Function(_LocationDone value)? locationDone,
    TResult Function(_LocationFailed value)? locationFailed,
    TResult Function(_Granted value)? granted,
    TResult Function(_Denied value)? denied,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $LocationStateCopyWith<$Res> {
  factory $LocationStateCopyWith(
          LocationState value, $Res Function(LocationState) then) =
      _$LocationStateCopyWithImpl<$Res, LocationState>;
}

/// @nodoc
class _$LocationStateCopyWithImpl<$Res, $Val extends LocationState>
    implements $LocationStateCopyWith<$Res> {
  _$LocationStateCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$InitialImplCopyWith<$Res> {
  factory _$$InitialImplCopyWith(
          _$InitialImpl value, $Res Function(_$InitialImpl) then) =
      __$$InitialImplCopyWithImpl<$Res>;
}

/// @nodoc
class __$$InitialImplCopyWithImpl<$Res>
    extends _$LocationStateCopyWithImpl<$Res, _$InitialImpl>
    implements _$$InitialImplCopyWith<$Res> {
  __$$InitialImplCopyWithImpl(
      _$InitialImpl _value, $Res Function(_$InitialImpl) _then)
      : super(_value, _then);
}

/// @nodoc

class _$InitialImpl implements _Initial {
  const _$InitialImpl();

  @override
  String toString() {
    return 'LocationState.initial()';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$InitialImpl);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() locationLoading,
    required TResult Function(Position position) locationDone,
    required TResult Function(String errorMessage) locationFailed,
    required TResult Function() granted,
    required TResult Function() denied,
  }) {
    return initial();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? locationLoading,
    TResult? Function(Position position)? locationDone,
    TResult? Function(String errorMessage)? locationFailed,
    TResult? Function()? granted,
    TResult? Function()? denied,
  }) {
    return initial?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? locationLoading,
    TResult Function(Position position)? locationDone,
    TResult Function(String errorMessage)? locationFailed,
    TResult Function()? granted,
    TResult Function()? denied,
    required TResult orElse(),
  }) {
    if (initial != null) {
      return initial();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_LocationLoading value) locationLoading,
    required TResult Function(_LocationDone value) locationDone,
    required TResult Function(_LocationFailed value) locationFailed,
    required TResult Function(_Granted value) granted,
    required TResult Function(_Denied value) denied,
  }) {
    return initial(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_LocationLoading value)? locationLoading,
    TResult? Function(_LocationDone value)? locationDone,
    TResult? Function(_LocationFailed value)? locationFailed,
    TResult? Function(_Granted value)? granted,
    TResult? Function(_Denied value)? denied,
  }) {
    return initial?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_LocationLoading value)? locationLoading,
    TResult Function(_LocationDone value)? locationDone,
    TResult Function(_LocationFailed value)? locationFailed,
    TResult Function(_Granted value)? granted,
    TResult Function(_Denied value)? denied,
    required TResult orElse(),
  }) {
    if (initial != null) {
      return initial(this);
    }
    return orElse();
  }
}

abstract class _Initial implements LocationState {
  const factory _Initial() = _$InitialImpl;
}

/// @nodoc
abstract class _$$LocationLoadingImplCopyWith<$Res> {
  factory _$$LocationLoadingImplCopyWith(_$LocationLoadingImpl value,
          $Res Function(_$LocationLoadingImpl) then) =
      __$$LocationLoadingImplCopyWithImpl<$Res>;
}

/// @nodoc
class __$$LocationLoadingImplCopyWithImpl<$Res>
    extends _$LocationStateCopyWithImpl<$Res, _$LocationLoadingImpl>
    implements _$$LocationLoadingImplCopyWith<$Res> {
  __$$LocationLoadingImplCopyWithImpl(
      _$LocationLoadingImpl _value, $Res Function(_$LocationLoadingImpl) _then)
      : super(_value, _then);
}

/// @nodoc

class _$LocationLoadingImpl implements _LocationLoading {
  const _$LocationLoadingImpl();

  @override
  String toString() {
    return 'LocationState.locationLoading()';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$LocationLoadingImpl);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() locationLoading,
    required TResult Function(Position position) locationDone,
    required TResult Function(String errorMessage) locationFailed,
    required TResult Function() granted,
    required TResult Function() denied,
  }) {
    return locationLoading();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? locationLoading,
    TResult? Function(Position position)? locationDone,
    TResult? Function(String errorMessage)? locationFailed,
    TResult? Function()? granted,
    TResult? Function()? denied,
  }) {
    return locationLoading?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? locationLoading,
    TResult Function(Position position)? locationDone,
    TResult Function(String errorMessage)? locationFailed,
    TResult Function()? granted,
    TResult Function()? denied,
    required TResult orElse(),
  }) {
    if (locationLoading != null) {
      return locationLoading();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_LocationLoading value) locationLoading,
    required TResult Function(_LocationDone value) locationDone,
    required TResult Function(_LocationFailed value) locationFailed,
    required TResult Function(_Granted value) granted,
    required TResult Function(_Denied value) denied,
  }) {
    return locationLoading(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_LocationLoading value)? locationLoading,
    TResult? Function(_LocationDone value)? locationDone,
    TResult? Function(_LocationFailed value)? locationFailed,
    TResult? Function(_Granted value)? granted,
    TResult? Function(_Denied value)? denied,
  }) {
    return locationLoading?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_LocationLoading value)? locationLoading,
    TResult Function(_LocationDone value)? locationDone,
    TResult Function(_LocationFailed value)? locationFailed,
    TResult Function(_Granted value)? granted,
    TResult Function(_Denied value)? denied,
    required TResult orElse(),
  }) {
    if (locationLoading != null) {
      return locationLoading(this);
    }
    return orElse();
  }
}

abstract class _LocationLoading implements LocationState {
  const factory _LocationLoading() = _$LocationLoadingImpl;
}

/// @nodoc
abstract class _$$LocationDoneImplCopyWith<$Res> {
  factory _$$LocationDoneImplCopyWith(
          _$LocationDoneImpl value, $Res Function(_$LocationDoneImpl) then) =
      __$$LocationDoneImplCopyWithImpl<$Res>;
  @useResult
  $Res call({Position position});
}

/// @nodoc
class __$$LocationDoneImplCopyWithImpl<$Res>
    extends _$LocationStateCopyWithImpl<$Res, _$LocationDoneImpl>
    implements _$$LocationDoneImplCopyWith<$Res> {
  __$$LocationDoneImplCopyWithImpl(
      _$LocationDoneImpl _value, $Res Function(_$LocationDoneImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? position = null,
  }) {
    return _then(_$LocationDoneImpl(
      null == position
          ? _value.position
          : position // ignore: cast_nullable_to_non_nullable
              as Position,
    ));
  }
}

/// @nodoc

class _$LocationDoneImpl implements _LocationDone {
  const _$LocationDoneImpl(this.position);

  @override
  final Position position;

  @override
  String toString() {
    return 'LocationState.locationDone(position: $position)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$LocationDoneImpl &&
            (identical(other.position, position) ||
                other.position == position));
  }

  @override
  int get hashCode => Object.hash(runtimeType, position);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$LocationDoneImplCopyWith<_$LocationDoneImpl> get copyWith =>
      __$$LocationDoneImplCopyWithImpl<_$LocationDoneImpl>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() locationLoading,
    required TResult Function(Position position) locationDone,
    required TResult Function(String errorMessage) locationFailed,
    required TResult Function() granted,
    required TResult Function() denied,
  }) {
    return locationDone(position);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? locationLoading,
    TResult? Function(Position position)? locationDone,
    TResult? Function(String errorMessage)? locationFailed,
    TResult? Function()? granted,
    TResult? Function()? denied,
  }) {
    return locationDone?.call(position);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? locationLoading,
    TResult Function(Position position)? locationDone,
    TResult Function(String errorMessage)? locationFailed,
    TResult Function()? granted,
    TResult Function()? denied,
    required TResult orElse(),
  }) {
    if (locationDone != null) {
      return locationDone(position);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_LocationLoading value) locationLoading,
    required TResult Function(_LocationDone value) locationDone,
    required TResult Function(_LocationFailed value) locationFailed,
    required TResult Function(_Granted value) granted,
    required TResult Function(_Denied value) denied,
  }) {
    return locationDone(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_LocationLoading value)? locationLoading,
    TResult? Function(_LocationDone value)? locationDone,
    TResult? Function(_LocationFailed value)? locationFailed,
    TResult? Function(_Granted value)? granted,
    TResult? Function(_Denied value)? denied,
  }) {
    return locationDone?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_LocationLoading value)? locationLoading,
    TResult Function(_LocationDone value)? locationDone,
    TResult Function(_LocationFailed value)? locationFailed,
    TResult Function(_Granted value)? granted,
    TResult Function(_Denied value)? denied,
    required TResult orElse(),
  }) {
    if (locationDone != null) {
      return locationDone(this);
    }
    return orElse();
  }
}

abstract class _LocationDone implements LocationState {
  const factory _LocationDone(final Position position) = _$LocationDoneImpl;

  Position get position;
  @JsonKey(ignore: true)
  _$$LocationDoneImplCopyWith<_$LocationDoneImpl> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$LocationFailedImplCopyWith<$Res> {
  factory _$$LocationFailedImplCopyWith(_$LocationFailedImpl value,
          $Res Function(_$LocationFailedImpl) then) =
      __$$LocationFailedImplCopyWithImpl<$Res>;
  @useResult
  $Res call({String errorMessage});
}

/// @nodoc
class __$$LocationFailedImplCopyWithImpl<$Res>
    extends _$LocationStateCopyWithImpl<$Res, _$LocationFailedImpl>
    implements _$$LocationFailedImplCopyWith<$Res> {
  __$$LocationFailedImplCopyWithImpl(
      _$LocationFailedImpl _value, $Res Function(_$LocationFailedImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? errorMessage = null,
  }) {
    return _then(_$LocationFailedImpl(
      null == errorMessage
          ? _value.errorMessage
          : errorMessage // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$LocationFailedImpl implements _LocationFailed {
  const _$LocationFailedImpl(this.errorMessage);

  @override
  final String errorMessage;

  @override
  String toString() {
    return 'LocationState.locationFailed(errorMessage: $errorMessage)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$LocationFailedImpl &&
            (identical(other.errorMessage, errorMessage) ||
                other.errorMessage == errorMessage));
  }

  @override
  int get hashCode => Object.hash(runtimeType, errorMessage);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$LocationFailedImplCopyWith<_$LocationFailedImpl> get copyWith =>
      __$$LocationFailedImplCopyWithImpl<_$LocationFailedImpl>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() locationLoading,
    required TResult Function(Position position) locationDone,
    required TResult Function(String errorMessage) locationFailed,
    required TResult Function() granted,
    required TResult Function() denied,
  }) {
    return locationFailed(errorMessage);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? locationLoading,
    TResult? Function(Position position)? locationDone,
    TResult? Function(String errorMessage)? locationFailed,
    TResult? Function()? granted,
    TResult? Function()? denied,
  }) {
    return locationFailed?.call(errorMessage);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? locationLoading,
    TResult Function(Position position)? locationDone,
    TResult Function(String errorMessage)? locationFailed,
    TResult Function()? granted,
    TResult Function()? denied,
    required TResult orElse(),
  }) {
    if (locationFailed != null) {
      return locationFailed(errorMessage);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_LocationLoading value) locationLoading,
    required TResult Function(_LocationDone value) locationDone,
    required TResult Function(_LocationFailed value) locationFailed,
    required TResult Function(_Granted value) granted,
    required TResult Function(_Denied value) denied,
  }) {
    return locationFailed(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_LocationLoading value)? locationLoading,
    TResult? Function(_LocationDone value)? locationDone,
    TResult? Function(_LocationFailed value)? locationFailed,
    TResult? Function(_Granted value)? granted,
    TResult? Function(_Denied value)? denied,
  }) {
    return locationFailed?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_LocationLoading value)? locationLoading,
    TResult Function(_LocationDone value)? locationDone,
    TResult Function(_LocationFailed value)? locationFailed,
    TResult Function(_Granted value)? granted,
    TResult Function(_Denied value)? denied,
    required TResult orElse(),
  }) {
    if (locationFailed != null) {
      return locationFailed(this);
    }
    return orElse();
  }
}

abstract class _LocationFailed implements LocationState {
  const factory _LocationFailed(final String errorMessage) =
      _$LocationFailedImpl;

  String get errorMessage;
  @JsonKey(ignore: true)
  _$$LocationFailedImplCopyWith<_$LocationFailedImpl> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$GrantedImplCopyWith<$Res> {
  factory _$$GrantedImplCopyWith(
          _$GrantedImpl value, $Res Function(_$GrantedImpl) then) =
      __$$GrantedImplCopyWithImpl<$Res>;
}

/// @nodoc
class __$$GrantedImplCopyWithImpl<$Res>
    extends _$LocationStateCopyWithImpl<$Res, _$GrantedImpl>
    implements _$$GrantedImplCopyWith<$Res> {
  __$$GrantedImplCopyWithImpl(
      _$GrantedImpl _value, $Res Function(_$GrantedImpl) _then)
      : super(_value, _then);
}

/// @nodoc

class _$GrantedImpl implements _Granted {
  const _$GrantedImpl();

  @override
  String toString() {
    return 'LocationState.granted()';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$GrantedImpl);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() locationLoading,
    required TResult Function(Position position) locationDone,
    required TResult Function(String errorMessage) locationFailed,
    required TResult Function() granted,
    required TResult Function() denied,
  }) {
    return granted();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? locationLoading,
    TResult? Function(Position position)? locationDone,
    TResult? Function(String errorMessage)? locationFailed,
    TResult? Function()? granted,
    TResult? Function()? denied,
  }) {
    return granted?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? locationLoading,
    TResult Function(Position position)? locationDone,
    TResult Function(String errorMessage)? locationFailed,
    TResult Function()? granted,
    TResult Function()? denied,
    required TResult orElse(),
  }) {
    if (granted != null) {
      return granted();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_LocationLoading value) locationLoading,
    required TResult Function(_LocationDone value) locationDone,
    required TResult Function(_LocationFailed value) locationFailed,
    required TResult Function(_Granted value) granted,
    required TResult Function(_Denied value) denied,
  }) {
    return granted(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_LocationLoading value)? locationLoading,
    TResult? Function(_LocationDone value)? locationDone,
    TResult? Function(_LocationFailed value)? locationFailed,
    TResult? Function(_Granted value)? granted,
    TResult? Function(_Denied value)? denied,
  }) {
    return granted?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_LocationLoading value)? locationLoading,
    TResult Function(_LocationDone value)? locationDone,
    TResult Function(_LocationFailed value)? locationFailed,
    TResult Function(_Granted value)? granted,
    TResult Function(_Denied value)? denied,
    required TResult orElse(),
  }) {
    if (granted != null) {
      return granted(this);
    }
    return orElse();
  }
}

abstract class _Granted implements LocationState {
  const factory _Granted() = _$GrantedImpl;
}

/// @nodoc
abstract class _$$DeniedImplCopyWith<$Res> {
  factory _$$DeniedImplCopyWith(
          _$DeniedImpl value, $Res Function(_$DeniedImpl) then) =
      __$$DeniedImplCopyWithImpl<$Res>;
}

/// @nodoc
class __$$DeniedImplCopyWithImpl<$Res>
    extends _$LocationStateCopyWithImpl<$Res, _$DeniedImpl>
    implements _$$DeniedImplCopyWith<$Res> {
  __$$DeniedImplCopyWithImpl(
      _$DeniedImpl _value, $Res Function(_$DeniedImpl) _then)
      : super(_value, _then);
}

/// @nodoc

class _$DeniedImpl implements _Denied {
  const _$DeniedImpl();

  @override
  String toString() {
    return 'LocationState.denied()';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$DeniedImpl);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() locationLoading,
    required TResult Function(Position position) locationDone,
    required TResult Function(String errorMessage) locationFailed,
    required TResult Function() granted,
    required TResult Function() denied,
  }) {
    return denied();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? locationLoading,
    TResult? Function(Position position)? locationDone,
    TResult? Function(String errorMessage)? locationFailed,
    TResult? Function()? granted,
    TResult? Function()? denied,
  }) {
    return denied?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? locationLoading,
    TResult Function(Position position)? locationDone,
    TResult Function(String errorMessage)? locationFailed,
    TResult Function()? granted,
    TResult Function()? denied,
    required TResult orElse(),
  }) {
    if (denied != null) {
      return denied();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_LocationLoading value) locationLoading,
    required TResult Function(_LocationDone value) locationDone,
    required TResult Function(_LocationFailed value) locationFailed,
    required TResult Function(_Granted value) granted,
    required TResult Function(_Denied value) denied,
  }) {
    return denied(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_LocationLoading value)? locationLoading,
    TResult? Function(_LocationDone value)? locationDone,
    TResult? Function(_LocationFailed value)? locationFailed,
    TResult? Function(_Granted value)? granted,
    TResult? Function(_Denied value)? denied,
  }) {
    return denied?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_LocationLoading value)? locationLoading,
    TResult Function(_LocationDone value)? locationDone,
    TResult Function(_LocationFailed value)? locationFailed,
    TResult Function(_Granted value)? granted,
    TResult Function(_Denied value)? denied,
    required TResult orElse(),
  }) {
    if (denied != null) {
      return denied(this);
    }
    return orElse();
  }
}

abstract class _Denied implements LocationState {
  const factory _Denied() = _$DeniedImpl;
}
