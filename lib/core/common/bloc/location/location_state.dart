part of 'location_bloc.dart';

@freezed
class LocationState with _$LocationState {
  const factory LocationState.initial() = _Initial;
  const factory LocationState.locationLoading() = _LocationLoading;
  const factory LocationState.locationDone(
    Position position,
  ) = _LocationDone;
  const factory LocationState.locationFailed(
    String errorMessage,
  ) = _LocationFailed;
  const factory LocationState.granted() = _Granted;
  const factory LocationState.denied() = _Denied;
}
