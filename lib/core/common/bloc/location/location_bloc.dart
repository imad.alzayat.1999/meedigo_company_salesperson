import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_extensions.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_preferences.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_strings.dart';
import 'package:meedigo_company_sales_person/core/routing/app_routes.dart';
import 'package:meedigo_company_sales_person/core/theming/app_colors.dart';

part 'location_event.dart';
part 'location_state.dart';
part 'location_bloc.freezed.dart';

class LocationBloc extends Bloc<LocationEvent, LocationState> {
  LocationBloc() : super(_Initial()) {
    on<LocationEvent>((event, emit) async {
      await event.when(
        getCurrentLocation: () async {
          emit(LocationState.locationLoading());
          try {
            final position = await Geolocator.getCurrentPosition(
              desiredAccuracy: LocationAccuracy.best,
            );
            emit(LocationState.locationDone(position));
          } catch (_) {
            emit(
              LocationState.locationFailed(
                AppStrings.getCurrentLocationErrorString,
              ),
            );
          }
        },
        checkLocationPermission: () async {
          bool isGranted = await _getLocationPermission();
          if (isGranted) {
            emit(LocationState.granted());
          } else {
            emit(LocationState.denied());
          }
        },
      );
    });
  }

  /// function to get location permission
  /// if location permission is while in use and always
  /// then the permission is enabled
  /// else the permission is disabled
  Future<bool> _getLocationPermission() async {
    LocationPermission permission = await Geolocator.checkPermission();
    if (permission == LocationPermission.denied) {
      permission = await Geolocator.requestPermission();
      if (permission != LocationPermission.whileInUse &&
          permission != LocationPermission.always) {
        return false;
      }
    }
    return true;
  }

  /// route to on boarding page
  goToTheNextPage({
    required BuildContext context,
  }) {
    bool _isOnBoarding = AppPreferences.getOnBoarding();
    bool _isAuthenticated = AppPreferences.isUserAuthenticated();
    bool _isCheckIn = AppPreferences.getCheckIn();
    if (_isAuthenticated == false) {
      if (_isOnBoarding == true) {
        context.pushNamedAndRemoveUntil(AppRoutes.sendSmsRoute);
      } else {
        context.pushNamedAndRemoveUntil(AppRoutes.onBoardingRoute);
      }
    } else {
      if (_isCheckIn == false) {
        context.pushNamedAndRemoveUntil(AppRoutes.checkInRoute);
      } else {
        // if (products.isEmpty) {
        //   context.pushNamedAndRemoveUntil(AppRoutes.mainRoute);
        // } else {
        //   final product = products[0];
        //   context.pushNamedAndRemoveUntil(
        //     AppRoutes.pharmacyContentRoute,
        //     arguments: PharmacyContentRouteParameters(
        //       marketId: product.marketId,
        //       pharmacyName: product.marketName,
        //     ),
        //   );
        // }
      }
    }
  }

  /// function to get hue color from hexidecimal color
  /// for markers only
  static double getHueFromColor(Color color) {
    final double hue = HSVColor.fromColor(color).hue;
    return hue;
  }

  /// get source marker
  Marker getSourceMarker({required LatLng sourcePosition}) {
    return Marker(
      markerId: const MarkerId('Origin'),
      infoWindow: const InfoWindow(title: 'Origin'),
      icon: BitmapDescriptor.defaultMarkerWithHue(
        getHueFromColor(AppColors.kPrimary),
      ),
      position: sourcePosition,
    );
  }

  /// get destination marker
  Marker getDestinationMarker({required LatLng destinationMarker}) {
    return Marker(
      markerId: const MarkerId('Destination'),
      infoWindow: const InfoWindow(title: 'Destination'),
      icon: BitmapDescriptor.defaultMarkerWithHue(
        getHueFromColor(AppColors.kActivated),
      ),
      position: destinationMarker,
    );
  }
}
