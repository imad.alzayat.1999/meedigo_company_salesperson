import 'package:equatable/equatable.dart';

class TabBarData extends Equatable{
  final String iconPath;
  final String key;

  TabBarData({required this.iconPath, required this.key});

  @override
  // TODO: implement props
  List<Object?> get props => [iconPath , key];
}