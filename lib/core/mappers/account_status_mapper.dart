import 'package:meedigo_company_sales_person/core/helpers/app_extensions.dart';
import 'package:meedigo_company_sales_person/features/main/data/models/account_status_response_model.dart';
import 'package:meedigo_company_sales_person/features/main/domain/entities/account_status_response.dart';

extension AccountStatusDataMapper on AccountStatusDataModel? {
  AccountStatusData toDomain() {
    return AccountStatusData(
      allowOrder: this?.allowOrder.orZero() ?? 0,
      active: this?.active.orZero() ?? 0,
    );
  }
}

extension AccountStatusResponseMapper on AccountStatusResponseModel? {
  AccountStatusResponse toDomain() {
    return AccountStatusResponse(
      accountStatusData: this?.accountStatusDataModel.toDomain(),
    );
  }
}
