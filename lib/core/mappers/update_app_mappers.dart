import 'package:meedigo_company_sales_person/core/helpers/app_extensions.dart';
import 'package:meedigo_company_sales_person/features/versioning/data/models/versioning_response_model.dart';
import 'package:meedigo_company_sales_person/features/versioning/domain/entities/versioning_response.dart';

extension VersioningDataMapper on VersioningDataModel? {
  VersioningData toDomain() {
    return VersioningData(
      required: this?.required.orZero() ?? 0,
      hasUpdate: this?.hasUpdate.orFalse() ?? false,
      dateRange: this?.dateRange.orEmpty() ?? "",
      directUrl: this?.directUrl.orEmpty() ?? "",
      platformUrl: this?.platformUrl.orEmpty() ?? "",
    );
  }
}

extension VersioningResponseMapper on VersioningResponseModel? {
  VersioningResponse toDomain() {
    return VersioningResponse(
      versioningData: this?.versioningDataModel.toDomain(),
    );
  }
}
