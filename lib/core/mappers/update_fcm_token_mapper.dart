import 'package:meedigo_company_sales_person/core/helpers/app_extensions.dart';
import 'package:meedigo_company_sales_person/features/main/data/models/update_fcm_token_response_model.dart';
import 'package:meedigo_company_sales_person/features/main/domain/entities/update_fcm_token_response.dart';

extension UpdateFcmTokenResponseMapper on UpdateFcmTokenResponseModel? {
  UpdateFcmTokenResponse toDomain() {
    return UpdateFcmTokenResponse(
      message: this?.message.orEmpty() ?? "",
    );
  }
}
