import 'package:meedigo_company_sales_person/core/helpers/app_extensions.dart';
import 'package:meedigo_company_sales_person/features/auth/data/models/check_code_response_model.dart';
import 'package:meedigo_company_sales_person/features/auth/domain/entities/check_code.dart';

extension CheckCodeDataMapper on CheckCodeDataModel? {
  CheckCodeData toDomain() {
    return CheckCodeData(
      verificationsCheck: this?.verificationsCheck.orFalse() ?? false,
      verificationsWay: this?.verificationsWay.orEmpty() ?? "",
    );
  }
}

extension CheckCodeResponseMapper on CheckCodeResponseModel? {
  CheckCodeResponse toDomain() {
    return CheckCodeResponse(
      checkCodeData: this?.checkCodeDataModel.toDomain(),
    );
  }
}
