import 'package:meedigo_company_sales_person/core/helpers/app_extensions.dart';
import 'package:meedigo_company_sales_person/features/attendance/data/models/attendance_response_model.dart';
import 'package:meedigo_company_sales_person/features/attendance/domain/entities/attendance_response.dart';

extension AttendanceResponseMapper on AttendanceResponseModel? {
  AttendanceResponse toDomain() {
    return AttendanceResponse(
      message: this?.message.orEmpty() ?? "",
    );
  }
}
