import 'package:meedigo_company_sales_person/core/helpers/app_extensions.dart';
import 'package:meedigo_company_sales_person/features/auth/data/models/login_response_model.dart';
import 'package:meedigo_company_sales_person/features/auth/domain/entities/login.dart';

extension LoginDataMapper on LoginDataModel? {
  LoginData toDomain() {
    List<String> representatives =
        (this?.representatives?.map((e) => e) ?? const Iterable.empty())
            .cast<String>()
            .toList();
    return LoginData(
      salesPersonId: this?.salesPersonId.orZero() ?? 0,
      token: this?.token.orEmpty() ?? "",
      type: this?.type.orEmpty() ?? "",
      companyId: this?.companyId.orZero() ?? 0,
      companyName: this?.companyName.orEmpty() ?? "",
      allowOrder: this?.allowOrder.orZero() ?? 0,
      active: this?.active.orZero() ?? 0,
      representatives: representatives,
    );
  }
}

extension LoginResponseMapper on LoginResponseModel? {
  LoginResponse toDomain() {
    return LoginResponse(
      loginData: this?.loginDataModel.toDomain(),
    );
  }
}
