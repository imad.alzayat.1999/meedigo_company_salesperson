import 'package:meedigo_company_sales_person/core/helpers/app_extensions.dart';
import 'package:meedigo_company_sales_person/features/auth/data/models/generate_code_response_model.dart';
import 'package:meedigo_company_sales_person/features/auth/domain/entities/generate_code.dart';

extension GenerateCodeResponseMapper on GenerateCodeResponseModel? {
  GenerateCodeResponse toDomain() {
    return GenerateCodeResponse(
      message: this?.message.orEmpty() ?? "",
    );
  }
}
