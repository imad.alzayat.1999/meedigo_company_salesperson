import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:dio/dio.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:get_it/get_it.dart';
import 'package:meedigo_company_sales_person/core/networking/network_info.dart';
import 'package:meedigo_company_sales_person/features/auth/data/datasources/auth_remote_datasource.dart';
import 'package:meedigo_company_sales_person/features/auth/data/repository/auth_repository_impl.dart';
import 'package:meedigo_company_sales_person/features/auth/domain/repository/auth_repository.dart';
import 'package:meedigo_company_sales_person/features/auth/domain/usecases/check_code_for_sms_usecase.dart';
import 'package:meedigo_company_sales_person/features/auth/domain/usecases/generate_code_for_sms_usecase.dart';
import 'package:meedigo_company_sales_person/features/auth/domain/usecases/generate_code_for_whatsapp_usecase.dart';
import 'package:meedigo_company_sales_person/features/auth/domain/usecases/login_usecase.dart';
import 'package:meedigo_company_sales_person/features/auth/presentation/bloc/generate_code/generate_code_bloc.dart';
import 'package:meedigo_company_sales_person/features/auth/presentation/bloc/login/login_bloc.dart';
import 'package:meedigo_company_sales_person/features/main/data/datasources/main_remote_datasource.dart';
import 'package:meedigo_company_sales_person/features/main/data/repository/main_repository_impl.dart';
import 'package:meedigo_company_sales_person/features/main/domain/repository/main_repository.dart';
import 'package:meedigo_company_sales_person/features/main/domain/usecases/update_fcm_token_usecase.dart';
import 'package:meedigo_company_sales_person/features/main/presentation/bloc/update_fcm_token/update_fcm_token_bloc.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../features/auth/domain/usecases/check_code_for_whatsapp_usecase.dart';
import '../../features/auth/presentation/bloc/check_code/check_code_bloc.dart';
import '../networking/api_services.dart';
import '../networking/dio_factory.dart';

final getIt = GetIt.asNewInstance();

/// Implementing all dependency injection for the application
class DependencyInjection {
  /// initialize dependency injection
  static Future<void> setup() async {
    /// local storage
    final sharedPreferences = await SharedPreferences.getInstance();
    getIt.registerLazySingleton(() => sharedPreferences);

    /// FlutterSecureStorage
    final flutterSecureStorage = FlutterSecureStorage();
    getIt.registerLazySingleton(() => flutterSecureStorage);

    /// dio & api service
    Dio dio = DioFactory.getDio();
    getIt.registerLazySingleton<ApiService>(
      () => ApiService(dio),
    );

    /// connectivity & internet connection check
    getIt.registerLazySingleton<Connectivity>(
      () => Connectivity(),
    );
    getIt.registerLazySingleton<NetworkInfo>(
      () => NetworkInfoImplementation(
        connectivity: getIt(),
      ),
    );

    /// DataSources (remote , local)
    getIt.registerLazySingleton<AuthRemoteDataSource>(
      () => AuthRemoteDataSourceImpl(
        apiService: getIt(),
      ),
    );
    getIt.registerLazySingleton<MainRemoteDataSource>(
      () => MainRemoteDataSourceImpl(
        apiService: getIt(),
      ),
    );

    /// Repositories
    getIt.registerLazySingleton<AuthRepository>(
      () => AuthRepositoryImpl(
        authRemoteDataSource: getIt(),
      ),
    );
    getIt.registerLazySingleton<MainRepository>(
      () => MainRepositoryImpl(
        mainRemoteDataSource: getIt(),
      ),
    );

    /// UseCases
    getIt.registerLazySingleton<GenerateCodeForSmsUseCase>(
      () => GenerateCodeForSmsUseCase(
        authRepository: getIt(),
      ),
    );
    getIt.registerLazySingleton<GenerateCodeForWhatsappUseCase>(
      () => GenerateCodeForWhatsappUseCase(
        authRepository: getIt(),
      ),
    );
    getIt.registerLazySingleton<CheckCodeForSmsUseCase>(
      () => CheckCodeForSmsUseCase(
        authRepository: getIt(),
      ),
    );
    getIt.registerLazySingleton<CheckCodeForWhatsappUseCase>(
      () => CheckCodeForWhatsappUseCase(
        authRepository: getIt(),
      ),
    );
    getIt.registerLazySingleton<LoginUseCase>(
      () => LoginUseCase(
        authRepository: getIt(),
      ),
    );
    getIt.registerLazySingleton<UpdateFcmTokenUseCase>(
      () => UpdateFcmTokenUseCase(
        mainRepository: getIt(),
      ),
    );

    /// Bloc & Cubit
    getIt.registerFactory<GenerateCodeBloc>(
      () => GenerateCodeBloc(
        generateCodeForSmsUseCase: getIt(),
        generateCodeForWhatsappUseCase: getIt(),
      ),
    );
    getIt.registerFactory<CheckCodeBloc>(
      () => CheckCodeBloc(
        checkCodeForSmsUseCase: getIt(),
        checkCodeForWhatsappUseCase: getIt(),
      ),
    );
    getIt.registerFactory<LoginBloc>(
      () => LoginBloc(
        loginUseCase: getIt(),
      ),
    );
    getIt.registerFactory<UpdateFcmTokenBloc>(
      () => UpdateFcmTokenBloc(
        updateFcmTokenUseCase: getIt(),
      ),
    );
  }
}
