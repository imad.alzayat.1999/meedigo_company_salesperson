import 'package:equatable/equatable.dart';
import 'package:meedigo_company_sales_person/features/versioning/domain/entities/versioning_response.dart';

/// Implementing all route parameters for the application

/// update app screen route parameters
class UpdateAppScreenRouteParameters extends Equatable {
  final VersioningData versioningData;

  UpdateAppScreenRouteParameters({
    required this.versioningData,
  });

  @override
  List<Object?> get props => [versioningData];
}

/// update app dialog route parameters
class UpdateAppDialogRouteParameters extends Equatable {
  final VersioningData versioningData;

  UpdateAppDialogRouteParameters({
    required this.versioningData,
  });

  @override
  // TODO: implement props
  List<Object?> get props => [versioningData];
}
