/// Implementing all routes of the application
class AppRoutes{
  static const String splashRoute = "/";
  static const String updateVersionRoute = "/update_version";
  static const String locationPermissionRoute = "/location_permission";
  static const String onBoardingRoute = "/on_boarding";
  static const String sendSmsRoute = "/send_sms";
  static const String verifyOtpRoute = "/verify_otp";
  static const String checkInRoute = "/check_in";
  static const String mainRoute = "/main";
  static const String pharmaciesByWarehouseRoute = "/pharmacies_by_warehouse";
  static const String searchForPharmacyRoute = "/search_for_pharmacy";
  static const String pharmacyLocationRoute = "/pharmacy_location";
  static const String createNewPharmacyRoute = "/create_new_pharmacy";
  static const String pickLocationRoute = "/pick_location";
  static const String pharmacyProductsRoute = "/pharmacy_products";
  static const String productDetailsRoute = "/product_details";
  static const String productsByBarcodeRoute = "/products_by_barcode";
  static const String searchForProductRoute = "/search_for_product";
  static const String orderDetailsRoute = "/order_details";
  static const String offlineOrderDetailsRoute = "/offline_order_details";
  static const String personalInformationRoute = "/personal_information";
  static const String myStatisticsRoute = "/my_statistics";
  static const String cartRoute = "/cart";
  static const String accountBlockedRoute = "/account_blocked";
}