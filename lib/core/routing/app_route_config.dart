import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:meedigo_company_sales_person/core/di/dependency_injection.dart';
import 'package:meedigo_company_sales_person/core/routing/app_routes.dart';
import 'package:meedigo_company_sales_person/core/routing/route_parameters.dart';
import 'package:meedigo_company_sales_person/features/attendance/presentation/bloc/attendance_bloc.dart';
import 'package:meedigo_company_sales_person/features/attendance/presentation/screens/check_in_screen.dart';
import 'package:meedigo_company_sales_person/features/auth/presentation/bloc/check_code/check_code_bloc.dart';
import 'package:meedigo_company_sales_person/features/auth/presentation/bloc/generate_code/generate_code_bloc.dart';
import 'package:meedigo_company_sales_person/features/auth/presentation/bloc/login/login_bloc.dart';
import 'package:meedigo_company_sales_person/features/auth/presentation/screens/send_sms_screen.dart';
import 'package:meedigo_company_sales_person/features/auth/presentation/screens/verify_otp_screen.dart';
import 'package:meedigo_company_sales_person/features/barcode/presentation/screens/barcode_scan_result_screen.dart';
import 'package:meedigo_company_sales_person/features/cart/presentation/screens/cart_screen.dart';
import 'package:meedigo_company_sales_person/features/details/presentation/screens/order_details/order_details_screen.dart';
import 'package:meedigo_company_sales_person/features/details/presentation/screens/product_details/product_details_screen.dart';
import 'package:meedigo_company_sales_person/features/enable_location/presentation/screen/enable_location_screen.dart';
import 'package:meedigo_company_sales_person/features/main/presentation/bloc/account_status/account_status_bloc.dart';
import 'package:meedigo_company_sales_person/features/main/presentation/bloc/update_fcm_token/update_fcm_token_bloc.dart';
import 'package:meedigo_company_sales_person/features/main/presentation/screens/main_screen.dart';
import 'package:meedigo_company_sales_person/features/map/presentation/screens/pharmacy_location_screen.dart';
import 'package:meedigo_company_sales_person/features/map/presentation/screens/pick_location_screen.dart';
import 'package:meedigo_company_sales_person/features/on_boarding/presentation/screens/on_boarding_screen.dart';
import 'package:meedigo_company_sales_person/features/page_not_found_screen.dart';
import 'package:meedigo_company_sales_person/features/pharmacy/presentation/screens/create_pharmacy_screen.dart';
import 'package:meedigo_company_sales_person/features/pharmacy/presentation/screens/pharmacies_screen.dart';
import 'package:meedigo_company_sales_person/features/pharmacy/presentation/screens/pharmacy_products_screen.dart';
import 'package:meedigo_company_sales_person/features/profile/presentation/screens/account_blocked_screen.dart';
import 'package:meedigo_company_sales_person/features/profile/presentation/screens/my_statistics_screen.dart';
import 'package:meedigo_company_sales_person/features/profile/presentation/screens/personal_information_screen.dart';
import 'package:meedigo_company_sales_person/features/saved/presentation/screens/saved_order_details_screen.dart';
import 'package:meedigo_company_sales_person/features/search/presentation/screens/search_for_pharmacy_screen.dart';
import 'package:meedigo_company_sales_person/features/search/presentation/screens/search_for_products_screen.dart';
import 'package:meedigo_company_sales_person/features/splash/presentation/splash_screen.dart';
import 'package:meedigo_company_sales_person/features/versioning/presentation/bloc/versioning_bloc.dart';
import 'package:meedigo_company_sales_person/features/versioning/presentation/screens/update_version_screen.dart';

/// Implement all route configurations
class AppRouteConfig {
  /// function to handle routing configuration
  Route<dynamic> onGenerateRoute(RouteSettings settings) {
    switch (settings.name) {
      case AppRoutes.splashRoute:
        return MaterialPageRoute(
          builder: (context) => BlocProvider(
            create: (context) =>
                getIt<VersioningBloc>()..checkAvailableUpdate(),
            child: SplashScreen(),
          ),
        );
      case AppRoutes.updateVersionRoute:
        final args = settings.arguments as UpdateAppScreenRouteParameters;
        return MaterialPageRoute(
          builder: (context) => BlocProvider(
            create: (context) => getIt<VersioningBloc>(),
            child: UpdateVersionScreen(
              parameters: args,
            ),
          ),
        );
      case AppRoutes.locationPermissionRoute:
        return MaterialPageRoute(
          builder: (context) => EnableLocationScreen(),
        );
      case AppRoutes.onBoardingRoute:
        return MaterialPageRoute(
          builder: (context) => OnBoardingScreen(),
        );
      case AppRoutes.sendSmsRoute:
        return MaterialPageRoute(
          builder: (context) => BlocProvider(
            create: (context) => getIt<GenerateCodeBloc>(),
            child: SendSmsScreen(),
          ),
        );
      case AppRoutes.verifyOtpRoute:
        return MaterialPageRoute(
          builder: (context) => MultiBlocProvider(
            providers: [
              BlocProvider(
                create: (context) => getIt<CheckCodeBloc>(),
              ),
              BlocProvider(
                create: (context) => getIt<GenerateCodeBloc>(),
              ),
              BlocProvider(
                create: (context) => getIt<LoginBloc>(),
              ),
            ],
            child: VerifyOtpScreen(),
          ),
        );
      case AppRoutes.checkInRoute:
        return MaterialPageRoute(
          builder: (context) => CheckInScreen(),
        );
      case AppRoutes.mainRoute:
        return MaterialPageRoute(
          builder: (context) => MultiBlocProvider(
            providers: [
              BlocProvider(
                create: (context) => getIt<AttendanceBloc>(),
              ),
              BlocProvider(create: (context) => getIt<AccountStatusBloc>(),),
              BlocProvider(
                create: (context) => getIt<UpdateFcmTokenBloc>(),
              ),
            ],
            child: MainScreen(),
          ),
        );
      case AppRoutes.pharmaciesByWarehouseRoute:
        return MaterialPageRoute(
          builder: (context) => PharmaciesScreen(),
        );
      case AppRoutes.pharmacyLocationRoute:
        return MaterialPageRoute(
          builder: (context) => PharmacyLocationScreen(),
        );
      case AppRoutes.createNewPharmacyRoute:
        return MaterialPageRoute(
          builder: (context) => CreatePharmacyScreen(),
        );
      case AppRoutes.pickLocationRoute:
        return MaterialPageRoute(
          builder: (context) => PickLocationScreen(),
        );
      case AppRoutes.searchForPharmacyRoute:
        return MaterialPageRoute(
          builder: (context) => SearchForPharmacyScreen(),
        );
      case AppRoutes.pharmacyProductsRoute:
        return MaterialPageRoute(
          builder: (context) => PharmacyProductsScreen(),
        );
      case AppRoutes.productDetailsRoute:
        return MaterialPageRoute(
          builder: (context) => ProductDetailsScreen(),
        );
      case AppRoutes.productsByBarcodeRoute:
        return MaterialPageRoute(
          builder: (context) => ProductsByBarcodeScreen(),
        );
      case AppRoutes.searchForProductRoute:
        return MaterialPageRoute(
          builder: (context) => SearchForProductsScreen(),
        );
      case AppRoutes.offlineOrderDetailsRoute:
        return MaterialPageRoute(
          builder: (context) => SavedOrderDetailsScreen(),
        );
      case AppRoutes.orderDetailsRoute:
        return MaterialPageRoute(
          builder: (context) => OrderDetailsScreen(),
        );
      case AppRoutes.personalInformationRoute:
        return MaterialPageRoute(
          builder: (context) => PersonalInformationScreen(),
        );
      case AppRoutes.myStatisticsRoute:
        return MaterialPageRoute(
          builder: (context) => MyStatisticsScreen(),
        );
      case AppRoutes.cartRoute:
        return MaterialPageRoute(
          builder: (context) => CartScreen(),
        );
      case AppRoutes.accountBlockedRoute:
        return MaterialPageRoute(
          builder: (context) => AccountBlockedScreen(),
        );
      default:
        return MaterialPageRoute(
          builder: (context) => PageNotFoundScreen(),
        );
    }
  }
}
