import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'app_localizations_delegate.dart';

class AppLocalizations {
  final Locale locale;
  AppLocalizations(this.locale);
  static AppLocalizations? of(BuildContext context) {
    return Localizations.of<AppLocalizations>(context, AppLocalizations);
  }

  static const LocalizationsDelegate<AppLocalizations> delegate =
      AppLocalizationsDelegate();
  late Map<String, String> _localizedStrings;

  Future<void> load() async {
    String jsonString = await rootBundle.loadString(
      'assets/lang/${locale.languageCode}.json',
    );
    Map<String, dynamic> jsonMap = json.decode(jsonString);
    _localizedStrings = jsonMap.map<String, String>((key, value) {
      return MapEntry(key, value.toString());
    });
  }

  String translateWithArgs(String key, Map<String, dynamic> args) {
    String translation = _localizedStrings[key] ?? "";

    // Replace placeholders in the translation string with actual values from args
    args.forEach((key, value) {
      translation = translation.replaceAll("{$key}", value.toString());
    });

    return translation;
  }

  bool get isEnLocale => locale.languageCode == 'en';

  bool get isArLocale => locale.languageCode == "ar";
}
