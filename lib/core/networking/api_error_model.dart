import 'package:json_annotation/json_annotation.dart';
part 'api_error_model.g.dart';

@JsonSerializable()
class ApiErrorModel {
  @JsonKey(name: "code")
  final int? code;

  @JsonKey(name: "message")
  final String? message;

  ApiErrorModel({required this.code, required this.message});

  factory ApiErrorModel.fromJson(Map<String, dynamic> json) =>
      _$ApiErrorModelFromJson(json);

  Map<String , dynamic> toJson() => _$ApiErrorModelToJson(this);
}

class LocalException implements Exception{
  final String message;

  LocalException({required this.message});
}