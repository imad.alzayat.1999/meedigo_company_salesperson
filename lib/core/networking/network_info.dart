import 'package:connectivity_plus/connectivity_plus.dart';

abstract class NetworkInfo {
  Future<ConnectivityResult> get connectivityResult;
}

class NetworkInfoImplementation implements NetworkInfo {
  final Connectivity connectivity;

  NetworkInfoImplementation({required this.connectivity});

  @override
  Future<ConnectivityResult> get connectivityResult =>
      connectivity.checkConnectivity();
}