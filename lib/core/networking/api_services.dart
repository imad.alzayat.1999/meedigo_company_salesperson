import 'package:dio/dio.dart';
import 'package:meedigo_company_sales_person/core/networking/api_requests.dart';
import 'package:meedigo_company_sales_person/core/networking/end_points.dart';
import 'package:meedigo_company_sales_person/features/attendance/data/models/attendance_response_model.dart';
import 'package:meedigo_company_sales_person/features/auth/data/models/check_code_response_model.dart';
import 'package:meedigo_company_sales_person/features/auth/data/models/generate_code_response_model.dart';
import 'package:meedigo_company_sales_person/features/auth/data/models/login_response_model.dart';
import 'package:meedigo_company_sales_person/features/main/data/models/update_fcm_token_response_model.dart';
import 'package:meedigo_company_sales_person/features/versioning/data/models/versioning_response_model.dart';
import 'package:retrofit/retrofit.dart';

import '../../features/main/data/models/account_status_response_model.dart';

part 'api_services.g.dart';

@RestApi()
abstract class ApiService {
  factory ApiService(Dio dio, {String baseUrl}) = _ApiService;

  @POST(EndPoints.generateCodeForSmsEndPoint)
  Future<GenerateCodeResponseModel> generateSmsOtpCode({
    @Body()
    required GenerateCodeForSmsAndWhatsappRequestBody
        generateCodeForSmsAndWhatsappRequestBody,
    @Header("App-Version") required String appVersion,
    @Header("Platform") required String platform,
  });

  @POST(EndPoints.generateCodeForWhatsappEndPoint)
  Future<GenerateCodeResponseModel> generateWhatsappOtpCode({
    @Body()
    required GenerateCodeForSmsAndWhatsappRequestBody
        generateCodeForSmsAndWhatsappRequestBody,
    @Header("App-Version") required String appVersion,
    @Header("Platform") required String platform,
  });

  @POST(EndPoints.checkCodeForSmsEndPoint)
  Future<CheckCodeResponseModel> checkCodeForSms({
    @Body()
    required CheckCodeForSmsAndWhatsappRequestBody
        checkCodeForSmsAndWhatsappRequestBody,
    @Header("App-Version") required String appVersion,
    @Header("Platform") required String platform,
  });

  @POST(EndPoints.checkCodeForWhatsappEndPoint)
  Future<CheckCodeResponseModel> checkCodeForWhatsapp({
    @Body()
    required CheckCodeForSmsAndWhatsappRequestBody
        checkCodeForSmsAndWhatsappRequestBody,
    @Header("App-Version") required String appVersion,
    @Header("Platform") required String platform,
  });

  @POST(EndPoints.loginEndPoint)
  Future<LoginResponseModel> login({
    @Body() required LoginRequestBody loginRequestBody,
    @Header("App-Version") required String appVersion,
    @Header("Platform") required String platform,
  });

  @POST(EndPoints.checkInEndPoint)
  Future<AttendanceResponseModel> checkIn({
    @Body() required AttendanceRequestBody attendanceRequestBody,
  });

  @POST(EndPoints.checkoutEndPoint)
  Future<AttendanceResponseModel> checkOut({
    @Body() required AttendanceRequestBody attendanceRequestBody,
  });

  @POST(EndPoints.createLocationEndPoint)
  Future<AttendanceResponseModel> createLocation({
    @Body() required AttendanceRequestBody attendanceRequestBody,
  });

  @GET(EndPoints.updateAppVersionEndPoint)
  Future<VersioningResponseModel> updateApp({
    @Header("App-Version") required String appVersion,
    @Header("Platform") required String platform,
    @Query("platform") required String appPlatform,
    @Query("version") required String version,
    @Query("app_name") required String appName,
  });

  @GET(EndPoints.getAccountStatusEndPoint)
  Future<AccountStatusResponseModel> getAccountStatus({
    @Path("id") required String id,
  });

  @PUT(EndPoints.updateFcmTokenEndPoint)
  Future<UpdateFcmTokenResponseModel> updateFcmToken({
    @Body() required UpdateFcmTokenRequestBody updateFcmTokenRequestBody,
    @Path("id") required int id,
  });
}
