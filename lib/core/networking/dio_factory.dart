import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_consts.dart';
import 'package:meedigo_company_sales_person/core/networking/end_points.dart';
import 'package:pretty_dio_logger/pretty_dio_logger.dart';

/// implementing dio configuration 
/// for handle with application api

class DioFactory {
  /// private constructor as I don't want to allow creating an instance of this class
  DioFactory._();

  static Dio? dio;

  static Dio getDio() {
    Duration timeOut = const Duration(
      seconds: AppConsts.dioTimeoutDuration,
    );

    /// request headers
    Map<String, dynamic> headers = {
      "Content-Type": "application/json",
      "Accept": "application/json"
    };

    if (dio == null) {
      dio = Dio();
      dio!
        ..options.connectTimeout = timeOut
        ..options.receiveTimeout = timeOut;
      dio!.options.baseUrl = EndPoints.developmentApiBaseUrl;
      dio!.options.headers = headers;
      addDioInterceptor();
      return dio!;
    } else {
      return dio!;
    }
  }

  static void addDioInterceptor() {
    dio?.interceptors.add(
      PrettyDioLogger(
        requestBody: true,
        requestHeader: true,
        responseHeader: true,
      ),
    );
  }

  static String getMainUrl() {
    if (kDebugMode) {
      return EndPoints.developmentApiBaseUrl;
    } else {
      return EndPoints.productionApiBaseUrl;
    }
  }
}