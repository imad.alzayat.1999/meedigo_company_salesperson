import 'package:equatable/equatable.dart';

/// Implementing all query parameters for all the application's api requests

/// update app api request query parameters
class UpdateAppRequestQueryParameters extends Equatable {
  final String appName;
  final String platform;
  final String version;

  UpdateAppRequestQueryParameters({
    this.appName = "company_salesperson",
    required this.platform,
    required this.version,
  });

  @override
  // TODO: implement props
  List<Object?> get props => [appName, platform, version];
}