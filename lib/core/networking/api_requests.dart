import 'package:json_annotation/json_annotation.dart';
part 'api_requests.g.dart';

@JsonSerializable()
class GenerateCodeForSmsAndWhatsappRequestBody {
  @JsonKey(name: "phone_number")
  final String phoneNumber;

  GenerateCodeForSmsAndWhatsappRequestBody({
    required this.phoneNumber,
  });

  Map<String, dynamic> toJson() =>
      _$GenerateCodeForSmsAndWhatsappRequestBodyToJson(this);
}

@JsonSerializable()
class CheckCodeForSmsAndWhatsappRequestBody {
  @JsonKey(name: "phone_number")
  final String phoneNumber;

  @JsonKey(name: "code")
  final String code;

  CheckCodeForSmsAndWhatsappRequestBody({
    required this.phoneNumber,
    required this.code,
  });

  Map<String, dynamic> toJson() =>
      _$CheckCodeForSmsAndWhatsappRequestBodyToJson(this);
}

@JsonSerializable()
class LoginRequestBody {
  @JsonKey(name: "phone_number")
  final String phoneNumber;

  @JsonKey(name: "code")
  final String code;

  @JsonKey(name: "firebase_token")
  final String firebaseToken;

  LoginRequestBody({
    required this.phoneNumber,
    required this.code,
    required this.firebaseToken,
  });

  Map<String, dynamic> toJson() => _$LoginRequestBodyToJson(this);
}

@JsonSerializable()
class AttendanceRequestBody {
  @JsonKey(name: "longitude")
  final String longitude;

  @JsonKey(name: "latitude")
  final String latitude;

  @JsonKey(name: "salesperson_id")
  final int salespersonId;

  AttendanceRequestBody({
    required this.longitude,
    required this.latitude,
    required this.salespersonId,
  });

  Map<String, dynamic> toJson() => _$AttendanceRequestBodyToJson(this);
}

@JsonSerializable()
class UpdateFcmTokenRequestBody {
  @JsonKey(name: "firebase_token")
  final String firebaseToken;

  UpdateFcmTokenRequestBody({
    required this.firebaseToken,
  });

  Map<String, dynamic> toJson() => _$UpdateFcmTokenRequestBodyToJson(this);
}
