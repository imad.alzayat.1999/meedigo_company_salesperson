/// implementing all application's api endpoints
class EndPoints {
  /// api base url (test , production)
  static const String developmentApiBaseUrl = "https://test.meedigo.com/api/v1";
  static const String productionApiBaseUrl = "https://app.meedigo.com/api/v1";

  /// application api endpoints
  /// auth apis
  static const String generateCodeForSmsEndPoint = "/verifications";
  static const String generateCodeForWhatsappEndPoint =
      "/verifications_whatsapp";
  static const String checkCodeForSmsEndPoint = "/verification_check";
  static const String checkCodeForWhatsappEndPoint =
      "/verification_check_whatsapp";
  static const String loginEndPoint = "/login";

  /// attendance apis
  static const String checkInEndPoint = "/attendance/checkin";
  static const String checkoutEndPoint = "/attendance/checkout";
  static const String createLocationEndPoint = "/location/create";

  /// versioning apis
  static const String updateAppVersionEndPoint = "/available_update";

  /// account apis
  static const String getAccountStatusEndPoint =
      "/salesperson/{id}/availability";
  static const String updateFcmTokenEndPoint =
      "/salesperson/{id}/update_firebase_token";
}
