abstract class Failure {
  final int? statusCode;
  final String? errorMessage;

  Failure({
    required this.errorMessage,
    required this.statusCode,
  });
}

class ServerFailure extends Failure {
  ServerFailure({
    required super.errorMessage,
    required super.statusCode,
  });
}

class LocalFailure extends Failure {
  LocalFailure(String errorMessage)
      : super(
          errorMessage: errorMessage,
          statusCode: 0,
        );
}
