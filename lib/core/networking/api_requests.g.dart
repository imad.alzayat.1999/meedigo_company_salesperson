// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'api_requests.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

GenerateCodeForSmsAndWhatsappRequestBody
    _$GenerateCodeForSmsAndWhatsappRequestBodyFromJson(
            Map<String, dynamic> json) =>
        GenerateCodeForSmsAndWhatsappRequestBody(
          phoneNumber: json['phone_number'] as String,
        );

Map<String, dynamic> _$GenerateCodeForSmsAndWhatsappRequestBodyToJson(
        GenerateCodeForSmsAndWhatsappRequestBody instance) =>
    <String, dynamic>{
      'phone_number': instance.phoneNumber,
    };

CheckCodeForSmsAndWhatsappRequestBody
    _$CheckCodeForSmsAndWhatsappRequestBodyFromJson(
            Map<String, dynamic> json) =>
        CheckCodeForSmsAndWhatsappRequestBody(
          phoneNumber: json['phone_number'] as String,
          code: json['code'] as String,
        );

Map<String, dynamic> _$CheckCodeForSmsAndWhatsappRequestBodyToJson(
        CheckCodeForSmsAndWhatsappRequestBody instance) =>
    <String, dynamic>{
      'phone_number': instance.phoneNumber,
      'code': instance.code,
    };

LoginRequestBody _$LoginRequestBodyFromJson(Map<String, dynamic> json) =>
    LoginRequestBody(
      phoneNumber: json['phone_number'] as String,
      code: json['code'] as String,
      firebaseToken: json['firebase_token'] as String,
    );

Map<String, dynamic> _$LoginRequestBodyToJson(LoginRequestBody instance) =>
    <String, dynamic>{
      'phone_number': instance.phoneNumber,
      'code': instance.code,
      'firebase_token': instance.firebaseToken,
    };

AttendanceRequestBody _$AttendanceRequestBodyFromJson(
        Map<String, dynamic> json) =>
    AttendanceRequestBody(
      longitude: json['longitude'] as String,
      latitude: json['latitude'] as String,
      salespersonId: (json['salesperson_id'] as num).toInt(),
    );

Map<String, dynamic> _$AttendanceRequestBodyToJson(
        AttendanceRequestBody instance) =>
    <String, dynamic>{
      'longitude': instance.longitude,
      'latitude': instance.latitude,
      'salesperson_id': instance.salespersonId,
    };

UpdateFcmTokenRequestBody _$UpdateFcmTokenRequestBodyFromJson(
        Map<String, dynamic> json) =>
    UpdateFcmTokenRequestBody(
      firebaseToken: json['firebase_token'] as String,
    );

Map<String, dynamic> _$UpdateFcmTokenRequestBodyToJson(
        UpdateFcmTokenRequestBody instance) =>
    <String, dynamic>{
      'firebase_token': instance.firebaseToken,
    };
