import 'package:dartz/dartz.dart';
import 'package:meedigo_company_sales_person/core/networking/api_failure.dart';

abstract class BaseUseCase<T , Parameters>{
  Future<Either<Failure , T>> call(Parameters parameters);
}

class NoParameters{
  const NoParameters();
}