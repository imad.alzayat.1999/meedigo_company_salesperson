import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:meedigo_company_sales_person/core/theming/app_colors.dart';

import '../../helpers/app_consts.dart';

class NotificationsService {
  static final FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
      FlutterLocalNotificationsPlugin();

  static Future<void> initNotification() async {
    AndroidInitializationSettings androidInitializationSettings =
        AndroidInitializationSettings(AppConsts.notificationsIconPath);

    var initializationSettingsIOS = DarwinInitializationSettings(
        requestAlertPermission: true,
        requestBadgePermission: true,
        requestCriticalPermission: true,
        requestSoundPermission: true,
        onDidReceiveLocalNotification:
            (int id, String? title, String? body, String? payload) async {});

    var initSettings = InitializationSettings(
      android: androidInitializationSettings,
      iOS: initializationSettingsIOS,
    );

    await flutterLocalNotificationsPlugin.resolvePlatformSpecificImplementation<
        AndroidFlutterLocalNotificationsPlugin>();

    await flutterLocalNotificationsPlugin.initialize(
      initSettings,
    );
  }

  static notificationDetails() {
    return const NotificationDetails(
      android: AndroidNotificationDetails(
        'meedigo_company_salesperson',
        'Meedigo Company SalesPerson',
        channelDescription: 'Meedigo Company SalesPerson Notification',
        importance: Importance.max,
        priority: Priority.max,
        icon: AppConsts.notificationsIconPath,
        color: AppColors.kTransparent,
      ),
      iOS: DarwinNotificationDetails(),
    );
  }

  static showNotification(
      {int id = 0, String? title, String? body, String? payload}) async {
    await flutterLocalNotificationsPlugin.show(
        id, title, body, await notificationDetails());
  }
}
