import 'dart:async';
import 'dart:convert';

import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:http/http.dart' as http;
import 'package:meedigo_company_sales_person/core/helpers/app_consts.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_extensions.dart';
import 'package:meedigo_company_sales_person/core/routing/app_routes.dart';
import 'package:meedigo_company_sales_person/core/utils/global_variables.dart';

import '../../di/dependency_injection.dart';
import 'notifications_service.dart';

@pragma('vm:entry-point')
Future<void> onBackgroundMessage(RemoteMessage message) async {
  await Firebase.initializeApp();
  getIt<FirebaseCloudMessagingService>().goToTheApplication();
  // FirebaseCloudMessagingService()._showNotification(message.notification);
}

class FirebaseCloudMessagingService {
  String? _token;
  String serverKey =
      "AAAAcJFY3_4:APA91bEa9MHhTEFTNJYqcNC-C23flh5xhoha2K3IQzv6GUznY3W_5H1DEp3uOyB_ChCTSJXDss4RwiCLJ20_znzRcA7ZUUTU8C2RpOHuly-QSX3uq565TvYb1086O4gcMWNjiLEkqnan";
  setNotifications(String title, String body) async {
    NotificationSettings settings =
        await FirebaseMessaging.instance.requestPermission(
      alert: true,
      announcement: true,
      badge: true,
      carPlay: true,
      criticalAlert: true,
      provisional: false,
      sound: true,
    );

    if (settings.authorizationStatus == AuthorizationStatus.authorized) {
      pushNotifications(title, body);
    } else {
      print('User declined or has not accepted permission');
    }
  }

  /// function to get fcm token
  Future<String> getFcmToken() async {
    try {
      final fcmToken = await getIt<FirebaseMessaging>().getToken();
      if (fcmToken == null) {
        return "";
      } else {
        return fcmToken;
      }
    } catch (e) {
      return "";
    }
  }

  pushNotifications(String title, String body) async {
    FirebaseMessaging.instance.setForegroundNotificationPresentationOptions(
      alert: true,
      sound: true,
      badge: true,
    );
    FirebaseMessaging.onBackgroundMessage(onBackgroundMessage);
    forgroundNotification();
    backgroundNotification();
    terminateNotification();
    NotificationsService.initNotification();
    var token = await getIt<FirebaseMessaging>().getToken();
    print(token);
    _token = token;
    if (title.isNotEmpty || body.isNotEmpty) {
      sendPushMessage(title: title, body: body);
    }
  }

  /// function to show notification
  _showNotification(RemoteNotification? notification) {
    if (notification != null) {
      if (notification.title != null) {
        NotificationsService.showNotification(
          title: notification.title!,
          body: notification.body!,
          payload: "",
        );
      }
    }
  }

  forgroundNotification() {
    FirebaseMessaging.onMessage.listen(
      (message) async {
        RemoteNotification? notification = message.notification;
        _showNotification(notification);
      },
    );
  }

  backgroundNotification() {
    FirebaseMessaging.onMessageOpenedApp.listen(
      (message) async {
        goToTheApplication();
      },
    );
  }

  terminateNotification() async {
    RemoteMessage? initialMessage =
        await FirebaseMessaging.instance.getInitialMessage();
    if (initialMessage != null) {
      // _showNotification(initialMessage.notification);
      goToTheApplication();
    }
  }

  String constructFCMPayload(String token, String title, String body) {
    return jsonEncode({
      'token': serverKey,
      'data': {
        'via': 'FlutterFire Cloud Messaging!!!',
      },
      "notification": {
        "title": title,
        "body": body,
        "android_channel_id": "meedigo_company_salesperson",
        "sound": "default"
      },
      'to': token
    });
  }

  /// function to go to the application
  /// when notification state is from background mode
  /// or terminate mode
  goToTheApplication() {
    Future.delayed(
      Duration(
        seconds: AppConsts.terminateNotificationDuration,
      ),
      () {
        GlobalVariable.navState.currentContext!.pushNamedAndRemoveUntil(
          AppRoutes.mainRoute,
        );
      },
    );
  }

  Future<void> sendPushMessage({
    required String title,
    required String body,
  }) async {
    _token = await getIt<FirebaseMessaging>().getToken();
    if (_token == null) {
      print('Unable to send FCM message, no token exists.');
      return;
    }

    try {
      var response = await http.post(
        Uri.parse('https://fcm.googleapis.com/fcm/send'),
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
          'Authorization': 'Key=$serverKey'
        },
        body: constructFCMPayload(_token!, title, body),
      );
      print(response.body);
      print('FCM request for device sent!');
    } catch (e) {
      print(e);
    }
  }
}
