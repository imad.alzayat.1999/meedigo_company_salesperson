import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

/// function to get responsive width of specific widget
double getWidth(double size) => size.w;

/// function to get responsive height of specific widget
double getHeight(double size) => size.h;

/// function to get responsive radius of specific widget
double getRadius(double size) => size.r;

/// function to build vertical spacing between widgets
SizedBox verticalSpacing(double size) => SizedBox(height: getHeight(size));

/// function to build horizontal spacing between widgets
SizedBox horizontalSpacing(double size) => SizedBox(width: getWidth(size));