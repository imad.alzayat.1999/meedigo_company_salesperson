import 'dart:io';

import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:meedigo_company_sales_person/core/common/entities/tabbar_data.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_assets.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_strings.dart';
import 'package:meedigo_company_sales_person/features/main/domain/entities/bottom_navbar_data.dart';
import 'package:meedigo_company_sales_person/features/notifications/presentation/screens/notifications_screen.dart';
import 'package:meedigo_company_sales_person/features/orders/presentation/screens/orders_screen.dart';
import 'package:meedigo_company_sales_person/features/profile/presentation/screens/profile_screen.dart';
import 'package:meedigo_company_sales_person/features/saved/presentation/screens/saved_order_screen.dart';
import 'package:meedigo_company_sales_person/features/warehouses/presentation/screens/warehouses_screen.dart';

import '../../features/on_boarding/domain/entities/on_boarding.dart';
import '../../features/pharmacy/entities/popup_menu_item_data.dart';

/// Implementing all constant values in the application
class AppConsts {
  /// all constant durations in (seconds , milliseconds)
  static const int onBoardingDuration = 300;
  static const int animatedOnBoardingIndicatorDuration = 300;
  static const int animatedContainerDuration = 400;
  static const int dioTimeoutDuration = 60 * 1000;
  static const int counterDuration = 1;
  static const int splashScreenDuration = 2;
  static const int pageViewDuration = 500;
  static const int terminateNotificationDuration = 3;

  /// main notifications icon path
  static const String notificationsIconPath = "@mipmap/ic_launcher";

  /// language keys
  static const String en = "en";
  static const String ar = "ar";

  /// local storage keys
  static const String loginKey = "login";
  static const String checkInKey = "checkIn";
  static const String onBoardingKey = "on_boarding";
  static const String versioningKey = "versioning";
  static const String lastClearedKey = "last_cleared";

  /// all constant array of data
  /// on boarding
  static List<OnBoarding> onBoardingData = [
    OnBoarding(
      title: AppStrings.firstOnBoardingPageTitleString,
      description: AppStrings.firstOnBoardingPageContentString,
      imagePath: AppImages.firstOnBoardingImage,
    ),
    OnBoarding(
      title: AppStrings.secondOnBoardingPageTitleString,
      description: AppStrings.secondOnBoardingPageContentString,
      imagePath: AppImages.secondOnBoardingImage,
    ),
    OnBoarding(
      title: AppStrings.thirdOnBoardingPageTitleString,
      description: AppStrings.thirdOnBoardingPageContentString,
      imagePath: AppImages.thirdOnBoardingImage,
    ),
    OnBoarding(
      title: AppStrings.fourthOnBoardingPageTitleString,
      description: AppStrings.fourthOnBoardingPageContentString,
      imagePath: AppImages.fourthOnBoardingImage,
    ),
  ];

  /// Bottom navigation bar
  static List<BottomNavBarData> bottomNavBarItems = [
    BottomNavBarData(
      text: AppStrings.homeTabString,
      iconPath: AppIcons.deliveryManIcon,
      screen: WarehousesScreen(),
    ),
    BottomNavBarData(
      text: AppStrings.ordersTabString,
      iconPath: AppIcons.ordersIcon,
      screen: OrdersScreen(),
    ),
    BottomNavBarData(
      text: AppStrings.savedTabString,
      iconPath: AppIcons.savedIcon,
      screen: SavedOrderScreen(),
    ),
    BottomNavBarData(
      text: AppStrings.notificationsTabString,
      iconPath: AppIcons.notificationsIcon,
      screen: NotificationsScreen(),
    ),
    BottomNavBarData(
      text: AppStrings.profileTabString,
      iconPath: AppIcons.userIcon,
      screen: ProfileScreen(),
    ),
  ];

  /// pharmacy products tabBar
  static List<TabBarData> pharmacyProductsTabBarItems = [
    TabBarData(
      iconPath: AppIcons.medicineIcon,
      key: AppStrings.medicineTabBarTitleString,
    ),
    TabBarData(
      iconPath: AppIcons.offerIcon,
      key: AppStrings.offersTabBarTitleString,
    ),
    TabBarData(
      iconPath: AppIcons.accessoryIcon,
      key: AppStrings.accessoriesTabBarTitleString,
    ),
  ];

  /// orders tabBar
  static List<TabBarData> ordersTabBarItems = [
    TabBarData(
      iconPath: AppIcons.pendingIcon,
      key: AppStrings.pendingTabBarTitleString,
    ),
    TabBarData(
      iconPath: AppIcons.acceptedIcon,
      key: AppStrings.acceptedTabBarTitleString,
    ),
    TabBarData(
      iconPath: AppIcons.deliveredIcon,
      key: AppStrings.deliveredTabBarTitleString,
    ),
  ];

  /// barcode tabBar
  static List<TabBarData> barcodeTabBarItems = [
    TabBarData(
      iconPath: AppIcons.medicineIcon,
      key: AppStrings.medicineTabBarTitleString,
    ),
    TabBarData(
      iconPath: AppIcons.accessoryIcon,
      key: AppStrings.accessoriesTabBarTitleString,
    ),
  ];

  /// product details tabBar
  static List<TabBarData> productDetailsTabBarItems = [
    TabBarData(
      iconPath: AppIcons.infoIcon,
      key: AppStrings.infoTabBarTitleString,
    ),
    TabBarData(
      iconPath: AppIcons.categoryIcon,
      key: AppStrings.similarProductsTabBarTitleString,
    ),
  ];

  /// popup menu (pharmacy card)
  static List<PopupMenuItemData> popupMenuListItems = [
    PopupMenuItemData(
      title: AppStrings.showLocationOnMapChoiceString,
      iconPath: AppIcons.locationPinIcon,
      routeUrl: "/",
    ),
  ];

  /// Grid delegates
  /// My statistics grid delegate
  static var myStatisticsGridDelegate =
      SliverGridDelegateWithFixedCrossAxisCount(
    crossAxisCount: 2, // number of items in each row
    mainAxisSpacing: 17.0, // spacing between rows
    crossAxisSpacing: 19.0, // spacing between columns
  );

  /// location tracking settings
  static LocationSettings getLocationSettingsForTracking() {
    int distanceFilter = 100;
    LocationSettings locationSettings;
    if (Platform.isAndroid) {
      locationSettings = AndroidSettings(
        accuracy: LocationAccuracy.high,
        distanceFilter: distanceFilter,
        forceLocationManager: true,
      );
    } else if (Platform.isIOS) {
      locationSettings = AppleSettings(
        accuracy: LocationAccuracy.high,
        activityType: ActivityType.fitness,
        distanceFilter: distanceFilter,
        pauseLocationUpdatesAutomatically: true,
        showBackgroundLocationIndicator: true,
      );
    } else {
      locationSettings = LocationSettings(
        accuracy: LocationAccuracy.high,
        distanceFilter: distanceFilter,
      );
    }
    return locationSettings;
  }
}
