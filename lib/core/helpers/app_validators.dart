import 'package:flutter/material.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_extensions.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_strings.dart';

/// Implementing all regular expressions for the app validators
class AppRegex {
  static bool hasNumber(String text) {
    return RegExp(r'^(?=.*?[0-9])').hasMatch(text);
  }

  static bool isUrl(String input) {
    return RegExp(
            r'(http|https)://[\w-]+(\.[\w-]+)+([\w.,@?^=%&amp;:/~+#-]*[\w@?^=%&amp;/~+#-])?')
        .hasMatch(input);
  }
}

/// Implementing all validation functions in the application
class AppValidators {
  /// function to validate phone number field
  static String? validatePhoneNumberField({
    required String? input,
    required BuildContext context,
  }) {
    if (input.isNullOrEmpty()) {
      return context.translate(
        AppStrings.phoneNumberRequiredValidationString,
      );
    } else if (!input!.startsWith("0 (9")) {
      return context.translate(
        AppStrings.phoneNumberTypeValidationString,
      );
    } else {
      return null;
    }
  }

  /// function to validate otp code field
  static String? validateOtpCodeField({
    required String? input,
    required BuildContext context,
  }) {
    if (input.isNullOrEmpty()) {
      return context.translate(
        AppStrings.otpCodeRequiredValidationString,
      );
    } else if (input!.length != 5) {
      return context.translate(
        AppStrings.otpCodeLengthValidationString,
      );
    } else {
      return null;
    }
  }
}
