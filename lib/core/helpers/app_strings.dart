/// Implementing all strings for the application
class AppStrings {
  /// application name
  static const String applicationName = "Meedigo Company Salesperson";

  /// update version screen strings
  static const String updateVersionTitleString = "update_version_title";
  static const String updateVersionContentString = "update_version_content";
  static const String updateVersionGooglePlayBtnString =
      "update_version_google_play_btn";
  static const String updateVersionDirectDownloadBtnString =
      "update_version_direct_download_btn";

  /// update version dialog strings
  static const String updateVersionDialogTitleString =
      "update_version_dialog_title";
  static const String updateVersionDialogContentString =
      "update_version_dialog_content";
  static const String updateVersionDialogGooglePlayBtnString =
      "update_version_dialog_google_play_btn";
  static const String updateVersionAppStoreBtnString =
      "update_version_app_store_btn";
  static const String updateVersionDialogDirectDownloadBtnString =
      "update_version_dialog_direct_download_btn";

  /// location permission screen strings
  static const String locationPermissionTitleString =
      "location_permission_title";
  static const String locationPermissionContentString =
      "location_permission_content";
  static const String locationPermissionEnableBtnString =
      "location_permission_enable_btn";

  /// on boarding screen strings
  static const String firstOnBoardingPageTitleString =
      "first_on_boarding_page_title";
  static const String secondOnBoardingPageTitleString =
      "second_on_boarding_page_title";
  static const String thirdOnBoardingPageTitleString =
      "third_on_boarding_page_title";
  static const String fourthOnBoardingPageTitleString =
      "fourth_on_boarding_page_title";
  static const String firstOnBoardingPageContentString =
      "first_on_boarding_page_content";
  static const String secondOnBoardingPageContentString =
      "second_on_boarding_page_content";
  static const String thirdOnBoardingPageContentString =
      "third_on_boarding_page_content";
  static const String fourthOnBoardingPageContentString =
      "fourth_on_boarding_page_content";
  static const String skipBtnString = "skip_btn";
  static const String nextBtnString = "next_btn";
  static const String getStartedBtnString = "get_started_btn";

  /// send sms screen strings
  static const String sendSmsTitleString = "send_sms_title";
  static const String sendSmsContentString = "send_sms_content";
  static const String sendSmsMessageBtnString = "send_sms_message_btn";

  /// verify otp screen strings
  static const String verifyOtpTitleString = "verify_otp_title";
  static const String verifyOtpContentString = "verify_otp_content";
  static const String otpCodeSectionTitleString = "otp_code_section_title";
  static const String verifyOtpResendCodeQuestionString =
      "verify_otp_resend_code_question";
  static const String verifyOtpResendCodeBtnString =
      "verify_otp_resend_code_btn";
  static const String verifyBtnString = "verify_btn";
  static const String sendWhatsappMessageBtnString =
      "send_whatsapp_message_btn";
  static const String contactWithSupportBtnString = "contact_with_support_btn";
  static const String resendCodeString = "resend_code";

  /// attendance screen strings
  static const String attendanceTitleString = "attendance_title";
  static const String attendanceContentString = "attendance_content";
  static const String attendanceBtnString = "attendance_btn";

  /// no internet screen strings
  static const String noInternetContentString = "no_internet_content";

  /// account blocked screen strings
  static const String accountBlockedTitleString = "account_blocked_title";
  static const String accountBlockedContentString = "account_blocked_content";

  /// main screen strings
  static const String homeTabString = "home_tab";
  static const String ordersTabString = "orders_tab";
  static const String notificationsTabString = "notifications_tab";
  static const String savedTabString = "saved_tab";
  static const String profileTabString = "profile_tab";

  /// pharmacies by warehouse screen strings
  static const String pharmaciesTitleString = "pharmacies_title";
  static const String showLocationOnMapChoiceString =
      "show_location_on_map_choice";

  /// create new pharmacy screen strings
  static const String createNewPharmacyTitleString =
      "create_new_pharmacy_title";
  static const String pharmacyNameLabelTextString = "pharmacy_name_label_text";
  static const String pharmacyPhoneNumberLabelTextString =
      "pharmacy_phone_number_label_text";
  static const String pharmacyLocationLabelTextString =
      "pharmacy_location_label_text";
  static const String createBtnString = "create_btn";

  /// pick location screen strings
  static const String pickLocationTitleString = "pick_location_title";
  static const String pickLocationBtnString = "pick_location_btn";

  /// search screens (search for pharmacy , search for product) strings
  static const String searchTitleString = "search_title";
  static const String searchPlaceholderString = "search_placeholder";
  static const String searchForPharmacyHintTextString =
      "search_for_pharmacy_hint_text";
  static const String searchForMedicineHintTextString =
      "search_for_medicine_hint_text";
  static const String searchForAccessoryHintTextString =
      "search_for_accessory_hint_text";

  /// pharmacy products screen strings
  static const String productsTitleString = "products_title";
  static const String brandFilterBtnString = "brand_filter_btn";

  /// product details screen strings
  static const String productDetailsTitleString = "product_details_title";
  static const String sellingPriceProductDetailsString =
      "selling_price_product_details";
  static const String pharmacistPriceProductDetailsString =
      "pharmacist_price_product_details";
  static const String compositionProductDetailsString =
      "composition_product_details";
  static const String packingProductDetailsString = "packing_product_details";
  static const String formulaProductDetailsString = "formula_product_details";
  static const String caliberProductDetailsString = "caliber_product_details";

  /// attention cart bottom sheet strings
  static const String attentionTitleString = "attention_title";
  static const String attentionCartDeletionQuestionString =
      "attention_cart_deletion_question";

  /// brands bottom sheet strings
  static const String brandsTitleString = "brands_title";
  static const String searchForBrandsHintTextString =
      "search_for_brands_hint_text";

  /// add to cart bottom sheet strings
  static const String addToCartTitleString = "add_to_cart_title";
  static const String addToCartBtnString = "add_to_cart_btn";
  static const String requireQuantityForAddToCartString =
      "require_quantity_for_add_to_cart";
  static const String availableOffersSectionTitleString =
      "available_offers_title";

  /// order details screen (offline , online) strings
  static const String orderDetailsTitleString = "order_details_title";
  static const String pendingStateString = "pending_state";
  static const String acceptedStateString = "accepted_state";
  static const String deliveredStateString = "delivered_state";
  static const String numberOfOrderProductsString = "number_of_order_products";
  static const String totalForOrderProductString = "total_for_order_product";
  static const String overallForOrderString = "overall_for_order";
  static const String deliverBtnString = "deliver_btn";
  static const String confirmBtnString = "confirm_btn";
  static const String refreshBtnString = "refresh_btn";

  /// profile screen strings
  static const String welcomeSalespersonString = "welcome_salesperson";
  static const String welcomeBackString = "welcome_back";
  static const String personalInformationChoiceString =
      "personal_information_choice";
  static const String languageChoiceString = "language_choice";
  static const String checkoutChoiceString = "checkout_choice";
  static const String myStatisticsChoiceString = "my_statistics_choice";
  static const String scanQrString = "scan_qr_choice";
  static const String contactUsTitleString = "contact_us_title";

  /// confirmation dialog strings
  static const String confirmationQuestionString = "confirmation_question";

  /// personal information screen strings
  static const String personalInformationTitleString =
      "personal_information_title";
  static const String nameLabelTextString = "name_label_text";
  static const String emailLabelTextString = "email_label_text";
  static const String nameHintTextString = "name_hint_text";
  static const String emailHintTextString = "email_hint_text";
  static const String editInformationBtnString = "edit_information_btn";
  static const String saveInformationBtnString = "save_information_btn";

  /// languages bottom sheet strings
  static const String languageBottomSheetTitleString =
      "language_bottom_sheet_title";
  static const String englishString = "english";
  static const String arabicString = "arabic";

  /// my statistics screen strings
  static const String myStatisticsTitleString = "my_statistics_title";
  static const String sellingProfitString = "selling_profit";
  static const String totalOrdersString = "total_orders";
  static const String totalDeliveredOrdersString = "total_delivered_orders";
  static const String overallPriceOfOrdersString = "overall_price_of_orders";
  static const String overallPriceOfDeliveredOrdersString =
      "overall_price_of_delivered_orders";

  /// cart screen strings
  static const String cartTitleString = "cart_title";
  static const String totalPriceOfCartProductString =
      "total_price_of_cart_product";
  static const String checkoutBtnString = "checkout_btn";
  static const String outOfStockMessageString = "out_of_stock_message";

  /// ration product dialog strings
  static const String rationProductDialogTitleString =
      "ration_product_dialog_title";
  static const String dearSalespersonSectionTitleString =
      "dear_salesperson_section_title";
  static const String rationMessageString = "ration_message";
  static const String rationLimitString = "ration_limit";
  static const String overflowWarehousesSectionTitleString =
      "overflow_warehouses_section_title";
  static const String rationProductsSectionTitleString =
      "ration_products_section_title";
  static const String reviewOrderWarningString = "review_order_warning";
  static const String okBtnString = "ok_btn";

  /// tabBar titles strings
  static const String medicineTabBarTitleString = "medicine_tab_bar_title";
  static const String offersTabBarTitleString = "offers_tab_bar_title";
  static const String accessoriesTabBarTitleString =
      "accessories_tab_bar_title";
  static const String infoTabBarTitleString = "info_tab_bar_title";
  static const String similarProductsTabBarTitleString =
      "similar_products_tab_bar_title";
  static const String pendingTabBarTitleString = "pending_tab_bar_title";
  static const String acceptedTabBarTitleString = "accepted_tab_bar_title";
  static const String deliveredTabBarTitleString = "delivered_tab_bar_title";

  /// card products (medicines , accessories) strings
  static const String sellingPriceProductCardString =
      "selling_price_product_card";
  static const String caliberProductCardString = "caliber_product_card";
  static const String packingProductCardString = "packing_product_card";

  /// order card strings
  static const String orderNumberCardString = "order_number_card";
  static const String orderSourceOrderCardString = "order_source_order_card";
  static const String orderDestinationOrderCardString =
      "order_destination_order_card";

  /// common strings
  static const String orString = "or";
  static const String cancelString = "cancel";
  static const String phoneNumberString = "phone_number";
  static const String tryAgainBtnString = "try_again_btn";
  static const String logoutBtnString = "logout_btn";
  static const String noBtnString = "no_btn";
  static const String yesBtnString = "yes_btn";
  static const String fixedOfferString = "fixed_offer";
  static const String discountOfferString = "discount_offer";
  static const String percentageOfferString = "percentage_offer";
  static const String numberOfItemsString = "number_of_items";

  /// messages
  static const String phoneNumberRequiredValidationString =
      "phone_number_required_validation";
  static const String phoneNumberTypeValidationString =
      "phone_number_type_validation";
  static const String otpCodeRequiredValidationString =
      "otp_code_required_validation";
  static const String otpCodeLengthValidationString =
      "otp_code_length_validation";
  static const String attendanceDoneString = "attendance_done";
  static const String badRequestErrorString = "bad_request_error";
  static const String forbiddenErrorString = "forbidden_error";
  static const String unauthorizedErrorString = "unauthorized_error";
  static const String notFoundErrorString = "not_found_error";
  static const String conflictErrorString = "conflict_error";
  static const String internalServerErrorString = "internal_server_error";
  static const String unknownErrorString = "unknown_error";
  static const String timeoutErrorString = "timeout_error";
  static const String defaultErrorString = "default_error";
  static const String cacheErrorString = "cache_error";
  static const String noInternetErrorString = "no_internet_error";
  static const String getCurrentLocationErrorString =
      "get_current_location_error";
}
