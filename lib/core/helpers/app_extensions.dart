import 'package:flutter/material.dart';
import 'package:intl/intl.dart' as intl;
import 'package:intl/intl.dart';

import '../localization/app_localizations.dart';

extension ContextHelpers on BuildContext {
  /// push named function
  /// (add new route to routes stack)
  Future<dynamic> pushNamed(
    String routeName, {
    Object? arguments,
  }) {
    return Navigator.of(this).pushNamed(
      routeName,
      arguments: arguments,
    );
  }

  /// push replacement named
  /// (add new route to routes stack with
  /// replace the current route with the new route)
  Future<dynamic> pushReplacementNamed(
    String routeName, {
    Object? arguments,
  }) {
    return Navigator.of(this).pushReplacementNamed(
      routeName,
      arguments: arguments,
    );
  }

  /// push named and remove until
  /// (add new route to routes stack
  /// with remove all prev routes from the stack)
  Future<dynamic> pushNamedAndRemoveUntil(
    String routeName, {
    Object? arguments,
  }) {
    return Navigator.of(this).pushNamedAndRemoveUntil(
      routeName,
      (route) => false,
      arguments: arguments,
    );
  }

  /// back to previous route function
  void pop() => Navigator.pop(this);

  /// function to check if back to previous route process is possible or not
  bool canPop() => Navigator.canPop(this);

  /// translate text function into (english , arabic)
  String translate(String text, {Map<String, dynamic>? args}) {
    return AppLocalizations.of(this)!.translateWithArgs(text, args!).toString();
  }

  /// function to get width of the screen
  double width() => MediaQuery.of(this).size.width;

  /// function to get height of the screen
  double height() => MediaQuery.of(this).size.height;

  /// function to check if the language or the application is english
  bool isEn() => AppLocalizations.of(this)!.isEnLocale;

  /// function to check if the language of the application is arabic
  bool isAr() => AppLocalizations.of(this)!.isArLocale;

  /// function to shift screen content little bit to the top
  /// to show the keyboard for type anything
  double viewInsetsBottom() => MediaQuery.of(this).viewInsets.bottom;
}

extension NumHelpers on num {
  String format() {
    return intl.NumberFormat.decimalPattern().format(this);
  }
}

extension NullableStringHelpers on String? {
  bool isNullOrEmpty() => this == null || this == "";
  
  String orEmpty({String empty = ""}) {
    if (this == null) {
      return empty;
    } else {
      return this!;
    }
  }
}

extension DateTimeHelpers on DateTime {
  /// function to format date to this formula (yyyy-MM-dd)
  String format() {
    return DateFormat("yyyy-MM-dd").format(this);
  }
}

extension StringHelpers on String {
  /// function to convert string to num
  num toNum() {
    return num.parse(this);
  }

  /// function to convert string to integer
  int toInt() {
    return int.parse(this);
  }

  /// function to convert string to double
  double toDouble() {
    return double.parse(this);
  }

  /// function to check if the written word is english
  bool isEnWritten() {
    return this.codeUnits.any(
          (char) => (char >= 65 && char <= 90) || (char >= 97 && char <= 122),
        );
  }

  /// function to check if the written word is arabic
  bool isArWritten() {
    return this.codeUnits.any(
          (char) => char >= 0x0600 && char <= 0x06FF,
        );
  }

  /// function to convert string to dateTime
  DateTime toDate() {
    if (this.isEmpty) {
      return DateTime.now();
    } else {
      return DateFormat("yyyy-MM-dd").parse(this);
    }
  }
}

extension NullableIntegerHelpers on int? {
  int orZero() {
    if (this == null) {
      return 0;
    } else {
      return this!;
    }
  }
}

extension NullableNumHelpers on num? {
  num orZero() {
    if (this == null) {
      return 0.0;
    } else {
      return this!;
    }
  }
}

extension NullableBoolHelpers on bool? {
  bool orFalse() {
    if (this == null) {
      return false;
    } else {
      return this!;
    }
  }
}

extension ScrollControllerHelpers on ScrollController {
  /// function to check if scrolling position
  /// is at the bottom of the screen
  bool isBottom() {
    if (!this.hasClients) {
      return false;
    } else {
      return this.position.pixels == this.position.maxScrollExtent;
    }
  }

  jumpToTop() {
    this.jumpTo(this.position.minScrollExtent);
  }
}
