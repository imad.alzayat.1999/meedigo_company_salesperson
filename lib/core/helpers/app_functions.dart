import 'dart:io';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:url_launcher/url_launcher.dart';

import 'package:flutter/material.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_extensions.dart';
import 'package:meedigo_company_sales_person/core/helpers/enums.dart';
import 'package:meedigo_company_sales_person/core/networking/api_error_handler.dart';
import 'package:meedigo_company_sales_person/core/widgets/snackbar_widget.dart';

import '../di/dependency_injection.dart';
import '../networking/api_failure.dart';
import 'app_validators.dart';

/// implementing all shared
/// functionalities in the application
class AppFunctions {
  /// function to clear all offline data from the application
  static clearAllStoredDataFromTheApp() async{
    getIt<SharedPreferences>().clear();
  }

  /// function to detect the current platform for the app
  static String getCurrentPlatform() => Platform.isAndroid ? "android" : "ios";

  /// function to launch an external url
  static launchExternalUrl(String url) async {
    if (AppRegex.isUrl(url)) {
      final parsedUrl = Uri.parse(url);
      if (!await launchUrl(
        parsedUrl,
        mode: LaunchMode.externalApplication,
      )) throw 'Could not launch $parsedUrl';
    } else {
      final parsedPhoneNumber = Uri(scheme: 'tel', path: url);
      if (!await launchUrl(parsedPhoneNumber))
        throw 'Could not launch $parsedPhoneNumber';
    }
  }

  /// function to show success message
  static showSuccessMessage({
    required BuildContext context,
    required String message,
  }) {
    showSnackBarByItsState(
      snackBarStates: SnackBarStates.success,
      context: context,
      message: context.translate(message),
    );
  }

  /// function to show warning message
  static showWarningMessage({
    required BuildContext context,
    required String message,
  }) {
    showSnackBarByItsState(
      snackBarStates: SnackBarStates.warning,
      context: context,
      message: context.translate(message),
    );
  }

  /// function to show failed message
  static showFailedMessage({
    required BuildContext context,
    Failure? failure,
    String message = "",
  }) {
    if (failure == null) {
      showSnackBarByItsState(
        snackBarStates: SnackBarStates.error,
        context: context,
        message: message,
      );
    } else {
      switch (failure.errorMessage) {
        case ApiErrors.badRequestError:
          showSnackBarByItsState(
            snackBarStates: SnackBarStates.error,
            context: context,
            message: context.translate(
              ApiErrors.badRequestError,
            ),
          );
        case ApiErrors.forbiddenError:
          showSnackBarByItsState(
            snackBarStates: SnackBarStates.error,
            context: context,
            message: context.translate(
              ApiErrors.forbiddenError,
            ),
          );
        case ApiErrors.unauthorizedError:
          showSnackBarByItsState(
            snackBarStates: SnackBarStates.error,
            context: context,
            message: context.translate(
              ApiErrors.unauthorizedError,
            ),
          );
        case ApiErrors.notFoundError:
          showSnackBarByItsState(
            snackBarStates: SnackBarStates.error,
            context: context,
            message: context.translate(
              ApiErrors.notFoundError,
            ),
          );
        case ApiErrors.conflictError:
          showSnackBarByItsState(
            snackBarStates: SnackBarStates.error,
            context: context,
            message: context.translate(
              ApiErrors.conflictError,
            ),
          );
        case ApiErrors.internalServerError:
          showSnackBarByItsState(
            snackBarStates: SnackBarStates.error,
            context: context,
            message: context.translate(
              ApiErrors.internalServerError,
            ),
          );
        case ApiErrors.unknownError:
          showSnackBarByItsState(
            snackBarStates: SnackBarStates.error,
            context: context,
            message: context.translate(
              ApiErrors.unknownError,
            ),
          );
        case ApiErrors.timeoutError:
          showSnackBarByItsState(
            snackBarStates: SnackBarStates.error,
            context: context,
            message: context.translate(
              ApiErrors.timeoutError,
            ),
          );
        case ApiErrors.defaultError:
          showSnackBarByItsState(
            snackBarStates: SnackBarStates.error,
            context: context,
            message: context.translate(
              ApiErrors.defaultError,
            ),
          );
        case ApiErrors.cacheError:
          showSnackBarByItsState(
            snackBarStates: SnackBarStates.error,
            context: context,
            message: context.translate(
              ApiErrors.cacheError,
            ),
          );
        case ApiErrors.noInternetError:
          showSnackBarByItsState(
            snackBarStates: SnackBarStates.error,
            context: context,
            message: context.translate(
              ApiErrors.noInternetError,
            ),
          );
        default:
          showSnackBarByItsState(
            snackBarStates: SnackBarStates.error,
            context: context,
            message: message,
          );
      }
    }
  }
}
