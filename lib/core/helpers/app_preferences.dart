import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:meedigo_company_sales_person/core/di/dependency_injection.dart';
import 'package:meedigo_company_sales_person/core/helpers/app_consts.dart';
import 'package:meedigo_company_sales_person/core/mappers/update_app_mappers.dart';
import 'package:meedigo_company_sales_person/features/auth/domain/entities/login.dart';
import 'package:meedigo_company_sales_person/features/versioning/data/models/versioning_response_model.dart';
import 'package:meedigo_company_sales_person/features/versioning/domain/entities/versioning_response.dart';
import 'package:shared_preferences/shared_preferences.dart';

/// implementing all local storage functionalities
/// in the application
class AppPreferences {
  /// function to set last cleared value
  static setLastCleared({bool value = true}) {
    getIt<SharedPreferences>().setBool(
      AppConsts.lastClearedKey,
      value,
    );
  }

  /// function to get last cleared value
  static bool getLastCleared() {
    bool? lastCleared = getIt<SharedPreferences>().getBool(
      AppConsts.lastClearedKey,
    );
    if (lastCleared == null) {
      return false;
    } else {
      return true;
    }
  }

  /// function to enable check in
  static passCheckIn() {
    getIt<SharedPreferences>().setBool(AppConsts.checkInKey, true);
  }

  /// function to pass on boarding page
  static passOnBoardingPage() {
    getIt<SharedPreferences>().setBool(AppConsts.onBoardingKey, true);
  }

  /// function to get checkin status
  static bool getCheckIn() {
    final data = getIt<SharedPreferences>().getBool(AppConsts.checkInKey);
    if (data == null) {
      return false;
    } else {
      return true;
    }
  }

  /// function to get on boarding status
  static bool getOnBoarding() {
    final isOnBoardingPassed = getIt<SharedPreferences>().getBool(
      AppConsts.onBoardingKey,
    );
    if (isOnBoardingPassed == null) {
      return false;
    } else {
      return true;
    }
  }

  /// function to clear check in data
  static clearCheckInData() {
    getIt<SharedPreferences>().remove(AppConsts.checkInKey);
  }

  /// function to clear on boarding data
  static clearOnBoardingData() {
    getIt<SharedPreferences>().remove(AppConsts.onBoardingKey);
  }

  /// function to save login data
  static saveLoginData(LoginData login) {
    final convertDataToJson = login.toJson();
    final encodeTheConvertedData = jsonEncode(convertDataToJson);
    getIt<SharedPreferences>().setString(
      AppConsts.loginKey,
      encodeTheConvertedData,
    );
  }

  /// function to check if the user has been authenticated with the application
  static bool isUserAuthenticated() {
    final userData = getIt<SharedPreferences>().getString(
      AppConsts.loginKey,
    );
    if (userData == null) {
      return false;
    } else {
      return true;
    }
  }

  /// function to get cached versioning response
  static VersioningResponse? getVersionInfo() {
    final cachedVersionInfo = getIt<SharedPreferences>().getString(
      AppConsts.versioningKey,
    );
    if (cachedVersionInfo == null) {
      return null;
    } else {
      final decodeData = jsonDecode(cachedVersionInfo);
      return VersioningResponseModel.fromJson(decodeData).toDomain();
    }
  }

  /// function to get saved logged in data
  static LoginData getUserInfo() {
    final loggedInData = getIt<SharedPreferences>().getString(
      AppConsts.loginKey,
    );
    if (loggedInData == null) {
      return LoginData(
        salesPersonId: 0,
        token: "Bearer ",
        type: "",
        companyId: 0,
        companyName: "",
        active: 0,
        allowOrder: 0,
        representatives: [],
      );
    } else {
      final decodeTheSavedData = jsonDecode(loggedInData);
      return LoginData.fromJson(decodeTheSavedData);
    }
  }

  /// Saves a [value] with a [key] in the FlutterSecureStorage.
  static setSecuredString(String key, String value) async {
    final flutterSecureStorage = getIt<FlutterSecureStorage>();
    debugPrint(
        "FlutterSecureStorage : setSecuredString with key : $key and value : $value");
    await flutterSecureStorage.write(key: key, value: value);
  }

  /// Gets an String value from FlutterSecureStorage with given [key].
  static getSecuredString(String key) async {
    final flutterSecureStorage = getIt<FlutterSecureStorage>();
    debugPrint('FlutterSecureStorage : getSecuredString with key :');
    return await flutterSecureStorage.read(key: key) ?? '';
  }

  /// Removes all keys and values in the FlutterSecureStorage
  static clearAllSecuredData() async {
    debugPrint('FlutterSecureStorage : all data has been cleared');
    final flutterSecureStorage = getIt<FlutterSecureStorage>();
    await flutterSecureStorage.deleteAll();
  }
}
