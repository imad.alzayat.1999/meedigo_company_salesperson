/// Implementing all images in the application
class AppImages {
  /// main images path
  static const String mainImagesPath = "assets/images";

  /// application's images
  static const String meedigoSplashGifImage = "$mainImagesPath/meedigo_gif.gif";
  static const String firstOnBoardingImage =
      "$mainImagesPath/first_on_boarding_image.svg";
  static const String secondOnBoardingImage =
      "$mainImagesPath/second_on_boarding_image.svg";
  static const String thirdOnBoardingImage =
      "$mainImagesPath/third_on_boarding_image.svg";
  static const String fourthOnBoardingImage =
      "$mainImagesPath/fourth_on_boarding_image.svg";
  static const String sendSmsImage = "$mainImagesPath/send_sms_image.svg";
  static const String meedigoLogoImage = "$mainImagesPath/meedigo_logo.png";
  static const String makePaymentImage =
      "$mainImagesPath/make_payment_image.svg";
}

/// Implementing all icons in the application
class AppIcons {
  /// main icons path
  static const String mainIconsPath = "assets/icons";

  /// application's icons
  static const String locationPinIcon = "$mainIconsPath/location_pin.svg";
  static const String fingerprintIcon = "$mainIconsPath/fingerprint.svg";
  static const String smsIcon = "$mainIconsPath/sms.svg";
  static const String whatsappIcon = "$mainIconsPath/whatsapp.svg";
  static const String searchIcon = "$mainIconsPath/search.svg";
  static const String phoneIcon = "$mainIconsPath/phone.svg";
  static const String optionsIcon = "$mainIconsPath/options.svg";
  static const String userAddIcon = "$mainIconsPath/user_add.svg";
  static const String deliveryManIcon = "$mainIconsPath/delivery_man.svg";
  static const String ordersIcon = "$mainIconsPath/orders.svg";
  static const String savedIcon = "$mainIconsPath/saved.svg";
  static const String notificationsIcon = "$mainIconsPath/notifications.svg";
  static const String userIcon = "$mainIconsPath/user.svg";
  static const String medicineIcon = "$mainIconsPath/medicine.svg";
  static const String offerIcon = "$mainIconsPath/offer.svg";
  static const String accessoryIcon = "$mainIconsPath/accessory.svg";
  static const String arrowDownIcon = "$mainIconsPath/arrow_down.svg";
  static const String attentionCartIcon = "$mainIconsPath/attention_cart.svg";
  static const String giftIcon = "$mainIconsPath/gift.svg";
  static const String plusIcon = "$mainIconsPath/plus.svg";
  static const String barcodeIcon = "$mainIconsPath/barcode.svg";
  static const String cartIcon = "$mainIconsPath/cart.svg";
  static const String pendingIcon = "$mainIconsPath/pending.svg";
  static const String acceptedIcon = "$mainIconsPath/accepted.svg";
  static const String deliveredIcon = "$mainIconsPath/delivered.svg";
  static const String trademarkIcon = "$mainIconsPath/trademark.svg";
  static const String infoIcon = "$mainIconsPath/info.svg";
  static const String categoryIcon = "$mainIconsPath/category.svg";
  static const String compositionIcon = "$mainIconsPath/composition.svg";
  static const String caliberIcon = "$mainIconsPath/caliber.svg";
  static const String packingIcon = "$mainIconsPath/packing.svg";
  static const String formulaIcon = "$mainIconsPath/formula.svg";
  static const String minusIcon = "$mainIconsPath/minus.svg";
  static const String trashIcon = "$mainIconsPath/trash.svg";
  static const String handshakeIcon = "$mainIconsPath/handshake.svg";
  static const String qrCodeIcon = "$mainIconsPath/qr_code.svg";
  static const String languageIcon = "$mainIconsPath/language.svg";
  static const String trendIcon = "$mainIconsPath/trend.svg";
  static const String checkoutIcon = "$mainIconsPath/checkout.svg";
  static const String logoutIcon = "$mainIconsPath/logout.svg";
  static const String instagramIcon = "$mainIconsPath/instagram.svg";
  static const String facebookIcon = "$mainIconsPath/facebook.svg";
  static const String arrowLeftIcon = "$mainIconsPath/arrow_left.svg";
  static const String arrowRightIcon = "$mainIconsPath/arrow_right.svg";
  static const String warningIcon = "$mainIconsPath/warning.svg";
  static const String totalDeliveredOrdersIcon =
      "$mainIconsPath/total_delivered_orders.svg";
  static const String totalOrdersIcon = "$mainIconsPath/total_orders.svg";
  static const String orderStatisticsIcon =
      "$mainIconsPath/order_statistics.svg";
  static const String enIcon = "$mainIconsPath/en.svg";
  static const String arIcon = "$mainIconsPath/ar.svg";
  static const String checkIcon = "$mainIconsPath/check.svg";
  static const String refreshIcon = "$mainIconsPath/refresh.svg";
  static const String arrowUpIcon = "$mainIconsPath/arrow_up.svg";
  static const String noteIcon = "$mainIconsPath/note.svg";
  static const String errorIcon = "$mainIconsPath/error.svg";
}

/// Implementing all lottie animations in the application
class AppJsons {
  /// main animation files path
  static const String mainJsonPath = "assets/lottie";

  /// application's animation files
  static const String noInternetJson = "$mainJsonPath/no_internet.json";
  static const String noNotificationsJson =
      "$mainJsonPath/no_notifications.json";
  static const String blockedJson = "$mainJsonPath/blocked.json";
  static const String loadingIndicatorJson =
      "$mainJsonPath/loading_indicator.json";
  static const String buttonLoadingIndicatorJson =
      "$mainJsonPath/button_loading_indicator.json";
  static const String buttonLoadingIndicatorDarkJson =
      "$mainJsonPath/button_dark_loading_indicator.json";
}
