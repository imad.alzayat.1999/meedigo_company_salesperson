enum SnackBarStates { success, warning, error }

enum LoadingSizes {
  fullScreen,
  fullScreenAuth,
  button,
  bottomSheet,
  loadingMore,
  auth,
}
