import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:meedigo_company_sales_person/firebase_options_dev.dart';
import 'package:meedigo_company_sales_person/meedigo_company_salesperson_application.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp(
    options: DefaultFirebaseOptions.currentPlatform,
  );
  runApp(const MeedigoCompanySalesPersonApplication());
}
