/// pharmacies_screen.dart

//class PharmaciesScreen extends StatelessWidget {
//   const PharmaciesScreen({super.key});
//
//   @override
//   Widget build(BuildContext context) {
//     return SalesAreaForSalesPersonWidget();
//   }
// }
//
// class SalesAreaForSalesPersonWidget extends StatelessWidget {
//   const SalesAreaForSalesPersonWidget({super.key});
//
//   @override
//   Widget build(BuildContext context) {
//     return BlocProvider(
//       create: (context) => getIt<PharmacyBloc>()..add(GetSalesAreaEvent()),
//       child: Scaffold(
//         appBar: appbarWidget(
//           context: context,
//           isCompaniesPage: true,
//           title: context.translate(AppStrings.pharmaciesString),
//           onTap: () =>
//               context.read<PharmacyBloc>().routeToPharmacyContentSearchPage(
//                     context: context,
//                   ),
//         ),
//         body: BlocBuilder<InternetBloc, InternetState>(
//           builder: (context, state) {
//             return state.maybeWhen(
//               orElse: () => PharmaciesOffline(),
//               connected: () => BlocBuilder<PharmacyBloc, PharmacyState>(
//                 builder: (context, state) {
//                   switch (state.salesAreasRequestStates) {
//                     case RequestStates.loading:
//                       return PharmaciesShimmerWidget();
//                     case RequestStates.success:
//                       return PharmaciesBySalesAreaWidget(
//                         salesAreas: state.salesAreas,
//                       );
//                     case RequestStates.error:
//                       return FailureWidget(
//                         onPressFunction: () => context.read<PharmacyBloc>()
//                           ..add(
//                             GetSalesAreaEvent(),
//                           ),
//                         failure: state.salesAreasFailure!,
//                       );
//                   }
//                 },
//               ),
//             );
//           },
//         ),
//       ),
//     );
//   }
// }
//
// class PharmaciesBySalesAreaWidget extends StatelessWidget {
//   final List<int> salesAreas;
//   const PharmaciesBySalesAreaWidget({
//     super.key,
//     required this.salesAreas,
//   });
//
//   @override
//   Widget build(BuildContext context) {
//     return BlocProvider(
//       create: (context) => getIt<PharmacyBloc>()
//         ..add(
//           GetPharmaciesEvent(
//             salesAreas: salesAreas,
//           ),
//         ),
//       child: Scaffold(
//         floatingActionButtonLocation: FloatingActionButtonLocation.startFloat,
//         floatingActionButton: RegisterNewPharmacistComponent(),
//         body: BlocConsumer<InternetBloc, InternetState>(
//           listener: (context, state) {
//             state.whenOrNull(connected: () {
//               context.read<PharmacyBloc>()..pharmaciesCurrentPage = 1;
//               BlocProvider.of<PharmacyBloc>(context)
//                 ..add(
//                   GetPharmaciesEvent(salesAreas: salesAreas),
//                 );
//             });
//           },
//           builder: (context, state) {
//             return state.maybeWhen(
//               orElse: () => PharmaciesOffline(),
//               connected: () => PharmaciesOnline(salesAreas: salesAreas),
//             );
//           },
//         ),
//       ),
//     );
//   }
// }
//
// class PharmaciesOnline extends StatelessWidget {
//   final List<int> salesAreas;
//   const PharmaciesOnline({super.key, required this.salesAreas});
//
//   @override
//   Widget build(BuildContext context) {
//     return BlocBuilder<PharmacyBloc, PharmacyState>(
//       builder: (context, state) {
//         switch (state.pharmacyRequestStates) {
//           case RequestStates.loading:
//             return PharmaciesShimmerWidget();
//           case RequestStates.success:
//             final pharmacists = state.pharmacies;
//             return RefreshIndicator(
//               onRefresh: () async {
//                 context.read<PharmacyBloc>()
//                   ..refreshPharmacies(
//                     context: context,
//                     salesAreas: salesAreas,
//                   );
//               },
//               child: PaginatedPharmaciesWidget(
//                 pharmacies: pharmacists,
//                 hasReachedMax: state.hasReachedMax,
//                 salesAreas: salesAreas,
//               ),
//             );
//           case RequestStates.error:
//             return FailureWidget(
//               onPressFunction: () => context.read<PharmacyBloc>()
//                 ..refreshPharmacies(
//                   context: context,
//                   salesAreas: salesAreas,
//                 ),
//               failure: state.pharmacyFailure!,
//             );
//         }
//       },
//     );
//   }
// }
//
// class PharmaciesOffline extends StatelessWidget {
//   const PharmaciesOffline({super.key});
//
//   @override
//   Widget build(BuildContext context) {
//     return BlocProvider(
//       create: (context) => getIt<OfflineDataBloc>()
//         ..add(
//           GetOfflinePharmacies(),
//         ),
//       child: BlocBuilder<OfflineDataBloc, OfflineDataState>(
//         builder: (context, state) {
//           switch (state.offlinePharmaciesRequestStates) {
//             case RequestStates.success:
//               final offlinePharmacies = state.offlinePharmacies;
//               return NotPaginatedPharmaciesWidget(
//                 pharmacies: offlinePharmacies,
//               );
//             default:
//               return Container();
//           }
//         },
//       ),
//     );
//   }
// }